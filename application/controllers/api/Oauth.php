<?php
use Restserver\Libraries\REST_Controller;

Header('Access-Control-Allow-Origin: *'); //for allow any domain, insecure
Header('Access-Control-Allow-Headers: *'); //for allow any headers, insecure
Header('Access-Control-Allow-Methods: GET, POST, OPTIONS, PUT, DELETE'); //method allowed
// defined('BASEPATH') OR exit('No direct script access allowed');

// This can be removed if you use __autoload() in config.php OR use Modular Extensions
/** @noinspection PhpIncludeInspection */
require(APPPATH.'/libraries/REST_Controller.php');



/**
 * This is an example of a few basic user interaction methods you could use
 * all done with a hardcoded array
 *
 * @package         CodeIgniter
 * @subpackage      Rest Server
 * @category        Controller
 * @author          Phil Sturgeon, Chris Kacerguis
 * @license         MIT
 * @link            https://github.com/chriskacerguis/codeigniter-restserver
 */
class Oauth extends REST_Controller {

    private $tbl_users = "tb_users";    
   
    function __construct() {
        // Construct the parent class
        parent::__construct();
        $this->load->model("general_model","general");
        $this->load->model('task_list/Allfiles_model');
    }   
    

    public function registration_post()
    {

        $params = json_decode(file_get_contents('php://input'), TRUE);

        $name = $email = $phone = $password = $pin = $otp = $id_proof = $id_proof_number = $profile_pic = $status = "";

        $created_at = $updated_at = current_date();


      
        if(isset($params['name']) && !empty($params['name']))
        {
            $_POST['name'] = $name = $params['name'];
        }

        if(isset($params['email']) && !empty($params['email']))
        {
            $_POST['email'] = $email = $params['email'];
        }
       

        if(isset($params['phone']) && !empty($params['phone']))
        {
            $_POST['phone'] = $phone = $params['phone'];
        }

        if(isset($params['password']) && !empty($params['password']))
        {
            $_POST['password'] = $password = $params['password'];
        }

       
       
        


        $errors = array();
        $this->form_validation->set_rules('email','email','required|trim|valid_email|is_unique[tb_users.email]', array('is_unique' => 'This %s already exists. Please login'));
        $this->form_validation->set_rules('phone','phone number','required|trim|numeric|is_unique[tb_users.phone]', array('is_unique' => 'This %s already exists. Please login'));
        $this->form_validation->set_rules('name','name','required|trim|min_length[3]|alpha');
        $this->form_validation->set_rules('password','password','required|trim|min_length[6]');
        
        if ($this->form_validation->run() == FALSE) 
        {
            $errors['errors'] = $this->form_validation->error_array();                
        }
        else
        {
            
            $response = array();  
            if(empty($errors))
            {
                $data   = array(
                    'email' => $email,
                    'full_name' => $name,
                    'phone' => $phone,
                    'password' => $this->encode_base64string($password),
                    'created_at' => $created_at,
                    'updated_at' => $updated_at,
                    'demo_wallet_amount' => 10000,
                    'status' => 0,
                    
                );
                
                $insert = $this->general->add($this->tbl_users,$data);
                $user_id = $this->db->insert_id();
                $insert_id = $this->db->insert_id();
                $pan_data = array(
                        'user_id' => $insert_id,
                        'status' => 1,
                        'created_at' => date('Y-m-d H:i:s'),
                         );
                $result = $this->Allfiles_model->data_save("tb_pan_bank_details",$pan_data);
                if($insert['status'] != true)
                {
                    $errors['errors'][] = $insert;
                }
                else
                {
                    $sms = $this->send_otp($phone);                    
                }   
            } 
        }



        if(isset($errors) && !empty($errors))
        {
            $errors['status'] = FALSE;
            $errors['message'] = "Email/Mobile already exist";
            $this->set_response($errors, REST_Controller::HTTP_NOT_FOUND); 
        }
        else
        {
            $response = array(
                "status" => TRUE,
                "message" => "User Register Successfully",
                "data" => array("User Name" => $name,"Email" => $email,"Mobile Number" => $phone,"User id" => $user_id, "SMS-gateway"=> $sms),
            );
            $this->add_contact_razorpay($response['data']); 
            $this->set_response($response, REST_Controller::HTTP_CREATED);
        }   
    }


    function add_contact_razorpay($data = [])
    {
        if(isset($data) && !empty($data) && is_array($data))
        {
            $name = $data["User Name"];
            $email = $data["Email"];
            $user_id = $data["User id"];
            $contact = $data["Mobile Number"];
            $pass_data = [
                "name"=>$name,
                "email"=>$email,
                "contact"=>$contact,
                "notes"=>[
                    "user_id"=>$user_id
                    ]
            ];
            $d = json_encode($pass_data);

            $url = 'https://api.razorpay.com/v1/contacts';
                $curl_pass = array(
                    "data"=>$d,
                    "url"=>$url,
                    "type"=>"POST",
                    "header"=> [RAZORPAY,'Content-Type: application/json']
                );					
            $response = _curl_request($curl_pass);   
            if(!empty($response))
            {
                $where = [["column"=>"user_id", "value"=>$user_id]];
                $data = [
                    "razorpay_response"=>json_encode($response),
                    "razorpay_contact_id"=>$response['id']
                ];
                $update = $this->general->update($this->tbl_users,$data,$where);
            }
        }
    }


    function send_otp($phone=null)
    {
        $res = ["status"=>false, "message"=>"OTP sent failed"];
        if(isset($phone) && !empty($phone) && is_numeric($phone))
        {
            $otp = rand(1000,9999);
            $sms_text = 'Your OTP to register/access ESY TRADE is'. $otp.' It will be valid for 3 minutes. - THANK YOU';
            $send = sms_gate_way($phone,$sms_text);

            $where = [["column"=>"phone", "value"=>$phone]];
            $data = [
                "otp"=>$otp
            ];
            $update = $this->general->update($this->tbl_users,$data,$where);
            $res = ["status" => true, "message"=> "OTP send successfully", "SMS-response"=>$send];
        }
        return $res;
    }

    function send_msg($mobile = null && $message = null && $senderid = null && $accusage = null && $unicode = null && $DLT_TE_ID=null)
    {
        $res = ["status"=>false, "message"=>"OTP sent failed"];
        $sms_text = 'Dear Parent your ward{#var1#}{#var2#}attended to the college at{#var3#}{#var4#}Sri Viswa Jr College';



    }


    public function otp_verify_post()
    {
       
        $params = json_decode(file_get_contents('php://input'), TRUE);
        $otp = $phone = "";  
        
        if(isset($params['otp']) && !empty($params['otp']))
        {
            $_POST['otp'] = $otp = $params['otp'];
        }

        if(isset($params['phone']) && !empty($params['phone']))
        {
            $_POST['phone'] = $phone = $params['phone'];
        }

        $errors = array();
       
        $this->form_validation->set_rules('otp','otp','required|trim|numeric');
        $this->form_validation->set_rules('phone','phone','required|trim|numeric');
        
        if ($this->form_validation->run() == FALSE) 
        {
            $errors['errors'] = $this->form_validation->error_array();                
        }
        else
        {
            $where = [['column'=>'phone','value'=>$phone]];
            $get = $this->general->get_row($this->tbl_users,'otp',$where);
            if(isset($get['resultSet']['otp']) && !empty($get['resultSet']['otp']))
            {
                if($otp == $get['resultSet']['otp'])
                {
                    $where = [["column"=>"phone", "value"=>$phone]];
                    $data = [
                        "status"=>1
                    ];
                    $update = $this->general->update($this->tbl_users,$data,$where);
                    $response = ["status"=>true,"message"=>"OTP validated successfully"];
                }
                else
                {
                    $response = ["status"=>false,"message"=>"OTP was not matched"];
                }
            }
            else
            {
                $response = ["status"=>false,"message"=>"Phone number not found"];
            }
        }

       
        if(isset($errors) && !empty($errors))
        {
            $errors['status'] = FALSE;
            $errors['message'] = "OTP verification failed";
            $this->set_response($errors, REST_Controller::HTTP_NOT_FOUND); 
        }
        else
        {
            $this->set_response($response, REST_Controller::HTTP_CREATED);
        }  
    }



    public function login_post()
    {

        $params = json_decode(file_get_contents('php://input'), TRUE);

        $phone = $password =  "";        

        // if(isset($params['email']) && !empty($params['email']))
        // {
        //     $_POST['email'] = $email = $params['email'];
        // }        

        if(isset($params['user_id']) && !empty($params['user_id']))
        {
            $_POST['user_id'] = $user_id = $params['user_id'];
        }

        if(isset($params['password']) && !empty($params['password']))
        {
            $_POST['password'] = $password = $params['password'];
        }

       

       
       
        


        $errors = array();
        // $this->form_validation->set_rules('email','email','required|trim|valid_email');
        $this->form_validation->set_rules('user_id','User Id','required|trim');
        $this->form_validation->set_rules('password','password','required|trim|min_length[6]');
        
        if ($this->form_validation->run() == FALSE) 
        {
            $errors['errors'] = $this->form_validation->error_array();                
        }
        else
        {
            
           

            $response = array();  
            if(empty($errors))
            {
                // $where = [
                //     ['column'=>'phone','value'=>$user_id],
                //     ['column'=>'status','value'=>1],                        
                //  ];
              
                $sql = "SELECT * FROM `tb_users` WHERE (email = '$user_id' OR phone = '$user_id') AND status = 1";
    
                // $where = $this->general->custom_query($sql,'array');
                
                $get_user = $this->general->custom_query($sql,'row'); 

                // echo $this->db->last_query();
                
    
                if(isset($get_user['resultSet']) && !empty($get_user['resultSet']))
                {
                    $d = $get_user['resultSet'];
                    if($this->encode_base64string($password) == $d['password'])
                    {
                        $user_id = $d['user_id'];
                        $full_name = $d['full_name'];
                        $phone = $d['phone'];
                        $email = $d['email'];
                    }
                    else
                    {
                        $errors['errors'][] = "Invalid password details, Please try again";
                    }
                }
                else
                {
                    $errors['errors'][] = "Invalid user details";
                }
               
            } 
        }



        if(isset($errors) && !empty($errors))
        {
            $errors['status'] = FALSE;
            $errors['message'] = "User login failed";
            $this->set_response($errors, REST_Controller::HTTP_NOT_FOUND); 
        }
        else
        {
            $response = array(
                "status" => TRUE,
                "message" => "User successfully Login",
                "USER-ID" => $user_id,
                "data" => array("User Name" => $full_name,"Email" => $email,"Mobile Number" => $phone,"User id" => $user_id),
            ); 
            $this->set_response($response, REST_Controller::HTTP_CREATED);
        }   
    }

    public function change_phone_number_post()
    {
              
        $params = json_decode(file_get_contents('php://input'), TRUE);
        $user_id = $phone = "";  
        
        if(isset($params['user_id']) && !empty($params['user_id']))
        {
            $_POST['user_id'] = $user_id = $params['user_id'];
        }

        if(isset($params['phone']) && !empty($params['phone']))
        {
            $_POST['phone'] = $phone = $params['phone'];
        }

        $errors = array();
       
        $this->form_validation->set_rules('user_id','user_id','required|trim|numeric');
        $this->form_validation->set_rules('phone','phone number','required|trim|numeric|is_unique[tb_users.phone]', array('is_unique' => 'This %s already exists.'));
        
        if ($this->form_validation->run() == FALSE) 
        {
            $errors['errors'] = $this->form_validation->error_array();                
        }
        else
        {
            $where = [['column'=>'user_id','value'=>$user_id]];
            $get = $this->general->get_row($this->tbl_users,'user_id',$where);
            if(isset($get['resultSet']['user_id']) && !empty($get['resultSet']['user_id']))
            {
                $user_id = $get['resultSet']['user_id'];                
                $where = [["column"=>"user_id", "value"=>$user_id]];
                $data = ["phone"=>$phone];
                $update = $this->general->update($this->tbl_users,$data,$where);               
                if($update['status'])
                {
                    $sms = $this->send_otp($phone); 
                    $response = ["status"=>true,"message"=>"Phone number updated successfully, Please Check OTP to verify", "SMS-gateway"=> $sms];
                }
                else
                {
                    $response = ["status"=>false,"message"=>"phone update failed"];
                }
            }
            else
            {
                $response = ["status"=>false,"message"=>"User not found"];
            }
        }

       
        if(isset($errors) && !empty($errors))
        {
            $errors['status'] = FALSE;
            $errors['message'] = "OTP verification failed";
            $this->set_response($errors, REST_Controller::HTTP_NOT_FOUND); 
        }
        else
        {
            $this->set_response($response, REST_Controller::HTTP_CREATED);
        }   
    }

    public function encode_base64string($encode_str)
    {
        return base64_encode(base64_encode($encode_str));
    }

    public function verifyotpforupdatepassword_post()
    {
        $params = json_decode(file_get_contents('php://input'), TRUE);
        $user_id = $password = $otp = "";

        if(isset($params['user_id']) && !empty($params['user_id']))
        {
            $_POST['user_id'] = $user_id = $params['user_id'];
        }
        if(isset($params['password']) && !empty($params['password']))
        {
            $_POST['password'] = $password = $params['password'];
        }
        if(isset($params['otp']) && !empty($params['otp']))
        {
            $_POST['otp'] = $otp = $params['otp'];
        }


        $errors = array();
        $this->form_validation->set_rules('user_id','User Id','required|trim|numeric');
        $this->form_validation->set_rules('otp','otp','required|trim|numeric');
        $this->form_validation->set_rules('password','password','required|trim|min_length[6]');

        if ($this->form_validation->run() == FALSE) 
        {
            $errors['errors'] = $this->form_validation->error_array();                
        }
        else
        {     $password = $this->encode_base64string($password);        
              $where = [['column'=>'user_id','value'=>$user_id]];
              $get = $this->general->get_row($this->tbl_users,'otp',$where);
              $getid = $this->general->get_row($this->tbl_users,'user_id',$where);

              if(isset($get['resultSet']['otp']) && !empty($get['resultSet']['otp']))
              { 
                if($otp == $get['resultSet']['otp'])
                {   
                    
                      $data = [
                        "status"=>1
                    ];
                     $update = $this->general->update($this->tbl_users,$data,$where);

                    
                         $response = ["status"=>true,"message"=>"OTP validated successfully"];
                         $user_id = $getid['resultSet']['user_id'];
                         $where = [["column"=>"user_id", "value"=>$user_id]];
                         $data = ["password"=>$password];
                         $update = $this->general->update($this->tbl_users,$data,$where);
                }
                else
                {
                    $response = ["status"=>false,"message"=>"OTP Not Matched"];
                }
               


        }
    }
        
        if(isset($errors) && !empty($errors))
        {
            $errors['status'] = FALSE;
            $errors['message'] = "OTP verification failed";
            $this->set_response($errors, REST_Controller::HTTP_NOT_FOUND); 
        }
        else
        {
            $response = array(
                "status" => TRUE,
                "message" => "Password Updated Successfully",
            ); 
            $this->set_response($response, REST_Controller::HTTP_CREATED);
        } 



    }

    public function verifyuserexit_post()
    {

        $params = json_decode(file_get_contents('php://input'), TRUE);
        $phone  = "";
        if(isset($params['phone']) && !empty($params['phone']))
        {
            $_POST['phone'] = $phone = $params['phone'];
        }
        $errors = array();
        $this->form_validation->set_rules('phone','phone number','required|trim|numeric');
       if($this->form_validation->run() == FALSE) 
        {
            $errors['errors'] = $this->form_validation->error_array();                
        }
        else
        {   
            $response = array(); 
            $sql = "SELECT * FROM `tb_users` WHERE phone = '$phone' AND status = 1"; 
            $get_user = $this->general->custom_query($sql,'row');
            if($get_user['status'] == 1)
            {
                    $sms = $this->send_otp($phone); 
                    $response = ["status"=>true,"message"=>"OTP Send Successfully, Please Check OTP to verify", "SMS-gateway"=> $sms];
                if(isset($get_user['resultSet']) && !empty($get_user['resultSet']))
                {
                        $d = $get_user['resultSet'];
                        $user_id = $d['user_id'];
                        $full_name = $d['full_name'];
                        $phone = $d['phone'];
                        $email = $d['email'];
                        $otp = $d['otp'];
                }
                else
                {
                     $errors['errors'][] = "Mobile Number Not Registered";
                }

            }
            else
            {
                    $errors['errors'][] = "Otp Generated failed";
                
            }


        }
         if(isset($errors) && !empty($errors))
        {
            $errors['status'] = FALSE;
            $errors['message'] = "Mobile Number Does Not Exist";
            $this->set_response($errors, REST_Controller::HTTP_NOT_FOUND); 
        }
        else
        {
            $response = array(
                "status" => TRUE,
                "message" => "User Exist",
                "USER-ID" => $user_id,
                "data" => array("User Name" => $full_name,"Email" => $email,"Mobile Number" => $phone,"User id" => $user_id, "OTP" => $otp),
            ); 
            $this->set_response($response, REST_Controller::HTTP_CREATED);
        } 
        

        
    }


   
}