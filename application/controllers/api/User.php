<?php

use Restserver\Libraries\REST_Controller;

Header('Access-Control-Allow-Origin: *'); //for allow any domain, insecure
Header('Access-Control-Allow-Headers: *'); //for allow any headers, insecure
Header('Access-Control-Allow-Methods: GET, POST, OPTIONS, PUT, DELETE'); //method allowed
// defined('BASEPATH') OR exit('No direct script access allowed');
// This can be removed if you use __autoload() in config.php OR use Modular Extensions
/** @noinspection PhpIncludeInspection */
require(APPPATH . '/libraries/REST_Controller.php');


/**
 * This is an example of a few basic user interaction methods you could use
 * all done with a hardcoded array
 *
 * @package         CodeIgniter
 * @subpackage      Rest Server
 * @category        Controller
 * @author          Phil Sturgeon, Chris Kacerguis
 * @license         MIT
 * @link            https://github.com/chriskacerguis/codeigniter-restserver
 */

class User extends REST_Controller
{

    private $tbl_users = "tb_users";
    function __construct()
    {
        // Construct the parent class
        parent::__construct();
        $this->load->model("general_model", "general");
        $this->load->model('User_model');
        $this->load->model('Restapi_model');
    }
    public function convertbase64string($encode_str)
    {
        return base64_encode(base64_encode($encode_str));
    }
    public function encode_base64string($encode_str)
    {
        return base64_encode(base64_encode($encode_str));
    }

    public function login_post()
    {
        $params = json_decode(file_get_contents('php://input'), TRUE);

        $phone = $pin =  "";

        if (isset($params['phone']) && !empty($params['phone'])) {
            $_POST['phone'] = $phone = $params['phone'];
        }

        if (isset($params['pin']) && !empty($params['pin'])) {
            $_POST['pin'] = $pin = $params['pin'];
        }
        $errors = array();
        $this->form_validation->set_rules('phone', 'Mobile Number', 'required|trim');
        $this->form_validation->set_rules('pin', 'Pin', 'required|trim|min_length[6]|max_length[6]');
        if ($this->form_validation->run() == FALSE) {
            $errors['errors'] = $this->form_validation->error_array();
        } else {
            $response = array();
            if (empty($errors)) {

                $sql = "SELECT * FROM `tb_users` WHERE (phone = '$phone') AND status = 1";

                // $where = $this->general->custom_query($sql,'array');

                $get_user = $this->general->custom_query($sql, 'row');

                // echo $this->db->last_query();


                if (isset($get_user['resultSet']) && !empty($get_user['resultSet'])) {
                    $d = $get_user['resultSet'];
                    if ($pin == $d['pin']) {
                        $full_name = $d['full_name'];
                        $phone = $d['phone'];
                        $role = $d['role'];

                        if ($role == 1) {
                            $role_name = "Admin";
                        } elseif ($role == 2) {
                            $role_name = "Technician";
                        } elseif ($role == 3) {
                            $role_name = "Customer";
                        }
                    } else {
                        $errors['errors'][] = "Invalid Pin details, Please try again";
                    }
                } else {
                    $errors['errors'][] = "Invalid User details";
                }
            }
        }

        if (isset($errors) && !empty($errors)) {
            $errors['status'] = FALSE;
            $errors['message'] = "User login failed";
            $this->set_response($errors, REST_Controller::HTTP_OK);
        } else {
            $response = array(
                "status" => TRUE,
                "message" => "User successfully Login",
                "data" => array("User Name" => $full_name, "Mobile Number" => $phone, "Role" => $role_name),
            );
            $this->set_response($response, REST_Controller::HTTP_CREATED);
        }
    }

    public function registration_post()
    {
        $params = json_decode(file_get_contents('php://input'), TRUE);
        $created_at = $updated_at = current_date();

        $full_name = $email = $phone = $pin = $model_name = $make_year = $address = $landmark = $city = $state = $pincode = $role = "";

        if (isset($params['full_name']) && !empty($params['full_name'])) {
            $_POST['full_name'] = $full_name = $params['full_name'];
        }
        if (isset($params['email']) && !empty($params['email'])) {
            $_POST['email'] = $email = $params['email'];
        }
        if (isset($params['phone']) && !empty($params['phone'])) {
            $_POST['phone'] = $phone = $params['phone'];
        }

        if (isset($params['address']) && !empty($params['address'])) {
            $_POST['address'] = $address = $params['address'];
        }
        if (isset($params['landmark']) && !empty($params['landmark'])) {
            $_POST['landmark'] = $landmark = $params['landmark'];
        }
        if (isset($params['city']) && !empty($params['city'])) {
            $_POST['city'] = $city = $params['city'];
        }
        if (isset($params['state']) && !empty($params['state'])) {
            $_POST['state'] = $state = $params['state'];
        }
        if (isset($params['pincode']) && !empty($params['pincode'])) {
            $_POST['pincode'] = $pincode = $params['pincode'];
        }
        if (isset($params['role']) && !empty($params['role'])) {
            $_POST['role'] = $role = $params['role'];
        }

        $errors = array();

        $this->form_validation->set_rules('email', 'Email', 'required|trim|valid_email|is_unique[tb_users.email]', array('is_unique' => 'This %s already exists. Please login'));
        $this->form_validation->set_rules('phone', 'Phone Number', 'required|trim|numeric|is_unique[tb_users.phone]', array('is_unique' => 'This %s already exists. Please login'));
        $this->form_validation->set_rules('full_name', 'Full Name', 'required|min_length[3]');
        $this->form_validation->set_rules('address', 'Address', 'required');
        $this->form_validation->set_rules('landmark', 'Landmark', 'required');
        $this->form_validation->set_rules('city', 'City', 'required');
        $this->form_validation->set_rules('state', 'State', 'required');
        $this->form_validation->set_rules('pincode', 'Pincode', 'required|min_length[6]|max_length[6]');
        $this->form_validation->set_rules('role', 'Role', 'required');
        // if ($role == 2) {
        //     if (isset($params['pin']) && !empty($params['pin'])) {
        //         $_POST['pin'] = $pin = $params['pin'];
        //     }
        //     $this->form_validation->set_rules('pin', 'Pin', 'required|trim|min_length[6]|max_length[6]');
        // }

        if ($this->form_validation->run() == FALSE) {
            $errors['errors'] = $this->form_validation->error_array();
        } else {
            $response = array();

            if (empty($errors)) {

                if ($role == 2) {

                    $data   = array(
                        'email' => $email,
                        'full_name' => $full_name,
                        'phone' => $phone,
                        'pin' => 123456,
                        'address' => $address,
                        'landmark' => $landmark,
                        'city' => $city,
                        'state' => $state,
                        'pincode' => $pincode,
                        'role' => $role,
                        'created_at' => $created_at,
                        'updated_at' => $updated_at,
                        'status' => 1,
                        'pin_flag' => 0
                    );
                } elseif ($role == 3) {

                    if (isset($params['model_name']) && !empty($params['model_name'])) {
                        $_POST['model_name'] = $model_name = $params['model_name'];
                    }
                    if (isset($params['make_year']) && !empty($params['make_year'])) {
                        $_POST['make_year'] = $make_year = $params['make_year'];
                    }
                    $this->form_validation->set_rules('model_name', 'Model Name', 'required');
                    $this->form_validation->set_rules('make_year', 'Make Year', 'required');

                    $data   = array(
                        'email' => $email,
                        'full_name' => $full_name,
                        'phone' => $phone,
                        'address' => $address,
                        'landmark' => $landmark,
                        'city' => $city,
                        'state' => $state,
                        'pincode' => $pincode,
                        'role' => $role,
                        'model_name' => $model_name,
                        'make_year' => $make_year,
                        'created_at' => $created_at,
                        'updated_at' => $updated_at,
                        'status' => 1,
                    );
                }

                $insert = $this->general->add($this->tbl_users, $data);
                $user_id = $this->db->insert_id();
                if ($insert['status'] != true) {
                    $errors['errors'][] = $insert;
                }
            }
        }
        if (isset($errors) && !empty($errors)) {
            $errors['status'] = FALSE;
            $errors['message'] = "Email/Mobile already exist";
            $this->set_response($errors, REST_Controller::HTTP_OK);
        } else {
            if ($role == 1) {
                $role_name = "Admin";
            } elseif ($role == 2) {
                $role_name = "Technician";
            } elseif ($role == 3) {
                $role_name = "Customer";
            }
            $response = array(
                "status" => TRUE,
                "message" => "User Register Successfully",
                "data" => array("User Name" => $full_name, "Email" => $email, "Mobile Number" => $phone, "User id" => $user_id, "Role" => $role_name),
            );
            $this->set_response($response, REST_Controller::HTTP_OK);
        }
    }


    public function getuser_get($id = null)
    {
        $where = [];
        $like = [];
        if (isset($id) && !empty($id) && is_numeric($id)) {
            $where[] = ["column" => "d.role", "value" => $id];
        }
        $order_by = [];
        $order_by[] =  ["column" => "d.user_id", "type" => (isset($_GET['order']) && !empty($_GET['order'])) ? $_GET['order'] : 'DESC'];
        $limit_start = (isset($_GET['limit_start'])) ? $_GET['limit_start'] : 0;
        $limit_end = (isset($_GET['limit_end'])) ? $_GET['limit_end'] : 0;
        $array = [
            "fileds" => "d.*",
            "table" => $this->tbl_users . ' AS d',
            "join_tables" => [],
            "where" => $where,
            "like" => $like,
            "limit" => ["start" => $limit_start, "end" => $limit_end],
            "order_by" => $order_by,
        ];
        $p = $this->general->get_all_from_join($array);
        if (isset($p['resultSet']) && !empty($p['resultSet'])) {
            $res = [
                'status' => TRUE,
                'message' => '',
                'data' => $p['resultSet']
            ];
        } else {
            $res = [
                'status' => FALSE,
                'message' => 'Data not found',
            ];
        }
        $this->response($res, REST_Controller::HTTP_OK);
    }

    public function updateUser_post()
    {
        $params = json_decode(file_get_contents('php://input'), TRUE);
        $updated_at = current_date();
        $user_id = $full_name = $email = $phone = $pin = $model_name = $make_year = $address = $landmark = $city = $state = $pincode = $role = "";
        if (isset($params['user_id']) && !empty($params['user_id'])) {
            $_POST['user_id'] = $user_id = $params['user_id'];
        }
        if (isset($params['full_name']) && !empty($params['full_name'])) {
            $_POST['full_name'] = $full_name = $params['full_name'];
        }
        if (isset($params['email']) && !empty($params['email'])) {
            $_POST['email'] = $email = $params['email'];
        }
        if (isset($params['phone']) && !empty($params['phone'])) {
            $_POST['phone'] = $phone = $params['phone'];
        }

        if (isset($params['address']) && !empty($params['address'])) {
            $_POST['address'] = $address = $params['address'];
        }
        if (isset($params['landmark']) && !empty($params['landmark'])) {
            $_POST['landmark'] = $landmark = $params['landmark'];
        }
        if (isset($params['city']) && !empty($params['city'])) {
            $_POST['city'] = $city = $params['city'];
        }
        if (isset($params['state']) && !empty($params['state'])) {
            $_POST['state'] = $state = $params['state'];
        }
        if (isset($params['pincode']) && !empty($params['pincode'])) {
            $_POST['pincode'] = $pincode = $params['pincode'];
        }
        if (isset($params['role']) && !empty($params['role'])) {
            $_POST['role'] = $role = $params['role'];
        }
        $errors = array();
        $this->form_validation->set_rules('email', 'Email', 'required|trim');
        $this->form_validation->set_rules('phone', 'Phone Number', 'required|trim|numeric|min_length[10]|max_length[10]');
        $this->form_validation->set_rules('full_name', 'Full Name', 'required|min_length[3]');
        $this->form_validation->set_rules('address', 'Address', 'required');
        $this->form_validation->set_rules('landmark', 'Landmark', 'required');
        $this->form_validation->set_rules('city', 'City', 'required');
        $this->form_validation->set_rules('state', 'State', 'required');
        $this->form_validation->set_rules('pincode', 'Pincode', 'required|min_length[6]|max_length[6]');
        $this->form_validation->set_rules('role', 'Role', 'required');

        if ($role == 2) {
            if (isset($params['pin']) && !empty($params['pin'])) {
                $_POST['pin'] = $pin = $params['pin'];
            }
            $this->form_validation->set_rules('pin', 'Pin', 'required|trim|min_length[6]|max_length[6]');
        }
        if ($this->form_validation->run() == FALSE) {
            $errors['errors'] = $this->form_validation->error_array();
        } else {
            $response = array();
            if (empty($errors)) {

                if($role == 2)
                {
                    $data   = array(
                        'email' => $email,
                        'full_name' => $full_name,
                        'phone' => $phone,
                        'pin' => $pin,
                        'address' => $address,
                        'landmark' => $landmark,
                        'city' => $city,
                        'state' => $state,
                        'pincode' => $pincode,
                        'role' => $role,
                        'updated_at' => $updated_at,
                        'status' => 1,
                    );
                }
                else if($role == 3)
                {
                    if (isset($params['model_name']) && !empty($params['model_name'])) {
                        $_POST['model_name'] = $model_name = $params['model_name'];
                    }
                    if (isset($params['make_year']) && !empty($params['make_year'])) {
                        $_POST['make_year'] = $make_year = $params['make_year'];
                    }
                    $this->form_validation->set_rules('model_name', 'Model Name', 'required');
                    $this->form_validation->set_rules('make_year', 'Make Year', 'required');
                    $data   = array(
                        'email' => $email,
                        'full_name' => $full_name,
                        'phone' => $phone,
                        'address' => $address,
                        'landmark' => $landmark,
                        'city' => $city,
                        'state' => $state,
                        'pincode' => $pincode,
                        'role' => $role,
                        'model_name' => $model_name,
                        'make_year' => $make_year,
                        'updated_at' => $updated_at,
                        'status' => 1,
                    );
                }
               
                $where = [['column' => 'user_id', 'value' => $user_id]];
                $update = $this->general->update($this->tbl_users, $data, $where);
                $user_id = $this->db->insert_id();
                if ($update['status'] != true) {
                    $errors['errors'][] = $update;
                    $errors['errors'][] = "User Data not found";
                }
            }
        }
        if (isset($errors) && !empty($errors)) {
            $errors['status'] = FALSE;
            $errors['message'] = "User Updated Failed";
            $this->set_response($errors, REST_Controller::HTTP_OK);
        } else {
            if ($role == 1) {
                $role_name = "Admin";
            } elseif ($role == 2) {
                $role_name = "Technician";
            } elseif ($role == 3) {
                $role_name = "Customer";
            }
            $response = array(
                "status" => TRUE,
                "message" => "User Updated Successfully",
                "data" => array("Full Name" => $full_name, "Phone" => $phone, "Model Name" => $model_name),
            );
            $this->set_response($response, REST_Controller::HTTP_CREATED);
        }
    }

    public function deleteUser_post()
    {
        $params = json_decode(file_get_contents('php://input'), TRUE);
        $user_id =  "";
        if (isset($params['user_id']) && !empty($params['user_id'])) {
            $_POST['user_id'] = $user_id = $params['user_id'];
        }
        $errors = array();
        $this->form_validation->set_rules('user_id', 'User Id', 'required|numeric');
        if ($this->form_validation->run() == FALSE) {
            $errors['errors'] = $this->form_validation->error_array();
        } else {
            $response = array();
            if (empty($errors)) {
                $where = [['column' => 'user_id', 'value' => $user_id]];
                $delete = $this->general->delete_row($this->tbl_users, $where);
                if ($delete['status'] != true) {
                    $errors['Message'][] = "Record Not Found";
                }
            }
        }
        if (isset($errors) && !empty($errors)) {
            $errors['status'] = FALSE;
            $errors['message'] = "User Deleted Failed";
            $this->set_response($errors, REST_Controller::HTTP_OK);
        } else {
            $response = array(
                "status" => TRUE,
                "message" => "User Deleted Successfully",
            );
            $this->set_response($response, REST_Controller::HTTP_CREATED);
        }
    }


    public function change_pin_post()
    {
        $params = json_decode(file_get_contents('php://input'), TRUE);
        $created_at = $updated_at = current_date();

        $phone = $pin = $confirm_pin = "";

        if (isset($params['phone']) && !empty($params['phone'])) {
            $_POST['phone'] = $phone = $params['phone'];
        }

        if (isset($params['pin']) && !empty($params['pin'])) {
            $_POST['pin'] = $pin = $params['pin'];
        }
        if (isset($params['confirm_pin']) && !empty($params['confirm_pin'])) {
            $_POST['confirm_pin'] = $confirm_pin = $params['confirm_pin'];
        }

        
        $errors = array();
        $this->form_validation->set_rules('phone', 'Mobile Number', 'required|trim');
        // $this->form_validation->set_rules('pin', 'Pin', 'required|trim|numeric|min_length[6]|max_length[6]');
        // $this->form_validation->set_rules('confirm_pin', 'Confirm Pin', 'required|trim|numeric|min_length[6]|max_length[6]');
        if ($this->form_validation->run() == FALSE) {
            $errors['errors'] = $this->form_validation->error_array();
        } else {
            $response = array();
            if (empty($errors)) {

                $sql = "SELECT * FROM `tb_users` WHERE (phone = '$phone')";

                // $where = $this->general->custom_query($sql,'array');

                $get_user = $this->general->custom_query($sql, 'row');

                // echo $this->db->last_query();


                if (isset($get_user['resultSet']) && !empty($get_user['resultSet'])) {
                    $d = $get_user['resultSet'];
                    $this->form_validation->set_rules('pin', 'Pin', 'required|trim|numeric|min_length[6]|max_length[6]');
                    $this->form_validation->set_rules('confirm_pin', 'Confirm Pin', 'required|trim|numeric|min_length[6]|max_length[6]');

                    if ($this->form_validation->run() == FALSE) {
                        $errors['errors'] = $this->form_validation->error_array();
                    } 
                    else {

                        if($pin == $confirm_pin) 
                        {
                            $user_id = $d['user_id'];
                            $data   = array(
                                'pin' => $pin,
                                "pin_flag" => 1,
                                'updated_at' => $updated_at,
                            );
                            $where = [['column' => 'user_id', 'value' => $user_id]];
                            $update = $this->general->update($this->tbl_users, $data, $where);
                        }

                        else
                        {
                            $errors['errors'][] = "Pin and Confirm Pin not matched";

                        }  
                    }
                    
                        // if($pin == $confirm_pin) 
                        // {
                        //     $user_id = $d['user_id'];
                        //     $data   = array(
                        //         'pin' => $pin,
                        //         "pin_flag" => 1,
                        //         'updated_at' => $updated_at,
                        //     );
                        //     $where = [['column' => 'user_id', 'value' => $user_id]];
                        //     $update = $this->general->update($this->tbl_users, $data, $where);
                        // }

                        // else
                        // {
                        //     $errors['errors'][] = "Pin and Confirm Pin not matched";

                        // }  
                    
                } else {
                    $errors['errors'][] = "No User Found";
                }
            }
        }

        if (isset($errors) && !empty($errors)) {
            $errors['status'] = FALSE;
            $errors['message'] = "Pin update failed";
            $this->set_response($errors, REST_Controller::HTTP_OK);
        } else {
            $response = array(
                "status" => TRUE,
                "message" => "Pin updated succesfully",
                
            );
            $this->set_response($response, REST_Controller::HTTP_CREATED);
        }

    }
}
