<?php
use Restserver\Libraries\REST_Controller;
Header('Access-Control-Allow-Origin: *'); //for allow any domain, insecure
Header('Access-Control-Allow-Headers: *'); //for allow any headers, insecure
Header('Access-Control-Allow-Methods: GET, POST, OPTIONS, PUT, DELETE'); //method allowed
// defined('BASEPATH') OR exit('No direct script access allowed');

// This can be removed if you use __autoload() in config.php OR use Modular Extensions
/** @noinspection PhpIncludeInspection */
require(APPPATH.'/libraries/REST_Controller.php');



/**
 * This is an example of a few basic user interaction methods you could use
 * all done with a hardcoded array
 *
 * @package         CodeIgniter
 * @subpackage      Rest Server
 * @category        Controller
 * @author          Phil Sturgeon, Chris Kacerguis
 * @license         MIT
 * @link            https://github.com/chriskacerguis/codeigniter-restserver
 */
class Banks extends REST_Controller {

    private $tbl_bank = "tb_pan_bank_details";    
    private $tbl_users = "tb_users";    
    private $tbl_deposits = "tb_deposits";    
   
    function __construct() {
        // Construct the parent class
        parent::__construct();
        $this->load->model("general_model","general");
    }	
    

    public function bank_post()
    {

        $params = json_decode(file_get_contents('php://input'), TRUE);

        $user_id = $pan_number = $bank_name = $branch =  $ifsc_code = $account_number = "";

        $created_on  = current_date();


        if(isset($params['user_id']) && !empty($params['user_id']))
        {
            $_POST['user_id'] = $user_id = $params['user_id'];
        }

        // if(isset($params['pan_number']) && !empty($params['pan_number']))
        // {
        //     $_POST['pan_number'] = $pan_number = $params['pan_number'];
        // }

        if(isset($params['bank_name']) && !empty($params['bank_name']))
        {
            $_POST['bank_name'] = $bank_name = $params['bank_name'];
        } 

        if(isset($params['beneficiary_name']) && !empty($params['beneficiary_name']))
        {
            $_POST['beneficiary_name'] = $beneficiary_name = $params['beneficiary_name'];
        }       

        if(isset($params['branch']) && !empty($params['branch']))
        {
            $_POST['branch'] = $branch = $params['branch'];
        }  

        if(isset($params['ifsc_code']) && !empty($params['ifsc_code']))
        {
            $_POST['ifsc_code'] = $ifsc_code = $params['ifsc_code'];
        } 

        if(isset($params['account_number']) && !empty($params['account_number']))
        {
            $_POST['account_number'] = $account_number = $params['account_number'];
        }      
       
        


        $errors = array();
        $this->form_validation->set_rules('user_id','user_id','required|trim|numeric');
        // $this->form_validation->set_rules('pan_number','Pan number','required|trim');
        $this->form_validation->set_rules('bank_name','Bank name','required|trim');
        $this->form_validation->set_rules('beneficiary_name','Beneficiary Name','required|trim');
        $this->form_validation->set_rules('branch','Branch','required|trim');
        $this->form_validation->set_rules('ifsc_code','Ifsc code','required|trim');
        $this->form_validation->set_rules('account_number','Ifsc code','required|trim');
        
        if ($this->form_validation->run() == FALSE) 
        {
            $errors['errors'] = $this->form_validation->error_array();                
        }
        else
        {
            
            // if($deposite_amount < 100)
            // {
            //     $errors['errors'][] = "Deposit amount must be greater than 100";
            // }

            $response = array();  
            if(empty($errors))
            {
                $data   = array(                    
                    'bank_name' => $bank_name,
                    'branch' => $branch,
                    'ifsc_code' => $ifsc_code,
                    'account_number' => $account_number,
                    'beneficiary_name' => $beneficiary_name,
                    'status' => 0,                                
                    'updated_at' => $created_on,             
                );
               
                $where = [['column'=>'user_id','value'=>$user_id]];
                $get = $this->general->get_row($this->tbl_bank,'id',$where);
                if(isset($get['resultSet']['id']) && !empty($get['resultSet']['id']))
                {
                    $bank_id = $get['resultSet']['id'];
                    $where = [['column'=>'id','value'=>$bank_id]];                       
                    $update = $this->general->update($this->tbl_bank,$data,$where);
                    if($update['status'] != true)
                    {
                        $errors['errors'][] = $update;
                    }   
                }
                else
                {
                    $data['user_id'] = $user_id;
                    $data['created_at'] = $created_on;
                    $insert = $this->general->add($this->tbl_bank,$data);
                   
                    if($insert['status'] != true)
                    {
                        $errors['errors'][] = $insert;
                    }
                    else
                    {
                        $bank_id = $this->db->insert_id();
                    }   
                }
                
                
            } 
        }



        if(isset($errors) && !empty($errors))
        {
            $errors['status'] = FALSE;
            $errors['message'] = "Bank adding failed";
            $this->set_response($errors, REST_Controller::HTTP_NOT_FOUND); 
        }
        else
        {
            $response = array(
                "status" => TRUE,
                "message" => "Bank details added successfully",               
            );
            $data['user_id'] = $user_id;
            $data['bank_id'] = $bank_id;
            $this->add_fund_account_razorpay($data); 
            $this->set_response($response, REST_Controller::HTTP_CREATED);
        }   
    } 
    
    
    public function banks_get($id=null)
    {
        $where = [];  $like = [];
        if(isset($id) && !empty($id) && is_numeric($id))
        {
            $where[] = ["column" => "d.id","value" => $id];   
        }

        if(isset($_GET['user_id']) && !empty($_GET['user_id']))
        {
            $where[] = ["column" => "d.user_id","value" => $_GET['user_id']];   
        }

            

        $order_by = [];            
        $order_by[] =  ["column" => "d.id", "type" => (isset($_GET['order']) && !empty($_GET['order']))?$_GET['order']:'DESC'];

        $limit_start = (isset($_GET['limit_start']))?$_GET['limit_start']:0;
        $limit_end = (isset($_GET['limit_end']))?$_GET['limit_end']:0;

        $array = [
            "fileds" => "d.*, u.full_name, u.email, u.phone",
            "table" => $this->tbl_bank.' AS d',
            "join_tables" => [
                ["table"=>$this->tbl_users.' AS u',"join_on"=>'u.user_id = d.user_id',"join_type" => "left"]],
            "where" => $where,
            "like" => $like,
            "limit" => ["start" => $limit_start, "end" => $limit_end],
            "order_by" => $order_by,
        ];       
       
       
        $p = $this->general->get_all_from_join($array); 
        if(isset($p['resultSet']) && !empty($p['resultSet']))
        {
            $res = [
                'status'=>TRUE,
                'message'=>'',
                'data'=>$p['resultSet']
            ];
        }
        else
        {
            $res = [
                'status'=>FALSE,
                'message'=>'Data not found',
            ];
        }
        $this->response($res, REST_Controller::HTTP_OK); 
    }  


    public function pan_post()
    {

        $params = json_decode(file_get_contents('php://input'), TRUE);

        $user_id = $pan_number = $pan_file = "";

        $created_on  = current_date();


        if(isset($params['user_id']) && !empty($params['user_id']))
        {
            $_POST['user_id'] = $user_id = $params['user_id'];
        }

        if(isset($params['pan_number']) && !empty($params['pan_number']))
        {
            $_POST['pan_number'] = $pan_number = $params['pan_number'];
        }

        if(isset($params['pan_file']) && !empty($params['pan_file']))
        {
            $_POST['pan_file'] = $pan_file = $params['pan_file'];
        }     
       
        


        $errors = array();
        $this->form_validation->set_rules('user_id','user_id','required|trim|numeric');
        $this->form_validation->set_rules('pan_number','Pan number','required|trim');
        $this->form_validation->set_rules('pan_file','Pan file','required|trim');       
        
        if ($this->form_validation->run() == FALSE) 
        {
            $errors['errors'] = $this->form_validation->error_array();                
        }
        else
        {
            
            // if($deposite_amount < 100)
            // {
            //     $errors['errors'][] = "Deposit amount must be greater than 100";
            // }

            $response = array();  
            if(empty($errors))
            {

                // $image_parts = explode(";base64,", $pan_file);
                if(isset($pan_file) && !empty($pan_file))
                {
                    $file_data = base64_decode($pan_file);
                    $f = finfo_open();
                    $mime_type = finfo_buffer($f, $file_data, FILEINFO_MIME_TYPE);
                    $file_type = explode('/', $mime_type)[0];
                    $extension = explode('/', $mime_type)[1];
        
                    // echo "mime_type=>".$mime_type.PHP_EOL; // will output mimetype, f.ex. image/jpeg
                    // echo "file_type=>".$file_type.PHP_EOL; // will output file type, f.ex. image
                    // echo "extension=>".$extension.PHP_EOL; // will output extension, f.ex. jpeg
                    // exit;
        
                     $acceptable_mimetypes = [
                            'image/png',
                            'image/jpeg',
                        ];
        
                    // you can write any validator below, you can check a full mime type or just an extension or file type
                    if (!in_array($mime_type, $acceptable_mimetypes))
                    {
                        $errors['errors'][] = "File is not an image, Only png,jpeg acceptable"; 
                    }
        
                    // or example of checking just a type
                    if ($file_type !== 'image')
                    {
                        $errors['errors'][] = "File is not an image";                                   
                    }
                }
                else
                {
                    $errors['errors'][] = "File is not an image file"; 
                }


                $directoryName =  'uploads/user_pans/';
                if(!is_dir($directoryName)){
                    //Directory does not exist, so lets create it.
                    mkdir($directoryName, 0755, true);
                }                

                $where = [["column"=>"user_id","value"=>$user_id]];
                $get = $this->general->get_row($this->tbl_bank,'pan_proof',$where);
                if(isset($get['resultSet']['pan_proof']) && !empty($get['resultSet']['pan_proof']))
                {
                    if(is_file_exists("uploads/user_pans/".$get['resultSet']['pan_proof']))
                    {
                        unlink("uploads/user_pans/".$get['resultSet']['pan_proof']);
                    }                       
                }
                // $image_parts = explode(";base64,", $_POST['image']);
                // $image_type_aux = explode("image/", $image_parts[0]);
                // $image_type = $image_type_aux[1];
                $image_base64 = base64_decode($pan_file);
                $image_name = uniqid().time() . '.png';
                $file = $directoryName . $image_name;
                file_put_contents($file, $image_base64);



                $data   = array(                    
                    'pan_proof' => $image_name,
                    'pan_number' => $pan_number,                                                   
                    'pan_image_url' => base_url("uploads/user_pans/".$image_name),                                                   
                    'updated_at' => $created_on,
                    'pan_base64' => $pan_file             
                );
               
                $where = [['column'=>'user_id','value'=>$user_id]];
                $get = $this->general->get_row($this->tbl_bank,'id',$where);
                if(isset($get['resultSet']['id']) && !empty($get['resultSet']['id']))
                {
                    $bank_id = $get['resultSet']['id'];
                    $where = [['column'=>'id','value'=>$bank_id]];                       
                    $update = $this->general->update($this->tbl_bank,$data,$where);
                    if($update['status'] != true)
                    {
                        $errors['errors'][] = $update;
                    }   
                }
                else
                {
                    $data['user_id'] = $user_id;
                    $data['created_at'] = $created_on;
                    $insert = $this->general->add($this->tbl_bank,$data);
                    if($insert['status'] != true)
                    {
                        $errors['errors'][] = $insert;
                    }   
                }
                
                
            } 
        }



        if(isset($errors) && !empty($errors))
        {
            $errors['status'] = FALSE;
            $errors['message'] = "Pan adding failed";
            $this->set_response($errors, REST_Controller::HTTP_NOT_FOUND); 
        }
        else
        {
            $response = array(
                "status" => TRUE,
                "message" => "Pan details added successfully",               
            ); 
            $this->set_response($response, REST_Controller::HTTP_CREATED);
        }   
    }

    public function ifsc_code_verify_post()
    {
        $params = json_decode(file_get_contents('php://input'), TRUE);

        $ifsc_code = "";

        $created_on  = current_date();


        if(isset($params['ifsc_code']) && !empty($params['ifsc_code']))
        {
            $_POST['ifsc_code'] = $ifsc_code = $params['ifsc_code'];
        }


        $errors = array();
        $this->form_validation->set_rules('ifsc_code','ifsc_code','required|trim');
        if ($this->form_validation->run() == FALSE) 
        {
            $errors['errors'] = $this->form_validation->error_array();                
        }
        else
        {
        
            if(empty($errors))
            {
                $ifsc_url = 'https://ifsc.razorpay.com/'.$ifsc_code;
                    $curl_pass = array(
                        "data"=>'',
                        "url"=>$ifsc_url,
                        "type"=>"GET",
                    );					
                $response = _curl_request($curl_pass);              
            }
            
             
        }

        if(isset($errors) && !empty($errors))
        {
            $errors['status'] = FALSE;
            $errors['message'] = "Failed to get details";
            $this->set_response($errors, REST_Controller::HTTP_NOT_FOUND); 
        }
        else
        {
            $response = array(
                "status" => TRUE,
                "message" => $response,               
            ); 
            $this->set_response($response, REST_Controller::HTTP_CREATED);
        }  
    }


    public function order_post()
    {
        $params = json_decode(file_get_contents('php://input'), TRUE);

        $amount = $currency = $receipt ="";

        $created_on  = current_date();


        if(isset($params['amount']) && !empty($params['amount']))
        {
            $_POST['amount'] = $amount = $params['amount'];
        }
        if(isset($params['currency']) && !empty($params['currency']))
        {
            $_POST['currency'] = $currency = $params['currency'];
        }
        if(isset($params['receipt']) && !empty($params['receipt']))
        {
            $_POST['receipt'] = $receipt = $params['receipt'];
        }


        $errors = array();
        $this->form_validation->set_rules('amount','amount','required|trim|numeric');
        $this->form_validation->set_rules('currency','currency','required|trim');
        $this->form_validation->set_rules('receipt','receipt','required|trim');
        if ($this->form_validation->run() == FALSE) 
        {
            $errors['errors'] = $this->form_validation->error_array();                
        }
        else
        {
        
            if(empty($errors))
            {
                $post_data = $_POST;
                // pr($post_data);
                $ifsc_url = 'https://api.razorpay.com/v1/orders';
                    $curl_pass = array(
                        "data"=>json_encode($post_data),
                        "url"=>$ifsc_url,
                        "type"=>"POST",
                        "header"=>array(
                            'content-type: application/json',
                            'Authorization: Basic cnpwX2xpdmVfNHJCeUtrOVZBVEJFUGE6OGFRdE9Xc1F3VzBkWkRIWWJqY05Ha3Bo'
                          ),
                    );					
                $response = _curl_request($curl_pass);              
            }
            
             
        }

        if(isset($errors) && !empty($errors))
        {
            $errors['status'] = FALSE;
            $errors['message'] = "Failed to get details";
            $this->set_response($errors, REST_Controller::HTTP_NOT_FOUND); 
        }
        else
        {
            $response = array(
                "status" => TRUE,
                "message" => $response,               
            ); 
            $this->set_response($response, REST_Controller::HTTP_CREATED);
        }  
    }


    function add_fund_account_razorpay($data = [])
    {
        if(isset($data) && !empty($data) && is_array($data))
        {
            $ifsc_code = $data["ifsc_code"];
            $account_number = $data["account_number"];
            $user_id = $data["user_id"];
            $bank_id = $data["bank_id"];
            if(!empty($user_id))
            {
                $contact_id = ""; $name = "";
                $where = [["column"=>"user_id", "value"=>$user_id]];
                $get = $this->general->get_row($this->tbl_users,'razorpay_contact_id,full_name',$where);
                if(isset($get['resultSet']['razorpay_contact_id']) && !empty($get['resultSet']['razorpay_contact_id']))
                {
                    $contact_id = $get['resultSet']['razorpay_contact_id'];
                    $name = $get['resultSet']['full_name'];
                }


                $pass_data = [                
                    "contact_id"=>$contact_id,                    
                    "account_type"=>"bank_account",
                    "bank_account"=>[
                        "name"=>$name,
                        "ifsc"=>$ifsc_code,
                        "account_number"=>$account_number,
                        ]
                ];
                $d = json_encode($pass_data);
    
                $url = 'https://api.razorpay.com/v1/fund_accounts';
                    $curl_pass = array(
                        "data"=>$d,
                        "url"=>$url,
                        "type"=>"POST",
                        "header"=> [RAZORPAY,'Content-Type: application/json']
                    );					
                $response = _curl_request($curl_pass);                  
                if(!empty($response))
                {
                    $where = [["column"=>"id", "value"=>$bank_id]];
                    $data = [
                        "razorpay_response"=>json_encode($response),
                        "razorpay_fund_id"=>$response['id']
                    ];
                    $update = $this->general->update($this->tbl_bank,$data,$where);
                }
            }        
            
        }
    }
   
}