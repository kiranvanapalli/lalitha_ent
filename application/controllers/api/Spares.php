<?php
use Restserver\Libraries\REST_Controller;
Header('Access-Control-Allow-Origin: *'); //for allow any domain, insecure
Header('Access-Control-Allow-Headers: *'); //for allow any headers, insecure
Header('Access-Control-Allow-Methods: GET, POST, OPTIONS, PUT, DELETE'); //method allowed
// defined('BASEPATH') OR exit('No direct script access allowed');
// This can be removed if you use __autoload() in config.php OR use Modular Extensions
/** @noinspection PhpIncludeInspection */
require(APPPATH . '/libraries/REST_Controller.php');
/**
 * This is an example of a few basic user interaction methods you could use
 * all done with a hardcoded array
 *
 * @package         CodeIgniter
 * @subpackage      Rest Server
 * @category        Controller
 * @author          Phil Sturgeon, Chris Kacerguis
 * @license         MIT
 * @link            https://github.com/chriskacerguis/codeigniter-restserver
 */
class Spares extends REST_Controller
{
    private $tbl_spares = "tb_spares";
    private $tbl_trans = "tb_transactiontype";
    function __construct()
    {
        // Construct the parent class
        parent::__construct();
        $this->load->model("general_model", "general");
        $this->load->model('User_model');
        $this->load->model('Restapi_model');
    }
    public function convertbase64string($encode_str)
    {
        return base64_encode(base64_encode($encode_str));
    }
    public function encode_base64string($encode_str)
    {
        return base64_encode(base64_encode($encode_str));
    }
    public function spare_post()
    {
        $params = json_decode(file_get_contents('php://input'), TRUE);
        $created_at = $updated_at = current_date();
        $spare_name = $product_count = $product_price = $support_file = $upload_role = "";
        if (isset($params['spare_name']) && !empty($params['spare_name'])) {
            $_POST['spare_name'] = $spare_name = $params['spare_name'];
        }
        if (isset($params['product_count']) && !empty($params['product_count'])) {
            $_POST['product_count'] = $product_count = $params['product_count'];
        }
        if (isset($params['product_price']) && !empty($params['product_price'])) {
            $_POST['product_price'] = $product_price = $params['product_price'];
        }
        if (isset($params['spare_img_name']) && !empty($params['spare_img_name'])) {
            $_POST['spare_img_name'] = $spare_img_name = $params['spare_img_name'];
        }
        if (isset($params['upload_role']) && !empty($params['upload_role'])) {
            $_POST['upload_role'] = $upload_role = $params['upload_role'];
        }
        $errors = array();
        $this->form_validation->set_rules('spare_name', 'Spare Name', 'required');
        $this->form_validation->set_rules('product_count', 'Product Count', 'required|trim|numeric');
        $this->form_validation->set_rules('product_price', 'Product Price', 'required|trim|numeric');
        $this->form_validation->set_rules('spare_img_name', 'Spare Image File', 'required');
        $this->form_validation->set_rules('upload_role', 'Uploaded Role', 'required');
        if ($this->form_validation->run() == FALSE) {
            $errors['errors'] = $this->form_validation->error_array();
        } else {
           
            $response = array();
            if (empty($errors)) {
               
                $data   = array(
                    'spare_name' => $spare_name,
                    'product_count' => $product_count,
                    'product_price' => $product_price,
                    'product_image' => $spare_img_name,
                    'upload_role' => $upload_role,
                    'created_at' => $created_at,
                    'updated_at' => $updated_at,
                    'status' => 1,
                );
                $insert = $this->general->add($this->tbl_spares, $data);
                $spare_id = $this->db->insert_id();
                if ($insert['status'] != true) {
                    $errors['errors'][] = $insert;
                }
            }
        }
        if (isset($errors) && !empty($errors)) {
            $errors['status'] = FALSE;
            $errors['message'] = "Spare Added Failed";
            $this->set_response($errors, REST_Controller::HTTP_NOT_FOUND);
        } else {
            if ($upload_role == 1) {
                $role_name = "Admin";
            } elseif ($upload_role == 2) {
                $role_name = "Technician";
            } elseif ($upload_role == 3) {
                $role_name = "Customer";
            }
            $response = array(
                "status" => TRUE,
                "message" => "Spare added Successfully",
                "data" => array("Spare Name" => $spare_name, "Product Price" => $product_price, "Product Count" => $product_count),
            );
            $this->set_response($response, REST_Controller::HTTP_CREATED);
        }
    }
    public function getpares_get($id = null)
    {
        $where = [];
        $like = [];
        if (isset($id) && !empty($id) && is_numeric($id)) {
            $where[] = ["column" => "d.spare_id", "value" => $id];
        }
        $order_by = [];
        $order_by[] =  ["column" => "d.spare_id", "type" => (isset($_GET['order']) && !empty($_GET['order'])) ? $_GET['order'] : 'DESC'];
        $limit_start = (isset($_GET['limit_start'])) ? $_GET['limit_start'] : 0;
        $limit_end = (isset($_GET['limit_end'])) ? $_GET['limit_end'] : 0;
        $array = [
            "fileds" => "d.*",
            "table" => $this->tbl_spares . ' AS d',
            "join_tables" => [],
            "where" => $where,
            "like" => $like,
            "limit" => ["start" => $limit_start, "end" => $limit_end],
            "order_by" => $order_by,
        ];
        $p = $this->general->get_all_from_join($array);
        if (isset($p['resultSet']) && !empty($p['resultSet'])) {
            $res = [
                'status' => TRUE,
                'message' => '',
                'data' => $p['resultSet']
            ];
        } else {
            $res = [
                'status' => FALSE,
                'message' => 'Data not found',
            ];
        }
        $this->response($res, REST_Controller::HTTP_OK);
    }
    public function editSpares_post()
    {
        $updated_at = current_date();
        $spare_id = $spare_name = $product_count = $product_price = $upload_role = $spare_img_name = "";
      

        if (isset($_POST['spare_id']) && !empty($_POST['spare_id'])) {
             $spare_id = $_POST['spare_id'];
        }
        if (isset($_POST['spare_name']) && !empty($_POST['spare_name'])) {
            $spare_name =  $_POST['spare_name']; 
        }
        if (isset($_POST['product_count']) && !empty($_POST['product_count'])) {
            $product_count =  $_POST['product_count']; 
        }
        // if (isset($_POST['spare_img_name']) && !empty($_POST['spare_img_name'])) {
          
        // }
        if (isset($_POST['product_price']) && !empty($_POST['product_price'])) {
           $product_price =  $_POST['product_price'];
        }
        if (isset($_POST['upload_role']) && !empty($_POST['upload_role'])) {
            $upload_role =  $_POST['upload_role'];
        }
        $errors = array();
        $this->form_validation->set_rules('spare_name', 'Spare Name', 'required');
        $this->form_validation->set_rules('spare_id', 'Spare Id', 'required|numeric');
        $this->form_validation->set_rules('product_count', 'Product Count', 'required|trim|numeric');
        //  $this->form_validation->set_rules('spare_img_name', 'Spare Image File', 'required');
        // $this->form_validation->set_rules('spare_img_name', 'Document', 'trim|xss_clean');
      
        $this->form_validation->set_rules('product_price', 'Product Price', 'required|trim|numeric');
        // $this->form_validation->set_rules('support_file', 'Support File', 'required');
        $this->form_validation->set_rules('upload_role', 'Uploaded Role', 'required');
        if ($this->form_validation->run() == FALSE) {
            $errors['errors'] = $this->form_validation->error_array();
        } else {
            $response = array();
            if (empty($errors)) {
                if (isset($_FILES['spare_img_name']['name']) && !empty($_FILES['spare_img_name']['name'])) {
                    // $spare_img_name =$_FILES['spare_img_name'];
                    $spare_img_name = $_FILES['spare_img_name']['name'];
                    $uploaddir = './uploads/new_spares/';
                    $file = basename(str_replace(" ", "", $_FILES['spare_img_name']['name']));
                    $file_incrypt = md5(uniqid(rand(), true)) . '.' . $file;
                    $uploadfile = $uploaddir . $file_incrypt;

                    if (move_uploaded_file($_FILES['spare_img_name']['tmp_name'], $uploadfile)) {
                        $spare_img_name  = base_url() . 'uploads/new_spares/' . $file_incrypt;
                    }
                  
                }
                else
                {
                    $where = [['column' => 'spare_id', 'value' => $spare_id]];
                    $get = $this->general->get_row($this->tbl_spares, '*', $where);
                    // print_r($get['resultSet']['product_image']);
                    $total = $get['resultSet'];
                    $spare_img_name = $total['product_image'];
                }
                
                $data   = array(
                    'spare_name' => $spare_name,
                    'product_count' => $product_count,
                    'product_price' => $product_price,
                    'product_image' => $spare_img_name,
                    'upload_role' => $upload_role,
                    'updated_at' => $updated_at,
                    'status' => 1,
                );
                $where = [['column' => 'spare_id', 'value' => $spare_id]];
                $update = $this->general->update($this->tbl_spares, $data, $where);
                $spare_id = $this->db->insert_id();
                if ($update['status'] != true) {
                    $errors['errors'][] = $update;
                }
            }
        }
        if (isset($errors) && !empty($errors)) {
            $errors['status'] = FALSE;
            $errors['message'] = "Spare Updated Failed";
            $this->set_response($errors, REST_Controller::HTTP_NOT_FOUND);
        } else {
            if ($upload_role == 1) {
                $role_name = "Admin";
            } elseif ($upload_role == 2) {
                $role_name = "Technician";
            } elseif ($upload_role == 3) {
                $role_name = "Customer";
            }
            $response = array(
                "status" => TRUE,
                "message" => "Spare Updated Successfully",
                "data" => array("Spare Name" => $spare_name, "Product Price" => $product_price, "Product Count" => $product_count),
            );
            $this->set_response($response, REST_Controller::HTTP_OK);
        }
    }
    public function deleteSpare_post()
    {
        $params = json_decode(file_get_contents('php://input'), TRUE);
        $spare_id =  "";
        if (isset($params['spare_id']) && !empty($params['spare_id'])) {
            $_POST['spare_id'] = $spare_id = $params['spare_id'];
        }
        $errors = array();
        $this->form_validation->set_rules('spare_id', 'Spare Id', 'required|numeric');
        if ($this->form_validation->run() == FALSE) {
            $errors['errors'] = $this->form_validation->error_array();
        } else {
            $response = array();

            if($spare_id != 21)
            {
                if (empty($errors)) {
                    $where = [['column' => 'spare_id', 'value' => $spare_id]];
                    $delete = $this->general->delete_row($this->tbl_spares, $where);
                    if ($delete['status'] != true) {
                        $errors['Message'][] = "Record Not Found";
                    }
                }
            }
            else
            {
                $errors['Message'][] = "U Can't Delete This Spare";
            }
            
        }
        if (isset($errors) && !empty($errors)) {
            $errors['status'] = FALSE;
            $errors['message'] = "Spare Deleted Failed";
            $this->set_response($errors, REST_Controller::HTTP_NOT_FOUND);
        } else {
            $response = array(
                "status" => TRUE,
                "message" => "Spare Deleted Successfully",
            );
            $this->set_response($response, REST_Controller::HTTP_CREATED);
        }
    }
    public function gettranstype_get()
    {
        $where = [];
        $like = [];
      
        $order_by = [];
        $order_by[] =  ["column" => "d.trans_type_id", "type" => (isset($_GET['order']) && !empty($_GET['order'])) ? $_GET['order'] : 'ASC'];
        $limit_start = (isset($_GET['limit_start'])) ? $_GET['limit_start'] : 0;
        $limit_end = (isset($_GET['limit_end'])) ? $_GET['limit_end'] : 0;
        $array = [
            "fileds" => "d.*",
            "table" => $this->tbl_trans . ' AS d',
            "join_tables" => [],
            "where" => $where,
            "like" => $like,
            "limit" => ["start" => $limit_start, "end" => $limit_end],
            "order_by" => $order_by,
        ];
        $p = $this->general->get_all_from_join($array);
        if (isset($p['resultSet']) && !empty($p['resultSet'])) {
            $res = [
                'status' => TRUE,
                'message' => '',
                'data' => $p['resultSet']
            ];
        } else {
            $res = [
                'status' => FALSE,
                'message' => 'Data not found',
            ];
        }
        $this->response($res, REST_Controller::HTTP_OK);
    }

    public function GetSparesavailable_get()
    {
        $where = [['column'=>'product_count>','value'=>0]];
        $like = [];
      
        $order_by = [];
        $order_by[] =  ["column" => "d.spare_id", "type" => (isset($_GET['order']) && !empty($_GET['order'])) ? $_GET['order'] : 'ASC'];
        $limit_start = (isset($_GET['limit_start'])) ? $_GET['limit_start'] : 0;
        $limit_end = (isset($_GET['limit_end'])) ? $_GET['limit_end'] : 0;
        $array = [
            "fileds" => "d.*",
            "table" => $this->tbl_spares . ' AS d',
            "join_tables" => [],
            "where" => $where,
            "like" => $like,
            "limit" => ["start" => $limit_start, "end" => $limit_end],
            "order_by" => $order_by,
        ];
        $p = $this->general->get_all_from_join($array);
        if (isset($p['resultSet']) && !empty($p['resultSet'])) {
            $res = [
                'status' => TRUE,
                'message' => '',
                'data' => $p['resultSet']
            ];
        } else {
            $res = [
                'status' => FALSE,
                'message' => 'Data not found',
            ];
        }
        $this->response($res, REST_Controller::HTTP_OK);
       
    }
}
 