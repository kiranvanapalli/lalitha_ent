<?php
use Restserver\Libraries\REST_Controller;

Header('Access-Control-Allow-Origin: *'); //for allow any domain, insecure
Header('Access-Control-Allow-Headers: *'); //for allow any headers, insecure
Header('Access-Control-Allow-Methods: GET, POST, OPTIONS, PUT, DELETE'); //method allowed
// defined('BASEPATH') OR exit('No direct script access allowed');

// This can be removed if you use __autoload() in config.php OR use Modular Extensions
/** @noinspection PhpIncludeInspection */
require(APPPATH.'/libraries/REST_Controller.php');



/**
 * This is an example of a few basic user interaction methods you could use
 * all done with a hardcoded array
 *
 * @package         CodeIgniter
 * @subpackage      Rest Server
 * @category        Controller
 * @author          Phil Sturgeon, Chris Kacerguis
 * @license         MIT
 * @link            https://github.com/chriskacerguis/codeigniter-restserver
 */
class Adminsettings extends REST_Controller {

    private $tbl_settings = "tb_admin_settings";    
    private $tbl_users = "tb_users";    
    private $tbl_deposits = "tb_deposits";    
   
    function __construct() {
        // Construct the parent class
        parent::__construct();
        $this->load->model("general_model","general");
    }	
    

    public function settings_post()
    {

        $params = json_decode(file_get_contents('php://input'), TRUE);

        $slot_time = "";

        $created_on  = current_date();


        if(isset($params['slot_time']) && !empty($params['slot_time']) && is_numeric($params['slot_time']))
        {
            $_POST['slot_time'] = $slot_time = $params['slot_time'];
        }     
        


        $errors = array();
        $this->form_validation->set_rules('slot_time','Slot time','required|trim|numeric');
              
        
        if ($this->form_validation->run() == FALSE) 
        {
            $errors['errors'] = $this->form_validation->error_array();                
        }
        else
        {
            
            if($slot_time < 10)
            {
                $errors['errors'][] = "slot time not less than 10";
            }

            $response = array();  
            if(empty($errors))
            {
                $data   = array(                    
                    'slot_time' => $slot_time,                                
                );

                $where = [['column'=>'fixed_id','value'=>101]];                       
                $update = $this->general->update($this->tbl_settings,$data,$where);
               
                if($update['status'] != true)
                {
                    $errors['errors'][] = $update;
                }               
            } 
        }



        if(isset($errors) && !empty($errors))
        {
            $errors['status'] = FALSE;
            $errors['message'] = "admin settings failed";
            $this->set_response($errors, REST_Controller::HTTP_NOT_FOUND); 
        }
        else
        {
            $response = array(
                "status" => TRUE,
                "message" => "admin settings added successfully",               
            ); 
            $this->set_response($response, REST_Controller::HTTP_CREATED);
        }   
    } 
    
    
    public function settings_get($id=null)
    {
        $where = [];  $like = [];
        if(isset($id) && !empty($id) && is_numeric($id))
        {
            $where[] = ["column" => "d.asset_id","value" => $id];   
        }

        if(isset($_GET['asset_name']) && !empty($_GET['asset_name']))
        {
            $where[] = ["column" => "d.asset_name","value" => $_GET['asset_name']];   
        }

            

        $order_by = [];            
        $order_by[] =  ["column" => "d.asset_id", "type" => (isset($_GET['order']) && !empty($_GET['order']))?$_GET['order']:'DESC'];

        $limit_start = (isset($_GET['limit_start']))?$_GET['limit_start']:0;
        $limit_end = (isset($_GET['limit_end']))?$_GET['limit_end']:0;

        $array = [
            "fileds" => "d.*",
            "table" => $this->tbl_prices.' AS d',
            "join_tables" => [],
            "where" => $where,
            "like" => $like,
            "limit" => ["start" => $limit_start, "end" => $limit_end],
            "order_by" => $order_by,
        ];       
       
       
        $p = $this->general->get_all_from_join($array); 
        if(isset($p['resultSet']) && !empty($p['resultSet']))
        {
            $res = [
                'status'=>TRUE,
                'message'=>'',
                'data'=>$p['resultSet']
            ];
        }
        else
        {
            $res = [
                'status'=>FALSE,
                'message'=>'Data not found',
            ];
        }
        $this->response($res, REST_Controller::HTTP_OK); 
    }  

   
}