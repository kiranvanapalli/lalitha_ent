<?php

use Restserver\Libraries\REST_Controller;

Header('Access-Control-Allow-Origin: *'); //for allow any domain, insecure
Header('Access-Control-Allow-Headers: *'); //for allow any headers, insecure
Header('Access-Control-Allow-Methods: GET, POST, OPTIONS, PUT, DELETE'); //method allowed
// defined('BASEPATH') OR exit('No direct script access allowed');
// This can be removed if you use __autoload() in config.php OR use Modular Extensions
/** @noinspection PhpIncludeInspection */
require(APPPATH . '/libraries/REST_Controller.php');
/**
 * This is an example of a few basic user interaction methods you could use
 * all done with a hardcoded array
 *
 * @package         CodeIgniter
 * @subpackage      Rest Server
 * @category        Controller
 * @author          Phil Sturgeon, Chris Kacerguis
 * @license         MIT
 * @link            https://github.com/chriskacerguis/codeigniter-restserver
 */
class Image extends REST_Controller
{
    private $tbl_spares = "tb_spares";
    private $tb_spare_imgs = "tb_spare_imgs";
    private $tbl_trans = "tb_transactiontype";
    function __construct()
    {
        // Construct the parent class
        parent::__construct();
        $this->load->model("general_model", "general");
        $this->load->model('User_model');
        $this->load->model('Restapi_model');
    }
    public function convertbase64string($encode_str)
    {
        return base64_encode(base64_encode($encode_str));
    }
    public function encode_base64string($encode_str)
    {
        return base64_encode(base64_encode($encode_str));
    }

    public function uploadImage_post()
    {
        $errors = array();

        $this->form_validation->set_rules('billing_image', 'Document', 'trim|xss_clean');
        if ($this->form_validation->run() == FALSE) {
            $errors['form_validation'] = $this->form_validation->error_array();
        } else {
            if (empty($errors)) {

                $spare_img_name = '';
                if (isset($_FILES['spare_img_name']['name']) && !empty($_FILES['spare_img_name']['name'])) {

                    $spare_img_name = $_FILES['spare_img_name']['name'];
                    $uploaddir = './uploads/new_spares/';
                    $file = basename(str_replace(" ", "", $_FILES['spare_img_name']['name']));
                    $file_incrypt = md5(uniqid(rand(), true)) . '.' . $file;
                    $uploadfile = $uploaddir . $file_incrypt;

                    if (move_uploaded_file($_FILES['spare_img_name']['tmp_name'], $uploadfile)) {
                        $spare_img_name  = base_url() . 'uploads/new_spares/' . $file_incrypt;
                    }
                }

                $data  = array(
                    'spare_img_name'  => $spare_img_name,
                    'created_at' => date('Y-m-d H:i:s')
                );
                $spare_images_add = $this->Restapi_model->data_save($this->tb_spare_imgs, $data);

                $insert_id = $this->db->insert_id();
                if ($spare_images_add) {

                    if ($spare_images_add > 0) {

                        $response = array(
                            "status" => "true",
                            'message' => 'Spare Image added successfully!',
                            "data" => array(
                                'Id' => $insert_id,
                                'ImageURL' => $spare_img_name

                            )
                        );

                        $this->set_response($response, REST_Controller::HTTP_CREATED);
                    } else {
                        $response = array(
                            "status" => "false",
                            'message' => 'Spare Image add failed! please try again.'
                        );

                        $this->set_response($response, REST_Controller::HTTP_CREATED);
                    }
                }
            }
        }
    }

    public function UpdateImage_post()
    {
       $id = $_POST['spareimageid'];
       $spare_img_name =$_FILES['spare_img_name'];

        $this->form_validation->set_rules('spare_img_name', 'Document', 'trim|xss_clean');
        $this->form_validation->set_rules('id', 'Spare Image ID', 'trim|xss_clean');
        if ($this->form_validation->run() == FALSE) {
            $errors['form_validation'] = $this->form_validation->error_array();
        }
        else {
            if (empty($errors)) {

               
                if (isset($_FILES['spare_img_name']['name']) && !empty($_FILES['spare_img_name']['name'])) {

                    $spare_img_name = $_FILES['spare_img_name']['name'];
                    $uploaddir = './uploads/new_spares/';
                    $file = basename(str_replace(" ", "", $_FILES['spare_img_name']['name']));
                    $file_incrypt = md5(uniqid(rand(), true)) . '.' . $file;
                    $uploadfile = $uploaddir . $file_incrypt;

                    if (move_uploaded_file($_FILES['spare_img_name']['tmp_name'], $uploadfile)) {
                        $spare_img_name  = base_url() . 'uploads/new_spares/' . $file_incrypt;
                    }
                }
                $where = [['column' => 'spare_id', 'value' => $id]];
                $data  = array(
                    'product_image'  => $spare_img_name,
                    'created_at' => date('Y-m-d H:i:s')
                );
                $spare_images_add = $this->general->update($this->tbl_spares, $data,$where);

                $insert_id = $this->db->insert_id();
                if ($spare_images_add) {

                    if ($spare_images_add > 0) {

                        $response = array(
                            "status" => "true",
                            'message' => 'Spare Image Updated successfully!',
                            "data" => array(
                                'Id' => $id,
                                'ImageURL' => $spare_img_name

                            )
                        );

                        $this->set_response($response, REST_Controller::HTTP_OK);
                    } else {
                        $response = array(
                            "status" => "false",
                            'message' => 'Spare Image add failed! please try again.'
                        );

                        $this->set_response($response, REST_Controller::HTTP_CREATED);
                    }
                }
            }
        }
    }
}
