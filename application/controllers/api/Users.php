<?php
use Restserver\Libraries\REST_Controller;
Header('Access-Control-Allow-Origin: *'); //for allow any domain, insecure
Header('Access-Control-Allow-Headers: *'); //for allow any headers, insecure
Header('Access-Control-Allow-Methods: GET, POST, OPTIONS, PUT, DELETE'); //method allowed
// defined('BASEPATH') OR exit('No direct script access allowed');

// This can be removed if you use __autoload() in config.php OR use Modular Extensions
/** @noinspection PhpIncludeInspection */
require(APPPATH.'/libraries/REST_Controller.php');



/**
 * This is an example of a few basic user interaction methods you could use
 * all done with a hardcoded array
 *
 * @package         CodeIgniter
 * @subpackage      Rest Server
 * @category        Controller
 * @author          Phil Sturgeon, Chris Kacerguis
 * @license         MIT
 * @link            https://github.com/chriskacerguis/codeigniter-restserver
 */
class Users extends REST_Controller {

    private $tbl_users = "tb_users";    
    private $tbl_deposits = "tb_deposits";    
    private $tbl_bank = "tb_pan_bank_details";  
   
    function __construct() {
        // Construct the parent class
        parent::__construct();
        $this->load->model("general_model","general");
    }   
    
    
    public function users_get($id=null)
    {
        $where = [];  $like = [];
        if(isset($id) && !empty($id) && is_numeric($id))
        {
            $where[] = ["column" => "u.user_id","value" => $id];   
        }

        if(!empty($_GET['status']))
        {
            $where[] = ["column" => "u.status","value" => $_GET['status']];   
        }             

        $order_by = [];            
        $order_by[] =  ["column" => "u.user_id", "type" => (isset($_GET['order']) && !empty($_GET['order']))?$_GET['order']:'DESC'];

        $limit_start = (isset($_GET['limit_start']))?$_GET['limit_start']:0;
        $limit_end = (isset($_GET['limit_end']))?$_GET['limit_end']:0;

        $array = [
            "fileds" => "u.*, b.bank_name, b.branch, b.ifsc_code, b.account_number, b.pan_number, b.pan_image_url, b.pan_proof, b.razorpay_response as bank_razorpay_response, b.razorpay_fund_id",
            "table" => $this->tbl_users.' AS u',
            "join_tables" => [["table"=>$this->tbl_bank.' AS b',"join_on"=>'b.user_id = u.user_id',"join_type" => "left"]],
            "where" => $where,
            "like" => $like,
            "limit" => ["start" => $limit_start, "end" => $limit_end],
            "order_by" => $order_by,
        ];       
       
       
        $p = $this->general->get_all_from_join($array); 
        if(isset($p['resultSet']) && !empty($p['resultSet']))
        {
            $res = [
                'status'=>TRUE,
                'message'=>'',
                'data'=>$p['resultSet']
            ];
        }
        else
        {
            $res = [
                'status'=>FALSE,
                'message'=>'Data not found',
            ];
        }
        $this->response($res, REST_Controller::HTTP_OK); 
    }

    public function user_update_post()
    {

        $params = json_decode(file_get_contents('php://input'), TRUE);

        $user_id = $name = $email = $full_name = $phone = $password = $pin = $otp = $id_proof = $id_proof_number = $profile_pic = $status = "";

        $updated_at = current_date();


        if(isset($params['user_id']) && !empty($params['user_id']))
        {
            $_POST['user_id'] = $user_id = $params['user_id'];
        }


       
        if(isset($params['name']) && !empty($params['name']))
        {
            $_POST['name'] = $name = $params['name'];
        }

        if(isset($params['email']) && !empty($params['email']))
        {
            $_POST['email'] = $email = $params['email'];
        }

        // if(isset($first_name,$last_name) && !empty($last_name) && !empty($first_name))
        // {
        //     $_POST['full_name'] = $full_name = $first_name.' '.$last_name;
        // }

        if(isset($params['phone']) && !empty($params['phone']))
        {
            $_POST['phone'] = $phone = $params['phone'];
        }

        if(isset($params['password']) && !empty($params['password']))
        {
            $_POST['password'] = $password = $params['password'];
        }

       
       
        


        $errors = array();
        $this->form_validation->set_rules('user_id','User ID','required|trim|numeric');
        if(isset($_POST['user_id']) && !empty($_POST['user_id']) && is_numeric($_POST['user_id']))
        {
            $user_id = $_POST['user_id'];
            $where = [['column'=>'user_id','value'=>$user_id]];
            $get = $this->general->get_row($this->tbl_users,'email,phone',$where);
            if(isset($get['resultSet']) && !empty($get['resultSet']))
            {
                $d = $get['resultSet'];
                if(isset($email) && !empty($email) && ($d['email'] != $email))
                {
                    $this->form_validation->set_rules('email','email','required|trim|valid_email|is_unique[tb_users.email]', array('is_unique' => 'This %s already exists.'));
                }
                if(isset($phone) && !empty($phone) && ($d['phone'] != $phone))
                {
                    $this->form_validation->set_rules('phone','phone number','required|trim|numeric|is_unique[tb_users.phone]', array('is_unique' => 'This %s already exists.'));
                }
            }
            else
            {
                $errors['errors'][] = "User not exist in the databse";
            }
        }


        if(isset($params['password']) && !empty($params['password']))
        {
            $this->form_validation->set_rules('password','password','required|trim|min_length[6]');
        }
       
       
       
        
        if ($this->form_validation->run() == FALSE) 
        {
            $errors['errors'] = $this->form_validation->error_array();                
        }
        else
        {
            
            $response = array();  
            if(empty($errors))
            {


                $data   = ['updated_at' => $updated_at];

                if(isset($name) && !empty($name))
                {
                    $data['full_name'] = $name;
                }
                if(isset($phone) && !empty($phone))
                {
                    $data['phone'] = $phone;
                }
                if(isset($email) && !empty($email))
                {
                    $data['email'] = $email;
                }
                if(isset($password) && !empty($password))
                {
                    $data['password'] = $this->encode_base64string($password);
                }

                $update = $this->general->update($this->tbl_users,$data,$where);
                if($update['status'] != true)
                {
                    $errors['errors'][] = $update;
                }   
            } 
        }



        if(isset($errors) && !empty($errors))
        {
            $errors['status'] = FALSE;
            $errors['message'] = "User details failed to update";
            $this->set_response($errors, REST_Controller::HTTP_NOT_FOUND); 
        }
        else
        {
            $response = array(
                "status" => TRUE,
                "message" => "User updated Successfully",
                "data" => array("User Name" => $name,"Email" => $email,"Mobile Number" => $phone,"User id" => $user_id),
            ); 
            $this->set_response($response, REST_Controller::HTTP_CREATED);
        }   
    }


    public function encode_base64string($encode_str)
    {
        return base64_encode(base64_encode($encode_str));
    }  
   
}