<?php
use Restserver\Libraries\REST_Controller;
Header('Access-Control-Allow-Origin: *'); //for allow any domain, insecure
Header('Access-Control-Allow-Headers: *'); //for allow any headers, insecure
Header('Access-Control-Allow-Methods: GET, POST, OPTIONS, PUT, DELETE'); //method allowed
// defined('BASEPATH') OR exit('No direct script access allowed');

// This can be removed if you use __autoload() in config.php OR use Modular Extensions
/** @noinspection PhpIncludeInspection */
require(APPPATH.'/libraries/REST_Controller.php');



/**
 * This is an example of a few basic user interaction methods you could use
 * all done with a hardcoded array
 *
 * @package         CodeIgniter
 * @subpackage      Rest Server
 * @category        Controller
 * @author          Phil Sturgeon, Chris Kacerguis
 * @license         MIT
 * @link            https://github.com/chriskacerguis/codeigniter-restserver
 */
class Supports extends REST_Controller {

    private $tbl_bank = "tb_pan_bank_details";    
    private $tbl_users = "tb_users";    
    private $tbl_deposits = "tb_deposits";    
    private $tbl_support = "tb_supports";    
   
    function __construct() {
        // Construct the parent class
        parent::__construct();
        $this->load->model("general_model","general");
    }	
    

    public function support_post()
    {

        $params = json_decode(file_get_contents('php://input'), TRUE);

        $user_id = $text = $file = $issue = $support_file = "";

        $created_on  = current_date();


        if(isset($params['user_id']) && !empty($params['user_id']))
        {
            $_POST['user_id'] = $user_id = $params['user_id'];
        }

        if(isset($params['text']) && !empty($params['text']))
        {
            $_POST['text'] = $text = $params['text'];
        }

        if(isset($params['support_file']) && !empty($params['support_file']))
        {
            $_POST['support_file'] = $support_file = $params['support_file'];
        }       

        if(isset($params['issue']) && !empty($params['issue']))
        {
            $_POST['issue'] = $issue = $params['issue'];
        }      
        


        $errors = array();
        $this->form_validation->set_rules('user_id','user_id','required|trim|numeric');
        $this->form_validation->set_rules('text','text','required|trim');
        $this->form_validation->set_rules('issue','Issue','required|trim|in_list[1,2,3,4,5]');       
       
        
        if ($this->form_validation->run() == FALSE) 
        {
            $errors['errors'] = $this->form_validation->error_array();                
        }
        else
        {
            
            // if($deposite_amount < 100)
            // {
            //     $errors['errors'][] = "Deposit amount must be greater than 100";
            // }

            $a = [
                1 => "Money loading issue",
                2 => "Money withdrawal issue",
                3 =>"Bank details issue",
                4 =>"Pan card issue",
                5 => "Other issue "
            ];


            if(isset($support_file) && !empty($support_file))
            {
                $file_data = base64_decode($support_file);
                $f = finfo_open();
                $mime_type = finfo_buffer($f, $file_data, FILEINFO_MIME_TYPE);
                $file_type = explode('/', $mime_type)[0];
                $extension = explode('/', $mime_type)[1];
    
                // echo "mime_type=>".$mime_type.PHP_EOL; // will output mimetype, f.ex. image/jpeg
                // echo "file_type=>".$file_type.PHP_EOL; // will output file type, f.ex. image
                // echo "extension=>".$extension.PHP_EOL; // will output extension, f.ex. jpeg
                // exit;
    
                 $acceptable_mimetypes = [
                        'image/png',
                        'image/jpeg',
                    ];
    
                // you can write any validator below, you can check a full mime type or just an extension or file type
                if (!in_array($mime_type, $acceptable_mimetypes))
                {
                    $errors['errors'][] = "File is not an image, Only png,jpeg acceptable"; 
                }
    
                // or example of checking just a type
                if ($file_type !== 'image')
                {
                    $errors['errors'][] = "File is not an image";                                   
                }
            }

            $response = array();  
            if(empty($errors))
            {
                $image_name = $image_url = "";
                if(isset($support_file) && !empty($support_file))
                {
                    $directoryName =  'uploads/user_support/';
                    if(!is_dir($directoryName)){
                        //Directory does not exist, so lets create it.
                        mkdir($directoryName, 0755, true);
                    }

                    $image_base64 = base64_decode($support_file);
                    $image_name = uniqid().time() . '.png';
                    $file = $directoryName . $image_name;
                    file_put_contents($file, $image_base64);

                    $image_url = base_url("uploads/user_support/".$image_name);
                }
                

                $data   = array(                    
                    'user_id' => $user_id,
                    'user_text' => $text,
                    'image_name' => $image_name,
                    'image_url' => $image_url,
                    'issue_no' => $issue,                                
                    'issue_text' => $a[$issue],                                
                    'created_at' => $created_on,             
                    'image_base64' => $support_file            
                );
               
                $insert = $this->general->add($this->tbl_support,$data);
                if($insert['status'] != true)
                {
                    $errors['errors'][] = $insert;
                }             
                
            } 
        }



        if(isset($errors) && !empty($errors))
        {
            $errors['status'] = FALSE;
            $errors['message'] = "support adding failed";
            $this->set_response($errors, REST_Controller::HTTP_NOT_FOUND); 
        }
        else
        {
            $response = array(
                "status" => TRUE,
                "message" => "support ticket added successfully",               
            ); 
            $this->set_response($response, REST_Controller::HTTP_CREATED);
        }   
    } 
    
    
    public function supports_get($id=null)
    {
        $where = [];  $like = [];
        if(isset($id) && !empty($id) && is_numeric($id))
        {
            $where[] = ["column" => "d.support_id","value" => $id];   
        }

        if(isset($_GET['user_id']) && !empty($_GET['user_id']))
        {
            $where[] = ["column" => "d.user_id","value" => $_GET['user_id']];   
        }

            

        $order_by = [];            
        $order_by[] =  ["column" => "d.support_id", "type" => (isset($_GET['order']) && !empty($_GET['order']))?$_GET['order']:'DESC'];

        $limit_start = (isset($_GET['limit_start']))?$_GET['limit_start']:0;
        $limit_end = (isset($_GET['limit_end']))?$_GET['limit_end']:0;

        $array = [
            "fileds" => "d.*, u.full_name, u.email, u.phone",
            "table" => $this->tbl_support.' AS d',
            "join_tables" => [
                ["table"=>$this->tbl_users.' AS u',"join_on"=>'u.user_id = d.user_id',"join_type" => "left"]],
            "where" => $where,
            "like" => $like,
            "limit" => ["start" => $limit_start, "end" => $limit_end],
            "order_by" => $order_by,
        ];       
       
       
        $p = $this->general->get_all_from_join($array); 
        if(isset($p['resultSet']) && !empty($p['resultSet']))
        {
            $res = [
                'status'=>TRUE,
                'message'=>'',
                'data'=>$p['resultSet']
            ];
        }
        else
        {
            $res = [
                'status'=>FALSE,
                'message'=>'Data not found',
            ];
        }
        $this->response($res, REST_Controller::HTTP_OK); 
    }



    public function admin_support_post()
    {

        $params = json_decode(file_get_contents('php://input'), TRUE);

        $support_id = $text =  "";

        $created_on  = current_date();


        if(isset($params['support_id']) && !empty($params['support_id']))
        {
            $_POST['support_id'] = $support_id = $params['support_id'];
        }

        if(isset($params['text']) && !empty($params['text']))
        {
            $_POST['text'] = $text = $params['text'];
        }       


        $errors = array();
        $this->form_validation->set_rules('support_id','support_id','required|trim|numeric');
        $this->form_validation->set_rules('text','text','required|trim');             
       
        
        if ($this->form_validation->run() == FALSE) 
        {
            $errors['errors'] = $this->form_validation->error_array();                
        }
        else
        {
           

            $response = array();  
            if(empty($errors))
            {
                $data   = array(                    
                    'admin_response' => $text,                                                    
                    'updated_at' => $created_on,             
                );
                $where = [['column'=>'support_id','value'=>$support_id]];                       
                $update = $this->general->update($this->tbl_support,$data,$where);               
                if($update['status'] != true)
                {
                    $errors['errors'][] = $update;
                }            
                
            } 
        }



        if(isset($errors) && !empty($errors))
        {
            $errors['status'] = FALSE;
            $errors['message'] = "support response failed";
            $this->set_response($errors, REST_Controller::HTTP_NOT_FOUND); 
        }
        else
        {
            $response = array(
                "status" => TRUE,
                "message" => "support ticket updated successfully",               
            ); 
            $this->set_response($response, REST_Controller::HTTP_CREATED);
        }   
    } 

    function supports_issues()
    {
        $a = [
                1 => "Money loading issue",
                2 => "Money withdrawal issue",
                3 =>"Bank details issue",
                4 =>"Pan card issue",
                5 => "Other issue "
        ];
        return $a;
    }
    
   
}