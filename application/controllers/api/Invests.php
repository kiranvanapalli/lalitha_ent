<?php
use Restserver\Libraries\REST_Controller;
Header('Access-Control-Allow-Origin: *'); //for allow any domain, insecure
Header('Access-Control-Allow-Headers: *'); //for allow any headers, insecure
Header('Access-Control-Allow-Methods: GET, POST, OPTIONS, PUT, DELETE'); //method allowed
// defined('BASEPATH') OR exit('No direct script access allowed');

// This can be removed if you use __autoload() in config.php OR use Modular Extensions
/** @noinspection PhpIncludeInspection */
require(APPPATH.'/libraries/REST_Controller.php');



/**
 * This is an example of a few basic user interaction methods you could use
 * all done with a hardcoded array
 *
 * @package         CodeIgniter
 * @subpackage      Rest Server
 * @category        Controller
 * @author          Phil Sturgeon, Chris Kacerguis
 * @license         MIT
 * @link            https://github.com/chriskacerguis/codeigniter-restserver
 */
class Invests extends REST_Controller {

    private $tbl_users = "tb_users";    
    private $tbl_deposits = "tb_deposits";    
    private $tbl_user_invests = "tb_user_invests";    
    private $tbl_section_slots = "tb_section_slots";    
    private $tbl_slot_transtions = "tb_slot_transtions"; 
    private $tbl_settings = "tb_admin_settings";     
   
    function __construct() {
        // Construct the parent class
        parent::__construct();
        $this->load->model("general_model","general");
    }	
    

    public function invest_post()
    {

        $params = json_decode(file_get_contents('php://input'), TRUE);

        $user_id = $asset_id = $invest_amount = $invest_type =  "";

        $created_on  = current_date();


        if(isset($params['user_id']) && !empty($params['user_id']))
        {
            $_POST['user_id'] = $user_id = $params['user_id'];
        }

        if(isset($params['asset_id']) && !empty($params['asset_id']))
        {
            $_POST['asset_id'] = $asset_id = $params['asset_id'];
        }

        if(isset($params['invest_amount']) && !empty($params['invest_amount']))
        {
            $_POST['invest_amount'] = $invest_amount = $params['invest_amount'];
        }       

        if(isset($params['invest_type']) && !empty($params['invest_type']))
        {
            $_POST['invest_type'] = $invest_type = $params['invest_type'];
        }  

        if(isset($params['account_type']) && !empty($params['account_type']))
        {
            $_POST['account_type'] = $account_type = $params['account_type'];
        }      
       
        


        $errors = array(); $error_message = "Invest failed";
        $this->form_validation->set_rules('user_id','User Id','required|trim|numeric');
        $this->form_validation->set_rules('asset_id','Asset Id','required|trim|numeric');
        $this->form_validation->set_rules('invest_amount','Invest Amount','required|trim|numeric');
        $this->form_validation->set_rules('invest_type','Invest Type','required|trim|alpha|in_list[down,up]');
        $this->form_validation->set_rules('account_type','Account Type','required|trim|alpha|in_list[real,demo]');
        
        if ($this->form_validation->run() == FALSE) 
        {
            $errors['errors'] = $this->form_validation->error_array();                
        }
        else
        {

            $curr_hour = date('H');
            $check_time = ($curr_hour >= 8 && $curr_hour <= 22)?true:false;
            if(!$check_time)
            {
                $errors['errors'][] = "You can only bet between 8 AM to 11 PM ";
                $error_message = "You can only bet between 8 AM to 11 PM";                
            }
            
            

            if($account_type == "real")
            {
                $check_1 = $this->check_invest($user_id,$invest_amount);
                if($check_1 != true)
                {
                        $errors['errors'][] = "Please deposit amount. Your amount not sufficent to invest";
                        $error_message = "Please deposit amount. Your amount not sufficent to invest";
                }
            }
            else if($account_type == "demo")
            {
                $check_1 = $this->demo_check_invest($user_id,$invest_amount);
                if($check_1 != true)
                {
                        $errors['errors'][] = "Please deposit amount. Your demo amount not sufficent to invest";
                        $error_message = "Please deposit amount. Your demo amount not sufficent to invest";
                }
            }

           
            
            $response = array();  
            if(empty($errors))
            {
                $data   = array(
                    'user_id' => $user_id,
                    'asset_id' => $asset_id,
                    'invest_amount' => $invest_amount,
                    'invest_type' => $invest_type,
                    'account_type' => $account_type,
                    'created_on' => $created_on,
                    'received_data' => json_encode($_POST),             
                );
                $insert = $this->general->add($this->tbl_user_invests,$data);
                if($insert['status'] == true)
                {
                    
                    $slot = $this->add_slot($insert['insert_id']);
                    if($account_type == "real")
                    {
                        $this->update_invest($insert['insert_id'],$user_id);
                        $this->update_user_wallet($user_id,$invest_amount);
                        
                    }
                    else if($account_type == "demo")
                    {
                        $this->demo_update_invest($insert['insert_id'],$user_id);
                        $this->demo_update_user_wallet($user_id,$invest_amount);                        
                    }                  
                }
                else if($insert['status'] != true)
                {
                    $errors['errors'][] = $insert;
                }   
            } 
        }



        if(isset($errors) && !empty($errors))
        {
            $errors['status'] = FALSE;
            $errors['message'] = $error_message;
            $this->set_response($errors, REST_Controller::HTTP_NOT_FOUND); 
        }
        else
        {
            $response = array(
                "status" => TRUE,
                "message" => "Invested amount successfully",
                "invest_id" => $insert['insert_id'],
                "slot_id" => $slot['slot_id']
            ); 
            $this->set_response($response, REST_Controller::HTTP_CREATED);
        }   
    }
    
    
    function update_invest($invest_id=null,$user_id=null)
    {
        if(isset($invest_id,$user_id) && !empty($invest_id) && !empty($user_id))
        {
            $where = [['column'=>'user_id','value'=>$user_id]];
            $get = $this->general->get_row($this->tbl_users,'wallet_amount',$where);
            if(isset($get['resultSet']) && !empty($get['resultSet']))
            {
                $wallet_amount = $get['resultSet']['wallet_amount'];
                $where = [['column'=>'invest_id','value'=>$invest_id]];
                $data = ["user_amount_before_bet"=>$wallet_amount];
                $update = $this->general->update($this->tbl_user_invests,$data,$where);
            }
        }
    }

    function demo_update_invest($invest_id=null,$user_id=null)
    {
        if(isset($invest_id,$user_id) && !empty($invest_id) && !empty($user_id))
        {
            $where = [['column'=>'user_id','value'=>$user_id]];
            $get = $this->general->get_row($this->tbl_users,'demo_wallet_amount',$where);
            if(isset($get['resultSet']) && !empty($get['resultSet']))
            {
                $wallet_amount = $get['resultSet']['demo_wallet_amount'];
                $where = [['column'=>'invest_id','value'=>$invest_id]];
                $data = ["user_amount_before_bet"=>$wallet_amount];
                $update = $this->general->update($this->tbl_user_invests,$data,$where);
            }
        }
    }
    
    
    public function invests_get($id=null)
    {
        $where = [];  $like = [];
        if(isset($id) && !empty($id) && is_numeric($id))
        {
            $where[] = ["column" => "i.invest_id","value" => $id];   
        }

        if(isset($_GET['user_id']) && !empty($_GET['user_id']))
        {
            $where[] = ["column" => "i.user_id","value" => $_GET['user_id']];   
        }

        if(isset($_GET['invest_type']) && !empty($_GET['invest_type']))
        {
            $where[] = ["column" => "i.invest_type","value" => $_GET['invest_type']];   
        }

        if(isset($_GET['account_type']) && !empty($_GET['account_type']))
        {
            $where[] = ["column" => "i.account_type","value" => $_GET['account_type']];   
        }   

        if(isset($_GET['is_win']) && !empty($_GET['is_win']))
        {
            $where[] = ["column" => "i.is_win","value" => $_GET['is_win']];   
        }        

        $order_by = [];            
        $order_by[] =  ["column" => "i.invest_id", "type" => (isset($_GET['order']) && !empty($_GET['order']))?$_GET['order']:'DESC'];

        $limit_start = (isset($_GET['limit_start']))?$_GET['limit_start']:0;
        $limit_end = (isset($_GET['limit_end']))?$_GET['limit_end']:0;

        $array = [
            "fileds" => "i.invest_id, i.user_id, i.invest_amount, i.invest_type, i.account_type, i.created_on as invested_on, i.user_amount_before_bet, i.user_amount_after_bet, i.is_win, u.full_name, u.phone, u.email,tss.slot_start_date_and_time,tss.slot_end_date_and_time,i.user_profit",
            "table" => $this->tbl_user_invests.' AS i',
            "join_tables" =>[
                ["table"=>$this->tbl_users.' AS u',"join_on"=>'u.user_id = i.user_id',"join_type" => "left"],["table"=>$this->tbl_slot_transtions.' AS st',"join_on"=>'i.invest_id = st.invest_id',"join_type" => "left"],["table"=>$this->tbl_section_slots.' AS tss',"join_on"=>'st.slot_id = tss.slot_id',"join_type" => "left"]],
            "where" => $where,
            "like" => $like,
            "limit" => ["start" => $limit_start, "end" => $limit_end],
            "order_by" => $order_by,
        ];       
       
       
        $p = $this->general->get_all_from_join($array); 
        if(isset($p['resultSet']) && !empty($p['resultSet']))
        {
            $res = [
                'status'=>TRUE,
                'message'=>'',
                'data'=>$p['resultSet']
            ];
        }
        else
        {
            $res = [
                'status'=>FALSE,
                'message'=>'Data not found',
            ];
        }
        $this->response($res, REST_Controller::HTTP_OK); 
    }


    function check_invest($user_id=null,$invest_amount=null)
    {
        if(isset($user_id,$invest_amount) && !empty($user_id) && is_numeric($user_id) && !empty($invest_amount) && is_numeric($invest_amount))
        {
            $where = [['column'=>'user_id','value'=>$user_id]];
            $get = $this->general->get_row($this->tbl_users,'wallet_amount',$where);
            if(isset($get['resultSet']) && !empty($get['resultSet']))
            {
                $wallet_amount = $get['resultSet']['wallet_amount'];                
                if($invest_amount <= $wallet_amount)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
        }
    }


    function demo_check_invest($user_id=null,$invest_amount=null)
    {
        if(isset($user_id,$invest_amount) && !empty($user_id) && is_numeric($user_id) && !empty($invest_amount) && is_numeric($invest_amount))
        {
            $where = [['column'=>'user_id','value'=>$user_id]];
            $get = $this->general->get_row($this->tbl_users,'demo_wallet_amount',$where);
            if(isset($get['resultSet']) && !empty($get['resultSet']))
            {
                $wallet_amount = $get['resultSet']['demo_wallet_amount'];                
                if($invest_amount <= $wallet_amount)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
        }
    }


    function update_user_wallet($user_id=null,$invest_amount=null)
    {
        if(isset($user_id,$invest_amount) && !empty($user_id) && is_numeric($user_id) && !empty($invest_amount) && is_numeric($invest_amount))
        {
            $where = [['column'=>'user_id','value'=>$user_id]];
            $get = $this->general->get_row($this->tbl_users,'wallet_amount',$where);
            if(isset($get['resultSet']) && !empty($get['resultSet']))
            {
                $wallet_amount = $get['resultSet']['wallet_amount'];
                $total_amount = $wallet_amount-$invest_amount;
                $where = [['column'=>'user_id','value'=>$user_id]];
                $data = ["wallet_amount"=>$total_amount];
                $update = $this->general->update($this->tbl_users,$data,$where);
                if($update['status'])
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
        }
    }


    function demo_update_user_wallet($user_id=null,$invest_amount=null)
    {
        if(isset($user_id,$invest_amount) && !empty($user_id) && is_numeric($user_id) && !empty($invest_amount) && is_numeric($invest_amount))
        {
            $where = [['column'=>'user_id','value'=>$user_id]];
            $get = $this->general->get_row($this->tbl_users,'demo_wallet_amount',$where);
            if(isset($get['resultSet']) && !empty($get['resultSet']))
            {
                $wallet_amount = $get['resultSet']['demo_wallet_amount'];
                $total_amount = $wallet_amount-$invest_amount;
                $where = [['column'=>'user_id','value'=>$user_id]];
                $data = ["demo_wallet_amount"=>$total_amount];
                $update = $this->general->update($this->tbl_users,$data,$where);
                if($update['status'])
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
        }
    }


    function add_slot($invest_id = null)
    {
        $response = [
            "status" => false,
            "error" => "failed at get requierd parameters at add_slot()",                   
        ];
        if(isset($invest_id) && !empty($invest_id) && is_numeric($invest_id))
        {
            $seconds = $this->get_slot_time();
            $startTime = date('Y-m-d H:i');
            $rightTime =  date('Y-m-d H:i:s');            
            $endTime = date("Y-m-d H:i", (strtotime($startTime) + $seconds));             
            $endTime_2 = date("Y-m-d H:i:s", (strtotime($startTime) + $seconds));


            $slot_date = date('Y-m-d');
            $slot_start_time = $startTime;
            $slot_end_time = $endTime;           
            $slot_start_time_seconds =  strtotime($startTime);
            $slot_end_time_seconds =  strtotime($endTime);
            $slot_start_date_and_time =  current_date();
            $slot_end_date_and_time =  $endTime_2;

            $l = "'".$rightTime."'"; 
            $account_type = "'".$_POST['account_type']."'";
            $slot_type = $_POST['account_type'];
            $sql = "SELECT * FROM tb_section_slots where slot_type = ".$account_type." and slot_start_date_and_time <= ".$l." and slot_end_date_and_time >= ".$l." and slot_status = '0'";
            $res = $this->general->custom_query($sql,'row');
            if(isset($slot_type) && !empty($slot_type) && isset($res['resultSet']['slot_type']) && !empty($res['resultSet']) && ($res['resultSet']['slot_type'] == $slot_type))
            {
                $slot_id = $res['resultSet']['slot_id'];
            }
            else
            {
                $data   = array(
                    'slot_date' => $slot_date,
                    'slot_start_time' => $slot_start_time,
                    'slot_end_time' => $slot_end_time,                    
                    'slot_start_time_seconds' => $slot_start_time_seconds,
                    'slot_end_time_seconds' => $slot_end_time_seconds,
                    'slot_start_date_and_time' => $slot_start_date_and_time,
                    'slot_end_date_and_time' => $slot_end_date_and_time,                        
                    'slot_type' => $slot_type,  
                    'received_data' =>  json_encode($_POST)                      
                );
                $insert = $this->general->add($this->tbl_section_slots,$data);
                if($insert['status'] == true)
                {
                    $slot_id =  $insert['insert_id'];
                }               
            }
            if(isset($slot_id,$invest_id) && !empty($slot_id) && !empty($invest_id))
            {
                $data   = array(
                    'slot_id' => $slot_id,
                    'invest_id' => $invest_id,
                );
                $insert = $this->general->add($this->tbl_slot_transtions,$data);

                $response = [
                    "status" => true,
                    "slot_id" => $slot_id,
                    "slot_trn_id" => $insert['insert_id'],
                ];
            }
            else
            {
                $response = [
                    "status" => false,
                    "error" => "something went wrong at logic",                   
                ];
            }           
        }
        return $response;       
    }



    function get_slot_time()
    {
        $seconds = 150;// by default
        $where = [["column"=>"fixed_id","value"=>101]];
        $get = $this->general->get_row($this->tbl_settings,'slot_time',$where);
        if(isset($get['resultSet']['slot_time']) && !empty($get['resultSet']['slot_time']))
        {
            $seconds = $get['resultSet']['slot_time'];
        }
        return $seconds;
    }
   
}