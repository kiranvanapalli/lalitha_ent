<?php
use Restserver\Libraries\REST_Controller;

Header('Access-Control-Allow-Origin: *'); //for allow any domain, insecure
Header('Access-Control-Allow-Headers: *'); //for allow any headers, insecure
Header('Access-Control-Allow-Methods: GET, POST, OPTIONS, PUT, DELETE'); //method allowed
// defined('BASEPATH') OR exit('No direct script access allowed');

// This can be removed if you use __autoload() in config.php OR use Modular Extensions
/** @noinspection PhpIncludeInspection */
require(APPPATH.'/libraries/REST_Controller.php');



/**
 * This is an example of a few basic user interaction methods you could use
 * all done with a hardcoded array
 *
 * @package         CodeIgniter
 * @subpackage      Rest Server
 * @category        Controller
 * @author          Phil Sturgeon, Chris Kacerguis
 * @license         MIT
 * @link            https://github.com/chriskacerguis/codeigniter-restserver
 */
class Prices extends REST_Controller {

    private $tbl_prices = "tb_assets";    
    private $tbl_users = "tb_users";    
    private $tbl_deposits = "tb_deposits";    
   
    function __construct() {
        // Construct the parent class
        parent::__construct();
        $this->load->model("general_model","general");
    }	
    

    public function price_post()
    {

        $params = json_decode(file_get_contents('php://input'), TRUE);

        $asset_name = $today_value = $incr_decr =  $prasent_value = $status = "";

        $created_on  = current_date();


        if(isset($params['asset_name']) && !empty($params['asset_name']))
        {
            $_POST['asset_name'] = $asset_name = $params['asset_name'];
        }

        if(isset($params['today_value']) && !empty($params['today_value']))
        {
            $_POST['today_value'] = $today_value = $params['today_value'];
        }

        if(isset($params['incr_decr']) && !empty($params['incr_decr']))
        {
            $_POST['incr_decr'] = $incr_decr = $params['incr_decr'];
        }       

        if(isset($params['prasent_value']) && !empty($params['prasent_value']))
        {
            $_POST['prasent_value'] = $prasent_value = $params['prasent_value'];
        }  

        if(isset($params['status']))
        {
            $_POST['status'] = $status = $params['status'];
        }    
        


        $errors = array();
        $this->form_validation->set_rules('asset_name','Asset name','required|trim');
        $this->form_validation->set_rules('today_value','Today value','required|trim');
        $this->form_validation->set_rules('incr_decr','Incr decr','required|trim');
        $this->form_validation->set_rules('prasent_value','Prasent value','required|trim');
        $this->form_validation->set_rules('status','status','required|trim');        
        
        if ($this->form_validation->run() == FALSE) 
        {
            $errors['errors'] = $this->form_validation->error_array();                
        }
        else
        {
            
            // if($deposite_amount < 100)
            // {
            //     $errors['errors'][] = "Deposit amount must be greater than 100";
            // }

            $response = array();  
            if(empty($errors))
            {
                $data   = array(                    
                    'asset_name' => $asset_name,
                    'today_value' => $today_value,
                    'incr_decr' => $incr_decr,
                    'prasent_value' => $prasent_value,
                    'status' => $status,                                
                    'created_at' => $created_on,             
                    'updated_at' => $created_on,             
                );
               
                $insert = $this->general->add($this->tbl_prices,$data);
                if($insert['status'] != true)
                {
                    $errors['errors'][] = $insert;
                }   
                
                
            } 
        }



        if(isset($errors) && !empty($errors))
        {
            $errors['status'] = FALSE;
            $errors['message'] = "Pricing adding failed";
            $this->set_response($errors, REST_Controller::HTTP_NOT_FOUND); 
        }
        else
        {
            $response = array(
                "status" => TRUE,
                "message" => "Price details added successfully",               
            ); 
            $this->set_response($response, REST_Controller::HTTP_CREATED);
        }   
    } 
    
    
    public function prices_get($id=null)
    {
        $where = [];  $like = [];
        if(isset($id) && !empty($id) && is_numeric($id))
        {
            $where[] = ["column" => "d.asset_id","value" => $id];   
        }

        if(isset($_GET['asset_name']) && !empty($_GET['asset_name']))
        {
            $where[] = ["column" => "d.asset_name","value" => $_GET['asset_name']];   
        }

            

        $order_by = [];            
        $order_by[] =  ["column" => "d.asset_id", "type" => (isset($_GET['order']) && !empty($_GET['order']))?$_GET['order']:'DESC'];

        $limit_start = (isset($_GET['limit_start']))?$_GET['limit_start']:0;
        $limit_end = (isset($_GET['limit_end']))?$_GET['limit_end']:0;

        $array = [
            "fileds" => "d.*",
            "table" => $this->tbl_prices.' AS d',
            "join_tables" => [],
            "where" => $where,
            "like" => $like,
            "limit" => ["start" => $limit_start, "end" => $limit_end],
            "order_by" => $order_by,
        ];       
       
       
        $p = $this->general->get_all_from_join($array); 
        if(isset($p['resultSet']) && !empty($p['resultSet']))
        {
            $res = [
                'status'=>TRUE,
                'message'=>'',
                'data'=>$p['resultSet']
            ];
        }
        else
        {
            $res = [
                'status'=>FALSE,
                'message'=>'Data not found',
            ];
        }
        $this->response($res, REST_Controller::HTTP_OK); 
    }  

   
}