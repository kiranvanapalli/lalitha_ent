<?php

use Restserver\Libraries\REST_Controller;

Header('Access-Control-Allow-Origin: *'); //for allow any domain, insecure
Header('Access-Control-Allow-Headers: *'); //for allow any headers, insecure
Header('Access-Control-Allow-Methods: GET, POST, OPTIONS, PUT, DELETE'); //method allowed
// defined('BASEPATH') OR exit('No direct script access allowed');
// This can be removed if you use __autoload() in config.php OR use Modular Extensions
/** @noinspection PhpIncludeInspection */
require(APPPATH . '/libraries/REST_Controller.php');
/**
 * This is an example of a few basic user interaction methods you could use
 * all done with a hardcoded array
 *
 * @package         CodeIgniter
 * @subpackage      Rest Server
 * @category        Controller
 * @author          Phil Sturgeon, Chris Kacerguis
 * @license         MIT
 * @link            https://github.com/chriskacerguis/codeigniter-restserver
 */


class Orders extends REST_Controller
{
    private $tbl_spares = "tb_spares";
    private $tbl_users = "tb_users";
    private $tbl_trans = "tb_transactiontype";
    private $tbl_spares_order = "tb_spares_order";
    private $tbl_orders = "tb_orders";
    function __construct()
    {
        // Construct the parent class
        parent::__construct();
        $this->load->model("general_model", "general");
        $this->load->model('User_model');
        $this->load->model('Restapi_model');
    }

    public function CreateOrder_post()
    {
        $params = json_decode(file_get_contents('php://input'), TRUE);
        $created_at = $updated_at = current_date();
        $trans_type_id = $user_id  = "";
        $spares  = [];

        if (isset($params['trans_type_id']) && !empty($params['trans_type_id'])) {
            $_POST['trans_type_id'] = $trans_type_id = $params['trans_type_id'];
        }
        if (isset($params['user_id']) && !empty($params['user_id'])) {
            $_POST['user_id'] = $user_id = $params['user_id'];
        }
        if (isset($params['spares']) && !empty($params['spares'])) {
            $_POST['spares'] = $spares[] = $params['spares'];
        }
        $errors = array();
        $this->form_validation->set_rules('trans_type_id', 'Transcation Type', 'required|trim|numeric');
        $this->form_validation->set_rules('user_id', 'User Id', 'required|trim|numeric');
        $this->form_validation->set_rules('spares[]', 'Spares', 'required');
        if ($this->form_validation->run() == FALSE) {
            $errors['errors'] = $this->form_validation->error_array();
        } else {
            $response = array();

            $randnumber = mt_rand(1111, 9999);
            $order_code = "LT" . $randnumber;




            $total_amount = '';
            $data = array(
                "invoice_id" =>  $order_code,
                "user_id" => $user_id,
                "trans_type_id" => $trans_type_id,
                "total_amount" => $total_amount,
                "status" => 1,
                "created_at" => $created_at
            );
            $insert = $this->general->add($this->tbl_orders, $data);
            $insert_id = $this->db->insert_id();
            foreach ($spares as $spare_data) {


                for ($i = 0; $i < count($spare_data); $i++) {

                    $this->getSpareID($spare_data[$i], $insert_id, $user_id);
                }
            }


            $where = [['column' => 'order_id', 'value' => $insert_id]];
            $get = $this->general->get_row($this->tbl_orders, '*', $where);



            if (isset($get['resultSet']) && !empty($get['resultSet'])) {

                $query = "SELECT SUM(amount) as total FROM tb_spares_order WHERE order_id = " . $insert_id . " GROUP BY `order_id`";
                $res = $this->general->custom_query($query, 'row');
                if ($res) {
                    $totla_amount = $res['resultSet']['total'];
                }


                $data = array(
                    'total_amount' => $totla_amount
                );
                $update = $this->general->update($this->tbl_orders, $data, $where);

                if ($update) {
                    $this->GeneratePDF($insert_id);
                }
                $Order_data = $this->general->get_row($this->tbl_orders, '*', $where);
            }
            if ($insert['status'] != true) {
                $errors['errors'][] = $insert;
            }
        }
        if (isset($errors) && !empty($errors)) {
            $errors['status'] = FALSE;
            $errors['message'] = "Spare Added Failed";
            $this->set_response($errors, REST_Controller::HTTP_NOT_FOUND);
        } else {
            $response = array(
                "status" => TRUE,
                "message" => "Order Sucessfully Placed",
                "Data" => $Order_data['resultSet']
            );
            $this->set_response($response, REST_Controller::HTTP_CREATED);
        }
    }

    function getSpareID($id = null, $insert_id =  null, $user_id =  null)
    {

        if (isset($id) && !empty($id)) {
            $where = [['column' => 'spare_id', 'value' => $id]];
            $get = $this->general->get_row($this->tbl_spares, '*', $where);
            if (isset($get['resultSet']) && !empty($get['resultSet'])) {
                $spareData = $get['resultSet'];
            }

            if ($spareData['product_count'] > 0) {
                $data = array(
                    "order_id" => $insert_id,
                    "user_id" => $user_id,
                    "spare_id" => $spareData['spare_id'],
                    "qty" => 1,
                    "amount" =>  $spareData['product_price'],
                    "created_at" => current_date()
                );
                $insert = $this->general->add($this->tbl_spares_order, $data);

                if ($insert['status'] == true) {
                    $where_data = [['column' => 'spare_id', 'value' => $id]];
                    $remove_qty = $spareData['product_count'] - 1;

                    $data = array(
                        "product_count" => $remove_qty
                    );

                    $update = $this->general->update($this->tbl_spares, $data, $where_data);
                    if ($update['status'] != true) {
                        $errors['errors'][] = $update;
                    }
                }
            }
            if (isset($errors) && !empty($errors)) {
                $errors['status'] = FALSE;
                $errors['message'] = "No Products";
                $this->set_response($errors, REST_Controller::HTTP_NOT_FOUND);
            }
        }
    }
    function GeneratePDF($id)
    {
        $where = [
            ['column' => 'o.order_id', 'value' => $id]
        ];
        $array = [
            "fileds" => "s.*, u.*,o.*",
            "table" => $this->tbl_orders . ' AS o',
            "join_tables" => [
                ["table" => $this->tbl_users . ' AS u', "join_on" => 'u.user_id = o.user_id', "join_type" => "left"],
                ["table" => $this->tbl_spares_order . ' AS so', "join_on" => 'so.order_id = o.order_id', "join_type" => "left"],
                ["table" => $this->tbl_spares . ' AS s', "join_on" => 's.spare_id = so.spare_id', "join_type" => "left"]
            ],
            "where" => $where,
            "like" => [],
            "limit" => [],
            "order_by" => [],
        ];
        $get = $this->general->get_all_from_join($array);
        $html_data = $get['resultSet'];
        $data['html_data'] =  $get['resultSet'];;
        $this->load->view('order_html', $data);

        $html = $this->output->get_output();
        $this->load->library('pdf');
        $this->dompdf->set_base_path('/assets/css/style.css');

        $this->dompdf->loadHtml($html);
        $this->dompdf->setPaper('A4', 'portrait');
        $this->dompdf->render();
        $output = $this->dompdf->output();
        $fileName = $html_data[0]['invoice_id'];
        $file_location = "uploads/orders/" . $fileName . ".pdf";
        file_put_contents($file_location, $output);
        $data = array(
            "invoice_document" => base_url() . $file_location
        );
        $where_data = [['column' => 'order_id', 'value' => $id]];
        $update = $this->general->update($this->tbl_orders, $data, $where_data);
        if ($update['status'] != true) {
            $errors['errors'][] = $update;
        }
    }


    public function OrderCreateInstallation_post()
    {

        $params = json_decode(file_get_contents('php://input'), TRUE);
        $created_at = $updated_at = current_date();
        $trans_type_id = $user_id = $user_mobile  = "";


        if (isset($params['trans_type_id']) && !empty($params['trans_type_id'])) {
            $_POST['trans_type_id'] = $trans_type_id = $params['trans_type_id'];
        }
        if (isset($params['user_id']) && !empty($params['user_id'])) {
            $_POST['user_id'] = $user_id = $params['user_id'];
        }
        

        $errors = array();
        $this->form_validation->set_rules('trans_type_id', 'Transcation Type', 'required|trim|numeric');
        $this->form_validation->set_rules('user_id', 'User Id', 'required|trim|numeric');
        if ($this->form_validation->run() == FALSE) {
            $errors['errors'] = $this->form_validation->error_array();
        } else {

            $where =  [['column' => 'user_id', 'value' => $user_id]];

            $get_users = $this->general->get_row($this->tbl_users, '*', $where);

            $where_installment = [['column' => 'spare_id', 'value' => 21]];

            $get_installment_data = $this->general->get_row('tb_spares', '*', $where_installment);

           $price =$get_installment_data['resultSet'];

            if ($get_users['status'] != true) {
                $errors['errors'][] = "User Not Found";
            } else {
                if ($trans_type_id == 1) {
                    $response = array();
                    $randnumber = mt_rand(11111, 99999);
                    $order_code = "LTI" . $randnumber;
                    $total_amount = $price['product_price'];
                    $spare_id = 21;
                    $spare_amount = $price['product_price'];
                    $spareData = [];
                    $data = array(
                        "invoice_id" =>  $order_code,
                        "user_id" => $user_id,
                        "trans_type_id" => $trans_type_id,
                        "total_amount" => $total_amount,
                        "status" => 1,
                        "created_at" => $created_at
                    );
                    $insert = $this->general->add($this->tbl_orders, $data);
                    $order_id = $this->db->insert_id();

                    if ($insert['status'] != true) {
                        $errors['errors'][] = $insert;
                    } else {

                        $where = [['column' => 'spare_id', 'value' => $spare_id]];
                        $get = $this->general->get_row($this->tbl_spares, '*', $where);

                        if (isset($get['resultSet']) && !empty($get['resultSet'])) {
                            $spareData = $get['resultSet'];

                            $data = array(
                                "order_id" => $order_id,
                                "user_id" => $user_id,
                                "spare_id" =>  $spare_id,
                                "qty" => 1,
                                "amount" => $spare_amount
                            );

                            $insert_order = $this->general->add($this->tbl_spares_order, $data);
                            if ($insert_order['status'] != true) {
                                $errors['errors'][] = $insert_order;
                            } else {
                                $this->GeneratePDF($order_id);
                            }
                            $where_order = [['column' => 'order_id', 'value' => $order_id]];
                            $Order_data = $this->general->get_row($this->tbl_orders, '*', $where_order);
                        }
                    }
                }
            }
        }
        if (isset($errors) && !empty($errors)) {
            $errors['status'] = FALSE;
            $errors['message'] = "Order Created Failed";
            $this->set_response($errors, REST_Controller::HTTP_CREATED);
        } else {
            $response = array(
                "status" => TRUE,
                "message" => "Order Sucessfully Placed",
                "Data" =>  $Order_data['resultSet']
            );
            $this->set_response($response, REST_Controller::HTTP_OK);
        }
    }
}
