<?php
use Restserver\Libraries\REST_Controller;
Header('Access-Control-Allow-Origin: *'); //for allow any domain, insecure
Header('Access-Control-Allow-Headers: *'); //for allow any headers, insecure
Header('Access-Control-Allow-Methods: GET, POST, OPTIONS, PUT, DELETE'); //method allowed
// defined('BASEPATH') OR exit('No direct script access allowed');

// This can be removed if you use __autoload() in config.php OR use Modular Extensions
/** @noinspection PhpIncludeInspection */
require(APPPATH.'/libraries/REST_Controller.php');



/**
 * This is an example of a few basic user interaction methods you could use
 * all done with a hardcoded array
 *
 * @package         CodeIgniter
 * @subpackage      Rest Server
 * @category        Controller
 * @author          Phil Sturgeon, Chris Kacerguis
 * @license         MIT
 * @link            https://github.com/chriskacerguis/codeigniter-restserver
 */
class Slots extends REST_Controller {

    private $tbl_users = "tb_users";    
    private $tbl_deposits = "tb_deposits";    
    private $tbl_user_invests = "tb_user_invests";    
    private $tbl_section_slots = "tb_section_slots";    
    private $tbl_slot_transtions = "tb_slot_transtions";    
   
    function __construct() {
        // Construct the parent class
        parent::__construct();
        $this->load->model("general_model","general");
    }	
    

    public function slots_get($id=null)
    {
        if(isset($id) && !empty($id) && is_numeric($id))
        {
            $where = [['column'=>'slot_id','value'=>$id]];
            $res = $this->general->get_row($this->tbl_section_slots,'*',$where);
            $invests = $this->get_invests($id); 

            if(isset($res['resultSet']) && !empty($res['resultSet']))
            {
                $slot_status = $res['resultSet']['slot_status'];
                if($slot_status == 0)
                {
                    $data = $this->send_process_response($res['resultSet']);
                    
                    if($data['slot_status'] == 'complete')
                    {
                        $this->get_all_slots($id);
                    }
                    else
                    {
                        $res = [
                            'status'=>TRUE,
                            'message'=>'slot time is processing...',
                            'data'=>$data,
                            'user_details'=> $invests
                        ];
                        $this->response($res, REST_Controller::HTTP_OK);
                    }
                   
                }
                else
                {
                    $this->get_all_slots($id);
                }
            }   
        }
        else
        {
            $this->get_all_slots();
        }       
    }


    function get_all_slots($id=null)
    {
        $where = [];  $like = [];
        if(isset($id) && !empty($id) && is_numeric($id))
        {
            $where[] = ["column" => "s.slot_id","value" => $id];   
        }

        if(isset($_GET['slot_status']) && !empty($_GET['slot_status']))
        {
            $where[] = ["column" => "s.slot_status","value" => $_GET['slot_status']];   
        }

        if(isset($_GET['slot_type']) && !empty($_GET['slot_type']))
        {
            $where[] = ["column" => "s.slot_type","value" => $_GET['slot_type']];   
        }

           

        $order_by = [];            
        $order_by[] =  ["column" => "s.slot_id", "type" => (isset($_GET['order']) && !empty($_GET['order']))?$_GET['order']:'DESC'];

        $limit_start = (isset($_GET['limit_start']))?$_GET['limit_start']:0;
        $limit_end = (isset($_GET['limit_end']))?$_GET['limit_end']:0;

        $array = [
            "fileds" => "s.*",
            "table" => $this->tbl_section_slots.' AS s',
            "join_tables" => [],
            "where" => $where,
            "like" => $like,
            "limit" => ["start" => $limit_start, "end" => $limit_end],
            "order_by" => $order_by,
        ];       
       
       
        $p = $this->general->get_all_from_join($array); 
        if(isset($p['resultSet']) && !empty($p['resultSet']))
        {
            $invests = $this->get_invests($id);           
            
            $res = [
                'status'=>TRUE,
                'message'=>'completed',
                'data'=>$p['resultSet'],
                'user_details'=> $invests
            ];

            if(isset($id) && !empty($id) && is_numeric($id))
            {
                $res["win_type_oppo"] = ($p['resultSet'][0]['slot_won_type'] == 'up')?'down':'up';
                 
            }

        }
        else
        {
            $res = [
                'status'=>FALSE,
                'message'=>'Data not found',
            ];
        }
        $this->response($res, REST_Controller::HTTP_OK); 
    }


    function send_process_response($data = [])
    {
        if(isset($data) && !empty($data))
        {
            $data['slot_status_message'] = 'Process';
            $now = date('Y-m-d H:i:s');           
            
            $slot_end_time = strtotime($data['slot_end_date_and_time']);
            $now_time = strtotime($now);

            $slot_exceed = false;
            if(($slot_end_time < $now_time) OR ($slot_end_time == $now_time))
            {
                $slot_exceed = true;
                $data['slot_remain_close_seconds'] = '00';
                $data['slot_remain_close_time'] = '00:00:00';
                $this->winning_algorithm($data['slot_id']);
                $data['slot_status'] = "complete";
            }
            else if(isset($_GET['admin_decided']) && !empty($_GET['admin_decided']))
            {
                $admin_decided = $_GET['admin_decided'];
                $data['slot_remain_close_seconds'] = '00';
                $data['slot_remain_close_time'] = '00:00:00';
                $this->winning_algorithm($data['slot_id'],$admin_decided);
                $data['slot_status'] = "complete";
            }
            else 
            {
                // $this->winning_algorithm($data['slot_id']);
                $diff =  $slot_end_time - $now_time;            
                $data['slot_remain_close_seconds'] = $diff;
                $data['slot_remain_close_time'] = $this->seconds_time($diff);
                $data['slot_status'] = "progress";
            }            
            
            $data['slot_current_time'] = $now;            
            $data['slot_exceed'] = $slot_exceed;
        }
        
        return $data;
    }

    function seconds_time($seconds = null)
    {
        $res = 'failed';
        if($seconds)
        {
            $H = floor($seconds / 3600);
            $i = ($seconds / 60) % 60;
            $s = $seconds % 60;
            $res = sprintf("%02d:%02d:%02d", $H, $i, $s);
        }        
        return $res;
    }

    function winning_algorithm($slot_id=null,$admin_decided=null)
    {
        if(isset($slot_id) && !empty($slot_id) && is_numeric($slot_id))
        {
            $where = [
                ['column'=>'t.slot_id','value'=>$slot_id],
                ['column'=>'s.slot_status','value'=>'0'],
            ];
            $array = [
                "fileds" => "s.*, i.*",
                "table" => $this->tbl_slot_transtions.' AS t',
                "join_tables" => [
                    ["table"=>$this->tbl_user_invests.' AS i',"join_on"=>'i.invest_id = t.invest_id',"join_type" => "left"],
                    ["table"=>$this->tbl_section_slots.' AS s',"join_on"=>'s.slot_id = t.slot_id',"join_type" => "left"]
                ],
                "where" => $where,
                "like" => [],
                "limit" => [],
                "order_by" => [],
            ];
            $get = $this->general->get_all_from_join($array);                   
            if(isset($get['resultSet']) && !empty($get['resultSet']))
            {
                $data = $get['resultSet'];
                if(count($data) > 0)
                {
                    
                    $up = []; $down = []; $total = []; $up_users_count = []; $down_users_count = []; $up_users_amount = []; $down_users_amount = [];

                    foreach($data as $v)
                    {
                        if($v['invest_type'] == 'up')
                        {
                            $up[] = $v['invest_amount'];
                            $up_users_count[] = $v['user_id'];
                            $up_users_amount[] = ['user_id'=>$v['user_id'],'amount'=>$v['invest_amount'],'invest_id'=>$v['invest_id'],'account_type'=>$v['account_type']];
                        }
                        else
                        {
                            $down[] = $v['invest_amount'];
                            $down_users_count[] = $v['user_id'];
                            $down_users_amount[] = ['user_id'=>$v['user_id'],'amount'=>$v['invest_amount'],'invest_id'=>$v['invest_id'],'account_type'=>$v['account_type']];
                        }
                        $total[] = $v['invest_amount'];
                    }
                   
                   
                    
                    
                   

                    $up_users_count = count($up_users_count);
                    $down_users_count = count($down_users_count);
                    $up_amount = array_sum($up);
                    $down_amount = array_sum($down);
                    $total_slot_amount = array_sum($total);

                    // echo "up_users_count => ".$up_users_count.PHP_EOL;    
                    // echo "down_users_count => ".$down_users_count.PHP_EOL;  
                    // echo "up_amount => ".$up_amount.PHP_EOL;  
                    // echo "down_amount => ".$down_amount.PHP_EOL;  
                    // echo "up_users_amount => ".json_encode($up_users_amount).PHP_EOL;  
                    // echo "down_users_amount => ".json_encode($down_users_amount).PHP_EOL;  
                    // echo "total_slot_amount => ".$total_slot_amount.PHP_EOL;  
                    // exit;


                        $slot_won_type = null; $slot_winning_check = null;
                        if(isset($admin_decided) && !empty($admin_decided))
                        {
                            if($admin_decided == 'up')
                            {
                                $slot_won_type = 'up';
                                $slot_winning_check = 'admin decided';
                            }
                            else if($admin_decided == 'down')
                            {
                                $slot_won_type = 'down';
                                $slot_winning_check = 'admin decided';
                            }
                            else if($admin_decided == 'all')
                            {
                                $slot_won_type = 'all';
                                $slot_winning_check = 'admin decided';
                            }
                            else
                            {
                                echo "something went wrong check admin_decided parameter";
                                exit;
                            }
                        }
                        else if(count($data) == 1)
                        {
                            // pr($data);
                            $this_user_id = $data[0]['user_id'];
                            $this_account_type = $data[0]['account_type'];
                            $w = $this->check_single_user_win($this_user_id,$this_account_type);
                            if($w == 'win')
                            {
                                $slot_won_type = $data[0]['invest_type'];
                            }
                            else
                            {
                                $slot_won_type = ($data[0]['invest_type'] == 'up')?'down':'up';
                            }                           
                            $slot_winning_check = 'single user';
                        }
                        else if($up_amount < $down_amount)
                        {
                            // echo "Up is win";
                            $slot_won_type = 'up';
                            $slot_winning_check = 'up amount less';
                        }
                        else if($down_amount < $up_amount)
                        {
                            // echo " Down is win";
                            $slot_won_type = 'down';
                            $slot_winning_check = 'down amount less';
                        }
                        else if($up_amount == $down_amount)
                        {
                                if($up_users_count > $down_users_count)
                                {
                                    // echo "Up Users to be win";
                                    $slot_won_type = 'up';
                                    $slot_winning_check = 'amount equal, up more users';
                                }
                                else if($down_users_count > $up_users_count)
                                {
                                    // echo "Down Users to be win";
                                    $slot_won_type = 'down';
                                    $slot_winning_check = 'amount equal, down more users';
                                }
                                else
                                {
                                    // echo "random select up or down";
                                    $arr = array( 'up', 'down');
                                    $key = array_rand($arr);
                                    $slot_won_type = $arr[$key];
                                    $slot_winning_check = 'random picked';
                                }
                        }


                        $data = [
                            'slot_status' => '1',
                            'slot_total_up_amount' => $up_amount,
                            'slot_total_down_amount' => $down_amount,
                            'slot_total_amount' => $total_slot_amount,
                            'slot_won_type' => $slot_won_type,
                            'slot_winning_check' => $slot_winning_check,
                        ];
                        // pr($data);
                        $where = [['column'=>'slot_id','value'=>$slot_id]];
                        $this->general->update($this->tbl_section_slots,$data,$where);

                        if($slot_won_type == 'up')
                        {
                            if(isset($up_users_amount) && !empty($up_users_amount))
                            {
                                foreach($up_users_amount as $v)
                                {
                                    //later amount logic will implemeted here
                                    if($v['account_type'] == 'real')
                                    {
                                        $this->update_user_wallet_winner($v['user_id'],$v['amount'],$v['invest_id']);
                                    }
                                    else if($v['account_type'] == 'demo')
                                    {
                                        $this->demo_update_user_wallet_winner($v['user_id'],$v['amount'],$v['invest_id']);
                                    }
                                }
                            }

                            if(isset($down_users_amount) && !empty($down_users_amount))
                            {
                                foreach($down_users_amount as $v)
                                {
                                    //later amount logic will implemeted here
                                    if($v['account_type'] == 'real')
                                    {
                                        $this->update_user_wallet_looser($v['user_id'],$v['amount'],$v['invest_id']);
                                    }
                                    else if($v['account_type'] == 'demo')
                                    {
                                        $this->demo_update_user_wallet_looser($v['user_id'],$v['amount'],$v['invest_id']);
                                    }
                                    
                                }
                            }
                        }
                        else if($slot_won_type == 'down')
                        {
                            if(isset($up_users_amount) && !empty($up_users_amount))
                            {
                                foreach($up_users_amount as $v)
                                {
                                    //later amount logic will implemeted here                                   
                                    if($v['account_type'] == 'real')
                                    {
                                        $this->update_user_wallet_looser($v['user_id'],$v['amount'],$v['invest_id']);
                                    }
                                    else if($v['account_type'] == 'demo')
                                    {
                                        $this->demo_update_user_wallet_looser($v['user_id'],$v['amount'],$v['invest_id']);
                                    }
                                }
                            }

                            if(isset($down_users_amount) && !empty($down_users_amount))
                            {
                                foreach($down_users_amount as $v)
                                {
                                    //later amount logic will implemeted here
                                    if($v['account_type'] == 'real')
                                    {
                                        $this->update_user_wallet_winner($v['user_id'],$v['amount'],$v['invest_id']);
                                    }
                                    else if($v['account_type'] == 'demo')
                                    {
                                        $this->demo_update_user_wallet_winner($v['user_id'],$v['amount'],$v['invest_id']);
                                    }
                                }
                            }
                        }
                        else if($slot_won_type == 'all')
                        {
                            if(isset($up_users_amount) && !empty($up_users_amount))
                            {
                                foreach($up_users_amount as $v)
                                {
                                    //later amount logic will implemeted here                                   
                                    if($v['account_type'] == 'real')
                                    {
                                        $this->update_user_wallet_winner($v['user_id'],$v['amount'],$v['invest_id']);
                                    }
                                    else if($v['account_type'] == 'demo')
                                    {
                                        $this->demo_update_user_wallet_winner($v['user_id'],$v['amount'],$v['invest_id']);
                                    }
                                }
                            }

                            if(isset($down_users_amount) && !empty($down_users_amount))
                            {
                                foreach($down_users_amount as $v)
                                {
                                    //later amount logic will implemeted here
                                    if($v['account_type'] == 'real')
                                    {
                                        $this->update_user_wallet_winner($v['user_id'],$v['amount'],$v['invest_id']);
                                    }
                                    else if($v['account_type'] == 'demo')
                                    {
                                        $this->demo_update_user_wallet_winner($v['user_id'],$v['amount'],$v['invest_id']);
                                    }
                                }
                            }
                        }            
                }
                else
                {
                    $errors[] = "Only one user in this slot";
                }
            }
        }

        if(empty($errors))
        {
            $this->get_all_slots($slot_id);
        }
        else
        {
            $res = [
                'status'=>False,
                'message'=>'something went wrong in calculations',
                'errors'=>$errors
            ];
            $this->response($res, REST_Controller::HTTP_OK);
        }
    }



    function update_user_wallet_looser($user_id=null,$invest_amount=null,$invest_id=null)
    {
        if(isset($user_id,$invest_amount,$invest_id) && !empty($user_id) && is_numeric($user_id) && !empty($invest_amount) && is_numeric($invest_amount)  && !empty($invest_id) && is_numeric($invest_id))
        {
            $where = [['column'=>'user_id','value'=>$user_id]];
            $get = $this->general->get_row($this->tbl_users,'wallet_amount',$where);
            if(isset($get['resultSet']) && !empty($get['resultSet']))
            {
                $wallet_amount = $get['resultSet']['wallet_amount'];
                // $total_amount = $wallet_amount-$invest_amount;
                // $where = [['column'=>'user_id','value'=>$user_id]];
                // $data = [
                //             "wallet_amount"=>$total_amount,                           
                //         ];
                // $update = $this->general->update($this->tbl_users,$data,$where);

                $where = [['column'=>'invest_id','value'=>$invest_id]];
                $data = [
                            "user_amount_after_bet"=>$wallet_amount,
                            "is_win"=>'loss',
                            "user_profit"=>$invest_amount,                           
                        ];
                $update = $this->general->update($this->tbl_user_invests,$data,$where);
                if($update['status'])
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
        }
    }



    function update_user_wallet_winner($user_id=null,$invest_amount=null,$invest_id=null)
    {
        if(isset($user_id,$invest_amount,$invest_id) && !empty($user_id) && is_numeric($user_id) && !empty($invest_amount) && is_numeric($invest_amount)  && !empty($invest_id) && is_numeric($invest_id))
        {
            $where = [['column'=>'user_id','value'=>$user_id]];
            $get = $this->general->get_row($this->tbl_users,'wallet_amount',$where);
            if(isset($get['resultSet']) && !empty($get['resultSet']))
            {
                $winning_percentage = 90; 
                $win_amount = $this->get_percentage($invest_amount,$winning_percentage);
                $wallet_amount = $get['resultSet']['wallet_amount'];
                $total_amount = $wallet_amount+$invest_amount+$win_amount;
                $where = [['column'=>'user_id','value'=>$user_id]];
                $data = ["wallet_amount"=>$total_amount];
                $update = $this->general->update($this->tbl_users,$data,$where);

                $where = [['column'=>'invest_id','value'=>$invest_id]];
                $data = [
                            "user_amount_after_bet"=>$total_amount,
                            "is_win"=>'win',                           
                            "user_profit"=>$win_amount,                           
                        ];
                $update = $this->general->update($this->tbl_user_invests,$data,$where);
                if($update['status'])
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
        }
    }


    function demo_update_user_wallet_looser($user_id=null,$invest_amount=null,$invest_id=null)
    {
        if(isset($user_id,$invest_amount,$invest_id) && !empty($user_id) && is_numeric($user_id) && !empty($invest_amount) && is_numeric($invest_amount)  && !empty($invest_id) && is_numeric($invest_id))
        {
            $where = [['column'=>'user_id','value'=>$user_id]];
            $get = $this->general->get_row($this->tbl_users,'demo_wallet_amount',$where);
            if(isset($get['resultSet']) && !empty($get['resultSet']))
            {
                $wallet_amount = $get['resultSet']['demo_wallet_amount'];
                $total_amount = $wallet_amount-$invest_amount;
                $where = [['column'=>'user_id','value'=>$user_id]];
                $data = [
                            "demo_wallet_amount"=>$total_amount,                           
                        ];
                $update = $this->general->update($this->tbl_users,$data,$where);

                $where = [['column'=>'invest_id','value'=>$invest_id]];
                $data = [
                            "user_amount_after_bet"=>$total_amount,
                            "is_win"=>'loss', 
                            "user_profit"=>$invest_amount,                          
                        ];
                $update = $this->general->update($this->tbl_user_invests,$data,$where);
                if($update['status'])
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
        }
    }



    function demo_update_user_wallet_winner($user_id=null,$invest_amount=null,$invest_id=null)
    {
        if(isset($user_id,$invest_amount,$invest_id) && !empty($user_id) && is_numeric($user_id) && !empty($invest_amount) && is_numeric($invest_amount)  && !empty($invest_id) && is_numeric($invest_id))
        {
            $where = [['column'=>'user_id','value'=>$user_id]];
            $get = $this->general->get_row($this->tbl_users,'demo_wallet_amount',$where);
            if(isset($get['resultSet']) && !empty($get['resultSet']))
            {
                $winning_percentage = 90; 
                $win_amount = $this->get_percentage($invest_amount,$winning_percentage);
                $wallet_amount = $get['resultSet']['demo_wallet_amount'];
                $total_amount = $wallet_amount+$invest_amount+$win_amount;
                $where = [['column'=>'user_id','value'=>$user_id]];
                $data = ["demo_wallet_amount"=>$total_amount];
                $update = $this->general->update($this->tbl_users,$data,$where);

                $where = [['column'=>'invest_id','value'=>$invest_id]];
                $data = [
                            "user_amount_after_bet"=>$total_amount,
                            "is_win"=>'win', 
                            "user_profit"=>$win_amount,                          
                        ];
                $update = $this->general->update($this->tbl_user_invests,$data,$where);
                if($update['status'])
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
        }
    }



    function get_invests($id=null)
    {
        $data = [];
        if(isset($id) && !empty($id) && is_numeric($id))
        {

            if(isset($_GET['user_id']) && !empty($_GET['user_id']))
            {
                $where[] = ["column" => "u.user_id","value" => $_GET['user_id']];   
            }

            if(isset($_GET['account_type']) && !empty($_GET['account_type']))
            {
                $where[] = ["column" => "i.account_type","value" => $_GET['account_type']];   
            }

            $where[] = ["column" => "st.slot_id","value" => $id];  
            $order_by[] =  ["column" => "st.invest_id", "type" => (isset($_GET['order']) && !empty($_GET['order']))?$_GET['order']:'DESC'];
            $array = [
                "fileds" => "u.user_id,u.email,u.full_name,u.phone,i.invest_id,i.invest_amount,i.invest_type,i.created_on,i.user_amount_before_bet,i.user_amount_after_bet,i.is_win,i.account_type,i.user_profit,st.slot_id",
                "table" => $this->tbl_slot_transtions.' AS st',
                "join_tables" => [
                    ["table"=>$this->tbl_user_invests.' AS i',"join_on"=>'i.invest_id = st.invest_id',"join_type" => "left"],
                    ["table"=>$this->tbl_users.' AS u',"join_on"=>'u.user_id = i.user_id',"join_type" => "left"]
                ],
                "where" => $where,
                "like" => [],
                "limit" => [],
                "order_by" => $order_by,
            ];       
        
        
            $p = $this->general->get_all_from_join($array);            
            if(isset($p['resultSet']) && !empty($p['resultSet']))
            {
                $data = $p['resultSet'];
            }
        }        
        return $data;
    }


    public function get_current_bet_status_get()
    {
        if(isset($_GET['user_id']) && !empty($_GET['user_id']))
        {
            $user_id = $_GET['user_id'];
            $where[] = ["column" => "user_id","value" => $user_id]; 
            $order_by[] =  ["column" => "invest_id", "type" => 'DESC'];
            $array = [
                "fileds" => "i.invest_id",
                "table" => $this->tbl_user_invests.' AS i',
                "join_tables" => [],
                "where" => $where,
                "like" => [],
                "limit" => [],
                "order_by" => $order_by,
            ];       
            $get = $this->general->get_row_from_join($array);
            if(isset($get['resultSet']['invest_id']) && !empty($get['resultSet']['invest_id']))
            {
                $invest_id = $get['resultSet']['invest_id'];
                $where_2[] = ["column" => "st.invest_id","value" => $invest_id]; 
                
                $array_2 = [
                    "fileds" => "u.email,u.full_name,u.phone,i.invest_id,i.invest_amount,i.invest_type,i.created_on,i.user_amount_before_bet,i.user_amount_after_bet,i.is_win,i.account_type,s.slot_status,s.slot_id",
                    "table" => $this->tbl_slot_transtions.' AS st',
                    "join_tables" => [
                        ["table"=>$this->tbl_user_invests.' AS i',"join_on"=>'i.invest_id = st.invest_id',"join_type" => "left"],
                        ["table"=>$this->tbl_users.' AS u',"join_on"=>'u.user_id = i.user_id',"join_type" => "left"],
                        ["table"=>$this->tbl_section_slots.' AS s',"join_on"=>'s.slot_id = st.slot_id',"join_type" => "left"],
                    ],
                    "where" => $where_2,
                    "like" => [],
                    "limit" => [],
                    "order_by" => [],
                ];  
                $p = $this->general->get_row_from_join($array_2); 
                
                if(isset($p['resultSet']) && !empty($p['resultSet']))
                {
                    if($p['resultSet']['slot_status'] == 1)
                    {
                        $res['status'] = true;
                        $res['message'] = "completed";
                        $res['data'] = $p['resultSet'];
                        $res['win_type'] = ($p['resultSet']['invest_type'] == 'down')?'up':'down';
                    }
                    else
                    {
                        $res['status'] = true;
                        $res['message'] = "progress";
                        $res['data'] = $p['resultSet'];
                        $res['win_type'] = null;
                    }
                }

            }
            else
            {
                $res['status'] = false;
                $res['message'] = "User don't have any bets";
            }
            
        }
        else
        {
            $res['status'] = false;
            $res['message'] = "User Id required";
        }
        $this->response($res, REST_Controller::HTTP_OK); 
    }


    function get_percentage($total, $number)
    {
        if ( $total > 0 )
        {
            return round($number * ($total / 100),2);
        }
        else
        {
            return 0;
        }
    }


    function check_single_user_win($user_id=null,$account_type=null)
    {
        $win = 'win';
        if(isset($user_id,$account_type) && !empty($user_id) && !empty($account_type))
        {
            $where = [
                        ["column"=>"account_type","value"=>$account_type],
                        ["column"=>"user_id","value"=>$user_id]
                    ];
            $res = $this->general->get_row($this->tbl_user_invests,'*',$where);
            if(isset($res['resultSet']) && !empty($res['resultSet']))
            {
                if($res['resultSet']['is_win'] != null)
                {
                    // one time win and one time lose algothrim used for single user
                    $win = ($res['resultSet']['is_win'] == 'win')?'loss':'win';
                }                
            }
        }
        return $win;
    }

    
   
}