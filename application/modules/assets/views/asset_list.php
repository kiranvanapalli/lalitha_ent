<style type="text/css">
    .wallet-wrapper
    {
    text-align: center;
    }
    .float-lg-right
    {
        color: green !important;
    }
    .verify_otp
    {
        text-align: center;
        color: orange !important;
    }
    .resend_otp
    {
        color:red !important;
        float: right;
    }
    .smsCode {
                text-align: center;
                line-height: 30px;
                font-size: 30px;
                border: solid 1px #ccc;
                box-shadow: 0 0 5px #ccc inset;
                width:100%;
                outline: none;
                border-radius: 3px;
                margin-left: 150%;
            }

    .table
    {
        text-align: center;
    }
    .container{
    display: flex;
    flex-flow: column;
    height: 100%;
    align-items: space-around;
    justify-content: center;
}

.userInput{
    display: flex;
    justify-content: center;
}
.container input{
    margin: 10px;
    height: 35px;
    width: 65px;
    border: none;
    border-radius: 5px;
    text-align: center;
    font-family: arimo;
    font-size: 1.2rem;
    background: #eef2f3;

}


    
</style>
<!-- BEGIN: Content-->
<div class="app-content content">
    <div class="content-overlay"></div>
    <div class="content-wrapper">
        <div class="content-header row">
            <div class="content-header-left col-md-6 col-12 mb-2 breadcrumb-new">
                <h3 class="content-header-title mb-0 d-inline-block">Assets</h3>
                <div class="row breadcrumbs-top d-inline-block">
                    <div class="breadcrumb-wrapper col-12">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="<?php echo base_url(); ?>dashboard">Home</a>
                            </li>
                            <li class="breadcrumb-item active">Assets</li>
                        </ol>
                    </div>
                </div>
            </div>
            <div class="content-header-right col-md-6 col-12">
                <div class="btn-group float-md-right">
                    <button class="btn btn-primary round btn-min-width mr-1 mb-1" type="button" aria-haspopup="true" aria-expanded="false" data-toggle="modal" id="asset_model" data-target="#addassets">  <i class="la la-plus-circle"></i> Add Asset</button>
                </div>
            </div>
        </div>
    </div>
    <div class="content-body">
        <section id="pagination">
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-header">
                            <h4 class="card-title">Asset Details</h4>
                        </div>
                        <div class="card-body">
                            <div class="table-responsive">
                                <table class="table" name="asset_table" id="asset_table">
                                    <thead>
                                        <tr>
                                            <th>S.no</th>
                                            <th>Asset Name</th>
                                            <th>Today Value</th>
                                            <th>% Incress/Decress</th>
                                            <th>Present Value</th>
                                            <th>Status</th>
                                            <th>Edit</th>
                                            <th>Delete</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
</div>
<!-- END: Content-->
<!-- Modal -->
<div class="modal fade text-left" id="addassets" tabindex="-1" role="dialog" aria-labelledby="asset_model" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="asset_title"><i class="la la-road2"></i>Add Asset Information</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"> <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="card-content collpase show">
                    <div class="card-body">
                        <form  method="post" class ="asset_form" id="asset_form">
                            <div class="form-body">
                                <div class="row">
                                    <div class="form-group col-12 mb-2">
                                        <label for="asset_name">Asset Name</label>
                                        <input type="text" id="asset_name" class="form-control" placeholder="Asset Name" name="asset_name" data-toggle="tooltip" data-trigger="hover" data-placement="top" data-title="Asset Name">
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="form-group col-12 mb-2">
                                        <label for="today_asset_price">Today Asset Price</label>
                                        <input type="text" id="today_asset_price" class="form-control" placeholder="Today Asset Price" name="today_asset_price" data-toggle="tooltip" data-trigger="hover" data-placement="top" data-title="Today Asset Price">
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="form-group col-12 mb-2">
                                        <label for="in_or_de_asset_value">% Of Increase Or Decrease Asset Price</label>
                                        <input type="number" id="in_or_de_asset_value" class="form-control" placeholder="% Of Increase Or Decrease Asset Price" name="in_or_de_asset_value" data-toggle="tooltip" data-trigger="hover" data-placement="top" data-title="% Of Increase Or Decrease Asset Price">
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="form-group col-12 mb-2">
                                        <label for="present_asset_value">Present Asset Value</label>
                                        <input type="text" id="present_asset_value" class="form-control" placeholder="Present Asset Value " name="present_asset_value" data-toggle="tooltip" data-trigger="hover" data-placement="top" data-title="Present Asset Value">
                                    </div>
                                </div>
                                <div class="row">
                                <div class="form-group col-12 mb-2 status_div" id='status_div'>
                                        
                                            <label for="status">Status</label>
                                            <select class="form-control" name="status" id="status">
                                                <option value="">Select Status</option>
                                                <option value="1">Active</option>
                                                <option value="0">In Active</option>
                                            </select>
                                        
                                    </div>
                                </div>
                            </div>
                             <input type="hidden" id="edit_id" name="edit_id" value="">
                            <div class="form-actions">
                                <button type="button" class="btn btn-warning mr-1" > <i class="ft-x"></i> Cancel</button>
                                <button type="submit" class="btn btn-primary" id="btn_submit"> <i class="la la-check-square-o"></i> Save</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>