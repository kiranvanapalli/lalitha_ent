<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Assets extends MX_Controller
{
    public function __construct()
    {
      parent::__construct();
      /*if user not loged in redirect to home page*/
      modules::run('admin/admin/is_logged_in');
      $this->load->model('task_list/Allfiles_model'); 
      $this->load->library('my_file_upload');

    }

    public function index()
    {
      $where = '';
      $data['file'] = 'assets/asset_list';
      $data['custom_js']  = 'assets/all_files_js';
      $data['validation_js']       = 'admin/all_common_js/frontend_validation_admin';
      $type = "array";
      $all_assets = $this->Allfiles_model->GetDataAll("tb_assets",$where,$type,'asset_id',$limit='');
      $data['all_assets']   = $all_assets;
      $this->load->view('admin_template/main',$data);  
    }

    public function getassets()
    {   
      $where = '';
      $draw = intval($this->input->get("draw"));
      $start = intval($this->input->get("start"));
      $length = intval($this->input->get("length"));
      $type = "array";
      $all_assets = $this->Allfiles_model->GetDataAll("tb_assets",$where,$type,'asset_id',$limit='');
      $data_assets = array();
      $i = 1;
      foreach($all_assets as $assets) {
        $status = '';
        $edit_action = '';
        $delete_action = '';
        $password='';
        if($assets['status'] == 1) {
          $status = "<span class='btn btn-sm btn-outline-primary'>Active</span>";
        } else {
          $status = "<span class='btn btn-sm btn-outline-danger'>InActive</span>";
        }
       
         $edit_action = '<a class="btn btn-sm btn-info edit" id="'.$assets['asset_id'].'" href="#" title="Edit"><i class="fa fa-edit aria-hidden="true""></i></a>';
         $delete_action = '<a class="btn btn-sm btn-danger delete" id="'.$assets['asset_id'].'" href="#" title="Remove"><i class="fa fa-trash" aria-hidden="true"></i></a>';
         $data_assets[] = array( 



          '<td class="align-middle">'.$i++.'</td>',
          '<td class="align-middle">'.$assets['asset_name'].'</td>',
          '<td class="align-middle">'.$assets['today_value'].'</td>',
          '<td class="align-middle">'.$assets['incr_decr'].'</td>',
          '<td class="align-middle">'.$assets['prasent_value'].'</td>',
          '<td class="align-middle">'.$status.'</td>', 
          '<td class="align-middle">'.$edit_action.'</td>',
          '<td class="align-middle">'.$delete_action.'</td>',


         
        );
      }
      $result = array(
       "draw" => $draw,
       "recordsTotal" => count($all_assets),
       "recordsFiltered" => count($all_assets),
       "data" => $data_assets,
     );

      echo json_encode($result); 
    }

    public function addassetdetails()
    {  
      $asset_name =  $this->input->post("asset_name");
      $today_value =  $this->input->post("today_asset_price");
      $incr_decr  =  $this->input->post("in_or_de_asset_value");
      $prasent_value =  $this->input->post("present_asset_value");
      $data = array(
       'asset_name' =>$asset_name,
       'today_value' =>$today_value,
       'incr_decr' => $incr_decr, 
       'prasent_value' => $prasent_value, 
       'created_at' => date('Y-m-d H:i:s'),  
       'status' => 1,
     ); 
     $result = $this->Allfiles_model->data_save("tb_assets",$data);
     echo  json_encode($result);
    }
    public function edit_asset()
    {
       if (isset($_POST['asset_id']) && !empty($_POST['asset_id'])) 
       {
          $where = ['asset_id' => $_POST['asset_id']];
          $type = "row";
          $result  = $this->Allfiles_model->GetDataAll("tb_assets as a",$where,$type,$order='',$limit='');
          echo json_encode($result); 
            
       }
       else
        {
            echo "Something went wrong please try again!";
        }    
    }

    public function update_asset()
    {
    if(!empty($_POST['edit_id']))
        {   

            $response = [];
            $where = ['asset_id' => $_POST['edit_id']];
            $asset_name =  $this->input->post("asset_name");
            $today_value =  $this->input->post("today_asset_price");
            $incr_decr  =  $this->input->post("in_or_de_asset_value");
            $prasent_value =  $this->input->post("present_asset_value");
            $data = array(
             'asset_name' =>$asset_name,
             'today_value' =>$today_value,
             'incr_decr' => $incr_decr, 
             'prasent_value' => $prasent_value, 
             'updated_at' => date('Y-m-d H:i:s'),  
             'status' => $this->input->post('status'),
           ); 
            $update_asset =   $this->Allfiles_model->updateData("tb_assets",$data,$where);
            if($update_asset) 
            {
                $response = ['status' => 'success'];
            }
            else
            {
               $response = ['status' => 'fail'];
            }  
            echo json_encode($response);
        } 
                                   
    }

    public function delete_asset()
     {
       if (isset($_POST['asset_id']) && !empty($_POST['asset_id'])) 

       {
      
            $where = ['asset_id' => $_POST['asset_id']];
            $result  = $this->Allfiles_model->deleteData("tb_assets",$where);
            echo $result;
        }         
       }

      
}