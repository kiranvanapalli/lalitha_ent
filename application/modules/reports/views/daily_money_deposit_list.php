<style type="text/css">
    .wallet-wrapper
    {
    text-align: center;
    }
    .float-lg-right
    {
        color: green !important;
    }
    .verify_otp
    {
        text-align: center;
        color: orange !important;
    }
    .resend_otp
    {
        color:red !important;
        float: right;
    }
    .smsCode {
                text-align: center;
                line-height: 30px;
                font-size: 30px;
                border: solid 1px #ccc;
                box-shadow: 0 0 5px #ccc inset;
                width:100%;
                outline: none;
                border-radius: 3px;
                margin-left: 150%;
            }

    .table
    {
        text-align: center;
    }
    .container{
    display: flex;
    flex-flow: column;
    height: 100%;
    align-items: space-around;
    justify-content: center;
}

.userInput{
    display: flex;
    justify-content: center;
}
.container input{
    margin: 10px;
    height: 35px;
    width: 65px;
    border: none;
    border-radius: 5px;
    text-align: center;
    font-family: arimo;
    font-size: 1.2rem;
    background: #eef2f3;

}


    
</style>
<!-- BEGIN: Content-->
<div class="app-content content">
    <div class="content-overlay"></div>
    <div class="content-wrapper">
        <div class="content-header row">
            <div class="content-header-left col-md-6 col-12 mb-2 breadcrumb-new">
                <h3 class="content-header-title mb-0 d-inline-block">User Deposit Details</h3>
                <div class="row breadcrumbs-top d-inline-block">
                    <div class="breadcrumb-wrapper col-12">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="<?php echo base_url(); ?>dashboard">Home</a>
                            </li>
                            <li class="breadcrumb-item active">User Deposit Details</li>
                        </ol>
                    </div>
                </div>
            </div>
            
        </div>
    </div>
    <div class="content-body">
        <section id="pagination">
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-header">
                            <h4 class="card-title">User Deposit Details</h4>
                        </div>
                        <div class="card-body">
                            <div class="table-responsive">
                                <table class="table align-middle" name="user_deposit_table" id="user_deposit_table">
                                    <thead>
                                        <tr>
                                            <th>S.no</th>
                                            <th>Name</th>
                                            <th>Mobile Number</th>
                                            <th>Email</th>
                                            <th>Amount</th>
                                            <th>Date</th>
                                            <!-- <th>Status</th> -->
                                            
                                        </tr>
                                    </thead>
                                    <tbody class="align-middle">
                                        
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
</div>
<!-- END: Content-->
<!-- Modal -->

