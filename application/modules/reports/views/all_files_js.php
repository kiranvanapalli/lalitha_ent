<script>
    $(document).ready(function() {
         var user_deposit_table = $('#user_deposit_table').DataTable({

                'searching': true,
                'paging': true,
                "responsive": true,
                "processing": true,
                // "serverSide": true,
                'lengthChange': true,
                // "dom": 'Bfrtip',

                "buttons": [
                    'csv', 'excel', 'pdf', 'print'
                ],

                "order": [],
                "ajax": {
                    "url": "<?php echo base_url('money_deposit_details_getdata'); ?>",
                    "type": "POST",
                },
                "columnDefs": [{
                    targets: -1,
                    "orderable": false,
                }, ],
            });

           
            // Refilter the table
      
       
            var betting_history_table = $('#betting_history_table').DataTable({
                'searching': true,
                'paging': true,
                "responsive": true,
                "processing": true,
                // "serverSide": true,
                'lengthChange': true,
                "dom": 'Blfrtip',
                "buttons": [
                    'csv', 'excel', 'pdf', 'print'
                ],
                "order": [],
                "ajax": {
                    "url": "<?php echo base_url('betting_report_get_users'); ?>",
                    "type": "POST",
                    "data":function(data) {
                     data.from_date = $('#from_date').val();
                     data.to_date = $('#to_date').val();
                 },
                },
                "columnDefs": [{
                    targets: -1,
                    "orderable": false,
                }, ],
            });

            var slot_reports = $('#slot_report_table').DataTable({
                'searching': true,
                'paging': true,
                "responsive": true,
                "processing": true,
                // "serverSide": true,
                 "autoWidth": false,
                'lengthChange': true,
                "dom": 'Blfrtip',
                "buttons": [
                    'csv', 'excel', 'pdf', 'print'
                ],
                "order": [],
                "ajax": {
                    "url": "<?php echo base_url('getslotreport'); ?>",
                    "type": "POST",
                    "data":function(data) {
                     data.from_date = $('#from_date').val();
                     data.to_date = $('#to_date').val();
                 },
                },
                "columnDefs": [{
                    targets: -1,
                    "orderable": false,
                }, ],
            });

           
            // Refilter the table
        });
      $('#clear').click(function() {
        
             $('#from_date').val('');
             $('#to_date').val('');
              window.location.href = "<?php echo base_url();?>betting_report";

            });



        
</script>