<style type="text/css">
    div.dt-buttons {
    position: relative;
    float: right;
    margin-left: 160px;
}
td,th
{
    text-align: center;
}
</style>
<!-- <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.25/css/jquery.dataTables.min.css"> -->
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/datetime/1.1.0/css/dataTables.dateTime.min.css">
<script type="text/javascript" src="https://code.jquery.com/jquery-3.5.1.js"></script>
<!-- <script type="text/javascript" src="https://cdn.datatables.net/1.10.25/js/jquery.dataTables.min.js"></script> -->
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.18.1/moment.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/datetime/1.1.0/js/dataTables.dateTime.min.js"></script>
<!-- BEGIN: Content-->
<div class="app-content content">
    <div class="content-overlay"></div>
    <div class="content-wrapper">
        <div class="content-header row">
            <div class="content-header-left col-md-6 col-12 mb-2 breadcrumb-new">
                <h3 class="content-header-title mb-0 d-inline-block">Betting History Details</h3>
                <div class="row breadcrumbs-top d-inline-block">
                    <div class="breadcrumb-wrapper col-12">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="<?php echo base_url(); ?>dashboard">Home</a>
                        </li>
                        <li class="breadcrumb-item active">Betting History Details</li>
                    </ol>
                </div>
            </div>
        </div>
        
    </div>
</div>
<div class="content-body">
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                
                <!-- #Add custom filters in the server-side dataTable -->
                <form  method="post" id="serach_filter">
                <div class="row well ">
                    <div class="col-sm-4">
                        <label class="control-label" for="from_date">Start date</label>
                        <input class="form-control" type="date"  id="from_date" name="from_date" value="<?php echo set_value('from_date'); ?>">
                    </div>
                    <div class="col-sm-4">
                        <label class="control-label" >End date</label>
                        <!-- <input class="form-control datepicker" type="text" name="final_date" id="max" placeholder="yyyy-mm-dd" style="height: 40px;"/> -->
                        <input class="form-control" type="date"  id="to_date" name="to_date" value="<?php echo set_value('to_date','0'); ?>">
                    </div>
                    <div class="col-sm-2">
                        <button class="btn btn-success btn-block" type="submit" name="search_records" id="search_records" style="margin-top: 2em;">
                        <i class="fa fa-filter"></i> Search
                        </button>
                    </div>
                    <div class="col-sm-2">
                        <button class="btn btn-info btn-block" type="button" name="clear" id="clear" style="margin-top: 2em;">
                         Reset
                        </button>
                    </div>
                    <div class="col-sm-12 text-danger" id="error_log"></div>
                </div>
            </form>
                <br/><br/>
                <section id="pagination">
                    <div class="row">
                        <div class="col-12">
                            <div class="card">
                                <div class="card-header">
                                    <h4 class="card-title">Betting History Details</h4>
                                </div>
                                <div class="card-body">
                                    <div class="table-responsive">
                                        <table class="table align-middle" width="100%" name="betting_history_table" id="betting_history_table">
                                            <thead>
                                                <tr>
                                                    <th>S.no</th>
                                                    <th>Name</th>
                                                    <th>Mobile Number</th>
                                                    <th>Slot Type</th>
                                                    <th>Bet On</th>
                                                    <th>Betting Amount</th>
                                                    <th>Date</th>
                                                    <!-- <th>Time Slote</th> -->
                                                    <th>Status</th>
                                                    
                                                </tr>
                                            </thead>
                                            <tbody class="align-middle">
                                                
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
            </div>
        </div>
    </div>
</div>
</div>
<!-- END: Content-->
<!-- Modal -->