<?php
// error_reporting(E_ALL & ~E_NOTICE);
defined('BASEPATH') OR exit('No direct script access allowed');
class Reports extends MX_Controller
{
    public function __construct()
    {
      parent::__construct();
      /*if user not loged in redirect to home page*/
      modules::run('admin/admin/is_logged_in');
      $this->load->model('task_list/Allfiles_model'); 
      $this->load->library('my_file_upload');

    }

    public function index()
    {
      $where = '';
      $data['file'] = 'reports/daily_money_deposit_list';
      $data['custom_js']  = 'reports/all_files_js';
      $data['validation_js']       = 'admin/all_common_js/frontend_validation_admin';
      $this->load->view('admin_template/main',$data);  
    }
    public function data()
    {
        $api_url = "http://demo.esytrading.com/api/deposits/deposits";
         $pass_data = [                
                                    
                        "from_date"=>$this->input->post('from_date'),
                        "to_date"=>$this->input->post('to_date'),
                    ];
         $d = json_encode($pass_data);
         $curl_pass = array(
            "data"=>$d,
            "url"=>$api_url,
            "type"=>"GET",
            "header" => array(
              'Content-Type:application/json',
              'X-API-KEY: easy_trade',
              'App-Secret: 1234'
            )
        );					
        
    $response = _curl_request($curl_pass,true);

    return $response;
    }
    public function getusers()
    {       

            $draw = intval($this->input->get("draw"));
            $start = intval($this->input->get("start"));
            $length = intval($this->input->get("length"));
            $data['user_list']=$this->data(); 
            $user_list=$this->data();
            $i = 1;
            if($user_list['message'] == "Data not found")
            {

                  $data_users['data'] = "";

            }
            else
            { 
            foreach($user_list['data'] as $users) {
            $status = '';
            $edit_action = '';
            $delete_action = '';
            $password='';
            $data_users[] = array( 



              '<td class="align-middle">'.$i++.'</td>',
              '<td class="align-middle">'.$users['full_name'].'</td>',
              '<td class="align-middle">'.$users['phone'].'</td>',
              '<td class="align-middle">'.$users['email'].'</td>',
              '<td class="align-middle">'.$users['deposite_amount'].'</td>',
              '<td class="align-middle">'.$users['transtion_on'].'</td>',
              // '<td class="align-middle">'.$status.'</td>', 


            );
          }
        }
      

          $result = array(
           "draw" => $draw,
           "recordsTotal" => count($user_list),
           "recordsFiltered" => count($user_list),
           "data" => $data_users,
         );

      echo json_encode($result);  
      }


      public function bet_data()
    {
      $where = '';
      $data['file'] = 'reports/betting_history_list';
      $data['custom_js']  = 'reports/all_files_js';
      $data['validation_js']       = 'admin/all_common_js/frontend_validation_admin';
      $this->load->view('admin_template/main',$data);  
    }
    public function betting_report_get_users()
    {


      $where = [];
      
        // date("m-d-Y", strtotime($_POST['to_date']));  
          

            $from_date =  $_POST['from_date'];
            $to_date = $_POST['to_date'];  
            $from_date_col = 'a.create_without_time >='; 
            $to_date_col = 'a.create_without_time <='; 


            if (isset($_POST['to_date'],$_POST['from_date']) && !empty($_POST['to_date']) && !empty($_POST['from_date']))
            {
               
                $where[] = ['column' => $from_date_col, 'value' => $from_date];
                $where[] = ['column' => $to_date_col, 'value' => $to_date];
            }
          

      $draw = intval($this->input->get("draw"));
      $start = intval($this->input->get("start"));
      $length = intval($this->input->get("length"));
       $row_type = "array";
        $order_by =  ["column" => "a.invest_id", "Type" => "DESC"];
        $array = [
            "fileds" => "a.*,b.full_name as full_name,b.phone as phone",
            "table" => 'tb_user_invests as a',
            "join_tables" => [['table' => 'tb_users as b','join_on' => 'a.user_id = b.user_id','join_type' => 'left']],
            "where" => $where,           
            "row_type" => $row_type, 
            "order_by" => $order_by,               
        ]; 
 
        $all_users = $this->Allfiles_model->GetDataFromJoin($array);

        // echo $this->db->last_query();
        $data_users = array();

       $i = 1;


      foreach($all_users as $users) {

        $password='';
        if($users['is_win'] == "win") {
          $status = "<span class='btn btn-sm btn-outline-primary'>Bet Win</span>";
        } else {
          $status = "<span class='btn btn-sm btn-outline-danger'>Bet Lose</span>";
        }
        $data_users[] = array( 



          '<td class="align-middle">'.$i++.'</td>',
          '<td class="align-middle">'.$users['full_name'].'</td>',
          '<td class="align-middle">'.$users['phone'].'</td>',
          '<td class="align-middle">'.$users['account_type'].'</td>',
          '<td class="align-middle">'.$users['invest_type'].'</td>',
          '<td class="align-middle">'.$users['invest_amount'].'</td>',
          '<td class="align-middle">'.$users['created_on'].'</td>',
          '<td class="align-middle">'.$status.'</td>', 


        );
      }

      $result = array(
       "draw" => $draw,
       "recordsTotal" => count($all_users),
       "recordsFiltered" => count($all_users),
       "data" => $data_users,
     );

      echo json_encode($result);



    }

    public function slot_report()
    {
      
      $data['file'] = 'reports/slots_list';
      $data['custom_js']  = 'reports/all_files_js';
      $data['validation_js']       = 'admin/all_common_js/frontend_validation_admin';
      $this->load->view('admin_template/main',$data);  
    }

    public function getslots_report()
    {
        $where = [];
      
        // date("m-d-Y", strtotime($_POST['to_date']));  
          

            $from_date =  $_POST['from_date'];
            $to_date = $_POST['to_date'];  
            $from_date_col = 'a.slot_date >='; 
            $to_date_col = 'a.slot_date <='; 


            if (isset($_POST['to_date'],$_POST['from_date']) && !empty($_POST['to_date']) && !empty($_POST['from_date']))
            {
               
                // $where[] = ['column' => $from_date_col, 'value' => $from_date];
                // $where[] = ['column' => $to_date_col, 'value' => $to_date];
                $where[] = [$from_date_col =>$from_date ] ; 
                $where[] = [$to_date_col =>$to_date ] ; 

              }
          
      $where = ['`slot_status`' => '1']  ;    
      $draw = intval($this->input->get("draw"));
      $start = intval($this->input->get("start"));
      $length = intval($this->input->get("length"));
       $type = "array";
      $all_slot_reports =  $this->Allfiles_model->GetDataAll("tb_section_slots",$where,$type,'slot_id',$limit='');
      // echo $this->db->last_query();
      $data_slot = array();
       $i = 1;
      foreach($all_slot_reports as $slot_reports) {

        $password='';
        if($slot_reports['slot_status'] == 1) {
          $status = "<span class='btn btn-sm btn-outline-primary'>Close</span>";
        } else {
          $status = "<span class='btn btn-sm btn-outline-danger'>Progress</span>";
        }
         $slot_start_time = $slot_reports['slot_start_time'];
         $slot_end_time = $slot_reports['slot_end_time'];

         $slot_type = $slot_reports['slot_type'];

         if ($slot_type == "real") 
         {

            $slot_mode = "Real";
            
         }
         else
         {
           $slot_mode = "Demo";
         }

        $data_slot[] = array( 




          '<td class="align-middle">'.$slot_reports['slot_id'].'</td>',
          '<td class="align-middle">'.$slot_mode.'</td>',
          '<td class="align-middle">'.$slot_reports['slot_up_users'].'</td>',
          '<td class="align-middle">'.$slot_reports['slot_down_users'].'</td>',
          '<td class="align-middle">'.$slot_reports['slot_total_up_amount'].'</td>',
          '<td class="align-middle">'.$slot_reports['slot_total_down_amount'].'</td>',
          '<td class="align-middle" width="2%">'.$slot_start_time.'</td>',
          '<td class="align-middle" width="2%">'.$slot_end_time.'</td>',
          '<td class="align-middle">'.$slot_reports['slot_winning_check'].'</td>',
          '<td class="align-middle">'.$slot_reports['slot_won_type'].'</td>',
          '<td class="align-middle">'.$status.'</td>',

        );
      }

      $result = array(
       "draw" => $draw,
       "recordsTotal" => count($all_slot_reports),
       "recordsFiltered" => count($all_slot_reports),
       "data" => $data_slot,
     );

      echo json_encode($result); 




    }
    

    
}