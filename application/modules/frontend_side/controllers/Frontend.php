<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Frontend extends MX_Controller

{
    private $tbl_spares = "tb_spares";
    private $tbl_users = "tb_users";
    private $tbl_trans = "tb_transactiontype";
    private $tbl_spares_order = "tb_spares_order";
    private $tbl_orders = "tb_orders";

    public function __construct()

    {

        parent::__construct();

        $this->load->model('task_list/Allfiles_model');
        $this->load->model("general_model", "general");
        $this->load->model('User_model');
        $this->load->model('Restapi_model');
    }



    public function index()

    {


        // $data['file'] = 'frontend_side/index';
        
        $this->load->view('frontend_side/index'); 

    }
    public function contactus()
    {

        $data['file'] = 'frontend_side/contact';
        $data['custom_js'] = 'frontend_side/custom_js';
        $this->load->view('user_template/main', $data);
    }

    public function login()

    {

        $data['file'] = 'frontend_side/login';

        $data['validation_js']       = 'frontend_side/frontend_validation_js';

        $this->load->view('user_template/main', $data);
    }





    public function about()

    {
        $data['file'] = 'frontend_side/about';

        $this->load->view('user_template/main', $data);
    }

    // banks


    public function error_page()
    {

        $data['file'] = 'frontend_side/404';

        $this->load->view('user_template/main', $data);
    }


    public function products()
    {

        $data['file'] = 'frontend_side/products';

        $this->load->view('user_template/main', $data);
    }


    public function testimonial()
    {

        $data['file'] = 'frontend_side/testimonial';

        $this->load->view('user_template/main', $data);
    }




    public function download()
    {
        $data['file'] = 'frontend_side/download';
        $this->load->view('user_template/main', $data);
    }






    public function savecontact()

    {

        if (isset($_POST['name']) && !empty($_POST['name'])) {



            $contactusadd  = array(

                'name'  => $_POST["name"],

                'email_address' => $_POST["email"],

                'phone_no' => $_POST["phone"],

                'some_message' => $_POST['message'],

                'created_at' => date('Y-m-d H:i:s'),

                'status' => 1

            );



            $result = $this->Allfiles_model->data_save("tb_contact_us", $contactusadd);



            if ($result) {

                $this->session->set_flashdata('success', 'Contact Us service successfully. We will contact you to shortly');

                redirect(base_url(''));
            } else {

                $this->session->set_flashdata('error', 'Contact Us Service failed! Please try again.');

                redirect(base_url('contactus'));
            }
        } else {

            $this->session->set_flashdata('error', 'Something went wrong! Please try again.');

            redirect(base_url('contactus'));
        }
    }

    public function privacy_policy()
    {
        $data['file'] = 'frontend_side/Privacy_policy';
        $this->load->view('user_template/main', $data);
    }
    public function terms_conditions()
    {
        $data['file'] = 'frontend_side/Terms&conditions';
        $this->load->view('user_template/main', $data);
    }
    public function fare_policy()
    {
        $data['file'] = 'frontend_side/Fare_policy';
        $this->load->view('user_template/main', $data);
    }
    public function loadHTML()
    {
        $id = 3;
        $where = [
            ['column' => 'o.order_id', 'value' => $id]
        ];
        $array = [
            "fileds" => "s.*, u.*,o.*",
            "table" => $this->tbl_orders . ' AS o',
            "join_tables" => [
                ["table" => $this->tbl_users . ' AS u', "join_on" => 'u.user_id = o.user_id', "join_type" => "left"],
                ["table" => $this->tbl_spares_order . ' AS so', "join_on" => 'so.order_id = o.order_id', "join_type" => "left"],
                ["table" => $this->tbl_spares . ' AS s', "join_on" => 's.spare_id = so.spare_id', "join_type" => "left"]
            ],
            "where" => $where,
            "like" => [],
            "limit" => [],
            "order_by" => [],
        ];
        $get = $this->general->get_all_from_join($array);
        $html_data = $get['resultSet'];
        $data['html_data'] =  $get['resultSet'];;
        // echo "<pre>";
        // print_r($html_data);
        // echo "</pre>";

        $this->load->view('order_html', $data);
    }
}
