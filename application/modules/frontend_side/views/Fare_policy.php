
	<div class="container">
		<div class="row">
			<div class="col-lg-12 text-center mt-25 mb-15">
				<h3>REFUND & CANCELLATION POLICY : </h3>
			</div>
		</div>
	</div>
	
	<div class="container text-justify-p">
		 
		
		<div class="row">
			<div class="col-lg-12">
				<p>
				1. These User terms and conditions (together with the documents referred to in it) (“Terms”) are the terms on which you may make use of our website www.esytrading.com  and the associated mobile and software applications (either existing or available in the future) and our digital assets trading services (“Online Platforms” whether as a guest or a registered user. Use of the Online Platform includes accessing, browsing, or registering to use the Online Platforms. Please read these Terms carefully before you start to use the Online Platforms. We recommend that you print a copy of these Terms for future reference.

<br><br>
					2.	Esy Trade  has implemented a transparent refund policy. In case you are not satisfied with our service level, you can request a refund.
<br><br>
					3.	If an account was funded via debit or credit card the funds can be refunded to the card.
<br><br>
					4.	Please contact Esy Trade  Customer Support in this case: via any convenient method posted on 	
					
					<a href="https://www.esytrading.com/contact-us/">https://www.esytrading.com/contact-us/.</a> Your request will be reviewed with-in 5 business days and replied to in a timely manner. After reviewing the request, it usu-ally takes maximum 30 days for the refund to be executed. Please note that any viola-tions of Esy Trade Customer Agreement or Partner Agreement or any other legal regu-lations cannot be a subject to refund request. The same applies to any profits or losses received as a result of your trading activity.
<br><br>
					5.	Our focus is complete customer satisfaction. In the event, if you are displeased with the services provided, we will refund back the money, provided the reasons are genuine and proved after investigation. Please read the fine prints of each deal before buying it, it provides all the details about the services or the product you pur-chase.
<br><br>
					6.	This policy can be modified or edited without prior notice
<br><br>
					7.	For each Trading you did on the website or Mobile app , After Successful Gaining or loosing depends completely on the market Risks.
<br><br>
					8.	Money debited from your account and any discrimination in adding to your wallets will be solved within 3 Business Working Days through email support
<br><br>
					<strong>Money deposit  Issues : <a href="mailto: support@esytrading.com">support@esytrading.com</a> </strong>
<br><br>
					9.	Withdrawals will be Credited to the customer account within 2 minutes to 3 hours. Any issues regarding withdrawals will be solved within 2 Business Working days through email support
<br><br>
					<strong>Money Withdrawal  Issues : <a href="mailto:support@esytrading.com">support@esytrading.com</a></strong>
<br><br>
					10.	If Any Error Regarding Trading , customer can contact the company through support op-tion available @Dashboard in the login page . Option Availble on both the website and mobile app through email support.
<br><br>       
					<strong>Trading Issues :  <a href="mailto:care@esytrading.com">care@esytrading.com</a> </strong>

				</p>
			</div>
		</div>

		
	</div>
