<!-- CONTENT -->
<!-- Intro Section -->
<section class="inner-intro  padding About-bg overlay-dark light-color">
  <div class="container">
    <div class="row title">
      <h1>Download</h1>
      <div class="page-breadcrumb">
        <a href="<?php echo base_url(); ?>">Home</a> / <span>Download</span>
      </div>
    </div>
  </div>
</section>
<!-- Intro Section -->

<section class="about-1">
    <div class="container">
        <div class="row justify-content-lg-end justify-content-center align-items-center">
            <div class="col-lg-6 d-lg-block d-none left-img">
                <img src="../user_assets/assets/images/left-img.png" class="img-fluid" alt="">
            </div>
            <div class="col-lg-6">
                <div class="about-1-right">
                    <h2 class="section-title">Creative & Profitable Trading App , Your Own <span>EsyTrade App to Earn Money.</span></h2>
<!--                    <p>Occaecat cupidatat non proident, sunt in culpa que officia deserunt and mollit anim id est laborum aecat and cupidatat non proident, sunt in culpa.</p>-->
                    <div class="btn-group-area">
                        <a href="https://play.google.com/store/apps/details?id=com.myproduct.easytrade" target="_blank" class="app-btn-1"><img src="../user_assets/assets/images/google-play.png" class="img-fluid" alt=""></a>
<!--                        <a href="javascript:;" class="app-btn-2"><img src="../user_assets/assets/images/app-store.png" class="img-fluid" alt=""></a>-->
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>