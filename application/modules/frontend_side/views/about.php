﻿  <!-- CONTENT --> 
  <!-- Intro Section -->
<section class="inner-intro  padding About-bg overlay-dark light-color">
				<div class="container">
					<div class="row title">
						<h1>About us</h1>
						<div class="page-breadcrumb">
							<a href="<?php echo base_url(); ?>">Home</a> / <span>About us</span>
						</div>
					</div>
				</div>
			</section>

  <!-- Intro Section --> 
  <!-- About Section -->
  <div id="about-section" class="pt-80 pt-xs-60">
    <div class="container">
      <div class="row">
        <div class="col-sm-6 col-md-8">
          <div class="block-title v-line">
            <h2><span>Esy </span> Trade Platform</h2>
<!--            <p class="italic"> Aenean suscipit eget mi act </p>-->
          </div>
          <div class="text-content">
            <p style=" line-height: 1.9; "><strong>Esy Trade Platform</strong>.We pursue to be a team which can co-create with you and turn your business ideas into a successful product/solution.We are a reputed full-cycle mobile application development company which commenced its operations in 2019, and ever since, we have helped our clients lead in the business domain. We offer comprehensive solutions in Healthcare, IoT, and Multimedia that empower you to stand apart.</p>
          </div>
          <div class="post-content">
            <div class="post-img"> <img class="img-responsive" src="<?php echo base_url(); ?>user_assets/assets/images/blog.jpg" alt="Photo"> </div>
          </div>
        </div>
        <div class="col-sm-6 col-md-4">
          <div class="dark-bg our-vision light-color padding-40">
            <div class="block-title">
              <h2><span>we </span> Provide</h2>
            </div>
            <p>Cumulations is a technology-driven company with a team of talented technical architects, developers, and designers. The management team comes with extensive experience in the services industry and mobility domain and boasts over 5+ years of combined experience.<br><br>We Are the Leading Api Provider of Trading . we Provide Software Accessible Easy</p>
			  
<!--
			  <p>An IoT solution is always a right blend of hardware, software and cloud infrastructure. We excel in all these areas with the right teams and partnerships. Our expertise lies in building Android and iOS applications supported by a strong cloud-based backend infrastructure.</p>
			  
-->
          </div>
        </div>
		
		  <div class="col-sm-12 col-md-12">
          <div class="pb-15 pt-15">
			  <p>An IoT solution is always a right blend of hardware, software and cloud infrastructure. We excel in all these areas with the right teams and partnerships. Our expertise lies in building Android and iOS applications supported by a strong cloud-based backend infrastructure.</p>
          </div>
        </div>
		  
      </div>
    </div>
  </div>
  <!-- About Section End--> 

  <!-- Mission Section -->


