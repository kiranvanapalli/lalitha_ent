﻿
			<!-- CONTENT -->
			<!-- Intro Section -->
			<section class="inner-intro  padding CONTACT-bg overlay-dark light-color">
				<div class="container">
					<div class="row title">
						<h1>Contact</h1>
						<div class="page-breadcrumb">
							<a href="<?php echo base_url(); ?>">Home</a>/<span>Contact</span>
						</div>
					</div>
				</div>
			</section>
			<!-- End Intro Section -->
			<!-- Contact Section -->
			<section class="padding ptb-xs-60">
				<div class="container">

					<div class="row">

						<div class="col-sm-8">

							<div class="headeing pb-30">
								<h2>Get in Touch</h2>
								<span class="b-line l-left line-h"></span>
							</div>
							<!-- Contact FORM -->
							<form class="contact-form " id="contact" method="post" name="contactform" action=<?php echo base_url(); ?>frontend_side/Frontend/savecontact>
								<!-- IF MAIL SENT SUCCESSFULLY -->
								<div id="success">
									<div role="alert" class="alert alert-success">
										<strong>Thanks</strong> for using our template. Your message has been sent.
									</div>
								</div>
								<!-- END IF MAIL SENT SUCCESSFULLY -->
								<div class="row">
									<div class="col-sm-12">
										<div class="form-field">
											<input class="input-sm form-full" id="name" type="text" name="name" placeholder="Your Name">
										</div>
										<div class="form-field">
											<input class="input-sm form-full form-control" id="email" type="text" name="email" placeholder="Email" >
										</div>
										<div class="form-field">
											<input class="input-sm form-full" id="phone" type="text" name="phone" placeholder="Mobile Number">
										</div>
										<div class="form-field">
											<textarea class="form-full" id="message" rows="7" name="message" placeholder="Your Message" ></textarea>
										</div>
									</div>
									<div class="col-sm-12 mt-30">
										<button class="btn-text wprt-container" type="submit" id="submit_form" name="submit_form">
											Send Message
										</button>
									</div>
								</div>
							</form>
							<!-- END Contact FORM -->
						</div>
						
						

						<div class="col-sm-4 contact">
							<div class="headeing pb-20">
								<h2>Contact Info</h2>
								<span class="b-line l-left line-h"></span>
							</div>
							<div class="contact-info">
								<ul class="info">
									<li>
										<div class="icon ion-ios-email"></div>
										<div class="content">
											<p>
												<a href="mailto: Support@esytrading.com"><span><strong>Payment Issues :</strong> Support@esytrading.com</span></a>
											</p>
											<p>
												<a href="mailto: care@esytrading.com"><span><strong>Trading Issues :</strong> care@esytrading.com</span></a>
											</p>
										</div>
									</li>
									
									<li>
										<div class="icon ion-ios-time"></div>
										<div class="content" style="line-height: 2.5;">
											<p>
												24/7 Customer Support
											</p>
											
										</div>
									</li>
								</ul>
								<ul class="event-social">
									<li></li>
									<li></li>
									<li>
										<a href="#"><i class="fa fa-facebook" aria-hidden="true"><span>facebook</span></i></a>
									</li>
								</ul>
							</div>
						</div>

					</div>
				</div>
				<!-- Map Section -->

			</section>