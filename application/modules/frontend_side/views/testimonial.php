﻿
			<!-- CONTENT -->
			<!-- Intro Section -->
			<section class="inner-intro  padding TESTIMONIAL-bg overlay-dark light-color">
				<div class="container">
					<div class="row title">
						<h1>Testimonial</h1>
						<div class="page-breadcrumb">
							<a href="<?php echo base_url(); ?>">Home</a>/<span>Testimonial</span>
						</div>
					</div>
				</div>
			</section>
			<!-- End Intro Section -->
			<!-- Testimonial section -->
			<div id="testimonial-section" class="padding">
				<div class="container">
					<div class="row ">
						<div class="col-sm-12">
							<div class="heading-box pb-30">
								<h2><span>Our</span> Testimonial</h2>
								<span class="b-line l-left"></span>
							</div>
						</div>
					</div>
					<div class="row mt-60">
						<div class="col-sm-6">
							<div class="about-block gray-bg   padding-40 clearfix">
								<div class="client-avtar">
									<img src="<?php echo base_url(); ?>user_assets/assets/images/Female-Avater.jpg" class="img-responsive" alt="Responsive image">
								</div>
								<div class="text-box">
									<div class="text-content">
										<p>
											I Love This App Because It is Providing Demo Trading Accounts We Can Practise in Demo And Later We Can Trade In Real Account. Very Easy To Trade .
										</p>
									</div>
									<div class="box-title">
										<h4><span>By</span>Raghu Yadav </h4>
									</div>
								</div>
							</div>
						</div>
						<div class="col-sm-6">
							<div class="about-block dark-gray-bg  padding-40 clearfix">
								<div class="client-avtar left-pos">
									<img src="<?php echo base_url(); ?>user_assets/assets/images/Male-Avater.jpg" class="img-responsive" alt="Responsive image">
								</div>
								<div class="text-box">
									<div class="text-content">
										<p>
											Just Daily 1 Hr Trading In This company I Am Earning My Freelance Income. Prices Option I love the Most.
										</p>
									</div>
									<div class="box-title">
										<h4><span>By</span> Anil Kumar Ikea</h4>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="row mt-80">
						<div class="col-sm-6">
							<div class="about-block dark-gray-bg   padding-40 clearfix">
								<div class="client-avtar">
									<img src="<?php echo base_url(); ?>user_assets/assets/images/Male-Avater.jpg" class="img-responsive" alt="Responsive image">
								</div>
								<div class="text-box">
									<div class="text-content">
										<p>
											My Planning Daily 10% Of My Investment Is More Than Enough For Me. 
										</p>
									</div>
									<div class="box-title">
										<h4><span>By</span> Supriya Negi</h4>
									</div>
								</div>
							</div>
						</div>
						<div class="col-sm-6">
							<div class="about-block gray-bg  padding-40 clearfix">
								<div class="client-avtar left-pos">
									<img src="<?php echo base_url(); ?>user_assets/assets/images/Male-Avater.jpg" class="img-responsive" alt="Responsive image">
								</div>
								<div class="text-box">
									<div class="text-content">
										<p>
											 Previously Only In Websites We Traded. Recently They launched Ios App. Very easy And new Features added.
										</p>
									</div>
									<div class="box-title">
										<h4><span>By</span> Harsha</h4>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="row mt-80">
						<div class="col-sm-6">
							<div class="about-block gray-bg   padding-40 clearfix">
								<div class="client-avtar">
									<img src="<?php echo base_url(); ?>user_assets/assets/images/Male-Avater.jpg" class="img-responsive" alt="Responsive image">
								</div>
								<div class="text-box">
									<div class="text-content">
										<p>
											I Am Interested In Crypto Currencies And I Started Trading In Many Apps & Earned From Many Bitcoin apps. In Esy Trading Process Is very easy For Trade And to earn.
										</p>
									</div>
									<div class="box-title">
										<h4><span>By</span> Ali Mohammed</h4>
									</div>
								</div>
							</div>
						</div>
						<div class="col-sm-6">
							<div class="about-block dark-gray-bg  padding-40 clearfix">
								<div class="client-avtar left-pos">
									<img src="<?php echo base_url(); ?>user_assets/assets/images/Female-Avater.jpg" class="img-responsive" alt="Responsive image">
								</div>
								<div class="text-box">
									<div class="text-content">
										<p>
											Daily Trade on Coins Investment I liked The Most. No Matter Whether The Prices Are Increasing Or Decreasing. Just Sell And Buy Option We Can Earn Huge Money. Risk Level is Also High, Good Planning and Advisor should be There
										</p>
									</div>
									<div class="box-title">
										<h4><span>By</span> Sujatha Mani</h4>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<!-- Testimonial section end -->
			<!--End Contact-->
			<!-- FOOTER -->
