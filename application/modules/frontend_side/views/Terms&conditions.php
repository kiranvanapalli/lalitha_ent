	<div class="container">
		<div class="row">
			<div class="col-lg-12 text-center mt-25 mb-15">
				<h3>Terms & Conditions</h3>
			</div>
		</div>
	</div>
	
	<div class="container text-justify-p">
		<div class="row">
			<div class="col-lg-12">
				<h4><strong>Legal & Compliance</strong></h4>
				<h6>PLEASE READ THESE TERMS AND CONDITIONS CAREFULLY BEFORE USING THE ONLINE PLATFORMS</h6>
			</div>
		</div> 
		
		<div class="row">
			<div class="col-lg-12">
				<h4><strong>A.	INTRODUCTION: </strong></h4>
				<p>1. These User terms and conditions (together with the documents referred to in it) (“Terms”) are the terms on which you may make use of our website www.esytrading.com  and the associated mobile and software applications (either existing or available in the future) and our digital assets trading services (“Online Platforms” whether as a guest or a registered user. Use of the Online Platform includes accessing, browsing, or registering to use the Online Platforms. Please read these Terms carefully before you start to use the Online Platforms. We recommend that you print a copy of these Terms for future reference.
<br><br>
				2. By using the Online Platforms, you confirm that you accept these Terms and that you agree to comply with them. 
<br><br>
				3. If you do not agree to these Terms, you must not use the Online Platforms If you do not agree with any amendments made by to these Terms at any time in future, you must stop using the Online Platforms with immediate effect.    
				
				</p>
			</div>
		</div>
		
		<div class="row">
			<div class="col-lg-12">
				<h4><strong> B. OTHER APPLICABLE TERMS  </strong></h4>
				<p>
1. These Terms refer to, incorporate, and include the following additional terms, which also apply to your use of the Online Platforms: 
<br><br>
2. Our Privacy Policy, which sets out the terms on which we process any personal data we collect from you, or that you provide to us. By using the Online Platforms, you consent to such processing and you warrant that all data provided by you is accurate.
<br><br>
 3. Our Anti-Money Laundering Policy, which sets out the terms regarding identity verification of our Users and the procedures followed for early identification and reporting of prohibited/illegal activities which may be committed by using our services. When using the Online Platforms, you must comply with the terms of the AML Policy.
 <br><br>
					4. Our Beta Testing Terms & Conditions, which sets out the terms and conditions you must agree to in order to participate as a Beta tester and/or Licensee.
   
				
				</p>
			</div>
		</div>
		
		<div class="row">
			<div class="col-lg-12">
				<h4><strong> C. DEFINITIONS: </strong></h4>
				<p>

					1. “Coin Wallet” means an online address accessible through the Online Platforms and operated by a User for storage of its Digital Assets; 
<br><br>
					2. “Content” means any information, text, graphics, or other materials uploaded by the Company or the users, which appears on the Online Platforms for other users to access. 
<br><br>
					3. “Digital Assets” refers to blockchain based crypto currencies such as Bitcoins, Litecoin, Ethereum, Ripple, etc., as well as proprietary coins and tokens; 
<br><br>
					4. “Fiat Wallet” means an online address accessible through the Online Platforms and operated by a User for the storage of the User’s fiat currency holdings;
 <br><br>
					5. “Funds” refers to both Digital Assets and fiat currency, as the case maybe;
 <br><br>
					6. “Linked Bank Account” refers to any bank account owned and operated by the User and held with a Scheduled Commercial Bank, whose details were provided by the User during the activation process mentioned below; 
<br><br>
					7. “Sanctions Lists” has the meaning given to it under the AML Policy;
<br><br>
					8. “Wallet(s)” refers to a User’s Coin Wallet and/or its Fiat Wallet, as the case maybe; 
<br><br>
					9. Any reference to “you” or “your” or “user” refers to you as a user of the Online Platforms and the Services and any reference to “we”, “our” and “us” shall refer to the Company, its subsidiaries, affiliated entities, permitted assigns (as and when applicable) as the provider of the Services.

				</p>
			</div>
		</div>
		
		<div class="row">
			<div class="col-lg-12">
				<h4><strong>       D. CHANGES TO THESE TERMS AND CONDITIONS : </strong></h4>
				<p>

					1. We may revise these Terms at any time by amending this page. Please check this page from time to time to take notice of any changes we made, as they are binding on you. If you do not agree with any part of these Terms or any amendments made to these Terms from time to time, please stop using the Online Platform and the Servicers with immediate effect.
 <br><br>
					2. We may, without prior notice, change the Services; add or remove functionalities or features; stop providing the Services or features of the services, to you or to users generally; or create usage limits for the Services.

				</p>
			</div>
		</div>
		
		<div class="row">
			<div class="col-lg-12">
				<h4><strong> E. CHANGES TO THE ONLINE PLATFORMS :  </strong></h4>
				<p>

					1. We may update the Online Platforms from time to time and may change the Content at any time. However, please note that any of the Content on the Online Platforms may be out of date at any given time and we are under no obligation to update it.
 <br><br>
					2. We do not guarantee that the Online Platforms, or any Content on it, will be free from errors or omissions.

				
				</p>
			</div>
		</div>
		
		<div class="row">
			<div class="col-lg-12">
				<h4><strong> F. ACCESSING THE ONLINE PLATFORMS:   </strong></h4>
				<p>

					1. We do not guarantee that your use of the Online Platforms, will always be available or be uninterrupted. Access to the Online Platforms is permitted on a temporary basis. We may suspend, withdraw, discontinue or change all or any part of the Online Platforms without notice. We will not be liable to you including without limitation for any losses incurred due to volatility of prices of the Digital Assets if for any reason the Online Platforms are unavailable at any time or for any period.
<br><br>
 					2. You are also responsible for ensuring that all persons who access the Online Platforms through your internet connection are aware of these Terms and other applicable terms and conditions, and that they comply with them

   
				
				</p>
			</div>
		</div>
		
		<div class="row">
			<div class="col-lg-12">
				<h4><strong> G. YOUR ACCOUNT AND PASSWORD:   </strong></h4>
				<p>

					1. If you choose to register with us through the Online Platforms, an account will be created for your use on the Online Platforms and you will be provided with required Account Information to enable your access to the Account. In order to access any Services, you must activate your account (“User Account”) by following the identity verification process specified in the AML Policy. Failure to complete the User Account activation process as per the AML Policy will entitle the Company to terminate the said User Account. The term “Account Information” refers to a password and such information which may be provided by you as part of our security and verification procedures. If you register on the Online Platforms through any third-party website like Gmail etc., the login information of such third-party account, as the case may be, shall be considered part of the Account Information. You must always treat Account Information as confidential and must not disclose it to any third party. Any access to the Online Platforms through your Account shall be considered as access by you or on your behalf and you shall be solely responsible for any activity carried out in, by or through your Account either on the Online Platforms or any other website accessed by you through the Online Platforms. 
<br><br>
					2. You represent and warrant that: (A) you are competent to contract i.e. (i) if you are an individual, that you are over eighteen years of age, or (ii) that if you are registering on behalf of an entity, that you are authorized to enter into, and bind the entity to, these Terms and register for the Services, in accordance with the applicable laws including applicable G. CHANGES TO THE ONLINE PLATFORMS I. YOUR ACCOUNT AND PASSWORDDEFINITIONS P a g e | 6 laws of India; and (B) you are: (i) not  included in any Sanctions Lists; and (ii) a non-U.S user; C) your membership has not been suspended or terminated by us for any reason whatsoever; and (D) your use of Services will not violate any and all laws and regulations applicable to you, including but not limited to our AML Policy, and applicable regulations on anti-money laundering, anti-corruption, and counter-terrorist financing. If we change the eligibility criteria to be registered with the Online Platforms and you no longer comply with the new eligibility criteria, as determined by us in our sole discretion, you accept that we may close your Account without any liability for us. You are solely responsible for ensuring that you are in compliance with these Terms and with all laws, rules and regulations applicable to you. If your right to access the Services is revoked or use of the Services is in any way prohibited, in such circumstances, you agree not to use or access the Online Platforms or use the Services in any way.
<br><br>
 					3. We have the right to disable your access to the User Account or any part of it, whether chosen by you or allocated by us, at any time, if in our reasonable opinion you have failed to comply with any of the provisions of these Terms, including without limitation the AML Policy. 
<br><br>
					4. If you know or suspect that anyone other than you know or has unauthorized access to your Account Information or any part of it, you must promptly notify us by sending us an email at 
					<a href="mailto:Support@esytrading.com">support@esytrading.com </a>  We are not liable for any losses or other consequences of unauthorised use of your account

				
				</p>
			</div>
		</div>
		
		<div class="row">
			<div class="col-lg-12">
				<h4><strong> H.  USER REPRESENTATIONS, COVENANTS AND OBLIGATIONS:  </strong></h4>
				<p>

					1. These Terms govern your behaviour on the Online Platforms and set forth your obligations. You agree, confirm, and represent the following: 
<br><br>
					2. You shall comply with all the obligations set forth in these Terms, including without limitation the AML Policy. Pursuant to the economic sanctions programs administered or enforced by any country or government or international authority, In the event that we are required to block funds or Digital Assets associated with your User Account or Linked Bank Account in accordance with a sanctions program, or other similar government sanctions programs, we may: (i) suspend your User Account or Linked Bank Account; (ii) terminate your User Account or Linked Bank Account; (iii) return funds or Digital Assets to the destination of their origin or to an account specified by authorities; or (iv) require you withdraw funds or Digital Assets from your User Account or Linked Bank Account within a certain period of time. We are not responsible for any losses, whether direct or indirect, that you may incur as a result of our complying with Applicable Laws, the guidance or direction of any regulatory authority or government agency, or any writ of attachment, lien, levy, subpoena, warrant, or other legal order.
 <br><br>
					3. You will use the Services rendered by us for lawful purposes only and comply with these Terms and all applicable lawsand all applicable judicial orders and precedent (“Applicable Laws”) while using and transacting on the Online Platforms. 
<br><br>
					4. Creation and maintenance of all Content in your Account shall be your sole  responsibility. 
<br><br>
					5. You are responsible for safeguarding the password that you use as a part of your Account Information to access the Services and for any activities or actions under your Account. We encourage you to use “strong” passwords preferably using a combination of upper- and lower-case letters, numbers and symbols with your Account. The Company will not be liable for any loss or damage  arising from your failure to comply with this instruction. You acknowledge that you will irreversibly lose your Digital Assets if you delete your Account.
 <br><br>
					6. You shall provide us with only such information (including without limitation Identification Documents submitted by you) that is true and accurate to the best of your knowledge.
 <br><br>
					7. You shall not cancel any orders initiated but not executed on the exchange. In case any order is partially executed, we may in our sole discretion permit cancellation of the unexecuted order. You acknowledge that all orders and/or transactions are irreversible once executed. 
<br><br>
					8. You must maintain sufficient Funds in your Wallets before initiating any order and/or transaction. In case you have insufficient funds in your Wallet then the Company may either cancel your order or execute a partial transaction using the Funds available in your Wallet. 
<br><br>
					9. You understand that certain taxes may be applicable upon the trading of Digital Assets and you would be required to determine your tax liability under the Applicable Laws. You acknowledge that you’re solely responsible for payment of any taxes that may arise in connection with your use of Services.
 <br><br>
					10. As the price of Digital Assets are very volatile and subject to fluctuation, you acknowledge that the actual market rate at which an order and/or transaction is executed may vary.
 <br><br>
					11. The Company may be required to suspend trading in cases of a force majeure event. You acknowledge that: (i) your access to the Services and/or the Funds during such periods may be limited or restricted; and (ii) the market conditions may differ significantly, following the completion of such Force Majeure Events.
 <br><br>
					12. We do not control the underlying technology which governs the mining, creation, sale of any Digital Assets. You acknowledge the Company does not exercise any control over the market price or circulation or volatility of the Digital Assets and that the contract for sale of any of the Digital Assets shall be a strictly bipartite contract between the seller and the buyer.
 <br><br>
					13. It is require a person to pay money, give something of value, or expend significant effort (in legal terms, "consideration") to enter or participate in an activity or promotion in which he or she may win a prize if there is a significant degree of chance involved (e.g., a random drawing to determine winners) ("Illegal Gambling"). Accordingly, we have an internal gambling policy that prohibits the use of Esy Trade for storing, sending, or receiving wagers or proceeds of Illegal Gambling. Proceeds derived from trading. 

				
				</p>
			</div>
		</div>
		
		<div class="row">
			<div class="col-lg-12">
				<h4><strong> I. FEES:   </strong></h4>
				<p>
		
					1. Creation of an Account on the Online Platforms and usage of the Online Platforms is free.
 <br><br>
					2. Trading on the Online Platform is subject to payment of a fee on each transaction executed (“Transaction Fee”). The Transaction Fee 1% chargeable on each trade shall be as provide in the Fee Schedule.
 <br><br>
					3. Deposit of Digital Assets in the Coin Wallet is free of charge. Deposit of fiat currency in the Fiat Wallet through credit card, debit card or net banking will be subject to a transaction fee of 2%. Fee applicable on withdrawal of Digital Assets or fiat currency shall be as provided in the Fee Schedule. Deposits and withdrawals on Digital Assets or fiat currency will be subject to withdrawal limits provided in the Fee Schedule.

<br><br>
					4. Esy Trade platform will charge for withdrawals as per the chart . 
<br>
   Withdrawals from 10 to 5000 will charge 5% + gst
<br>
   Withdrawals from 5000 to 10000 will charge 7.5%+ gst 

				</p>
			</div>
		</div>
		
		<div class="row">
			<div class="col-lg-12">
				<h4><strong> J. DISCLAIMER OF WARRANTIES :   </strong></h4>
				<p>
				1. You expressly acknowledge and agree that use of the Services and the Online Platforms is at your sole risk. The Services and the Online Platforms are provided on an "as is" and "as available" basis. Although we make best efforts to provide high quality Services to all our users, to the fullest extent allowed by law, we expressly disclaim and waive all warranties and conditions of any kind, whether express or implied, including, but not limited to the warranties of merchantability, fitness for a particular purpose or title. The contents of the Services or the Online Platforms may contain bugs, errors, problems or other limitations. We assume no liability or responsibility for any errors or omissions in Content. 
<br><br>
					2. We are not responsible for the Content uploaded by you on the Online Platforms. We are not responsible for any direct or indirect damages or losses caused to you, including without limitation, lost profits, business interruption or other loss resulting from use of or reliance in any way on anything available on the Online Platforms. It is solely your responsibility to evaluate the accuracy, reliability, completeness and usefulness of Content available on the Online Platforms that is used by you. 
<br><br>
					3. We make no warranty that the Services or the Online Platforms will meet your requirements or that the Services or your access to the Online Platforms will be uninterrupted, timely, accurate or reliable; nor do we make any warranty as to the permanent availability of any information and/or that may be stored or transferred through the Services or the Online Platforms.. In case there is any defect in any software being used for the provision of the Services, we do not make any warranty that defects in such software will be corrected. You understand and agree that any material and/or data downloaded or otherwise obtained through use of the Services or Online Platforms is done at your own discretion and risk and you will be solely responsible for any damage to your computer system or loss of data that results from the download of such material or data. In case you store or transfer any information and/or data through the Services or the Online Platforms, you are strongly advised to make back-up duplicate copies and are solely responsible for any loss. 
<br><br>
					4. No advice or information, whether oral or written, obtained by you from the Services or the Online Platforms shall create any warranty not expressly made herein.

				</p>
			</div>
		</div>
		
		
	</div>
