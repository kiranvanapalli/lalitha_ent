<!DOCTYPE html>

<html>

<meta http-equiv="content-type" content="text/html;charset=utf-8" /><!-- /Added by HTTrack -->
<head>
<link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300" rel="stylesheet">
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title> :: lalitha enterprises :: </title>
<meta name="description" content="Coming Soon lalitha enterprises ">
<link rel="shortcut icon" href="user_assets/assets/img/favicon.png" type="image/x-icon">
<link rel="icon" href="user_assets/assets/img/favicon.png" type="image/x-icon">
<link href="user_assets/assets/css/bootstrap.min.css" rel="stylesheet">
<link href="user_assets/assets/css/font-awesome.min.css" rel="stylesheet">
<link href="user_assets/assets/css/main.css" rel="stylesheet">
</head>
<body>

<!--main-->
<section class="">
  <div class="container">
    <div class="row">
      <div class="col-md-6 col-sm-6"> 
        <!--logo-->
        <a href="<?php echo base_url(); ?>"><div class="logo"><img src="user_assets/assets/img/logo_1.png" data-at2x="user_assets/assets/img/logo_1.png" alt="logo"></div></a>
        <!--logo end--> 
      </div>
      <div class="col-md-6 col-sm-6"> 
        
        <!--social-->
        <div class="social text-center">
          <ul>
            <li><a href="https://twitter.com/" target="_blank"><i class="fa fa-twitter"></i></a></li>
            <li><a href="https://www.facebook.com/" target="_blank"><i class="fa fa-facebook"></i></a></li>
            <li><a href="https://plus.google.com/" target="_blank"><i class="fa fa-google-plus"></i></a></li>
          </ul>
        </div>
        <!--social end--> 
      </div>
    </div>
    <div class="row">
      <div class="col-md-12"> 
        
        <!--welcome-message-->
        <header>
          <h1 class=" text-center" style="padding-bottom: 10px;">Privacy Policy</h1>
			<p>A privacy policy outlines how your website collects, uses, shares, and sells the personal information of your visitors. If you collect personal information from users, you need a privacy policy in most jurisdictions. Even if you aren’t subject to privacy policy laws, being transparent with users about how you collect and handle their data is a best business practice in today’s digital world.<br><br>

Our simple privacy policy template will help you comply with strict privacy laws and build trust with your users.
				<br><br>

Download the free privacy policy template at the bottom of this page, or copy and paste the full text onto your site. If you’d rather let us help you customize a document that’s tailored specifically to your business, our privacy policy generator will create one for you in minutes.</p>
        </header>
        <!--welcome-message end--> 
       
        
      </div>
    </div>
  </div>
	<div class="container">
		<div class="row">
			<div class="col-lg-12 text-center" style="padding: 50px 0px 0px 0px;">
				<small class="wow fadeInUp">&copy; Copyright Start. All Rights Reserved.</small>
			</div>
		</div>
	</div>
</section>
<!--main end--> 


<script src="user_assets/assets/js/jquery-2.1.4.min.js"></script> 
<script src="user_assets/assets/js/wow.min.js"></script> 
<script src="user_assets/assets/js/retina.min.js"></script> 
<script src="user_assets/assets/js/tweetie.min.js"></script> 
<script src="user_assets/assets/js/jquery.downCount.js"></script> 
<script src="user_assets/assets/js/jquery.form.min.js"></script> 
<script src="user_assets/assets/js/jquery.validate.min.js"></script> 
<script src="user_assets/assets/js/jquery.simple-text-rotator.min.js"></script> 
<script src="user_assets/assets/js/main.js"></script> 
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBSUq0I2FCVkhgjNR6fTRbqm19ULreKA4k&amp;callback=initMap"></script> 
<script src="user_assets/assets/js/gmap.js"></script>
</body>

</html>
