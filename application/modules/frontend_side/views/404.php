﻿  <!-- CONTENT -->
  <div class="page-404 ptb-80 ptb-xs-60">
    <div class="container">       
      <!--Footer Info -->
      <div class="row text-center">
        <div class="col-sm-6 col-sm-offset-3"> <strong class="big-font">4<i class="ion-android-happy"></i>4</strong>
          <h2>Page Not Found..</h2>
          <p> Lorem ipsum dolor sit amet, consectetur adipisicing elit. Repudiandae odit iste exerc ssumenda voluptas quidem sit maiores odio velit voluptate. </p>
          <a href="index1.html" class="btn-text mt-15">Back to home page</a> </div>
      </div>
    </div>
  </div>
