<style type="text/css">
.table {
text-align: center;
}
.previewimage
{
margin-left: 40%;
width: 24%;
height: 220px;
}
</style>
<div class="app-content content">
   <div class="content-overlay"></div>
   <div class="content-wrapper">
      <div class="content-header row">
         <div class="content-header-left col-md-6 col-12 mb-2 breadcrumb-new">
            <h3 class="content-header-title mb-0 d-inline-block">Withdraw Information</h3>
            <div class="row breadcrumbs-top d-inline-block">
               <div class="breadcrumb-wrapper col-12">
                  <ol class="breadcrumb">
                     <li class="breadcrumb-item"><a href="<?php echo base_url(); ?>dashboard">Home</a>
                  </li>
                  <li class="breadcrumb-item active">Withdraw Information</li>
               </ol>
            </div>
         </div>
      </div>
      <div class="content-header-right col-md-6 col-12">
         <div class="btn-group float-md-right">
            <a class="btn btn-primary round btn-min-width mr-1 mb-1" aria-haspopup="true" aria-expanded="false" href ="<?php echo base_url(); ?>withdraw"><i class="ft-users"></i> Withdraw List</a>
         </div>
      </div>
   </div>
   <div class="row">
      <div class="col-md-12">
         <div class="card">
            <div class="card-header">
               <h4 class="card-title" id="row-separator-colored-controls">Withdraw Information</h4>
               <a class="heading-elements-toggle"><i class="la la-ellipsis-h font-medium-3"></i></a>
               <div class="heading-elements">
                  <ul class="list-inline mb-0">
                     <li><a data-action="expand"><i class="ft-maximize"></i></a>
                  </li>
               </ul>
            </div>
         </div>
         <div class="card-content collapse show">
            <div class="card-body">
               <form method="post" id='edit_withdraw_form' name='withdraw_form' class='form form-horizontal' enctype="multipart/form-data">
                  <div class="form-body">
                     <h4 class="form-section"><i class="la la-eye"></i> About Withdraw</h4>
                     <div class="row">
                        <div class="col-md-6">
                           <div class="form-group row mx-auto last">
                              <label class="col-md-3 label-control" for="fullname">Full Name</label>
                              <div class="col-md-9">
                                 <input type="text" id="fullname" class="form-control border-primary" placeholder="Full Name" name="fullname" data-toggle="tooltip" data-trigger="hover" data-placement="top" data-title="Full Name" value="<?php echo $details['full_name']; ?>" readonly>
                              </div>
                           </div>
                        </div>
                        <div class="col-md-6">
                           <div class="form-group row mx-auto last">
                              <label class="col-md-3 label-control" for="mobile_number">Mobile Number</label>
                              <div class="col-md-9">
                                 <input type="text" id="mobile_number" class="form-control border-primary" placeholder="Mobile Number" name="mobile_number" data-toggle="tooltip" data-trigger="hover" data-placement="top" data-title="Mobile Number" value="<?php echo $details['phone']; ?>" readonly>
                              </div>
                           </div>
                        </div>
                     </div>
                     <div class="row">
                        <div class="col-md-6">
                           <div class="form-group row mx-auto last">
                              <label class="col-md-3 label-control" for="Email">Email</label>
                              <div class="col-md-9">
                                 <input type="text" id="email" class="form-control border-primary" placeholder="Email" name="email" data-toggle="tooltip" data-trigger="hover" data-placement="top" data-title="Email" value="<?php echo $details['email']; ?>" readonly>
                              </div>
                           </div>
                           
                        </div>
                        <div class="col-md-6">
                           <div class="form-group row mx-auto last">
                              <label class="col-md-3 label-control" for="wallet_amount">Wallet Amount</label>
                              <div class="col-md-9">
                                 <input type="text" id="wallet_amount" class="form-control border-primary" placeholder="Wallet Amount" name="wallet_amount" data-toggle="tooltip" data-trigger="hover" data-placement="top" data-title="Issue Text" autocomplete="off" value="<?php echo $details['wallet_amount']; ?>" readonly>
                              </div>
                           </div>
                           
                        </div>
                     </div>
                     <div class="row">
                        <div class="col-md-6">
                           <div class="form-group row mx-auto last">
                              <label class="col-md-3 label-control" for="bank_name">Bank Name</label>
                              <div class="col-md-9">
                                 <input type="text" id="bank_name" class="form-control border-primary" placeholder="Bank Name" name="Bank Name" data-toggle="tooltip" data-trigger="hover" data-placement="top" data-title="Bank Name" value="<?php echo $details['bank_name']; ?>" readonly>
                              </div>
                           </div>
                           
                        </div>
                        <div class="col-md-6">
                           <div class="form-group row mx-auto last">
                              <label class="col-md-3 label-control" for="branch">Branch</label>
                              <div class="col-md-9">
                                 <input type="text" id="branch" class="form-control border-primary" placeholder="Branch" name="branch" data-toggle="tooltip" data-trigger="hover" data-placement="top" data-title="Issue Text" autocomplete="off" value="<?php echo $details['branch']; ?>" readonly>
                              </div>
                           </div>
                           
                        </div>
                     </div>
                     <div class="row">
                        <div class="col-md-6">
                           <div class="form-group row mx-auto last">
                              <label class="col-md-3 label-control" for="ifsc_code">IFSC Code</label>
                              <div class="col-md-9">
                                 <input type="text" id="ifsc_code" class="form-control border-primary" placeholder="IFSC Code" name="IFSC Code" data-toggle="tooltip" data-trigger="hover" data-placement="top" data-title="Bank Name" value="<?php echo $details['ifsc_code']; ?>" readonly>
                              </div>
                           </div>
                           
                        </div>
                        <div class="col-md-6">
                           <div class="form-group row mx-auto last">
                              <label class="col-md-3 label-control" for="account_number">Account Number</label>
                              <div class="col-md-9">
                                 <input type="text" id="acc_number" class="form-control border-primary" placeholder="Account Number" name="acc_number" data-toggle="tooltip" data-trigger="hover" data-placement="top" data-title="Issue Text" autocomplete="off" value="<?php echo $details['account_number']; ?>" readonly>
                              </div>
                           </div>
                           
                        </div>
                     </div>
                     <div class="row">
                        <div class="col-md-3">
                           <div class="form-group row mx-auto last">
                              <label class="col-md-6 label-control" for="req_amount">Total Amount</label>
                              <div class="col-md-6">
                                 <input type="text" id="total_req_amount" class="form-control border-primary" placeholder="Total Request Amount" name="total_req_amount" data-toggle="tooltip" data-trigger="hover" data-placement="top" data-title="Total Request Amount" value="<?php echo $details['total_request']; ?>" readonly style ="margin-left: 8px;">
                              </div>
                           </div>
                        </div>
                        <div class="col-md-3">
                           <div class="form-group row mx-auto last">
                              <label class="col-md-6 label-control" for="req_amount">Request Amount</label>
                              <div class="col-md-6">
                                 <input type="text" id="req_amount" class="form-control border-primary" placeholder="Request Amount" name="req_amount" data-toggle="tooltip" data-trigger="hover" data-placement="top" data-title="Request Amount" value="<?php echo $details['withdraw_request_amount']; ?>" readonly>
                              </div>
                           </div>
                        </div>
                        <div class="col-md-6">
                           <div class="form-group row mx-auto last">
                              <label class="col-md-3 label-control" for="admin_approved">Admin Response</label>
                              <div class="col-md-9">
                                  <select class="form-control" name="admin_approved" id="admin_approved">
                                       <option value="">Select Response</option>
                                       <option value="approved">Approved</option>
                                       <option value="rejected">Rejected</option>
                                  </select>
                              </div>
                           </div>
                        </div>
                        
                     </div>
                  </div>
                  <h4 class="form-section"><i class="la la-user"></i> Admin Comment</h4>
                  <div class="form-group row mx-auto last text-center">
                     <div class="input-group col-md-12 text-center">
                        <textarea class="textarea" id="admin_comments" name="admin_comments" placeholder="Place Enter Comment" style="width: 257%; height: 250px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;"></textarea>
                     </div>
                  </div>
              
               <div class="form-actions text-right">
                  <button type="button" class="btn btn-warning mr-1"> <i class="la la-remove"></i> Cancel</button>
                  <button type="submit" class="btn btn-primary"> <i class="la la-check"></i> Update</button>
               </div>
               <input type="hidden" name="edit_id" id="edit_id" value="<?php echo $details['withdraw_id']; ?>">
            </form>
         </div>
      </div>
   </div>
</div>
</div>
</div>
</div>