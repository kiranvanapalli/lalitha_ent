<script>
$(document).ready(function(){
         $(document).on('submit', '#edit_withdraw_form', function(event){
          event.preventDefault();
                var admin_approved = $('#admin_approved').val();
              
                if (admin_approved == '') {
                    toastr["error"]("Please Select Admin Response");
                    
                    return false;
                }
                
          $.ajax({  
               url:"<?php echo base_url() ?>update_withdraw_details",  
               method:'POST',  
               data:new FormData(this),
               contentType:false,  
               processData:false, 
               dataType:'JSON',
               success:function(data)  
               {  
                  if(data.status == 1)
                  { 
                      
                      toastr["success"]("Withdraw Details Send success!");
                      window.location.href = "<?php echo base_url();?>withdraw";
                  }
                  else
                  {    
                         toastr["error"]("Withdraw Details updated failed! Please try again.");
                         return false;
                        
                  }

                    
               }       
          });   
      });
  });
</script>