<?php
// error_reporting(E_ALL & ~E_NOTICE);
defined('BASEPATH') OR exit('No direct script access allowed');
class Withdraw extends MX_Controller
{
    public function __construct()
    {
      parent::__construct();
      /*if user not loged in redirect to home page*/
      modules::run('admin/admin/is_logged_in');
      $this->load->model('task_list/Allfiles_model'); 
      $this->load->library('my_file_upload');

    }

    public function index()
    {
      $where = '';
      $data['file'] = 'withdraw/withdraw_list';
      $data['custom_js']  = 'withdraw/all_files_js';
      $data['validation_js']       = 'admin/all_common_js/frontend_validation_admin';
      $this->load->view('admin_template/main',$data);  
    }
    public function getwithdrawrequest()
    {
        $where ='';
        $draw = intval($this->input->get("draw"));
        $start = intval($this->input->get("start"));
        $length = intval($this->input->get("length"));
        $row_type = "array";
        $order_by =  ["column" => "a.requested_on", "Type" => "DESC"];
        $array = [
            "fileds" => "a.*,b.phone as phone,b.full_name as full_name",
            "table" => 'tb_withdraws as a',
            "join_tables" => [['table' => 'tb_users as b','join_on' => 'a.user_id = b.user_id','join_type' => 'left']],
            "where" => $where,           
            "row_type" => $row_type, 
            "order_by" => $order_by,               
        ]; 
 
        $all_withdraw_request = $this->Allfiles_model->GetDataFromJoin($array);



        $data_withdraw_request = array();
        $i = 1;
        foreach($all_withdraw_request as $withdraw_request) {

            $status = '';
            $edit_action = '';
            if($withdraw_request['status'] == 0) {
                $status = "<span class='btn btn-sm btn-outline-primary'>Close</span>";
            } else {
                $status = "<span class='btn btn-sm btn-outline-danger'>Open</span>";
            }

            $base_url = base_url();
            $withdraw_id = base64_encode(base64_encode($withdraw_request['withdraw_id']));

            $edit_action = '<a class="btn btn-sm btn-info edit_withdraw" id="'.$withdraw_request['withdraw_id'].'" href="'.$base_url.'editwithdraw?id='.$withdraw_id.'" title="Edit"><i class="fa fa-edit"></i></a>';
            

            $data_withdraw_request[] = array( 

              '<td>'.$i++.'</td>',
              '<td class="center">'.$withdraw_request['full_name'].'</td>', 
              '<td class="center">'.$withdraw_request['phone'].'</td>', 
              '<td class="center">'.$withdraw_request['withdraw_request_amount'].'</td>', 
              '<td>'.$withdraw_request['requested_on'].'</td>', 
              '<td class="center">'.$status.'</td>', 
              '<td class="center">'.$edit_action.'</td>', 
            );
        }

        $result = array(
                 "draw" => $draw,
                 "recordsTotal" => count($all_withdraw_request),
                 "recordsFiltered" => count($all_withdraw_request),
                 "data" => $data_withdraw_request,
        );

        echo json_encode($result); 

    }
      public function get_withdrawdetails($id)
    {

        $api_url = "http://demo.esytrading.com/api/Withdraw/withdrawdata/".$id;
            $curl_pass = array(
            "data"=>'',
            "url"=>$api_url,
            "type"=>"GET",
            "header" => array(
              'Content-Type:application/json',
              'X-API-KEY: easy_trade',
              'App-Secret: 1234'
            )
        );					
        
    $response = _curl_request($curl_pass,true);

    return $response;
    }

    
    public function editwithdraw()
    {
        if(isset($_GET['id']) && !empty($_GET['id'])) 
        { 
            // $data = $_GET['id'];  
            $id = base64_decode(base64_decode($_GET['id']));
            $data['file'] = 'withdraw/edit_withdraw';
            $data['custom_js'] = 'withdraw/custom_js';
           	$edit_withdraw = $this->get_withdrawdetails($id);
           	$d = $edit_withdraw['data'][0];
           	$data['details'] = $d;
            $this->load->view('admin_template/main',$data);
        }      
    }

    public function update_withdraw_details()
    {
      $api_url = "http://demo.esytrading.com/api/Withdraw/withdraw_admin_status";
      $where = ['withdraw_id' => $_POST['edit_id']];
      $pass_data = [                
                        "withdraw_id"=>$this->input->post('edit_id'),                    
                        "admin_approved"=>$this->input->post('admin_approved'),
                        "admin_comments"=>$this->input->post('admin_comments'),
                    ];
                    $d = json_encode($pass_data);
            $curl_pass = array(
            "data"=> $d,
            "url"=>$api_url,
            "type"=>"POST",
            "header" => array(
              'Content-Type:application/json',
              'X-API-KEY: easy_trade',
              'App-Secret: 1234'
            )
        );          
      
    $response = _curl_request($curl_pass,true);
    if($response['status'] == 1)
    {
      $data = [
        "status" => 0 ];
      $update_withdraw = $this->Allfiles_model->updateData("tb_withdraws",$data,$where);
     
     
    }
    
    echo json_encode($response);
    
    

    }

    // public function update_withdraw_details()
    // {
    // if(!empty($_POST['edit_id']))
    //     {   

    //       $response = [];
    //         $where = ['withdraw_id' => $_POST['edit_id']];
    //         $data = array(
    //                    'admin_response' => $this->input->post('admin_comment'),
    //                    'updated_at' => date('Y-m-d H:i:s'),  
    //                    'status' => $this->input->post('user_status'),
    //           ); 
    //         $update_user =   $this->Allfiles_model->updateData("tb_supports",$data,$where);
    //          if($update_user) 
    //          {
    //          $response = ['status' => 'success'];
    //         }
    //         else
    //         {
    //            $response = ['status' => 'fail'];
    //         }  
    //         echo json_encode($response);
    //     } 
                                   
    // }

    public function delete_user()
     {
       if (isset($_POST['user_id']) && !empty($_POST['user_id'])) 

       {
      
            $where = ['user_id' => $_POST['user_id']];
            $result  = $this->Allfiles_model->deleteData("tb_users",$where);
            echo $result;
        }         
       }



       
}