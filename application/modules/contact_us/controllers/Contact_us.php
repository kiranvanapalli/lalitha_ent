<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Contact_us extends MX_Controller
{
    public function __construct()
    {
      parent::__construct();
      /*if user not loged in redirect to home page*/
      modules::run('admin/admin/is_logged_in');
      $this->load->model('task_list/Allfiles_model'); 
      $this->load->library('my_file_upload');

    }
    public function index()
    {
      $where = '';
      $type = "array";
      $data['file'] = 'contact_us/contact_us_list';
      $data['custom_js']  = 'contact_us/all_files_js';
      $data['table_js']  = 'task_list/all_files/table_js';
      $data['validation_js']       = 'admin/all_common_js/frontend_validation_admin';
      $where = '';
      $type = "array";
      // $contact_us = $this->Allfiles_model->getSingleData('tb_contact_us')->result_array();
      $contact_us =  $this->Allfiles_model->GetDataAll("tb_contact_us",$where,$type,'id',$limit='');
      $data['contact_us']=$contact_us;
      $this->load->view('admin_template/main',$data); 
  }
    public function delete_message()
     {
       if (isset($_POST['id']) && !empty($_POST['id'])) 

       {
      
            $where = ['id' => $_POST['id']];
            $result  = $this->Allfiles_model->deleteData("tb_contact_us",$where);
            echo $result;
        }         
      }

    public function get_message_data()
    {
    $result = array();
      $post_data = $this->input->post();
      if(!empty($post_data) && count($post_data) > 0)
      {
        $whr = ['id'=> $post_data['id']];
             $result = $this->Allfiles_model->getSingleData("tb_contact_us",$whr)->row_array();
             echo json_encode($result);
      }
    }

      
}