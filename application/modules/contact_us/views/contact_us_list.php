<!-- BEGIN: Content-->
<div class="app-content content">
    <div class="content-overlay"></div>
    <div class="content-wrapper">
        <div class="content-header row">
            <div class="content-header-left col-md-6 col-12 mb-2 breadcrumb-new">
                <h3 class="content-header-title mb-0 d-inline-block">Contact Us</h3>
                <div class="row breadcrumbs-top d-inline-block">
                    <div class="breadcrumb-wrapper col-12">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="<?php echo base_url(); ?>dashboard">Home</a>
                            </li>
                            <li class="breadcrumb-item active">Contact Us</li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="content-body">
        <section id="pagination">
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-header">
                            <h4 class="card-title">Contact Us Details</h4>
                        </div>
                        <div class="card-body">
                            <div class="table-responsive">
                                <table class="table text-center" name="contact_us_table" id="contact_us_table">
                                    <thead>
                                        <tr>
                                            <th>S.no</th>
                                            <th>Name</th>
                                            <th>Email</th>
                                            <th>Mobile Number</th>
                                            <th>Message</th>
                                            <th>Date</th>
                                            <th>Delete</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php
                                                $i=1;
                                                foreach ($contact_us as $contact_us){
                                                ?>
                                        <tr>
                                            <td>
                                                <?php echo $i++; ?>
                                            </td>
                                            <td>
                                                <?php echo $contact_us['name']; ?>
                                            </td>
                                            <td>
                                                <?php echo $contact_us['email_address']; ?>
                                            </td>
                                            <td>
                                                <?php echo $contact_us['phone_no']; ?>
                                            </td>
                                            <td class="text-center">
                                                <a href="javascript:void(0);" id="get_message"
                                                    class="badge badge-success round get_message"
                                                    data-id="<?php echo $contact_us['id']; ?>"
                                                    style="font-size: medium;"><i class="fa fa-eye"
                                                        aria-hidden="true"></i> View</a>
                                            </td>
                                            <td>    
                                            
                                                <?php
                                                 $date = $contact_us['created_at'];
                                                 $createddate = date('d-m-Y', strtotime($date));
                                                 echo $createddate; ?>
                                            </td>
                                            <td><a class="btn btn-xs btn-danger btn-sm delete" href="#"
                                                    id=<?php echo $contact_us['id']; ?>><i class="fa fa-trash" aria-hidden="true"></i></a></td>
                                        </tr>
                                        <?php } ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
</div>
<!-- END: Content-->

<!-- <div class="modal fade text-left show" id="msgshowmodel" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1"
    aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLongTitle">Message</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <p id="message" name="message"></p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn grey btn-outline-secondary" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div> -->
<div class="modal fade text-left show" id="msgshowmodel" tabindex="-1" role="dialog" aria-labelledby="myModalLabel8"
    style="padding-right: 17px;" aria-modal="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header bg-primary white">
                <h4 class="modal-title white" id="myModalLabel8">Message</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body">
                <p id="message" name="message"></p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn grey btn-outline-secondary" data-dismiss="modal">Close</button>
                <!-- <button type="button" class="btn btn-outline-primary">Save changes</button> -->
            </div>
        </div>
    </div>
</div>