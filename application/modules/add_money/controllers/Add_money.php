<?php
// error_reporting(E_ALL & ~E_NOTICE);
defined('BASEPATH') or exit('No direct script access allowed');
class Add_money extends MX_Controller
{
  private $tbl_spares = "tb_spares";
  private $tbl_users = "tb_users";
  private $tbl_trans = "tb_transactiontype";
  private $tbl_spares_order = "tb_spares_order";
  private $tbl_orders = "tb_orders";
  public function __construct()
  {
    parent::__construct();
    /*if user not loged in redirect to home page*/
    modules::run('admin/admin/is_logged_in');
    $this->load->model('task_list/Allfiles_model');
    $this->load->library('my_file_upload');
    $this->load->model("general_model", "general");
    $this->load->model('User_model');
    $this->load->model('Restapi_model');
  }

  public function index()
  {
    $where = '';
    $data['file'] = 'add_money/money_list';
    $data['custom_js']  = 'add_money/all_files_js';
    $data['validation_js']       = 'admin/all_common_js/frontend_validation_admin';
    $type = "array";
    $all_users = $this->Allfiles_model->GetDataAll("tb_users", $where, $type, 'user_id', $limit = '');
    $data['all_users']   = $all_users;
    $this->load->view('admin_template/main', $data);
  }
  public function data()
  {
    $api_url = "http://demo.esytrading.com/api/users/users";
    $curl_pass = array(
      "data" => '',
      "url" => $api_url,
      "type" => "GET",
      "header" => array(
        'Content-Type:application/json',
        'X-API-KEY: easy_trade',
        'App-Secret: 1234'
      )
    );

    $response = _curl_request($curl_pass, true);

    return $response;
  }




  public function getusers()
  {
    $draw = intval($this->input->get("draw"));
    $start = intval($this->input->get("start"));
    $length = intval($this->input->get("length"));
    $data['user_list'] = $this->data();
    $user_list = $this->data();
    $i = 1;
    // echo "<pre>";

    // print_r($user_list);

    // echo "</pre>";
    // die();
    if ($user_list['status'] != '') {

      foreach ($user_list['data'] as $users) {
        $status = '';
        $edit_action = '';
        // $delete_action = '';
        $password = '';
        if ($users['status'] == 1) {
          $status = "<span class='btn btn-sm btn-outline-primary'>Active</span>";
        } else {
          $status = "<span class='btn btn-sm btn-outline-danger'>InActive</span>";
        }

        $base_url = base_url();
        $user_id = base64_encode(base64_encode($users['user_id']));
        $user_password = base64_decode(base64_decode($users['password']));

        $full_name =  '<a class="edit_user" id="' . $users['user_id'] . '" href="' . $base_url . 'add_money_edituserdetails?id=' . $user_id . '" title="Edit">' . $users['full_name'] . '</a>';
        $edit_action = '<a class="dropdown-item edit_user" id="' . $users['user_id'] . '" href="' . $base_url . 'add_money_edituserdetails?id=' . $user_id . '" title="Edit"><i class="ft-edit float-right"></i>Edit </a>';

        // $delete_action = '<a class="dropdown-item delete_user" id="'.$users['user_id'].'" href="#" title="Remove"><i class="ft-trash float-right"></i>Delete</a>';
        $data_users[] = array(



          '<td class="align-middle">' . $i++ . '</td>',
          '<td class="align-middle">' . $full_name . '</td>',
          '<td class="align-middle">' . $users['phone'] . '</td>',
          '<td class="align-middle">' . $users['email'] . '</td>',
          '<td class = "align-middle">' . $users['wallet_amount'] . '</td>',
          // '<td class="align-middle">'.$user_password.'</td>',
          // '<td class="align-middle">'.$users['pin'].'</td>',
          '<td class="align-middle">' . $status . '</td>',
          '<td class="center">' . $edit_action . '</td>',

          // '<td class="align-middle">
          //     <div class="btn-group mr-1 align-middle">
          //       <button type="button" class="btn btn-warning dropdown-toggle data_users" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Action</button>
          //                 <div class="dropdown-menu">
          //                     <button class="dropdown-item datatable-users" type="button">'.$edit_action.'</button>

          //                                       </div>
          //                                 </div>
          // </td>',
        );
      }
    }

    $result = array(
      "draw" => $draw,
      "recordsTotal" => count($user_list),
      "recordsFiltered" => count($user_list),
      "data" => $data_users,
    );

    echo json_encode($result);
  }

  public function user_edit()
  {
    if (isset($_GET['id']) && !empty($_GET['id'])) {
      $id = base64_decode(base64_decode($_GET['id']));
      $data['file'] = 'add_money/edit_user';
      $data['validation_js'] = 'add_money/custom_js';
      $where = ['user_id' => $id];
      $type = "array";
      $edit_user = $this->Allfiles_model->get_data('tb_users', '*', 'user_id', $id);
      $data['edit_user']   = $edit_user['resultSet'];
      $this->load->view('admin_template/main', $data);
    }
  }

  public function update_user_details()
  {
    if (!empty($_POST['edit_id'])) {

      $response = [];
      $where = ['user_id' => $_POST['edit_id']];

      $user_id = $_POST['edit_id'];
      $amount123 = $this->input->post('wallet_amount_data');
      $add_amount = $this->input->post('amount');

      $wallet_amount_data1 = $amount123 + $add_amount;

      $data = array(
        'wallet_amount' => $wallet_amount_data1,
        'updated_at' => date('Y-m-d H:i:s'),

      );
      $update_wallet =   $this->Allfiles_model->updateData("tb_users", $data, $where);
      if ($update_wallet) {
        $save_details = array(
          'user_id' =>  $user_id,
          'amount' => $this->input->post('amount'),
          'comment' => $this->input->post('comment'),
          'status' => 1,
          'create_on' => date('Y-m-d H:i:s')

        );
        // $this->Allfiles_model->updateData("tb_pan_bank_details",$save_details,$where);
        $response = $this->Allfiles_model->data_save("tb_money_add_user", $save_details);

        // echo $this->db->last_query();

        $response = ['status' => 'success'];
      } else {
        $response = ['status' => 'fail'];
      }
      echo json_encode($response);
    }
  }
  public function moneyadd_list()
  {
    $where = '';
    $data['file'] = 'add_money/moneyadd_list';
    $data['custom_js']  = 'add_money/all_files_js';
    $data['validation_js']       = 'admin/all_common_js/frontend_validation_admin';
    // $type = "array";
    // $all_users = $this->Allfiles_model->GetDataAll("tb_money_add_user",$where,$type,'user_id',$limit='');
    // $data['all_users']   = $all_users;
    $this->load->view('admin_template/main', $data);
  }
  public function getmoneyaddusers()
  {
    $where = '';

    $draw = intval($this->input->get("draw"));
    $start = intval($this->input->get("start"));
    $length = intval($this->input->get("length"));

    $row_type = "array";
    $order_by =  ["column" => "a.money_add_id", "Type" => "DESC"];
    $array = [
      "fileds" => "a.*,b.full_name as full_name,b.phone as phone,b.email as email",
      "table" => 'tb_money_add_user as a',
      "join_tables" => [['table' => 'tb_users as b', 'join_on' => 'a.user_id = b.user_id', 'join_type' => 'left']],
      "where" => $where,
      "row_type" => $row_type,
      "order_by" => $order_by,
    ];

    $all_users = $this->Allfiles_model->GetDataFromJoin($array);

    $data_users = array();

    $i = 1;

    foreach ($all_users as $users) {

      $status = '';
      if ($users['status'] == 1) {
        $status = "<span class='btn btn-sm btn-outline-primary'>Active</span>";
      } else {
        $status = "<span class='btn btn-sm btn-outline-danger'>InActive</span>";
      }
      $data_users[] = array(
        '<td class="align-middle">' . $i++ . '</td>',
        '<td class="align-middle">' . $users['full_name'] . '</td>',
        '<td class="align-middle">' . $users['phone'] . '</td>',
        '<td class="align-middle">' . $users['email'] . '</td>',
        '<td class = "align-middle">' . $users['amount'] . '</td>',
        '<td class = "align-middle">' . $users['comment'] . '</td>',
        '<td class="align-middle">' . $users['create_on'] . '</td>',
        // '<td class="align-middle">'.$users['pin'].'</td>',
        // '<td class="align-middle">'.$status.'</td>', 
      );
    }

    $result = array(
      "draw" => $draw,
      "recordsTotal" => count($all_users),
      "recordsFiltered" => count($all_users),
      "data" => $data_users,
    );

    echo json_encode($result);
  }
  public function loadHTML()
  {
    $id = 2;
    $where = [
      ['column' => 'o.order_id', 'value' => $id]
    ];
    $array = [
      "fileds" => "s.*, u.*",
      "table" => $this->tbl_orders . ' AS o',
      "join_tables" => [
        ["table" => $this->tbl_users . ' AS u', "join_on" => 'u.user_id = o.user_id', "join_type" => "left"],
        ["table" => $this->tbl_spares_order . ' AS so', "join_on" => 'so.order_id = o.order_id', "join_type" => "left"],
        ["table" => $this->tbl_spares . ' AS s', "join_on" => 's.spare_id = so.spare_id', "join_type" => "left"]
      ],
      "where" => $where,
      "like" => [],
      "limit" => [],
      "order_by" => [],
    ];
    $get = $this->general->get_all_from_join($array);
    $html_data = $get['resultSet'];
    $body =  $this->load->view('order_html', $html_data, TRUE);
  }
}
