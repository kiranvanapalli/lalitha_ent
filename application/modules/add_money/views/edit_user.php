<style type="text/css">
   .table {
   text-align: center;
   }
   .previewimage
   {
   margin-left: 40%;
   width: 24%;
   height: 220px;
   }
</style>
<div class="app-content content">
   <div class="content-overlay"></div>
   <div class="content-wrapper">
      <div class="content-header row">
         <div class="content-header-left col-md-6 col-12 mb-2 breadcrumb-new">
            <h3 class="content-header-title mb-0 d-inline-block">User Information</h3>
            <div class="row breadcrumbs-top d-inline-block">
               <div class="breadcrumb-wrapper col-12">
                  <ol class="breadcrumb">
                     <li class="breadcrumb-item"><a href="<?php echo base_url(); ?>dashboard">Home</a>
                     </li>
                     <li class="breadcrumb-item active">User Information</li>
                  </ol>
               </div>
            </div>
         </div>
         <div class="content-header-right col-md-6 col-12">
            <div class="btn-group float-md-right">
               <a class="btn btn-primary round btn-min-width mr-1 mb-1" aria-haspopup="true" aria-expanded="false" href ="<?php echo base_url(); ?>user_details"><i class="ft-users"></i> User List</a>
            </div>
         </div>
      </div>
      <div class="row">
         <div class="col-md-12">
            <div class="card">
               <div class="card-header">
                  <h4 class="card-title" id="row-separator-colored-controls">User Information</h4>
                  <a class="heading-elements-toggle"><i class="la la-ellipsis-h font-medium-3"></i></a>
                  <div class="heading-elements">
                     <ul class="list-inline mb-0">
                        <li><a data-action="expand"><i class="ft-maximize"></i></a>
                        </li>
                     </ul>
                  </div>
               </div>
               <div class="card-content collapse show">
                  <div class="card-body">
                     <form method="post" id='edit_user_form' name='user_form' class='form form-horizontal' enctype="multipart/form-data">
                        <div class="form-body">
                           <h4 class="form-section"><i class="la la-eye"></i> About User</h4>
                           <div class="row">
                              <div class="col-md-6">
                                 <div class="form-group row mx-auto last">
                                    <label class="col-md-3 label-control" for="fullname">Full Name</label>
                                    <div class="col-md-9">
                                       <input type="text" id="fullname" class="form-control border-primary" placeholder="Full Name" name="fullname" data-toggle="tooltip" data-trigger="hover" data-placement="top" data-title="Full Name" value="<?php echo $edit_user['full_name']; ?>" disabled>
                                    </div>
                                 </div>
                              </div>
                              <div class="col-md-6">
                                 <div class="form-group row mx-auto last">
                                    <label class="col-md-3 label-control" for="mobile_number">Mobile Number</label>
                                    <div class="col-md-9">
                                       <input type="text" id="mobile_number" class="form-control border-primary" placeholder="Mobile Number" name="mobile_number" data-toggle="tooltip" data-trigger="hover" data-placement="top" data-title="Mobile Number" value="<?php echo $edit_user['phone']; ?>" disabled>
                                    </div>
                                 </div>
                              </div>
                           </div>
                           <div class="row">
                              <div class="col-md-6">
                                 <div class="form-group row mx-auto last">
                                    <label class="col-md-3 label-control" for="emai">Email</label>
                                    <div class="col-md-9">
                                       <input type="text" id="email" class="form-control border-primary" placeholder="Email" name="email" data-toggle="tooltip" data-trigger="hover" data-placement="top" data-title="Email Id" autocomplete="off" value="<?php echo $edit_user['email']; ?>" disabled>
                                    </div>
                                 </div>
                              </div>
                              <div class="col-md-6">
                                 <div class="form-group row mx-auto last">
                                    <label class="col-md-3 label-control" for="wallet_amount">Present Wallet Amount</label>
                                    <div class="col-md-9 wallet_amount">
                                       <input id="wallet_amount_data" type="text" class="form-control border-primary" name="wallet_amount_data" placeholder="Wallet Amount" data-toggle="tooltip" data-trigger="hover" data-placement="top" data-title="wallet_amount" value="<?php echo $edit_user['wallet_amount']; ?>" readonly>
                                    </div>
                                 </div>
                              </div>
                           </div>
                            <div class="row">
                              <div class="col-md-6">
                                 <div class="form-group row mx-auto last">
                                    <label class="col-md-3 label-control" for="emai">Add Amount</label>
                                    <div class="col-md-9">
                                       <input type="text" id="amount" class="form-control border-primary" placeholder="Add Amount" name="amount" data-toggle="tooltip" data-trigger="hover" data-placement="top" data-title="Enter Amount To Add in Wallet" autocomplete="off">
                                    </div>
                                 </div>
                              </div>
                              <div class="col-md-6">
                                 <div class="form-group row mx-auto last">
                                    <label class="col-md-3 label-control" for="after_wallet_amount">After Adding Totla Wallet Amount</label>
                                    <div class="col-md-9 after_wallet_amount">
                                       <input  type="text" class="form-control border-primary" id="after_wallet_amount_data" name="after_wallet_amount_data" data-toggle="tooltip" data-trigger="hover" data-placement="top" data-title="after_wallet_amount" readonly value="<?php echo $edit_user['wallet_amount']; ?>">
                                    </div>
                                 </div>
                              </div>
                           </div>
                           <div class="row">
                              <div class="col-lg-6">
                                 <div class="form-group row mx-auto last">
                                    <label class="col-md-3 label-control" for="emai">Comment</label>
                                    <div class="col-md-9">
                                      <!--  <input type="text" id="amount" class="form-control border-primary" placeholder="Add Amount" name="amount" data-toggle="tooltip" data-trigger="hover" data-placement="top" data-title="Enter Amount To Add in Wallet" autocomplete="off"> -->
                                        <textarea class="textarea" id="comment" name="comment" placeholder="Place Enter Comment"
                                  style="width: 257%; height: 250px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;"></textarea>
                                    </div>
                                 </div>
                              </div>
                               <div class="col-lg-6">
                                 <div class="form-group row mx-auto last">
                                    
                                 </div>
                              </div>
                           </div>
                           
                         
                           <!-- <h4 class="form-section"><i class="la la-user"></i> User Status</h4>
                           <div class="form-group row mx-auto last text-center">
                              <div class="input-group col-md-12 text-center">
                                 <div class="d-inline-block custom-control custom-radio mr-1">
                                    <input type="radio" name="user_status" class="custom-control-input" id="active" value="1" <?php if ($edit_user['status'] == 1) {
                                       echo "checked=checked";
                                       } ?>>
                                    <label class="custom-control-label" for="active">Active</label>
                                 </div>
                                 <div class="d-inline-block custom-control custom-radio">
                                    <input type="radio" name="user_status" class="custom-control-input" id="inactive" value= "0" <?php if($edit_user['status'] == 0)
                                       {
                                               echo "checked=checked";
                                       } ?>>
                                    <label class="custom-control-label" for="inactive">In Active</label>
                                 </div>
                              </div>
                           </div> -->
                        </div>
                        <div class="form-actions text-right">
                           <button type="button" class="btn btn-warning mr-1"> <i class="la la-remove"></i> Cancel</button>
                           <button type="submit" class="btn btn-primary"> <i class="la la-check"></i> Update</button>
                        </div>
                        <input type="hidden" name="edit_id" id="edit_id" value="<?php echo $edit_user['user_id']; ?>">
                     </form>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>