<style type="text/css">
   .table {
   text-align: center;
   }
   .previewimage
   {
   margin-left: 40%;
   width: 24%;
   height: 220px;
   }
</style>
<div class="app-content content">
   <div class="content-overlay"></div>
   <div class="content-wrapper">
      <div class="content-header row">
         <div class="content-header-left col-md-6 col-12 mb-2 breadcrumb-new">
            <h3 class="content-header-title mb-0 d-inline-block">User Information</h3>
            <div class="row breadcrumbs-top d-inline-block">
               <div class="breadcrumb-wrapper col-12">
                  <ol class="breadcrumb">
                     <li class="breadcrumb-item"><a href="<?php echo base_url(); ?>dashboard">Home</a>
                     </li>
                     <li class="breadcrumb-item active">User Information</li>
                  </ol>
               </div>
            </div>
         </div>
         <div class="content-header-right col-md-6 col-12">
            <div class="btn-group float-md-right">
               <a class="btn btn-primary round btn-min-width mr-1 mb-1" aria-haspopup="true" aria-expanded="false" href ="<?php echo base_url(); ?>user_details"><i class="ft-users"></i> User List</a>
            </div>
         </div>
      </div>
      <div class="row">
         <div class="col-md-12">
            <div class="card">
               <div class="card-header">
                  <h4 class="card-title" id="row-separator-colored-controls">User Information</h4>
                  <a class="heading-elements-toggle"><i class="la la-ellipsis-h font-medium-3"></i></a>
                  <div class="heading-elements">
                     <ul class="list-inline mb-0">
                        <li><a data-action="expand"><i class="ft-maximize"></i></a>
                        </li>
                     </ul>
                  </div>
               </div>
               <div class="card-content collapse show">
                  <div class="card-body">
                     <form method="post" id='edit_user_form' name='user_form' class='form form-horizontal' enctype="multipart/form-data">
                        <div class="form-body">
                           <h4 class="form-section"><i class="la la-eye"></i> About User</h4>
                           <div class="row">
                              <div class="col-md-6">
                                 <div class="form-group row mx-auto last">
                                    <label class="col-md-3 label-control" for="fullname">Full Name</label>
                                    <div class="col-md-9">
                                       <input type="text" id="fullname" class="form-control border-primary" placeholder="Full Name" name="fullname" data-toggle="tooltip" data-trigger="hover" data-placement="top" data-title="Full Name" value="<?php echo $edit_user['full_name']; ?>">
                                    </div>
                                 </div>
                              </div>
                              <div class="col-md-6">
                                 <div class="form-group row mx-auto last">
                                    <label class="col-md-3 label-control" for="mobile_number">Mobile Number</label>
                                    <div class="col-md-9">
                                       <input type="text" id="mobile_number" class="form-control border-primary" placeholder="Mobile Number" name="mobile_number" data-toggle="tooltip" data-trigger="hover" data-placement="top" data-title="Mobile Number" value="<?php echo $edit_user['phone']; ?>">
                                    </div>
                                 </div>
                              </div>
                           </div>
                           <div class="row">
                              <div class="col-md-6">
                                 <div class="form-group row mx-auto last">
                                    <label class="col-md-3 label-control" for="emai">Email</label>
                                    <div class="col-md-9">
                                       <input type="text" id="email" class="form-control border-primary" placeholder="Email" name="email" data-toggle="tooltip" data-trigger="hover" data-placement="top" data-title="Email Id" autocomplete="off" value="<?php echo $edit_user['email']; ?>">
                                    </div>
                                 </div>
                              </div>
                              <div class="col-md-6">
                                 <div class="form-group row mx-auto last">
                                    <label class="col-md-3 label-control" for="password">Password</label>
                                    <div class="col-md-9 password">
                                       <input id="password" type="password" class="form-control border-primary" name="password" placeholder="Password" data-toggle="tooltip" data-trigger="hover" data-placement="top" data-title="Password" value="<?php echo base64_decode(base64_decode($edit_user['password'])); ?>">
                                       <span toggle="#password" class="fa fa-2x fa-eye field-icon toggle-password"></span>
                                    </div>
                                 </div>
                              </div>
                           </div>
                           <div class="row">
                              <div class="col-md-6">
                                 <div class="form-group row mx-auto last">
                                    <label class="col-md-3 label-control" for="pan_no">Pan Number</label>
                                    <?php if($edit_user_pan['pan_number'] != '' ){
                                       $pan_number =  $edit_user_pan['pan_number']; ?>
                                        <div class="col-md-9">
                                       
                                       <input type="text" id="pan_no" class="form-control border-primary" placeholder="Pan Number" name="pan_no" data-toggle="tooltip" data-trigger="hover" data-placement="top" data-title="Pan Number" value="<?php echo $pan_number; ?>" > 
                                    </div>
                                    
                                    <?php }
                                    else
                                    {
                                       $pan_number =  ''; ?>
                                       <div class="col-md-9">
                                       
                                       <input type="text" id="pan_no" class="form-control border-primary" placeholder="Pan Number" name="pan_no" data-toggle="tooltip" data-trigger="hover" data-placement="top" data-title="Pan Number" value="<?php echo $pan_number; ?>" > 
                                    </div>

                                   <?php } ?>
                                 </div>
                              </div>
                              <div class="col-md-6">
                                 <div class="form-group row mx-auto last">
                                    <label class="col-md-3 label-control upload">Pan Attachment</label>
                                    <?php if( $edit_user_pan['pan_proof'] == '')
                                    { $pan_image = ''; ?>
                                    <div class="col-md-9 mx-auto">
                                       <label id="projectinput8" class="file center-block">
                                       <input type="file" id="pan_image" name="pan_image" onchange="previewFile(this);">
                                       <input type="hidden" name="pan_image_old" value="<?php echo $pan_image; ?>">
                                       <span class="old_img_gallery"></span>
                                       <span class="file-custom"></span>
                                       </label>
                                    </div>
                                 <?php }
                                 else
                                 {
                                    $pan_image = $edit_user_pan['pan_proof']; ?>
                                    <div class="col-md-9 mx-auto">
                                       <label id="projectinput8" class="file center-block">
                                       <input type="file" id="pan_image" name="pan_image" onchange="previewFile(this);">
                                       <input type="hidden" name="pan_image_old" value="<?php echo $pan_image; ?>">
                                       <span class="old_img_gallery"></span>
                                       <span class="file-custom"></span>
                                       </label>
                                    </div>
                                 <?php } ?>
                                 </div>
                              </div>
                              <?php if ($edit_user_pan['pan_proof'] != '') {
                                    $pan_attachment = $edit_user_pan['pan_proof'];
                               ?>
                              <div class="col-md-12">
                                <p class="new_image" style="display: none; text-align: center;">New Image</p>
                              <img id="previewImg" src="/examples/images/transparent.png" class="form-group img-thumbnail previewimage" alt="Placeholder" style="display: none;">
                              <p class="" style="text-align: center;">Old Image</p>
                              <a href="<?php echo base_url(); ?>uploads/user_pans/<?php echo $pan_attachment; ?>" title="Banner Image" target="_blank">
                                <img src="<?php echo base_url(); ?>uploads/user_pans/<?php echo $pan_attachment; ?>" alt="gallery" class="form-group img-thumbnail previewimage" id="previewImgdata"></a>
                              </div>
                           <?php }
                           else
                           {
                               $pan_attachment = ''; ?>

                               <div class="col-md-12">
                                <p class="new_image" style="display: none; text-align: center;">New Image</p>
                              <img id="previewImg" src="/examples/images/transparent.png" class="form-group img-thumbnail previewimage" alt="Placeholder" style="display: none;">
                              <!-- <p class="" style="text-align: center;">Old Image</p>
                              <a href="<?php echo base_url(); ?>uploads/user_pans/<?php echo $pan_attachment; ?>" title="Banner Image" target="_blank">
                                <img src="<?php echo base_url(); ?>uploads/user_pans/<?php echo $pan_attachment; ?>" alt="gallery" class="form-group img-thumbnail previewimage" id="previewImgdata"></a> -->
                              </div>
                           
                          <?php } ?>
                           </div>
                           
                           <h4 class="form-section"><i class="la la-bank"></i> Bank Details</h4>
                           <div class="row">
                              <div class="col-md-6">
                                 <div class="form-group row mx-auto last">
                                    <label class="col-md-3 label-control" for="bank_name">Select Bank</label>
                                     <?php if($edit_user_pan['bank_name'] != '')
                                       {
                                       $bank_name = $edit_user_pan['bank_name'] ; ?>
                                    <div class="col-md-9">

                                       <input type="text" name="bank_name" id="bank_name" class="form-control border-primary" readonly value="<?php echo $bank_name ?>">
                                    </div>
                                 <?php }
                                 else
                                 {
                                     $bank_name = "" ; ?>
                                     <div class="col-md-9">

                                       <input type="text" name="bank_name" id="bank_name" class="form-control border-primary" readonly value="<?php echo $bank_name ?>">
                                    </div>

                                <?php } ?>
                                 </div>
                              </div>
                              <div class="col-md-6">
                                 <div class="form-group row mx-auto last">
                                    <label class="col-md-3 label-control" for="branch">Branch</label>
                                     <?php if($edit_user_pan['branch'] != ''){ $branch =$edit_user_pan['branch'];  ?>
                                    <div class="col-md-9">
                                       <input type="text" name="branch" id="branch" class="form-control border-primary" value="<?php echo $branch; ?>"  readonly >
                                    </div>
                                 <?php }
                                 else
                                 {
                                    $branch = ''; ?>
                                    <div class="col-md-9">
                                       <input type="text" name="branch" id="branch" class="form-control border-primary" value="<?php echo $branch; ?>"  readonly >
                                    </div>
                               <?php  } ?>
                                 </div>
                              </div>
                           </div>
                           <div class="row">
                              <div class="col-md-6">
                                 <div class="form-group row mx-auto last">
                                    <label class="col-md-3 label-control" for="ifsc_code">IFSC Code</label>
                                    <?php if($edit_user_pan['ifsc_code'] != ''){$ifsc_code=$edit_user_pan['ifsc_code']; ?>
                                    <div class="col-md-7 bank_name">
                                       <input type="text" id="ifsc_code" class="form-control border-primary" placeholder="IFSC Code" name="ifsc_code" data-toggle="tooltip" data-trigger="hover" data-placement="top" data-title="Ifsc Code" value="<?php echo $ifsc_code; ?>">
                                    </div>
                                 <?php }
                                 else
                                 {  
                                    $ifsc_code = ''; ?>
                                    <div class="col-md-7 bank_name">
                                       <input type="text" id="ifsc_code" class="form-control border-primary" placeholder="IFSC Code" name="ifsc_code" data-toggle="tooltip" data-trigger="hover" data-placement="top" data-title="Ifsc Code" value="<?php echo $ifsc_code; ?>">
                                    </div>

                                 <?php } ?>
                                    <div class="col-md-2">
                                       <input type="button" class="btn btn-dropbox" name="search_ifsc" id="search_ifsc" value="Search IFSC" onclick="getIFSC()">
                                    </div>
                                 </div>
                              </div>
                              <div class="col-md-6">
                                 <div class="form-group row mx-auto last">
                                    <label class="col-md-3 label-control" for="bank_acc_number">Bank Account Number</label>
                                    <?php if($edit_user_pan['account_number'] != ''){$account_number = $edit_user_pan['account_number']; ?>
                                    <div class="col-md-9">
                                       <input type="text" id="bank_acc_number" class="form-control border-primary" placeholder="Bank Account Number" name="bank_acc_number" data-toggle="tooltip" data-trigger="hover" data-placement="top" data-title="Bank Account Number" value="<?php echo $account_number; ?>">
                                    </div>
                                 <?php }
                                 else
                                 {
                                    $account_number = ''; ?>
                                    <div class="col-md-9">
                                       <input type="text" id="bank_acc_number" class="form-control border-primary" placeholder="Bank Account Number" name="bank_acc_number" data-toggle="tooltip" data-trigger="hover" data-placement="top" data-title="Bank Account Number" value="<?php echo $account_number; ?>">
                                    </div>

                                 <?php } ?>
                                 </div>
                              </div>
                           </div>
                           <h4 class="form-section"><i class="la la-user"></i> User Status</h4>
                           <div class="form-group row mx-auto last text-center">
                              <div class="input-group col-md-12 text-center">
                                 <div class="d-inline-block custom-control custom-radio mr-1">
                                    <input type="radio" name="user_status" class="custom-control-input" id="active" value="1" <?php if ($edit_user['status'] == 1) {
                                       echo "checked=checked";
                                       } ?>>
                                    <label class="custom-control-label" for="active">Active</label>
                                 </div>
                                 <div class="d-inline-block custom-control custom-radio">
                                    <input type="radio" name="user_status" class="custom-control-input" id="inactive" value= "0" <?php if($edit_user['status'] == 0)
                                       {
                                               echo "checked=checked";
                                       } ?>>
                                    <label class="custom-control-label" for="inactive">In Active</label>
                                 </div>
                              </div>
                           </div>
                        </div>
                        <div class="form-actions text-right">
                           <button type="button" class="btn btn-warning mr-1"> <i class="la la-remove"></i> Cancel</button>
                           <button type="submit" class="btn btn-primary"> <i class="la la-check"></i> Update</button>
                        </div>
                        <input type="hidden" name="edit_id" id="edit_id" value="<?php echo $edit_user['user_id']; ?>">
                     </form>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <div class="content-body">
         <section id="pagination">
            <div class="row">
               <div class="col-12">
                  <div class="card">
                     <div class="card-header">
                        <h4 class="card-title">Deposit</h4>
                     </div>
                     <div class="card-body">
                        <div class="table-responsive">
                           <table class="table alt-pagination wallet-wrapper" name="deposit_table" id="deposit_table">
                              <thead>
                                 <tr>
                                    <th width="5%">S.no</th>
                                    <th>Date</th>
                                    <th>Amount</th>
                                    <th>Status</th>
                                 </tr>
                              </thead>
                              <tbody>
                              <?php
                                       $i=1;
                              if(isset($tras_data['data']) && !empty($tras_data['data']) )
                              {
                                foreach($tras_data['data'] as $deposit_details)
                                {                                     
                                  $data = $deposit_details['transation_response'];  
                                  $details = json_decode(($data), true);
                                  if($details['status'] == 1)
                                  {
                                    $status = "<div class='status badge badge-success badge-pill badge-sm'>Sucess</div>";
                                  }
                                  else
                                  {
                                    $status = "<status badge badge-danger badge-pill badge-sm'>Fail</div>";
                                  }
                                 ?>
                              <tr role="row" class="odd">
                                 <td class="align-middle"><?php echo $i++; ?> </td>
                                 <td class="align-middle"><?php echo $deposit_details['transtion_on']; ?></td>
                                 <td class="align-middle"><?php echo $deposit_details['deposite_amount']; ?></td>
                                 <td class="align-middle"><?php echo $status; ?></td>                  
                              </tr>
                              <?php }
                              } ?>
                             
                              </tbody>
                           </table>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </section>
      </div>
      <div class="content-body">
         <section id="pagination">
            <div class="row">
               <div class="col-12">
                  <div class="card">
                     <div class="card-header">
                        <h4 class="card-title">Withdraw</h4>
                     </div>
                     <div class="card-body">
                        <div class="table-responsive">
                           <table class="table alt-pagination wallet-wrapper" name="withdraw_table" id="withdraw_table">
                              <thead>
                                 <tr>
                                    <th width="5%">S.no</th>
                                    <th>Amount</th>
                                    <th>Request On</th>
                                    <th>Admin Status</th>
                                 </tr>
                              </thead>
                              <tbody>
                                 <?php 
                                     $i=1;
                                     if(isset($withdraw_data['data']) && !empty($withdraw_data['data']) )
                                     {
                                          foreach($withdraw_data['data'] as $withdraw)
                                          { 
                                             if(isset($withdraw['withdraw_id']) && !empty($withdraw['withdraw_id']))
                                             {
                                                if($withdraw['admin_approved'] == "approved")
                                                {
                                                   $admin_status = "<div class='status badge badge-success badge-pill badge-sm'>Sucess</div>";
                                                }
                                                elseif($withdraw['admin_approved'] == "rejected")
                                                {
                                                   $admin_status =  "<div class= 'status badge badge-danger badge-pill badge-sm'>Rejected</div>";
                                                }
                                                else
                                                {
                                                   $admin_status =  "<div class = 'status badge badge-info badge-pill badge-sm'>Pending</div>";
                                                }
                                             ?>
                                             <tr role="row" class="odd">
                                                <td class="align-middle" width="5%">
                                                   <div class="s_no"><?php echo $i++; ?></div>
                                                </td>
                                                
                                                <td class="align-middle">
                                                   <div class="amount"><?php echo $withdraw['total_request']; ?></div>
                                                </td>
                                                <td class="align-middle">
                                                   <div class="date"><?php echo $withdraw['requested_on']; ?></div>
                                                </td>
                                                <td class="align-middle">
                                                  <?php echo $admin_status; ?>
                                                </td>
                                             </tr>

                                          <?php }
                                          }
                                     } ?>
                              </tbody>
                           </table>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </section>
      </div>
      <div class="content-body">
         <section id="pagination">
            <div class="row">
               <div class="col-12">
                  <div class="card">
                     <div class="card-header">
                        <h4 class="card-title">Trade History</h4>
                     </div>
                     <div class="card-body">
                        <div class="table-responsive">
                           <table class="table alt-pagination wallet-wrapper" name="tradehistory_table" id="tradehistory_table">
                              <thead>
                                 <tr>
                                    <th>Sl.no</th>
                                    <th>Account Type</th>
                                    <th>Inverst On</th>
                                    <th>Inverst Amount</th>
                                    <th>Win or Lose</th>
                                    <th>Profit</th>
                                    <th>Inverst Date</th>
                                 </tr>
                              </thead>
                              <tbody>
                                 <?php
                                 $i=1;
                                 if(isset($invest_data['data']) && !empty($invest_data['data']))
                                 {
                                    foreach($invest_data['data'] as $invest)
                                    { ?>

                                    <tr role="row" class="odd">
                                    <td class="align-middle sorting_1">
                                       <div class="s.no"><?php echo $i++; ?></div>
                                    </td>
                                    <td class="align-middle">
                                       <div class="type"><?php echo $invest['account_type']; ?></div>
                                    </td>
                                    <td class="align-middle">
                                       <div class="inverst_type"><?php echo $invest['invest_type']; ?></div>
                                    </td>
                                    <td class="align-middle">
                                       <div class="volume"><?php echo $invest['invest_amount']; ?></div>
                                    </td>
                                    <td class="align-middle">
                                       <div class="unit-price"><?php echo $invest['is_win']; ?></div>
                                    </td>
                                    <td class="align-middle">
                                       <div class="total-price"><?php echo $invest['user_profit']; ?></div>
                                    </td>
                                    <td class="align-middle">
                                       <div class="fee"><?php echo $invest['invested_on']; ?></div>
                                    </td>
                                   
                                 </tr>
                                   <?php }
                              }
                                 ?>
                                 
                              </tbody>
                           </table>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </section>
      </div>
   </div>
</div>