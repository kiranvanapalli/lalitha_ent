<script>
$(document).ready(function(){
    $.ajaxSetup({
    headers: { "Content-Type": "application/json",
    "X-API-KEY":"easy_trade",
    "App-Secret":"1234" }
});
            var slot_table = $('#slot_table').DataTable({

                'searching': true,
                'paging': true,
                "responsive": true,
                "processing": true,
                // "serverSide": true,
                'lengthChange': true,
                 'autoWidth': false,
                // "dom": 'Blfrtip',

                "buttons": [
                    'csv', 'excel', 'pdf', 'print'
                ],

                "order": [],
                "ajax": {
                    "url": "<?php echo base_url('admin_dashboard/Admin_dashboards/getrealslotdata'); ?>",
                    "type": "POST",
                },
                "columnDefs": [{
                    targets: -1,
                    "orderable": false,
                }, ],
            });

         setInterval( function () {
        slot_table.ajax.reload();
    }, 20000 );

         var demo_slot_table = $('#demo_slot_table').DataTable({

                'searching': true,
                'paging': true,
                "responsive": true,
                "processing": true,
                // "serverSide": true,
                'lengthChange': true,
                 'autoWidth': false,
                // "dom": 'Blfrtip',

                "buttons": [
                    'csv', 'excel', 'pdf', 'print'
                ],

                "order": [],
                "ajax": {
                    "url": "<?php echo base_url('admin_dashboard/Admin_dashboards/getdemoslotdata'); ?>",
                    "type": "POST",
                },
                "columnDefs": [{
                    targets: -1,
                    "orderable": false,
                }, ],
            });

         setInterval( function () {
        demo_slot_table.ajax.reload();
    }, 20000 );
            $(document).on('click', '.up', function (event) {
                event.preventDefault();
                var slot_id = $(this).attr("id");
                console.log(slot_id);
                if (slot_id != '') {
                    $.ajax({
                         url: "http://demo.esytrading.com/api/Slots/slots/"+slot_id+"?admin_decided=up",
                         method: "GET",
                         data: $(this).serialize(),
                         success: function(data)
                         {
                         if (data) {
                            console.log("Up Users is win");
                            toastr["success"]("Up Users wining Updated Successfully!");
                            slot_table.ajax.reload();
                         }
                     }
                    });
                }

            });
             $(document).on('click', '.down', function (event) {
                event.preventDefault();
                var slot_id = $(this).attr("id");
                console.log(slot_id);
                if (slot_id != '') {
                    $.ajax({
                         url: "http://demo.esytrading.com/api/Slots/slots/"+slot_id+"?admin_decided=down",
                         method: "GET",
                         data: $(this).serialize(),
                         success: function(data)
                         {
                         if (data) {
                            console.log("Down Users is win");
                         }
                     }
                    });
                }

            });
              $(document).on('click', '.all', function (event) {
                event.preventDefault();
                var slot_id = $(this).attr("id");
                console.log(slot_id);
                if (slot_id != '') {
                    $.ajax({
                         url: "http://demo.esytrading.com/api/Slots/slots/"+slot_id+"?admin_decided=all",
                         method: "GET",
                         data: $(this).serialize(),
                         success: function(data)
                         {
                         if (data) {
                            console.log("All Users are win");
                         }
                     }
                    });
                }

            });
             $(document).on('click', '.demoup', function (event) {
                event.preventDefault();
                var slot_id = $(this).attr("id");
                console.log(slot_id);
                if (slot_id != '') {
                    $.ajax({
                         url: "http://demo.esytrading.com/api/Slots/slots/"+slot_id+"?admin_decided=up",
                         method: "GET",
                         data: $(this).serialize(),
                         success: function(data)
                         {
                         if (data) {
                            console.log("Up Users is win");
                            toastr["success"]("Up Users wining Updated Successfully!");
                            slot_table.ajax.reload();
                         }
                     }
                    });
                }

            });
             $(document).on('click', '.demodown', function (event) {
                event.preventDefault();
                var slot_id = $(this).attr("id");
                console.log(slot_id);
                if (slot_id != '') {
                    $.ajax({
                         url: "http://demo.esytrading.com/api/Slots/slots/"+slot_id+"?admin_decided=down",
                         method: "GET",
                         data: $(this).serialize(),
                         success: function(data)
                         {
                         if (data) {
                            console.log("Down Users is win");
                         }
                     }
                    });
                }

            });
             $(document).on('click', '.demoall', function (event) {
                event.preventDefault();
                var slot_id = $(this).attr("id");
                console.log(slot_id);
                if (slot_id != '') {
                    $.ajax({
                         url: "http://demo.esytrading.com/api/Slots/slots/"+slot_id+"?admin_decided=all",
                         method: "GET",
                         data: $(this).serialize(),
                         success: function(data)
                         {
                         if (data) {
                            console.log("Down Users is win");
                         }
                     }
                    });
                }

            });

           
            // var timeleft = $('#time').val(); 
            // console.log(timeleft);
            // var downloadTimer = setInterval(function(){
            // if(timeleft <= 0){
            //         clearInterval(downloadTimer);
            // }
            //  document.getElementById("time").value = timeleft - timeleft;
            //  timeleft -= 1;
            // }, 1000);

                
    });
</script>