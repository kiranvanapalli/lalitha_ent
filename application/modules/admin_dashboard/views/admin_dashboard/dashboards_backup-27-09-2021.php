<head>
    <style>
        footer.footer.footer-static.footer-light.navbar-border.navbar-shadow {
            margin: 0;
        }
        .card {
    margin-bottom: 1.275rem;
        }

        .card-title {
            font-weight: 700 !important;
        }

        .slot_style {
            vertical-align: inherit !important;
        }

        .card-body h2 {
            font-size: 1.7rem;
        }
        .card-content .card-body {
            height: 72px;
            margin-top: -5px;
        }
        .card .demo
        {
            margin-bottom: 0.475rem;
        }
        /*.realaccount
        {
                margin-top: -11px;
        }*/
    </style>
    <!-- Styles -->
    <style>
        #chartdiv {
            width: 100%;
            height: 500px;
        }

        div.dt-buttons {
            position: relative;
            float: right;
            margin-left: 160px;
        }

        .media-body h6,h3
        {
            color: white !important;
        }
        .card-content .card-body {
            background: #ecc04c;
        }
    </style>




</head>

<body>
    <div class="app-content content">
        <div class="content-overlay"></div>
        <div class="content-wrapper">
            <div class="content-header row"></div>
            <div class="content-body">
               
                


                <!-- Active Orders -->
                <div class="row">
                    <div class="col-lg-12 realaccount" id="UserOnUp">
                        <div class="card">
                            <div class="card-header">
                                <h4 class="card-title">Real Account Slot Details Dashboard</h4>

                            </div>
                            <div class="card-content">
                                <div class="table-responsive">

                                    <table class="mb-0 table table-bordered text-center" name="slot_table"
                                        id="slot_table">
                                        <thead>
                                            <tr>
                                                <th rowspan="2" class="slot_style">Slot</th>
                                                <!-- <th rowspan="2" class="slot_style" width="2%">Slot Account Type</th> -->
                                                <th colspan="2">Users</th>
                                                <th colspan="2">Amount</th>
                                                <th colspan="3">Win Type</th>
                                                <th rowspan = "2" class="slot_style">Time Pending</th>
                                                <th colspan="2">Date</th>
                                                <th rowspan="2" class="slot_style">Status</th>
                                                <!-- <th rowspan="2" class="slot_style">Slot Win</th> -->
                                            </tr>
                                            <tr>
                                                <th>Up</th>
                                                <th>Down</th>
                                                <th>Up</th>
                                                <th>Down</th>
                                                <th>Up</th>
                                                <th>Down</th>
                                                <th>Both</th>
                                                <th>Start Time</th>
                                                <th>End Time</th>
                                            </tr>
                                        </thead>
                                        <tbody>

                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row demoaccount">
                    <div class="col-lg-12" id="UserOnUp">
                        <div class="card demo">
                            <div class="card-header">
                                <h4 class="card-title">Demo Account Slot Details Dashboard</h4>

                            </div>
                            <div class="card-content">
                                <div class="table-responsive">

                                    <table class="mb-0 table table-bordered text-center" name="demo_slot_table"
                                        id="demo_slot_table">
                                        <thead>
                                            <tr>
                                                <th rowspan="2" class="slot_style">Slot</th>
                                              
                                                <th colspan="2">Users</th>
                                                <th colspan="2">Amount</th>
                                                <th colspan="3">Win Type</th>
                                                <th rowspan = "2" class="slot_style">Time Pending</th>
                                                <th colspan="2">Date</th>
                                                <th rowspan="2" class="slot_style">Status</th>
                                              
                                            </tr>
                                            <tr>
                                                <th>Up</th>
                                                <th>Down</th>
                                                <th>Up</th>
                                                <th>Down</th>
                                                <th>Up</th>
                                                <th>Down</th>
                                                <th>Both</th>
                                                <th>Start Time</th>
                                                <th>End Time</th>
                                            </tr>
                                        </thead>
                                        <tbody>

                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                 <div class="row">
                <div class="col-lg-4 col-12">
                    <div class="card pull-up">
                        <div class="card-content">
                            <div class="card-body">
                                <div class="media d-flex">
                                    <div class="media-body text-left">
                                        <h6 class="text-muted">Total Bet Amount  </h6>
                                        <h3><span>&#8377;</span> <?php echo $total_amount; ?></h3>
                                    </div>
                                    <div class="align-self-center">
                                        <i class="icon-trophy success font-large-2 float-right"></i>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-12">
                    <div class="card pull-up">
                        <div class="card-content">
                            <div class="card-body">
                                <div class="media d-flex">
                                    <div class="media-body text-left">
                                        <h6 class="text-muted">Total Win Amount  </h6>
                                         <h3><span>&#8377;</span> <?php echo $total_win_amount; ?></h3>
                                    </div>
                                    <div class="align-self-center">
                                        <i class="icon-trophy success font-large-2 float-right"></i>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-12">
                    <div class="card pull-up">
                        <div class="card-content">
                            <div class="card-body">
                                <div class="media d-flex">
                                    <div class="media-body text-left">
                                        <h6 class="text-muted">Loss Amount  </h6>
                                        <h3><span>&#8377;</span> <?php echo $total_lose_amount; ?></h3>
                                    </div>
                                    <div class="align-self-center">
                                        <i class="icon-trophy success font-large-2 float-right"></i>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                </div>

                <div class="row">
                <div class="col-lg-4 col-12">
                    <div class="card pull-up">
                        <div class="card-content">
                            <div class="card-body">
                                <div class="media d-flex">
                                    <div class="media-body text-left">
                                        <h6 class="text-muted">Withdrawals Amount </h6>
                                         <h3><span>&#8377;</span> <?php echo $total_withdrawamount; ?></h3>
                                    </div>
                                    <div class="align-self-center">
                                        <i class="icon-trophy success font-large-2 float-right"></i>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-12">
                    <div class="card pull-up">
                        <div class="card-content">
                            <div class="card-body">
                                <div class="media d-flex">
                                    <div class="media-body text-left">
                                        <h6 class="text-muted">Total Deposit Amount </h6>
                                         <h3><span>&#8377;</span> <?php echo $total_depositamount[0]['sumf']; ?></h3>
                                    </div>
                                    <div class="align-self-center">
                                        <i class="icon-trophy success font-large-2 float-right"></i>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-12">
                    <div class="card pull-up">
                        <div class="card-content">
                            <div class="card-body">
                                <div class="media d-flex">
                                    <div class="media-body text-left">
                                        <h6 class="text-muted">Total Commission Amount </h6>
                                         <h3><span>&#8377;</span> <?php echo $total_commissionamount[0]['commsion']; ?></h3>
                                    </div>
                                    <div class="align-self-center">
                                        <i class="icon-trophy success font-large-2 float-right"></i>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                </div>


            </div>
        </div>
    </div>

    <script src="<?php echo base_url(); ?>admin_assets/app-assets/vendors/js/charts/chart.min.js"></script>
    <script src="<?php echo base_url(); ?>admin_assets/app-assets/js/scripts/charts/chartjs/advance/bubble.min.js">
    </script>
    <script
        src="<?php echo base_url(); ?>admin_assets/app-assets/js/scripts/charts/chartjs/advance/combo-bar-line.min.js">
        </script>
    <script
        src="<?php echo base_url(); ?>admin_assets/app-assets/js/scripts/charts/chartjs/advance/combo-bar-line-data-label.min.js">
        </script>
    <script
        src="<?php echo base_url(); ?>admin_assets/app-assets/js/scripts/charts/chartjs/advance/different-point-sizes.min.js">
        </script>
    <script src="<?php echo base_url(); ?>admin_assets/app-assets/js/scripts/pages/dashboard-crypto.min.js"></script>
</body>