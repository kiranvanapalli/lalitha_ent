<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Admin_dashboards extends MX_Controller
{
    public function __construct()
    {
        parent::__construct();
        /*if user not loged in redirect to home page*/
        modules::run('admin/admin/is_logged_in');
        $this->load->model('Admin_dashboard_model','dashboard');
    }
    
    public function index()
    {
        $data['file'] = 'admin_dashboard/admin_dashboard/dashboards.php';
        $data['custom_js'] = 'admin_dashboard/admin_dashboard/custom_js/all_files_js.php';
        $data['total_amount'] = $this->Allfiles_model->gettotlaamount();
        $data['total_win_amount'] = $this->Allfiles_model->gettotlawinamount();
        $data['total_lose_amount'] = $this->Allfiles_model->gettotlaloseamount();
        $data['total_withdrawamount'] = $this->Allfiles_model->getwithdrawamount();
        $data['total_depositamount'] = $this->Allfiles_model->depositdetails();
        $data['total_commissionamount'] = $this->Allfiles_model->getcommissionamount();
        // print_r( $data['total_commissionamount']);
        $this->load->view('admin_template/main',$data);   
    }
    
    
    public function data()
    {
        $api_url = "http://demo.esytrading.com/api/Slots/slots";
        $curl_pass = array(
            "data"=>'',
            "url"=>$api_url,
            "type"=>"GET",
            "header" => array(
              'Content-Type:application/json',
              'X-API-KEY: easy_trade',
              'App-Secret: 1234'
            )
        );                  
        
    $response = _curl_request($curl_pass,true);

    return $response;
    }

    public function getslot($id)
    {
        $api_url = "http://demo.esytrading.com/api/Slots/slots/".$id;
         $curl_pass = array(
            "data"=>'',
            "url"=>$api_url,
            "type"=>"GET",
            "header" => array(
              'Content-Type:application/json',
              'X-API-KEY: easy_trade',
              'App-Secret: 1234'
            )
        );
        $response = _curl_request($curl_pass,true);

        return $response;  
    }


    public function getdemoslotdetails($id)
    {

         $api_url = "http://demo.esytrading.com/api/Slots/slots/".$id."?account_type=demo";
         $curl_pass = array(
            "data"=>'',
            "url"=>$api_url,
            "type"=>"GET",
            "header" => array(
              'Content-Type:application/json',
              'X-API-KEY: easy_trade',
              'App-Secret: 1234'
            )
        );
        $response = _curl_request($curl_pass,true);

        return $response;  

    }
    public function getrealslotdetails($id)
    {

         $api_url = "http://demo.esytrading.com/api/Slots/slots/".$id."?account_type=real";
         $curl_pass = array(
            "data"=>'',
            "url"=>$api_url,
            "type"=>"GET",
            "header" => array(
              'Content-Type:application/json',
              'X-API-KEY: easy_trade',
              'App-Secret: 1234'
            )
        );
        $response = _curl_request($curl_pass,true);

        return $response;  

    }
    public function getrealslotdata()
    {
            $draw = intval($this->input->get("draw"));
            $start = intval($this->input->get("start"));
            $length = intval($this->input->get("length"));
            $data['slot_list']=$this->data(); 
            $slot_list=$this->data();
            $data_slots = array(); 
            $slot_remain_close_time = '';
                foreach($slot_list['data'] as $slot_list) {
                      $slot_status = $slot_list['slot_status'];
                        $slot_remain_close_time = '';
                        if($slot_list['slot_status'] == 0)
                        {    
                        $slot_id = $slot_list['slot_id'];
                        $slot_user = $this->getrealslotdetails($slot_id);
                        $slot_remain_close_time = $slot_user['data']['slot_remain_close_time'];     
                        $up = [];
                        $down= [];
                        $slot_up_users = [];
                        $slot_down_users = [];
                        $inverst_type = '';
                        
                        foreach($slot_user['user_details'] as $slot_users)
                        {  

                           if($slot_users['account_type'] == "real" )
                           {
                            $slot_inverst_type = $slot_users['invest_type'];
                            if($slot_inverst_type == "up")
                            {   

                                $slot_up_users[] = $slot_users['user_id']; 
                                $up[] = $slot_users['invest_amount'];
                                

                            }
                            else
                            {    
                                $slot_down_users[] = $slot_users['user_id'];   
                                $down[] = $slot_users['invest_amount'];
                                 
                                 
                            }
                         }
                          $inverst_type = $slot_users['account_type'];
                        } 
                        

                        if ($inverst_type == "real") 
                        {
                        $slot_up_users = count($slot_up_users);
                        $slot_down_users = count($slot_down_users);
                        $slot_up_amount = array_sum($up);
                        $slot_down_amount = array_sum($down); 
                        $status = "<span class='btn btn-outline-primary btn-sm btn-secondary'>Processing</span>";
                        $slot_start_time = $slot_list['slot_start_time'];
                        $slot_end_time = $slot_list['slot_end_time'];
                        $slot_start_time = strtotime($slot_start_time);
                        $slot_end_time = strtotime($slot_end_time);
                        $start_time = date('H:i:s', $slot_start_time);
                        $end_time = date('H:i:s', $slot_end_time);
                        $time_duration =  $slot_list['slot_end_time_seconds'] -  $slot_list['slot_start_time_seconds'];
                        $data_slots[] = array( 
                      '<td class="align-middle" id="'.$slot_list['slot_id'].'">'.$slot_list['slot_id'].'</td>',
                      '<td class="align-middle" >'.$slot_up_users.'</td>',
                      '<td class="align-middle" >'.$slot_down_users.'</td>',
                      '<td class="align-middle" >'.$slot_up_amount.'</td>',
                      '<td class="align-middle" >'.$slot_down_amount.'</td>',
                      
                      '<td class="align-middle"><button type="button" class="btn btn-sm btn-success up" id="'.$slot_list['slot_id'].'">UP </button></td>',
                      '<td class="align-middle"><button type="button" class="btn btn-danger btn-sm down" id="'.$slot_list['slot_id'].'">Down </button></td>',
                      '<td class="align-middle"><button type="button" class="btn btn-danger btn-sm all" id="'.$slot_list['slot_id'].'">All </button></td>',
                      '<td class="align-middle time" id="time" >'.$slot_remain_close_time.'</td>',
                      '<td class="align-middle">'.$start_time.'</td>',
                      '<td class="align-middle">'.$end_time.'</td>',
                      '<td class="align-middle">'.$status.'</td>',
                    );
                        
                    }           
                 }
                }
                $result = array(
                       "draw" => $draw,
                       "recordsTotal" => count($slot_list),
                       "recordsFiltered" => count($slot_list),
                       "data" => $data_slots,
                     );
                     echo json_encode($result);  
              
    }
    public function getdemoslotdata()
    {
            $draw = intval($this->input->get("draw"));
            $start = intval($this->input->get("start"));
            $length = intval($this->input->get("length"));
            $data['slot_demo_list']=$this->data(); 
            $slot_demo_list=$this->data();
            $data_demo_slots = array();
            foreach($slot_demo_list['data'] as $slot_demo_list) 
            {
                $slot_status = $slot_demo_list['slot_status'];
                $slot_remain_close_time = '';
                if($slot_demo_list['slot_status'] == 0)
                {    
                    $slot_id = $slot_demo_list['slot_id'];
                    // echo $slot_id;
                    $slot_user = $this->getdemoslotdetails($slot_id);
                    $slot_remain_close_time = $slot_user['data']['slot_remain_close_time']; 

                    $up = [];
                    $down= [];
                    $slot_up_users = [];
                    $slot_down_users = [];
                    $slot_users= [];
                    $inverst_type = '';
                    foreach($slot_user['user_details'] as $slot_users)
                    {  
                       
                         $slot_inverst_type = $slot_users['invest_type'];
                         if($slot_users['account_type'] == "demo" )
                         {
                            if($slot_inverst_type == "up")
                            {   
                                $slot_up_users[] = $slot_users['user_id']; 
                                $up[] = $slot_users['invest_amount'];
                            }
                            else
                            {    
                                $slot_down_users[] = $slot_users['user_id'];   
                                $down[] = $slot_users['invest_amount'];
                            }
                         }
                        $inverst_type = $slot_users['account_type'];
                    }

                   
                    if($inverst_type == "demo") 
                    {
                        $slot_up_users = count($slot_up_users);
                        $slot_down_users = count($slot_down_users);
                        $slot_up_amount = array_sum($up);
                        $slot_down_amount = array_sum($down); 
                        $status = "<span class='btn btn-outline-primary btn-sm btn-secondary'>Processing</span>";
                        $slot_start_time = $slot_demo_list['slot_start_time'];
                        $slot_end_time = $slot_demo_list['slot_end_time'];
                        $slot_start_time = strtotime($slot_start_time);
                        $slot_end_time = strtotime($slot_end_time);
                        $start_time = date('H:i:s', $slot_start_time);
                        $end_time = date('H:i:s', $slot_end_time);
                        $time_duration =  $slot_demo_list['slot_end_time_seconds'] -  $slot_demo_list['slot_start_time_seconds'];
                        $data_demo_slots[] = array( 
                      '<td class="align-middle" id="'.$slot_demo_list['slot_id'].'">'.$slot_demo_list['slot_id'].'</td>',
                      '<td class="align-middle" >'.$slot_up_users.'</td>',
                      '<td class="align-middle" >'.$slot_down_users.'</td>',
                      '<td class="align-middle" >'.$slot_up_amount.'</td>',
                      '<td class="align-middle" >'.$slot_down_amount.'</td>',
                      
                      '<td class="align-middle"><button type="button" class="btn btn-sm btn-success demoup" id="'.$slot_demo_list['slot_id'].'">UP </button></td>',
                      '<td class="align-middle"><button type="button" class="btn btn-danger btn-sm demodown" id="'.$slot_demo_list['slot_id'].'">Down </button></td>',
                      '<td class="align-middle"><button type="button" class="btn btn-danger btn-sm demoall" id="'.$slot_demo_list['slot_id'].'">All </button></td>',
                      '<td class="align-middle time" id="time" >'.$slot_remain_close_time.'</td>',
                      '<td class="align-middle">'.$start_time.'</td>',
                      '<td class="align-middle">'.$end_time.'</td>',
                      '<td class="align-middle">'.$status.'</td>');
                    }      
                }           
            }
            $result1 = array(
                               "draw" => $draw,
                               "recordsTotal" => count($slot_demo_list),
                               "recordsFiltered" => count($slot_demo_list),
                               "data" => $data_demo_slots,
                            );
            echo json_encode($result1);    
    }


    
}