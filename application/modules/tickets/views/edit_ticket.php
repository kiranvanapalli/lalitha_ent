<style type="text/css">
.table {
text-align: center;
}
.previewimage
{
margin-left: 40%;
width: 24%;
height: 220px;
}
</style>
<div class="app-content content">
   <div class="content-overlay"></div>
   <div class="content-wrapper">
      <div class="content-header row">
         <div class="content-header-left col-md-6 col-12 mb-2 breadcrumb-new">
            <h3 class="content-header-title mb-0 d-inline-block">User Information</h3>
            <div class="row breadcrumbs-top d-inline-block">
               <div class="breadcrumb-wrapper col-12">
                  <ol class="breadcrumb">
                     <li class="breadcrumb-item"><a href="<?php echo base_url(); ?>dashboard">Home</a>
                  </li>
                  <li class="breadcrumb-item active">Ticket Information</li>
               </ol>
            </div>
         </div>
      </div>
      <div class="content-header-right col-md-6 col-12">
         <div class="btn-group float-md-right">
            <a class="btn btn-primary round btn-min-width mr-1 mb-1" aria-haspopup="true" aria-expanded="false" href ="<?php echo base_url(); ?>user_details"><i class="ft-users"></i> Ticket List</a>
         </div>
      </div>
   </div>
   <div class="row">
      <div class="col-md-12">
         <div class="card">
            <div class="card-header">
               <h4 class="card-title" id="row-separator-colored-controls">Ticket Information</h4>
               <a class="heading-elements-toggle"><i class="la la-ellipsis-h font-medium-3"></i></a>
               <div class="heading-elements">
                  <ul class="list-inline mb-0">
                     <li><a data-action="expand"><i class="ft-maximize"></i></a>
                  </li>
               </ul>
            </div>
         </div>
         <div class="card-content collapse show">
            <div class="card-body">
               <form method="post" id='edit_ticket_form' name='ticket_form' class='form form-horizontal' enctype="multipart/form-data">
                  <div class="form-body">
                     <h4 class="form-section"><i class="la la-eye"></i> About Ticket</h4>
                     <div class="row">
                        <div class="col-md-6">
                           <div class="form-group row mx-auto last">
                              <label class="col-md-3 label-control" for="fullname">Full Name</label>
                              <div class="col-md-9">
                                 <input type="text" id="fullname" class="form-control border-primary" placeholder="Full Name" name="fullname" data-toggle="tooltip" data-trigger="hover" data-placement="top" data-title="Full Name" value="<?php echo $details['full_name']; ?>" readonly>
                              </div>
                           </div>
                        </div>
                        <div class="col-md-6">
                           <div class="form-group row mx-auto last">
                              <label class="col-md-3 label-control" for="mobile_number">Mobile Number</label>
                              <div class="col-md-9">
                                 <input type="text" id="mobile_number" class="form-control border-primary" placeholder="Mobile Number" name="mobile_number" data-toggle="tooltip" data-trigger="hover" data-placement="top" data-title="Mobile Number" value="<?php echo $details['phone']; ?>" readonly>
                              </div>
                           </div>
                        </div>
                     </div>
                     <div class="row">
                        <div class="col-md-6">
                           <div class="form-group row mx-auto last">
                              <label class="col-md-3 label-control" for="Email">Email</label>
                              <div class="col-md-9">
                                 <input type="text" id="enail" class="form-control border-primary" placeholder="Email" name="email" data-toggle="tooltip" data-trigger="hover" data-placement="top" data-title="Email" value="<?php echo $details['email']; ?>" readonly>
                              </div>
                           </div>
                           
                        </div>
                        <div class="col-md-6">
                           <div class="form-group row mx-auto last">
                              <label class="col-md-3 label-control" for="issue_type">Issue Type</label>
                              <div class="col-md-9">
                                 <?php
                                 if($details['issue_no'] == 1)
                                 {
                                 $issue_text = "Money loading issue";
                                 }
                                 else if($details['issue_no'] == 2)
                                 {
                                 $issue_text = "Money withdrawal issue";
                                 }
                                 else if($details['issue_no'] == 3)
                                 {
                                 $issue_text = "Bank details issue";
                                 }
                                 else if($details['issue_no'] == 4)
                                 {
                                 $issue_text = "Pan card issue";
                                 }
                                 else if($details['issue_no'] == 5)
                                 {
                                 $issue_text = "Other issue";
                                 }
                                 ?>
                                 <input type="text" id="issue_text" class="form-control border-primary" placeholder="Issue Text" name="issue_text" data-toggle="tooltip" data-trigger="hover" data-placement="top" data-title="Issue Text" autocomplete="off" value="<?php echo $issue_text; ?>" readonly>
                              </div>
                           </div>
                           
                        </div>
                     </div>
                     <div class="row">
                        <div class="col-lg-6">
                           <div class="form-group row mx-auto last">
                              <?php
                              if($details['user_text'] == '')
                              {
                              $user_comment = '';
                              }
                              else
                              {
                              $user_comment = $details['user_text'];
                              }
                              ?>
                              <label class="col-md-3 label-control" for="UserComment">User Comment</label>
                              <div class="col-md-9">
                                 
                                 <textarea class="textarea" id="UserComment" name="UserComment"
                                 style="width: 257%; height: 250px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;" readonly><?php echo $user_comment; ?></textarea>
                              </div>
                           </div>
                        </div>
                        <div class="col-lg-6">
                           <div class="form-group row mx-auto last">
                              
                           </div>
                        </div>
                     </div>
                     <div class="row">
                        <div class="col-lg-6">
                           <div class="form-group row mx-auto last">
                             <?php   if($details['admin_response'] == '')
                                 {
                                 $admin_comment = '';
                                 }
                                 else
                                 {
                                 $admin_comment = $details['admin_response'];
                                 }
                                 ?>
                              <label class="col-md-3 label-control" for="admin_comment">Admin Comment</label>
                              <div class="col-md-9">
                                 
                                 <textarea class="textarea" id="admin_comment" name="admin_comment"
                                 style="width: 257%; height: 250px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;" required>
                                 <?php echo $admin_comment; ?></textarea>
                              </div>
                           </div>
                        </div>
                        <div class="col-lg-6">
                           <div class="form-group row mx-auto last">
                              
                           </div>
                        </div>
                     </div>
                       <?php if($details['image_name'] != '') { ?>

                       <div class="col-lg-12">
                        <div class="col-lg-6">
                           <div class="form-group row mx-auto last">
                              <label class="col-md-3 label-control upload">Support Attachment By User</label>
                              <div class="col-md-9 mx-auto">

                                 <label id="projectinput8" class="file center-block">
                                     <a href="<?php echo $details['image_url']; ?>" title="Banner Image" target="_blank"><img src="<?php echo $details['image_url']; ?>" alt="gallery" class="form-group img-thumbnail" id="previewImg"></a>
                                 </label>
                              </div>
                           </div>
                        </div>
                     </div>
                      <?php }  ?>
                     
                     <h4 class="form-section"><i class="la la-user"></i> Ticket Status</h4>
                     <div class="form-group row mx-auto last text-center">
                                        <div class="input-group col-md-12 text-center">
                                            <div class="d-inline-block custom-control custom-radio mr-1">
                                                <input type="radio" name="user_status" class="custom-control-input" id="active" value="1" <?php if ($details['status'] == 1) {
                                                      echo "checked=checked";
                                                } ?>>
                                                <label class="custom-control-label" for="active">Pending</label>
                                            </div>
                                            <div class="d-inline-block custom-control custom-radio">
                                                <input type="radio" name="user_status" class="custom-control-input" id="inactive" value= "0" <?php if($details['status'] == 0)
                                                {
                                                        echo "checked=checked";
                                                } ?>>
                                                <label class="custom-control-label" for="inactive">Close</label>
                                            </div>
                                        </div>
                                    </div>
                  </div>
                  <div class="form-actions text-right">
                     <button type="button" class="btn btn-warning mr-1"> <i class="la la-remove"></i> Cancel</button>
                     <button type="submit" class="btn btn-primary"> <i class="la la-check"></i> Update</button>
                  </div>
                  <input type="hidden" name="edit_id" id="edit_id" value="<?php echo $details['support_id']; ?>">
               </form>
            </div>
         </div>
      </div>
   </div>
</div>
</div>
</div>