<script>
$(document).ready(function(){
         $(document).on('submit', '#edit_ticket_form', function(event){
          event.preventDefault();
                var admin_comment = $('#admin_comment').text();
                console.log(admin_comment);
                if (admin_comment == '') {
                    toastr["error"]("Please Enter Admin Comments");
                    
                    return false;
                }
                
          $.ajax({  
               url:"<?php echo base_url() ?>update_ticket_details",  
               method:'POST',  
               data:new FormData(this),
               contentType:false,  
               processData:false, 
               dataType:'JSON',
               success:function(data)  
               {  
                  if(data.status == 'success')
                  { 
                      
                      toastr["success"]("Ticket Details Updated Successfully!");
                      window.location.href = "<?php echo base_url();?>support_tickets";
                  }
                  else
                  {    
                         toastr["error"]("Ticket Details updated failed! Please try again.");
                         return false;
                        
                  }

                    
               }       
          });   
      });
  });
</script>