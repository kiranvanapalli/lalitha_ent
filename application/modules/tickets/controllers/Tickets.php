<?php
// error_reporting(E_ALL & ~E_NOTICE);
defined('BASEPATH') OR exit('No direct script access allowed');
class Tickets extends MX_Controller
{
    public function __construct()
    {
      parent::__construct();
      /*if user not loged in redirect to home page*/
      modules::run('admin/admin/is_logged_in');
      $this->load->model('task_list/Allfiles_model'); 
      $this->load->library('my_file_upload');

    }

    public function index()
    {
      $where = '';
      $data['file'] = 'tickets/ticket_list';
      $data['custom_js']  = 'tickets/all_files_js';
      $data['validation_js']       = 'admin/all_common_js/frontend_validation_admin';
      $this->load->view('admin_template/main',$data);  
    }
    public function data()
    {
        $api_url = "https://esytrading.com/api/supports/supports";
        $curl_pass = array(
            "data"=>'',
            "url"=>$api_url,
            "type"=>"GET",
            "header" => array(
              'Content-Type:application/json',
              'X-API-KEY: easy_trade',
              'App-Secret: 1234'
            )
        );					
        
    $response = _curl_request($curl_pass,true);

    return $response;
    }

    
    

    public function getticketlist()
    {   
            $draw = intval($this->input->get("draw"));
            $start = intval($this->input->get("start"));
            $length = intval($this->input->get("length"));
            $data['ticket_list']=$this->data(); 
            $ticket_list_data=$this->data();
            $i = 1;
            // echo "<pre>";

            // print_r($user_list);
            
            // echo "</pre>";
            // die();	
            foreach($ticket_list_data['data'] as $ticket_list) {
            $status = '';
            $edit_action = '';
            $delete_action = '';
            $password='';
            if($ticket_list['status'] == 1) {
              $status = "<span class='btn btn-sm btn-outline-primary'>Pending</span>";
            } else {
              $status = "<span class='btn btn-sm btn-outline-danger'>Close</span>";
            }
            if($ticket_list['issue_no'] == 1)
            {
            	$issue_text = "Money loading issue";
            }
            else if($ticket_list['issue_no'] == 2)
            {
            	$issue_text = "Money withdrawal issue";
            } 
            else if($ticket_list['issue_no'] == 3)
            {
            	$issue_text = "Bank details issue";
            }
            else if($ticket_list['issue_no'] == 4)
            {
            	$issue_text = "Pan card issue";
            }
            else if($ticket_list['issue_no'] == 5)
            {
            	$issue_text = "Other issue";
            }
          
            $base_url = base_url();
            $user_id = base64_encode(base64_encode($ticket_list['support_id']));
            // $full_name =  '<a class="edit_user" id="'.$users['user_id'].'" href="'.$base_url.'edituserdetails?id='.$user_id.'" title="Edit">'.$users['full_name'].'</a>';
            $edit_action = '<a class="dropdown-item edit_user" id="'.$ticket_list['support_id'].'" href="'.$base_url.'editticket?id='.$user_id.'" title="Edit"><i class="ft-edit float-right"></i>Edit </a>';
            // $delete_action = '<a class="dropdown-item delete_user" id="'.$ticket_list['support_id'].'" href="#" title="Remove"><i class="ft-trash float-right"></i>Delete</a>';
            $data_tickets[] = array( 



              '<td class="align-middle">'.$i++.'</td>',
              '<td class="align-middle">'.$ticket_list['full_name'].'</td>',
              '<td class="align-middle">'.$ticket_list['phone'].'</td>',
              // '<td class="align-middle">'.$ticket_list['email'].'</td>', 
              '<td class="align-middle">'.$issue_text.'</td>',

              // '<td class="align-middle">'.$user_password.'</td>',
              '<td class="align-middle">'.$ticket_list['created_at'].'</td>',
              '<td class="align-middle">'.$status.'</td>', 
              '<td class="align-middle">'.$edit_action.'</td>',


              // '<td class="align-middle">
              //     <div class="btn-group mr-1 align-middle">
              //       <button type="button" class="btn btn-warning dropdown-toggle data_users" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Action</button>
              //                 <div class="dropdown-menu">
              //                     <button class="dropdown-item datatable-users" type="button">'.$edit_action.'</button>
              //                             <button class="dropdown-item datatable-users" type="button">'.$delete_action.'</button>
              //                                       </div>
              //                                 </div>
              // </td>',
            );
          }
      
          $result = array(
           "draw" => $draw,
           "recordsTotal" => count($ticket_list_data),
           "recordsFiltered" => count($ticket_list_data),
           "data" => $data_tickets,
         );

      echo json_encode($result);  
      } 

      public function get_support($id)
    {

        $api_url = "https://esytrading.com/api/supports/supports/".$id;
            $curl_pass = array(
            "data"=>'',
            "url"=>$api_url,
            "type"=>"GET",
            "header" => array(
              'Content-Type:application/json',
              'X-API-KEY: easy_trade',
              'App-Secret: 1234'
            )

        );	
       	
        
    $response = _curl_request($curl_pass,true);

    return $response;
    }

    
    public function editticket()
    {
        if(isset($_GET['id']) && !empty($_GET['id'])) 
        {
            $id = base64_decode(base64_decode($_GET['id']));
            $data['file'] = 'tickets/edit_ticket';
            $data['custom_js'] = 'tickets/custom_js';
           	$edit_ticket = $this->get_support($id);
           	$d = $edit_ticket['data'][0];
           	$data['details'] = $d;
            $this->load->view('admin_template/main',$data);
        }      
    }

    public function update_ticket_details()
    {
    if(!empty($_POST['edit_id']))
        {   

          $response = [];
            $where = ['support_id' => $_POST['edit_id']];
            $data = array(
                       'admin_response' => $this->input->post('admin_comment'),
                       'updated_at' => date('Y-m-d H:i:s'),  
                       'status' => $this->input->post('user_status'),
              ); 
            $update_user =   $this->Allfiles_model->updateData("tb_supports",$data,$where);
             if($update_user) 
             {
             $response = ['status' => 'success'];
            }
            else
            {
               $response = ['status' => 'fail'];
            }  
            echo json_encode($response);
        } 
                                   
    }

   



       
}