<style>
  /*--------------------------------------------------------------
>> TABLE OF CONTENTS:
----------------------------------------------------------------
1. Normalize
2. Typography
3. Invoice General Style
--------------------------------------------------------------*/
/*--------------------------------------------------------------
2. Normalize
----------------------------------------------------------------*/
@import url("https://fonts.googleapis.com/css2?family=Inter:wght@400;600;700&amp;display=swap");
*,
::after,
::before {
  -webkit-box-sizing: border-box;
          box-sizing: border-box;
}

html {
  line-height: 1.15;
  -webkit-text-size-adjust: 100%;
}

/* Sections
   ========================================================================== */
/**
 * Remove the margin in all browsers.
 */
body {
  margin: 0;
}

/**
 * Render the `main` element consistently in IE.
 */
main {
  display: block;
}

/**
 * Correct the font size and margin on `h1` elements within `section` and
 * `article` contexts in Chrome, Firefox, and Safari.
 */
h1 {
  font-size: 2em;
  margin: 0.67em 0;
}


.cs-logo

/* Grouping content
   ========================================================================== */
/**
 * 1. Add the correct box sizing in Firefox.
 * 2. Show the overflow in Edge and IE.
 */
hr {
  -webkit-box-sizing: content-box;
          box-sizing: content-box;
  /* 1 */
  height: 0;
  /* 1 */
  overflow: visible;
  /* 2 */
}

/**
 * 1. Correct the inheritance and scaling of font size in all browsers.
 * 2. Correct the odd `em` font sizing in all browsers.
 */
pre {
  font-family: monospace, monospace;
  /* 1 */
  font-size: 1em;
  /* 2 */
}

/* Text-level semantics
   ========================================================================== */
/**
 * Remove the gray background on active links in IE 10.
 */
a {
  background-color: transparent;
}

/**
 * 1. Remove the bottom border in Chrome 57-
 * 2. Add the correct text decoration in Chrome, Edge, IE, Opera, and Safari.
 */
abbr[title] {
  border-bottom: none;
  /* 1 */
  text-decoration: underline;
  /* 2 */
  -webkit-text-decoration: underline dotted;
          text-decoration: underline dotted;
  /* 2 */
}

/**
 * Add the correct font weight in Chrome, Edge, and Safari.
 */
b,
strong {
  font-weight: bolder;
}

/**
 * 1. Correct the inheritance and scaling of font size in all browsers.
 * 2. Correct the odd `em` font sizing in all browsers.
 */
code,
kbd,
samp {
  font-family: monospace, monospace;
  /* 1 */
  font-size: 1em;
  /* 2 */
}

/**
 * Add the correct font size in all browsers.
 */
small {
  font-size: 80%;
}

/**
 * Prevent `sub` and `sup` elements from affecting the line height in
 * all browsers.
 */
sub,
sup {
  font-size: 75%;
  line-height: 0;
  position: relative;
  vertical-align: baseline;
}

sub {
  bottom: -0.25em;
}

sup {
  top: -0.5em;
}

/* Embedded content
   ========================================================================== */
/**
 * Remove the border on images inside links in IE 10.
 */
img {
  border-style: none;
}

/* Forms
   ========================================================================== */
/**
 * 1. Change the font styles in all browsers.
 * 2. Remove the margin in Firefox and Safari.
 */
button,
input,
optgroup,
select,
textarea {
  font-family: inherit;
  /* 1 */
  font-size: 100%;
  /* 1 */
  line-height: 1.15;
  /* 1 */
  margin: 0;
  /* 2 */
}

/**
 * Show the overflow in IE.
 * 1. Show the overflow in Edge.
 */
button,
input {
  /* 1 */
  overflow: visible;
}

/**
 * Remove the inheritance of text transform in Edge, Firefox, and IE.
 * 1. Remove the inheritance of text transform in Firefox.
 */
button,
select {
  /* 1 */
  text-transform: none;
}

/**
 * Correct the inability to style clickable types in iOS and Safari.
 */
button,
[type='button'],
[type='reset'],
[type='submit'] {
  -webkit-appearance: button;
}

/**
 * Remove the inner border and padding in Firefox.
 */
button::-moz-focus-inner,
[type='button']::-moz-focus-inner,
[type='reset']::-moz-focus-inner,
[type='submit']::-moz-focus-inner {
  border-style: none;
  padding: 0;
}

/**
 * Restore the focus styles unset by the previous rule.
 */
button:-moz-focusring,
[type='button']:-moz-focusring,
[type='reset']:-moz-focusring,
[type='submit']:-moz-focusring {
  outline: 1px dotted ButtonText;
}

/**
 * Correct the padding in Firefox.
 */
fieldset {
  padding: 0.35em 0.75em 0.625em;
}

/**
 * 1. Correct the text wrapping in Edge and IE.
 * 2. Correct the color inheritance from `fieldset` elements in IE.
 * 3. Remove the padding so developers are not caught out when they zero out
 *    `fieldset` elements in all browsers.
 */
legend {
  -webkit-box-sizing: border-box;
          box-sizing: border-box;
  /* 1 */
  color: inherit;
  /* 2 */
  display: table;
  /* 1 */
  max-width: 100%;
  /* 1 */
  padding: 0;
  /* 3 */
  white-space: normal;
  /* 1 */
}

/**
 * Add the correct vertical alignment in Chrome, Firefox, and Opera.
 */
progress {
  vertical-align: baseline;
}

/**
 * Remove the default vertical scrollbar in IE 10+.
 */
textarea {
  overflow: auto;
}

/**
 * 1. Add the correct box sizing in IE 10.
 * 2. Remove the padding in IE 10.
 */
[type='checkbox'],
[type='radio'] {
  -webkit-box-sizing: border-box;
          box-sizing: border-box;
  /* 1 */
  padding: 0;
  /* 2 */
}

/**
 * Correct the cursor style of increment and decrement buttons in Chrome.
 */
[type='number']::-webkit-inner-spin-button,
[type='number']::-webkit-outer-spin-button {
  height: auto;
}

/**
 * 1. Correct the odd appearance in Chrome and Safari.
 * 2. Correct the outline style in Safari.
 */
[type='search'] {
  -webkit-appearance: textfield;
  /* 1 */
  outline-offset: -2px;
  /* 2 */
}

/**
 * Remove the inner padding in Chrome and Safari on macOS.
 */
[type='search']::-webkit-search-decoration {
  -webkit-appearance: none;
}

/**
 * 1. Correct the inability to style clickable types in iOS and Safari.
 * 2. Change font properties to `inherit` in Safari.
 */
::-webkit-file-upload-button {
  -webkit-appearance: button;
  /* 1 */
  font: inherit;
  /* 2 */
}

/* Interactive
   ========================================================================== */
/*
 * Add the correct display in Edge, IE 10+, and Firefox.
 */
details {
  display: block;
}

/*
 * Add the correct display in all browsers.
 */
summary {
  display: list-item;
}

/* Misc
   ========================================================================== */
/**
 * Add the correct display in IE 10+.
 */
template {
  display: none;
}

/**
 * Add the correct display in IE 10.
 */
[hidden] {
  display: none;
}

/*--------------------------------------------------------------
2. Typography
----------------------------------------------------------------*/
body,
html {
  color: #777777;
  font-family: 'Inter', sans-serif;
  font-size: 14px;
  font-weight: 400;
  line-height: 1.5em;
  overflow-x: hidden;
  background-color: #f5f7ff;
}

h1,
h2,
h3,
h4,
h5,
h6 {
  clear: both;
  color: #111111;
  padding: 0;
  margin: 0 0 20px 0;
  font-weight: 500;
  line-height: 1.2em;
}

h1 {
  font-size: 60px;
}

h2 {
  font-size: 48px;
}

h3 {
  font-size: 30px;
}

h4 {
  font-size: 24px;
}

h5 {
  font-size: 18px;
}

h6 {
  font-size: 16px;
}

p,
div {
  margin-top: 0;
  line-height: 1.5em;
}

p {
  margin-bottom: 15px;
}

ul {
  margin: 0 0 25px 0;
  padding-left: 20px;
  list-style: square outside none;
}

ol {
  padding-left: 20px;
  margin-bottom: 25px;
}

dfn,
cite,
em,
i {
  font-style: italic;
}

blockquote {
  margin: 0 15px;
  font-style: italic;
  font-size: 20px;
  line-height: 1.6em;
  margin: 0;
}

address {
  margin: 0 0 15px;
}

img {
  border: 0;
  max-width: 100%;
  height: auto;
  vertical-align: middle;
}

a {
  color: inherit;
  text-decoration: none;
  -webkit-transition: all 0.3s ease;
  transition: all 0.3s ease;
}

a:hover {
  color: #2ad19d;
}

button {
  color: inherit;
  -webkit-transition: all 0.3s ease;
  transition: all 0.3s ease;
}

a:hover {
  text-decoration: none;
  color: inherit;
}

table {
  width: 100%;
  caption-side: bottom;
  border-collapse: collapse;
}

th {
  text-align: left;
}

td {
  border-top: 1px solid #eaeaea;
}

td,
th {
  padding: 10px 15px;
  line-height: 1.55em;
}

dl {
  margin-bottom: 25px;
}

dl dt {
  font-weight: 600;
}

b,
strong {
  font-weight: bold;
}

pre {
  color: #777777;
  border: 1px solid #eaeaea;
  font-size: 18px;
  padding: 25px;
  border-radius: 5px;
}

kbd {
  font-size: 100%;
  background-color: #777777;
  border-radius: 5px;
}

/*--------------------------------------------------------------
3. Invoice General Style
----------------------------------------------------------------*/
.cs-f10 {
  font-size: 10px;
}

.cs-f11 {
  font-size: 11px;
}

.cs-f12 {
  font-size: 12px;
}

.cs-f13 {
  font-size: 13px;
}

.cs-f14 {
  font-size: 14px;
}

.cs-f15 {
  font-size: 15px;
}

.cs-f16 {
  font-size: 16px;
}

.cs-f17 {
  font-size: 17px;
}

.cs-f18 {
  font-size: 18px;
}

.cs-f19 {
  font-size: 19px;
}

.cs-f20 {
  font-size: 20px;
}

.cs-f21 {
  font-size: 21px;
}

.cs-f22 {
  font-size: 22px;
}

.cs-f23 {
  font-size: 23px;
}

.cs-f24 {
  font-size: 24px;
}

.cs-f25 {
  font-size: 25px;
}

.cs-f26 {
  font-size: 26px;
}

.cs-f27 {
  font-size: 27px;
}

.cs-f28 {
  font-size: 28px;
}

.cs-f29 {
  font-size: 29px;
}

.cs-light {
  font-weight: 300;
}

.cs-normal {
  font-weight: 400;
}

.cs-medium {
  font-weight: 500;
}

.cs-semi_bold {
  font-weight: 600;
}

.cs-bold {
  font-weight: 700;
}

.cs-m0 {
  margin: 0px;
}

.cs-mb0 {
  margin-bottom: 0px;
}

.cs-mb1 {
  margin-bottom: 1px;
}

.cs-mb2 {
  margin-bottom: 2px;
}

.cs-mb3 {
  margin-bottom: 3px;
}

.cs-mb4 {
  margin-bottom: 4px;
}

.cs-mb5 {
  margin-bottom: 5px;
}

.cs-mb6 {
  margin-bottom: 6px;
}

.cs-mb7 {
  margin-bottom: 7px;
}

.cs-mb8 {
  margin-bottom: 8px;
}

.cs-mb9 {
  margin-bottom: 9px;
}

.cs-mb10 {
  margin-bottom: 10px;
}

.cs-mb11 {
  margin-bottom: 11px;
}

.cs-mb12 {
  margin-bottom: 12px;
}

.cs-mb13 {
  margin-bottom: 13px;
}

.cs-mb14 {
  margin-bottom: 14px;
}

.cs-mb15 {
  margin-bottom: 15px;
}

.cs-mb16 {
  margin-bottom: 16px;
}

.cs-mb17 {
  margin-bottom: 17px;
}

.cs-mb18 {
  margin-bottom: 18px;
}

.cs-mb19 {
  margin-bottom: 19px;
}

.cs-mb20 {
  margin-bottom: 20px;
}

.cs-mb21 {
  margin-bottom: 21px;
}

.cs-mb22 {
  margin-bottom: 22px;
}

.cs-mb23 {
  margin-bottom: 23px;
}

.cs-mb24 {
  margin-bottom: 24px;
}

.cs-mb25 {
  margin-bottom: 25px;
}

.cs-mb26 {
  margin-bottom: 26px;
}

.cs-mb27 {
  margin-bottom: 27px;
}

.cs-mb28 {
  margin-bottom: 28px;
}

.cs-mb29 {
  margin-bottom: 29px;
}

.cs-mb30 {
  margin-bottom: 30px;
}

.cs-pt25 {
  padding-top: 25px;
}

.cs-width_1 {
  width: 8.33333333%;
}

.cs-width_2 {
  width: 16.66666667%;
}

.cs-width_3 {
  width: 25%;
}

.cs-width_4 {
  width: 33.33333333%;
}

.cs-width_5 {
  width: 41.66666667%;
}

.cs-width_6 {
  width: 50%;
}

.cs-width_7 {
  width: 58.33333333%;
}

.cs-width_8 {
  width: 66.66666667%;
}

.cs-width_9 {
  width: 75%;
}

.cs-width_10 {
  width: 83.33333333%;
}

.cs-width_11 {
  width: 91.66666667%;
}

.cs-width_12 {
  width: 100%;
}

.cs-accent_color,
.cs-accent_color_hover:hover {
  color: #2ad19d;
}

.cs-accent_bg,
.cs-accent_bg_hover:hover {
  background-color: #2ad19d;
}

.cs-primary_color {
  color: #111111;
}

.cs-secondary_color {
  color: #777777;
}

.cs-ternary_color {
  color: #353535;
}

.cs-ternary_color {
  border-color: #eaeaea;
}

.cs-focus_bg {
  background: #f6f6f6;
}

.cs-accent_10_bg {
  background-color: rgba(42, 209, 157, 0.1);
}

.cs-container {
  max-width: 880px;
  padding: 30px 15px;
  margin-left: auto;
  margin-right: auto;
}

.cs-text_center {
  text-align: center;
}

.cs-text_right {
  text-align: right;
}

.cs-border_bottom_0 {
  border-bottom: 0;
}

.cs-border_top_0 {
  border-top: 0;
}

.cs-border_bottom {
  border-bottom: 1px solid #eaeaea;
}

.cs-border_top {
  border-top: 1px solid #eaeaea;
}

.cs-border_left {
  border-left: 1px solid #eaeaea;
}

.cs-border_right {
  border-right: 1px solid #eaeaea;
}

.cs-table_baseline {
  vertical-align: baseline;
}

.cs-round_border {
  border: 1px solid #eaeaea;
  overflow: hidden;
  border-radius: 6px;
}

.cs-border_none {
  border: none;
}

.cs-border_left_none {
  border-left-width: 0;
}

.cs-border_right_none {
  border-right-width: 0;
}

.cs-invoice.cs-style1 {
  background: #fff;
  border-radius: 10px;
  padding: 50px;
}

.cs-invoice.cs-style1 .cs-invoice_head {
  display: -webkit-box;
  display: -ms-flexbox;
  display: flex;
  -webkit-box-pack: justify;
      -ms-flex-pack: justify;
          justify-content: space-between;
}

.cs-invoice.cs-style1 .cs-invoice_head.cs-type1 {
  -webkit-box-align: end;
      -ms-flex-align: end;
          align-items: flex-end;
  padding-bottom: 25px;
  border-bottom: 1px solid #eaeaea;
}

.cs-invoice.cs-style1 .cs-invoice_footer {
  display: -webkit-box;
  display: -ms-flexbox;
  display: flex;
}

.cs-invoice.cs-style1 .cs-invoice_footer table {
  margin-top: -1px;
}

.cs-invoice.cs-style1 .cs-left_footer {
  width: 55%;
  padding: 10px 15px;
}

.cs-invoice.cs-style1 .cs-right_footer {
  width: 46%;
}

.cs-invoice.cs-style1 .cs-note {
  display: -webkit-box;
  display: -ms-flexbox;
  display: flex;
  -webkit-box-align: start;
      -ms-flex-align: start;
          align-items: flex-start;
  margin-top: 40px;
}

.cs-invoice.cs-style1 .cs-note_left {
  margin-right: 10px;
  margin-top: 6px;
  margin-left: -5px;
  display: -webkit-box;
  display: -ms-flexbox;
  display: flex;
}

.cs-invoice.cs-style1 .cs-note_left svg {
  width: 32px;
}

.cs-invoice.cs-style1 .cs-invoice_left {
  max-width: 55%;
}

.cs-invoice_btns {
  display: -webkit-box;
  display: -ms-flexbox;
  display: flex;
  -webkit-box-pack: center;
      -ms-flex-pack: center;
          justify-content: center;
  margin-top: 30px;
}

.cs-invoice_btns .cs-invoice_btn:first-child {
  border-radius: 5px 0 0 5px;
}

.cs-invoice_btns .cs-invoice_btn:last-child {
  border-radius: 0 5px 5px 0;
}

.cs-invoice_btn {
  display: -webkit-inline-box;
  display: -ms-inline-flexbox;
  display: inline-flex;
  -webkit-box-align: center;
      -ms-flex-align: center;
          align-items: center;
  border: none;
  font-weight: 600;
  padding: 8px 20px;
  cursor: pointer;
}

.cs-invoice_btn svg {
  width: 24px;
  margin-right: 5px;
}

.cs-invoice_btn.cs-color1 {
  color: #111111;
  background: rgba(42, 209, 157, 0.15);
}

.cs-invoice_btn.cs-color1:hover {
  background-color: rgba(42, 209, 157, 0.3);
}

.cs-invoice_btn.cs-color2 {
  color: #fff;
  background: #2ad19d;
}

.cs-invoice_btn.cs-color2:hover {
  background-color: rgba(42, 209, 157, 0.8);
}

.cs-table_responsive {
  overflow-x: auto;
}

.cs-table_responsive > table {
  min-width: 600px;
}

.cs-50_col > * {
  width: 50%;
  -webkit-box-flex: 0;
      -ms-flex: none;
          flex: none;
}

.cs-bar_list {
  margin: 0;
  padding: 0;
  list-style: none;
  position: relative;
}

.cs-bar_list::before {
  content: '';
  height: 75%;
  width: 2px;
  position: absolute;
  left: 4px;
  top: 50%;
  -webkit-transform: translateY(-50%);
          transform: translateY(-50%);
  background-color: #eaeaea;
}

.cs-bar_list li {
  position: relative;
  padding-left: 25px;
}

.cs-bar_list li:before {
  content: '';
  height: 10px;
  width: 10px;
  border-radius: 50%;
  background-color: #eaeaea;
  position: absolute;
  left: 0;
  top: 6px;
}

.cs-bar_list li:not(:last-child) {
  margin-bottom: 10px;
}

.cs-table.cs-style1.cs-type1 {
  padding: 10px 30px;
}

.cs-table.cs-style1.cs-type1 tr:first-child td {
  border-top: none;
}

.cs-table.cs-style1.cs-type1 tr td:first-child {
  padding-left: 0;
}

.cs-table.cs-style1.cs-type1 tr td:last-child {
  padding-right: 0;
}

.cs-table.cs-style1.cs-type2 > * {
  padding: 0 10px;
}

.cs-table.cs-style1.cs-type2 .cs-table_title {
  padding: 20px 0 0 15px;
  margin-bottom: -5px;
}

.cs-table.cs-style2 td {
  border: none;
}

.cs-table.cs-style2 td,
.cs-table.cs-style2 th {
  padding: 12px 15px;
  line-height: 1.55em;
}

.cs-table.cs-style2 tr:not(:first-child) {
  border-top: 1px dashed #eaeaea;
}

.cs-list.cs-style1 {
  list-style: none;
  padding: 0;
  margin: 0;
}

.cs-list.cs-style1 li {
  display: -webkit-box;
  display: -ms-flexbox;
  display: flex;
}

.cs-list.cs-style1 li:not(:last-child) {
  border-bottom: 1px dashed #eaeaea;
}

.cs-list.cs-style1 li > * {
  -webkit-box-flex: 0;
      -ms-flex: none;
          flex: none;
  width: 50%;
  padding: 7px 0px;
}

.cs-list.cs-style2 {
  list-style: none;
  margin: 0 0 30px 0;
  padding: 12px 0;
  border: 1px solid #eaeaea;
  border-radius: 5px;
}

.cs-list.cs-style2 li {
  display: -webkit-box;
  display: -ms-flexbox;
  display: flex;
}

.cs-list.cs-style2 li > * {
  -webkit-box-flex: 1;
      -ms-flex: 1;
          flex: 1;
  padding: 5px 25px;
}

.cs-heading.cs-style1 {
  line-height: 1.5em;
  border-top: 1px solid #eaeaea;
  border-bottom: 1px solid #eaeaea;
  padding: 10px 0;
}

.cs-no_border {
  border: none !important;
}

.cs-grid_row {
  display: -ms-grid;
  display: grid;
  grid-gap: 20px;
  list-style: none;
  padding: 0;
}

.cs-col_2 {
  -ms-grid-columns: (1fr)[2];
      grid-template-columns: repeat(2, 1fr);
}

.cs-col_3 {
  -ms-grid-columns: (1fr)[3];
      grid-template-columns: repeat(3, 1fr);
}

.cs-col_4 {
  -ms-grid-columns: (1fr)[4];
      grid-template-columns: repeat(4, 1fr);
}

.cs-border_less td {
  border-color: transparent;
}

.cs-special_item {
  position: relative;
}

.cs-special_item:after {
  content: '';
  height: 52px;
  width: 1px;
  background-color: #eaeaea;
  position: absolute;
  top: 50%;
  -webkit-transform: translateY(-50%);
          transform: translateY(-50%);
  right: 0;
}

.cs-table.cs-style1 .cs-table.cs-style1 tr:not(:first-child) td {
  border-color: #eaeaea;
}

.cs-table.cs-style1 .cs-table.cs-style2 td {
  padding: 12px 0px;
}

.cs-ticket_wrap {
  display: -webkit-box;
  display: -ms-flexbox;
  display: flex;
}

.cs-ticket_left {
  -webkit-box-flex: 1;
      -ms-flex: 1;
          flex: 1;
}

.cs-ticket_right {
  -webkit-box-flex: 0;
      -ms-flex: none;
          flex: none;
  width: 215px;
}

.cs-box.cs-style1 {
  border: 2px solid #eaeaea;
  border-radius: 5px;
  padding: 20px 10px;
  min-width: 150px;
}

.cs-box.cs-style1.cs-type1 {
  padding: 12px 10px 10px;
}

.cs-max_w_150 {
  max-width: 150px;
}

.cs-left_auto {
  margin-left: auto;
}

.cs-title_1 {
  display: inline-block;
  border-bottom: 1px solid #eaeaea;
  min-width: 60%;
  padding-bottom: 5px;
  margin-bottom: 10px;
}

.cs-box2_wrap {
  display: -ms-grid;
  display: grid;
  grid-gap: 30px;
  list-style: none;
  padding: 0;
  -ms-grid-columns: (1fr)[2];
      grid-template-columns: repeat(2, 1fr);
}

.cs-box.cs-style2 {
  border: 1px solid #eaeaea;
  padding: 25px 30px;
  border-radius: 5px;
}

.cs-box.cs-style2 .cs-table.cs-style2 td {
  padding: 12px 0;
}

@media print {
  .cs-hide_print {
    display: none !important;
  }
}

@media (max-width: 767px) {
  .cs-mobile_hide {
    display: none;
  }
  .cs-invoice.cs-style1 {
    padding: 30px 20px;
  }
  .cs-invoice.cs-style1 .cs-right_footer {
    width: 100%;
  }
}

@media (max-width: 500px) {
  .cs-invoice.cs-style1 .cs-logo {
    margin-bottom: 10px;
  }
  .cs-invoice.cs-style1 .cs-invoice_head {
    -webkit-box-orient: vertical;
    -webkit-box-direction: normal;
        -ms-flex-direction: column;
            flex-direction: column;
  }
  .cs-invoice.cs-style1 .cs-invoice_head.cs-type1 {
    -webkit-box-orient: vertical;
    -webkit-box-direction: reverse;
        -ms-flex-direction: column-reverse;
            flex-direction: column-reverse;
    -webkit-box-align: center;
        -ms-flex-align: center;
            align-items: center;
    text-align: center;
  }
  .cs-invoice.cs-style1 .cs-invoice_head .cs-text_right {
    text-align: left;
  }
  .cs-list.cs-style2 li {
    -webkit-box-orient: vertical;
    -webkit-box-direction: normal;
        -ms-flex-direction: column;
            flex-direction: column;
  }
  .cs-list.cs-style2 li > * {
    padding: 5px 20px;
  }
  .cs-grid_row {
    grid-gap: 0px;
  }
  .cs-col_2,
  .cs-col_3,
  .cs-col_4 {
    -ms-grid-columns: (1fr)[1];
        grid-template-columns: repeat(1, 1fr);
  }
  .cs-table.cs-style1.cs-type1 {
    padding: 0px 20px;
  }
  .cs-box2_wrap {
    -ms-grid-columns: (1fr)[1];
        grid-template-columns: repeat(1, 1fr);
  }
  .cs-box.cs-style1.cs-type1 {
    max-width: 100%;
    width: 100%;
  }
  .cs-invoice.cs-style1 .cs-invoice_left {
    max-width: 100%;
  }
}

.cs-logo img {
    width: 150px;
    content:url('assets/img/logo.jpg');
}
</style>
<?php

$invoice_data = $html_data[0];
$date = date("d-m-Y", strtotime($invoice_data['created_at']));

?>
<!DOCTYPE html>
<html class="no-js" lang="en">

<meta http-equiv="content-type" content="text/html;charset=utf-8" /><!-- /Added by HTTrack -->

<head>
  <!-- Meta Tags -->
  <meta charset="utf-8">
  <meta http-equiv="x-ua-compatible" content="ie=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta name="author" content="ThemeMarch">
  <!-- Site Title -->
  <title>General Invoice</title>
  <link rel="stylesheet" href="assets/css/style.css">
</head>

<body>
  <div class="cs-container">
    <div class="cs-invoice cs-style1">
      <div class="cs-invoice_in" id="download_section">
        <div class="cs-invoice_head cs-type1 cs-mb25">
          <div class="cs-invoice_left">
            <p class="cs-invoice_number cs-primary_color cs-mb5 cs-f16"><b class="cs-primary_color">Invoice No:</b> <?php echo $invoice_data['invoice_id'] ?></p>
            <p class="cs-invoice_date cs-primary_color cs-m0"><b class="cs-primary_color">Date: </b><?php echo $date; ?></p>
          </div>
          <div class="cs-invoice_right cs-text_right">
            <div class="cs-logo">
            <img alt="Logo" src="data:image/jpeg;base64,/9j/4RFXRXhpZgAATU0AKgAAAAgABwESAAMAAAABAAEAAAEaAAUAAAABAAAAYgEbAAUAAAABAAAAagEoAAMAAAABAAIAAAExAAIAAAAiAAAAcgEyAAIAAAAUAAAAlIdpAAQAAAABAAAAqAAAANQALcbAAAAnEAAtxsAAACcQQWRvYmUgUGhvdG9zaG9wIENDIDIwMTkgKFdpbmRvd3MpADIwMjI6MDQ6MDYgMTc6MzM6MTEAAAOgAQADAAAAAf//AACgAgAEAAAAAQAAAoqgAwAEAAAAAQAAAaUAAAAAAAAABgEDAAMAAAABAAYAAAEaAAUAAAABAAABIgEbAAUAAAABAAABKgEoAAMAAAABAAIAAAIBAAQAAAABAAABMgICAAQAAAABAAAQHQAAAAAAAABIAAAAAQAAAEgAAAAB/9j/7QAMQWRvYmVfQ00AAv/uAA5BZG9iZQBkgAAAAAH/2wCEAAwICAgJCAwJCQwRCwoLERUPDAwPFRgTExUTExgRDAwMDAwMEQwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwBDQsLDQ4NEA4OEBQODg4UFA4ODg4UEQwMDAwMEREMDAwMDAwRDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDP/AABEIAGUAnAMBIgACEQEDEQH/3QAEAAr/xAE/AAABBQEBAQEBAQAAAAAAAAADAAECBAUGBwgJCgsBAAEFAQEBAQEBAAAAAAAAAAEAAgMEBQYHCAkKCxAAAQQBAwIEAgUHBggFAwwzAQACEQMEIRIxBUFRYRMicYEyBhSRobFCIyQVUsFiMzRygtFDByWSU/Dh8WNzNRaisoMmRJNUZEXCo3Q2F9JV4mXys4TD03Xj80YnlKSFtJXE1OT0pbXF1eX1VmZ2hpamtsbW5vY3R1dnd4eXp7fH1+f3EQACAgECBAQDBAUGBwcGBTUBAAIRAyExEgRBUWFxIhMFMoGRFKGxQiPBUtHwMyRi4XKCkkNTFWNzNPElBhaisoMHJjXC0kSTVKMXZEVVNnRl4vKzhMPTdePzRpSkhbSVxNTk9KW1xdXl9VZmdoaWprbG1ub2JzdHV2d3h5ent8f/2gAMAwEAAhEDEQA/APVUkkklKSSSSUpJJJJSkkkklKSSSSUpJJJJSkC/Ow8fIoxr7mV35Zc3HrcQHPLRvf6bfztrUdAxrm5VLMg0vqILtrLm7XiC6vdt930/zP8Ag0lJ0kHGyRfi15JrfQLGh/p3DY9s/mWs/Me1FSUukkkkpSSSSSn/0PVUkkklKSSSSUpJJJJTQ631rA6H023qWe8tpqgBrRue950rppZ+fbY7/wBSbK/eua+pv19y/rN1vJw3YleLi04/rVgOL7d29tfvf7Kvou+i2v8A64s7/HLbYMLpVId+jffY9zexc2vax39n1HrH/wAUH/ikzf8Awl/6NrSU+upJLnuq/XXpOCXV45+3Xt0IqI9MHwsyda/+2vWs/kJ0ISmaiCSmMZS2F09ASACSYA5K4zq/+M3p+PY6jo2M/qz2Ha69rhVjAglrtuU4P9bbH+Br9L/hlS6x1Dq+bhm7r+ZV0TpF2npPad9o/wBHXi65uXz76/0LH/6DYsvod9vVsx2L9UcBrfQA9frXVALDUD9H7NhV/q9N3t/V/pv/ANP6SeccYfNKz+7DX7ZM0IYBrllKX9TF/wCrJ/L/AIk3YxPrZ9dsmv7XbidPwcDQnJyTYypoP5zsi22hr/8ArNdizuo/4w4eK2Z13Ur3w1uL0qoY9BeXbPS/aGa3JzbvU/MdhUrRzf8AFZb1S0ZPV+vZWbk6+9zGbGzr+gocbGUN/kVLS+rv+LnoXQ8pudutzsyvWuzILdrDr76aa2sY1+v07PVf+4mcVfKAP+d/0luTJA/zeMYx5nJP/CnP/uIQd7D6Zi0sbY+rdkFo3use+9wdGrW3ZJfZtV1JJAknc2xKSSSQUpJJJJT/AP/R9VSSSSUpJJJJSkkkklPm/wDjm/o/SP8Ajrv+oaud/wAXfVsfo3U87PyWucw4vpVNaNX2Gxj/AEw76DdrBvetz/Gd1LpvU8vFwKrDYemvsOU5v0N7gK/s4s/OtZH6bZ9D+Z/nv5u99VvqE6xleX1lhqpgGrBHtdB+j9o2/wA1/wAR/Of6b/RKYYqjx5PTE/LH9KTb5bFh/nOZkY44/oQ/nMv9WLD1/rP9cbXMqH2bpsw4SW0gTO21/wDOZtv/AAf81/xK6rov1V6Z0kNsa37RlN/7UWASP+Jr+hQ3+p+k/wCEWvXXXVW2qporrYA1jGgBoA4a1rfoqSEs0iOGPpj2H/dLeZ5r3fTjgMOEfLjj/wB3L9KT4B9bsm7K+tPVbb3F7q8q2msuJO2utxrrrZP0We36Kbpn1p+sPScX7J0zNdi4+91hYyuoy90b3ufZU+x/0fznrS/xj9Is6b9asm3aRj9RjKpd2LiG15LJ/eZc31P6l1al9Sv+ZGS9+B9ZsZrL3OLsbPfdbXW4H/tNf6dtdVNjP8Da72XM/R/z38/E12vX9efrzdYyijqV1t9rgymptVJc57tGMaPQ/Ocva+m15dXTsWrOs9bMZUxuTboN1gaPVf7Q1vusVDo31U+rfRnnI6Xg1VWvGl8usfBH5l17rbGMd/IethJCkkkklKSSSSUpJJJJT//S9VSSSSUpJJJJTGwvDHGsAvg7Q4wJ7bnAOXP/AFp6kzD6H1Hq+BkF17GDDDm2E11v9X7PZZ6TXekzKx32v9+31P0fprolTf0jpj8Z+KcasY9loyLK2tDWutFjcr1LA2N/6djXv3fzidAgSBIsAiwkaEW8b9VPqzhdFwf+cf1gLaPRb6tFd3FDPzLrW/nZln5lf06vof0lcN9b/rvlfWXN3NtON02gn7Jih+0+H2nK2O92TY38z+bxq/0TP8Nbb7pdTTez07622sJB2PAcJB3NO137rkH9m9O/7i0/9tt/8ijkySnIyl/vJlIyNl+cvtH/AHYd/wBuu/8AJomMMnLvZj4jrsi+wxXTU973uP8AJYxxX0T+zenf9xaf+22/+RRKcXGoJNFLKieSxobP+aExFvH4X+Lyi/6mUdF6o8t6gHOyRktO91N9h3bWbj+lqYzbRfXu2ZH/AG3ZX5t176p9d6BY5ufjOdjjjMpBsocPOxo/Q/8AF5Hpr35JJD80NuYwQy7Y391thaP81rgtPoPRep/WHObg4DrXBxAvyNz3V1MP0rLXbtn0f5urd+lXvjun4DyXOxqXOPJNbSf+pRa6qqWBlTG1sHDWgAfc1JNqpqZRSymsQypoY0c6NG1qmkkkhSSSSSlJJJJKf//T9VSSSSUpJJJJSkkkklKSSSSUpJJJJSkkkklKSSSSUpJJJJSkkkklKSSSSU//1PUMnIrxca3JtkV0MdY+BJ2sG90D+qFHCzKc7Eqy6J9K9oezcIMHxCr9e/5D6j/4Vu/89vVX6t5FFP1aw7LLGtbVjB9hn6LWiXud/VTq9N+NM4xA8v7gsz9wY/8ABMeJsu6505vV29GLz9sc3eGgEt4Nmwv/AH/Tb6iJndVxMC3FpyC4PzbRTTtEjcf3v3eV54/Pstrs619kyf2kcwZtWR6ZNIoZ7G4zr5H6Pb9P2f4PYum+seRVk3/VzKqM1XZdVjCdPa7Y9v8A0U84wCB9v95u5Ph8YZMUTxcMozjk1/y+KHHLh/qfuu7ndVxMC7FoyC7fm2ejTtbI3H9/936SzLPrt0OvKdiOdb6rLDUQKyRuDvS/zdyD9aHNf1HoDmkOH20agyOWIln/AIvKv/Taf/Pz0BEVZHQn7GLFgw+3GU4ykTjyZNJcHqxT4f3ZOtjdTxcrMysKrd62EWC6RA/SAvZtd+d9FN0rquJ1bGOViFxrD3Vne0tMt50PxXNU43Wcj6zdb/ZeYzD2ux/W31izdNZ2Rv8Ao7Pel9V+oN6Z9T8zOshxottIB4c87GVt0/ftc1qRgK039P8Azk5OSgMZMJcUz93EYAnijLmMfFLj9P6Uv5vhk9Dh9c6dm9QyenY9hdk4n860ggaHZZsd+d6b/Y9aC816dmN6Xf0vOdjZNVzHvb1TJuqcyuxmQ7R/q7nbvQ+k32fpV6UhkjwkVsx89yowTjwWYSB1l+/jPBP/ABv5z/qiJ+Xisea33VteCAWlwBk/R9soq85u6NkdS+vXXn0dPwM4UW4Je7OLw9gOPW79W9Fj/pbPfv8A5CXUvrH9bcfD651yrPqGH0PqbsZmCcdpN1fqY9XpXZE7qmVsyPY+pvrP/Seo/wDm0xpvobbqX2PqY9rrKo9RgILm7huZvb+buU15pd1PqnQOvfXXqzLmZL8cYTW0vrDGOdk7K8B9ljXb9nTqHupf/wBzP5z9ArVPX/rlh4/UTkszL6WYOVczMy8OnFNGRTU66gsGPdfTdjW7Xey1nqep6aSn0FJcGep/W1zvq5iM6rUzI+sFWTkW3Ox2FlLRTj5FNONTLXWOxt9nputt/Tf4b9H+iQMn6zfWfGeehnNqs6hj9XxOnu6oKG7LKsyq20GzF3em3JxXt/SNqfX/AKL/AEnqJT31eTjW3W0V2sfdjlovra4FzC4b6/VYPdXvZ72b0VcBZd9YMbN+slPR2stzcWzpwycqqir7VZWcf1MzJrp/R4+Vmfn0Y1n6P32MqSzfrX1p+B0fF6bkX5V+ay+zJz6MJv2g/ZniqyirpuVa2ivJa93p5W5+xn89TX+kSU9+kuFf1j68WfVt5bj5FWdj5TWWZIxa/tFmHsNjsnH6dZc7Hfm+p+itx2Pez/Rf8Htft2j/AJnftn9qVbPQn9q+gdm+fQ9T7Bv3er636P0PU/n/APttJT//1fTs/wCy/Ycj7Z/RfSf9o5/m9p9X+b9/83+4uXwP/G/2Zf2L6P2d32vb9pn0NzPU5923f6e/0/8Az2vBElJDY/Nv+j8re5P+an/un5o/7m/mv+qf1/3H6eZ+zf2Q3bt/Zn2cRunb6Gz87f7tno/vLEy/+ZP7FwftX/Jkv+xT9o5l3qcfp/8At3+wvnxJCO/6W/6P8vmVy/zf+Cf53/IfP8mT/wBuP/Ufuv0Pjf8AM/7N0z7PHofaHfs3+e/n93u+l7v5z/T/AKNaTv2R+3mbv+V/s3s+n/Mb3f8AoP8Azu7/AIVfMySR/wALrurL80v90/Ll/nPm+f8AS/qf+Kf9Y/TOD+yP2p1D7H/Tpq+3/T52u9D6f6L6G7+ZWX/2F/sQcfsr7Tr/AD+31/8AhP8ACbP+M/V189JJD/C6bfy/xVY/m/8ABP8AkPk+f+b/AJfdP9U/TnWv2Z+zL/2tH2CB607ojc3Z/NfpP5zZ9BWcX0fs1PoT6Oxvp7t07YGzd6n6T6P+k96+Wkk3p13/AMFrH+ZHz/PL/YfLH5f9b+//AFOB9x67/wCNf/zls/bO39uepT6k/ap37avs38x+r/zXo/8AoxaOZ/zH/YnW/tUfsv7c79s/z/8AS9+Pv+h+m/nfsv8ARf0H/gi+fUkGJ+jD/wA0/tvXvV9P7R6dH7d9bfs2ek77J6vrfq+37Pu/mP8Arn6RY/Rf/G0+y9R/Zken9hf9u9T7V6v2Hb+l9H7Z+s/Zdn/cP/gv+CXhaSSn2r61/wDNn7d9VPthaOhfZsv0gQ8zV6ON9k9MtP2tt38z6Hp/rO9aWD/43v7K6d9j9P7D+02fYo9bd+0fd6P2jd+s/aP/AA97PS9L/B+ivA0klPvvW/8AmB63Uv2tHr/aMT7dH2j1PtHpn9m/Z/s/6T1vs2/+g/mep6yjmf8Ajd/83enev6f7K3n9m+n63repu/S+h9n/AMpet6v9J/wnqf0heCJJKfd7P/G2/wCbWN9D9kfav1f0vtHrfaod9L0P8pfatn0vV/Sen/I9Na3/AGJf80v8B/za+z+fp+l/599f1f8A0L+1f92V85JJKf/Z/+0ZZFBob3Rvc2hvcCAzLjAAOEJJTQQlAAAAAAAQAAAAAAAAAAAAAAAAAAAAADhCSU0EOgAAAAAA5QAAABAAAAABAAAAAAALcHJpbnRPdXRwdXQAAAAFAAAAAFBzdFNib29sAQAAAABJbnRlZW51bQAAAABJbnRlAAAAAENscm0AAAAPcHJpbnRTaXh0ZWVuQml0Ym9vbAAAAAALcHJpbnRlck5hbWVURVhUAAAAAQAAAAAAD3ByaW50UHJvb2ZTZXR1cE9iamMAAAAMAFAAcgBvAG8AZgAgAFMAZQB0AHUAcAAAAAAACnByb29mU2V0dXAAAAABAAAAAEJsdG5lbnVtAAAADGJ1aWx0aW5Qcm9vZgAAAAlwcm9vZkNNWUsAOEJJTQQ7AAAAAAItAAAAEAAAAAEAAAAAABJwcmludE91dHB1dE9wdGlvbnMAAAAXAAAAAENwdG5ib29sAAAAAABDbGJyYm9vbAAAAAAAUmdzTWJvb2wAAAAAAENybkNib29sAAAAAABDbnRDYm9vbAAAAAAATGJsc2Jvb2wAAAAAAE5ndHZib29sAAAAAABFbWxEYm9vbAAAAAAASW50cmJvb2wAAAAAAEJja2dPYmpjAAAAAQAAAAAAAFJHQkMAAAADAAAAAFJkICBkb3ViQG/gAAAAAAAAAAAAR3JuIGRvdWJAb+AAAAAAAAAAAABCbCAgZG91YkBv4AAAAAAAAAAAAEJyZFRVbnRGI1JsdAAAAAAAAAAAAAAAAEJsZCBVbnRGI1JsdAAAAAAAAAAAAAAAAFJzbHRVbnRGI1B4bEBywAAAAAAAAAAACnZlY3RvckRhdGFib29sAQAAAABQZ1BzZW51bQAAAABQZ1BzAAAAAFBnUEMAAAAATGVmdFVudEYjUmx0AAAAAAAAAAAAAAAAVG9wIFVudEYjUmx0AAAAAAAAAAAAAAAAU2NsIFVudEYjUHJjQFkAAAAAAAAAAAAQY3JvcFdoZW5QcmludGluZ2Jvb2wAAAAADmNyb3BSZWN0Qm90dG9tbG9uZwAAAAAAAAAMY3JvcFJlY3RMZWZ0bG9uZwAAAAAAAAANY3JvcFJlY3RSaWdodGxvbmcAAAAAAAAAC2Nyb3BSZWN0VG9wbG9uZwAAAAAAOEJJTQPtAAAAAAAQASwAAAABAAEBLAAAAAEAAThCSU0EJgAAAAAADgAAAAAAAAAAAAA/gAAAOEJJTQQNAAAAAAAEAAAAWjhCSU0EGQAAAAAABAAAAB44QklNA/MAAAAAAAkAAAAAAAAAAAEAOEJJTScQAAAAAAAKAAEAAAAAAAAAAThCSU0D9QAAAAAASAAvZmYAAQBsZmYABgAAAAAAAQAvZmYAAQChmZoABgAAAAAAAQAyAAAAAQBaAAAABgAAAAAAAQA1AAAAAQAtAAAABgAAAAAAAThCSU0D+AAAAAAAcAAA/////////////////////////////wPoAAAAAP////////////////////////////8D6AAAAAD/////////////////////////////A+gAAAAA/////////////////////////////wPoAAA4QklNBAAAAAAAAAIAAThCSU0EAgAAAAAABAAAAAA4QklNBDAAAAAAAAIBAThCSU0ELQAAAAAABgABAAAAAjhCSU0ECAAAAAAAEAAAAAEAAAJAAAACQAAAAAA4QklNBB4AAAAAAAQAAAAAOEJJTQQaAAAAAANJAAAABgAAAAAAAAAAAAABpQAAAooAAAAKAFUAbgB0AGkAdABsAGUAZAAtADIAAAABAAAAAAAAAAAAAAAAAAAAAAAAAAEAAAAAAAAAAAAAAooAAAGlAAAAAAAAAAAAAAAAAAAAAAEAAAAAAAAAAAAAAAAAAAAAAAAAEAAAAAEAAAAAAABudWxsAAAAAgAAAAZib3VuZHNPYmpjAAAAAQAAAAAAAFJjdDEAAAAEAAAAAFRvcCBsb25nAAAAAAAAAABMZWZ0bG9uZwAAAAAAAAAAQnRvbWxvbmcAAAGlAAAAAFJnaHRsb25nAAACigAAAAZzbGljZXNWbExzAAAAAU9iamMAAAABAAAAAAAFc2xpY2UAAAASAAAAB3NsaWNlSURsb25nAAAAAAAAAAdncm91cElEbG9uZwAAAAAAAAAGb3JpZ2luZW51bQAAAAxFU2xpY2VPcmlnaW4AAAANYXV0b0dlbmVyYXRlZAAAAABUeXBlZW51bQAAAApFU2xpY2VUeXBlAAAAAEltZyAAAAAGYm91bmRzT2JqYwAAAAEAAAAAAABSY3QxAAAABAAAAABUb3AgbG9uZwAAAAAAAAAATGVmdGxvbmcAAAAAAAAAAEJ0b21sb25nAAABpQAAAABSZ2h0bG9uZwAAAooAAAADdXJsVEVYVAAAAAEAAAAAAABudWxsVEVYVAAAAAEAAAAAAABNc2dlVEVYVAAAAAEAAAAAAAZhbHRUYWdURVhUAAAAAQAAAAAADmNlbGxUZXh0SXNIVE1MYm9vbAEAAAAIY2VsbFRleHRURVhUAAAAAQAAAAAACWhvcnpBbGlnbmVudW0AAAAPRVNsaWNlSG9yekFsaWduAAAAB2RlZmF1bHQAAAAJdmVydEFsaWduZW51bQAAAA9FU2xpY2VWZXJ0QWxpZ24AAAAHZGVmYXVsdAAAAAtiZ0NvbG9yVHlwZWVudW0AAAARRVNsaWNlQkdDb2xvclR5cGUAAAAATm9uZQAAAAl0b3BPdXRzZXRsb25nAAAAAAAAAApsZWZ0T3V0c2V0bG9uZwAAAAAAAAAMYm90dG9tT3V0c2V0bG9uZwAAAAAAAAALcmlnaHRPdXRzZXRsb25nAAAAAAA4QklNBCgAAAAAAAwAAAACP/AAAAAAAAA4QklNBBEAAAAAAAEBADhCSU0EFAAAAAAABAAAAAI4QklNBAwAAAAAEDkAAAABAAAAnAAAAGUAAAHUAAC4pAAAEB0AGAAB/9j/7QAMQWRvYmVfQ00AAv/uAA5BZG9iZQBkgAAAAAH/2wCEAAwICAgJCAwJCQwRCwoLERUPDAwPFRgTExUTExgRDAwMDAwMEQwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwBDQsLDQ4NEA4OEBQODg4UFA4ODg4UEQwMDAwMEREMDAwMDAwRDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDP/AABEIAGUAnAMBIgACEQEDEQH/3QAEAAr/xAE/AAABBQEBAQEBAQAAAAAAAAADAAECBAUGBwgJCgsBAAEFAQEBAQEBAAAAAAAAAAEAAgMEBQYHCAkKCxAAAQQBAwIEAgUHBggFAwwzAQACEQMEIRIxBUFRYRMicYEyBhSRobFCIyQVUsFiMzRygtFDByWSU/Dh8WNzNRaisoMmRJNUZEXCo3Q2F9JV4mXys4TD03Xj80YnlKSFtJXE1OT0pbXF1eX1VmZ2hpamtsbW5vY3R1dnd4eXp7fH1+f3EQACAgECBAQDBAUGBwcGBTUBAAIRAyExEgRBUWFxIhMFMoGRFKGxQiPBUtHwMyRi4XKCkkNTFWNzNPElBhaisoMHJjXC0kSTVKMXZEVVNnRl4vKzhMPTdePzRpSkhbSVxNTk9KW1xdXl9VZmdoaWprbG1ub2JzdHV2d3h5ent8f/2gAMAwEAAhEDEQA/APVUkkklKSSSSUpJJJJSkkkklKSSSSUpJJJJSkC/Ow8fIoxr7mV35Zc3HrcQHPLRvf6bfztrUdAxrm5VLMg0vqILtrLm7XiC6vdt930/zP8Ag0lJ0kHGyRfi15JrfQLGh/p3DY9s/mWs/Me1FSUukkkkpSSSSSn/0PVUkkklKSSSSUpJJJJTQ631rA6H023qWe8tpqgBrRue950rppZ+fbY7/wBSbK/eua+pv19y/rN1vJw3YleLi04/rVgOL7d29tfvf7Kvou+i2v8A64s7/HLbYMLpVId+jffY9zexc2vax39n1HrH/wAUH/ikzf8Awl/6NrSU+upJLnuq/XXpOCXV45+3Xt0IqI9MHwsyda/+2vWs/kJ0ISmaiCSmMZS2F09ASACSYA5K4zq/+M3p+PY6jo2M/qz2Ha69rhVjAglrtuU4P9bbH+Br9L/hlS6x1Dq+bhm7r+ZV0TpF2npPad9o/wBHXi65uXz76/0LH/6DYsvod9vVsx2L9UcBrfQA9frXVALDUD9H7NhV/q9N3t/V/pv/ANP6SeccYfNKz+7DX7ZM0IYBrllKX9TF/wCrJ/L/AIk3YxPrZ9dsmv7XbidPwcDQnJyTYypoP5zsi22hr/8ArNdizuo/4w4eK2Z13Ur3w1uL0qoY9BeXbPS/aGa3JzbvU/MdhUrRzf8AFZb1S0ZPV+vZWbk6+9zGbGzr+gocbGUN/kVLS+rv+LnoXQ8pudutzsyvWuzILdrDr76aa2sY1+v07PVf+4mcVfKAP+d/0luTJA/zeMYx5nJP/CnP/uIQd7D6Zi0sbY+rdkFo3use+9wdGrW3ZJfZtV1JJAknc2xKSSSQUpJJJJT/AP/R9VSSSSUpJJJJSkkkklPm/wDjm/o/SP8Ajrv+oaud/wAXfVsfo3U87PyWucw4vpVNaNX2Gxj/AEw76DdrBvetz/Gd1LpvU8vFwKrDYemvsOU5v0N7gK/s4s/OtZH6bZ9D+Z/nv5u99VvqE6xleX1lhqpgGrBHtdB+j9o2/wA1/wAR/Of6b/RKYYqjx5PTE/LH9KTb5bFh/nOZkY44/oQ/nMv9WLD1/rP9cbXMqH2bpsw4SW0gTO21/wDOZtv/AAf81/xK6rov1V6Z0kNsa37RlN/7UWASP+Jr+hQ3+p+k/wCEWvXXXVW2qporrYA1jGgBoA4a1rfoqSEs0iOGPpj2H/dLeZ5r3fTjgMOEfLjj/wB3L9KT4B9bsm7K+tPVbb3F7q8q2msuJO2utxrrrZP0We36Kbpn1p+sPScX7J0zNdi4+91hYyuoy90b3ufZU+x/0fznrS/xj9Is6b9asm3aRj9RjKpd2LiG15LJ/eZc31P6l1al9Sv+ZGS9+B9ZsZrL3OLsbPfdbXW4H/tNf6dtdVNjP8Da72XM/R/z38/E12vX9efrzdYyijqV1t9rgymptVJc57tGMaPQ/Ocva+m15dXTsWrOs9bMZUxuTboN1gaPVf7Q1vusVDo31U+rfRnnI6Xg1VWvGl8usfBH5l17rbGMd/IethJCkkkklKSSSSUpJJJJT//S9VSSSSUpJJJJTGwvDHGsAvg7Q4wJ7bnAOXP/AFp6kzD6H1Hq+BkF17GDDDm2E11v9X7PZZ6TXekzKx32v9+31P0fprolTf0jpj8Z+KcasY9loyLK2tDWutFjcr1LA2N/6djXv3fzidAgSBIsAiwkaEW8b9VPqzhdFwf+cf1gLaPRb6tFd3FDPzLrW/nZln5lf06vof0lcN9b/rvlfWXN3NtON02gn7Jih+0+H2nK2O92TY38z+bxq/0TP8Nbb7pdTTez07622sJB2PAcJB3NO137rkH9m9O/7i0/9tt/8ijkySnIyl/vJlIyNl+cvtH/AHYd/wBuu/8AJomMMnLvZj4jrsi+wxXTU973uP8AJYxxX0T+zenf9xaf+22/+RRKcXGoJNFLKieSxobP+aExFvH4X+Lyi/6mUdF6o8t6gHOyRktO91N9h3bWbj+lqYzbRfXu2ZH/AG3ZX5t176p9d6BY5ufjOdjjjMpBsocPOxo/Q/8AF5Hpr35JJD80NuYwQy7Y391thaP81rgtPoPRep/WHObg4DrXBxAvyNz3V1MP0rLXbtn0f5urd+lXvjun4DyXOxqXOPJNbSf+pRa6qqWBlTG1sHDWgAfc1JNqpqZRSymsQypoY0c6NG1qmkkkhSSSSSlJJJJKf//T9VSSSSUpJJJJSkkkklKSSSSUpJJJJSkkkklKSSSSUpJJJJSkkkklKSSSSU//1PUMnIrxca3JtkV0MdY+BJ2sG90D+qFHCzKc7Eqy6J9K9oezcIMHxCr9e/5D6j/4Vu/89vVX6t5FFP1aw7LLGtbVjB9hn6LWiXud/VTq9N+NM4xA8v7gsz9wY/8ABMeJsu6505vV29GLz9sc3eGgEt4Nmwv/AH/Tb6iJndVxMC3FpyC4PzbRTTtEjcf3v3eV54/Pstrs619kyf2kcwZtWR6ZNIoZ7G4zr5H6Pb9P2f4PYum+seRVk3/VzKqM1XZdVjCdPa7Y9v8A0U84wCB9v95u5Ph8YZMUTxcMozjk1/y+KHHLh/qfuu7ndVxMC7FoyC7fm2ejTtbI3H9/936SzLPrt0OvKdiOdb6rLDUQKyRuDvS/zdyD9aHNf1HoDmkOH20agyOWIln/AIvKv/Taf/Pz0BEVZHQn7GLFgw+3GU4ykTjyZNJcHqxT4f3ZOtjdTxcrMysKrd62EWC6RA/SAvZtd+d9FN0rquJ1bGOViFxrD3Vne0tMt50PxXNU43Wcj6zdb/ZeYzD2ux/W31izdNZ2Rv8Ao7Pel9V+oN6Z9T8zOshxottIB4c87GVt0/ftc1qRgK039P8Azk5OSgMZMJcUz93EYAnijLmMfFLj9P6Uv5vhk9Dh9c6dm9QyenY9hdk4n860ggaHZZsd+d6b/Y9aC816dmN6Xf0vOdjZNVzHvb1TJuqcyuxmQ7R/q7nbvQ+k32fpV6UhkjwkVsx89yowTjwWYSB1l+/jPBP/ABv5z/qiJ+Xisea33VteCAWlwBk/R9soq85u6NkdS+vXXn0dPwM4UW4Je7OLw9gOPW79W9Fj/pbPfv8A5CXUvrH9bcfD651yrPqGH0PqbsZmCcdpN1fqY9XpXZE7qmVsyPY+pvrP/Seo/wDm0xpvobbqX2PqY9rrKo9RgILm7huZvb+buU15pd1PqnQOvfXXqzLmZL8cYTW0vrDGOdk7K8B9ljXb9nTqHupf/wBzP5z9ArVPX/rlh4/UTkszL6WYOVczMy8OnFNGRTU66gsGPdfTdjW7Xey1nqep6aSn0FJcGep/W1zvq5iM6rUzI+sFWTkW3Ox2FlLRTj5FNONTLXWOxt9nputt/Tf4b9H+iQMn6zfWfGeehnNqs6hj9XxOnu6oKG7LKsyq20GzF3em3JxXt/SNqfX/AKL/AEnqJT31eTjW3W0V2sfdjlovra4FzC4b6/VYPdXvZ72b0VcBZd9YMbN+slPR2stzcWzpwycqqir7VZWcf1MzJrp/R4+Vmfn0Y1n6P32MqSzfrX1p+B0fF6bkX5V+ay+zJz6MJv2g/ZniqyirpuVa2ivJa93p5W5+xn89TX+kSU9+kuFf1j68WfVt5bj5FWdj5TWWZIxa/tFmHsNjsnH6dZc7Hfm+p+itx2Pez/Rf8Htft2j/AJnftn9qVbPQn9q+gdm+fQ9T7Bv3er636P0PU/n/APttJT//1fTs/wCy/Ycj7Z/RfSf9o5/m9p9X+b9/83+4uXwP/G/2Zf2L6P2d32vb9pn0NzPU5923f6e/0/8Az2vBElJDY/Nv+j8re5P+an/un5o/7m/mv+qf1/3H6eZ+zf2Q3bt/Zn2cRunb6Gz87f7tno/vLEy/+ZP7FwftX/Jkv+xT9o5l3qcfp/8At3+wvnxJCO/6W/6P8vmVy/zf+Cf53/IfP8mT/wBuP/Ufuv0Pjf8AM/7N0z7PHofaHfs3+e/n93u+l7v5z/T/AKNaTv2R+3mbv+V/s3s+n/Mb3f8AoP8Azu7/AIVfMySR/wALrurL80v90/Ll/nPm+f8AS/qf+Kf9Y/TOD+yP2p1D7H/Tpq+3/T52u9D6f6L6G7+ZWX/2F/sQcfsr7Tr/AD+31/8AhP8ACbP+M/V189JJD/C6bfy/xVY/m/8ABP8AkPk+f+b/AJfdP9U/TnWv2Z+zL/2tH2CB607ojc3Z/NfpP5zZ9BWcX0fs1PoT6Oxvp7t07YGzd6n6T6P+k96+Wkk3p13/AMFrH+ZHz/PL/YfLH5f9b+//AFOB9x67/wCNf/zls/bO39uepT6k/ap37avs38x+r/zXo/8AoxaOZ/zH/YnW/tUfsv7c79s/z/8AS9+Pv+h+m/nfsv8ARf0H/gi+fUkGJ+jD/wA0/tvXvV9P7R6dH7d9bfs2ek77J6vrfq+37Pu/mP8Arn6RY/Rf/G0+y9R/Zken9hf9u9T7V6v2Hb+l9H7Z+s/Zdn/cP/gv+CXhaSSn2r61/wDNn7d9VPthaOhfZsv0gQ8zV6ON9k9MtP2tt38z6Hp/rO9aWD/43v7K6d9j9P7D+02fYo9bd+0fd6P2jd+s/aP/AA97PS9L/B+ivA0klPvvW/8AmB63Uv2tHr/aMT7dH2j1PtHpn9m/Z/s/6T1vs2/+g/mep6yjmf8Ajd/83enev6f7K3n9m+n63repu/S+h9n/AMpet6v9J/wnqf0heCJJKfd7P/G2/wCbWN9D9kfav1f0vtHrfaod9L0P8pfatn0vV/Sen/I9Na3/AGJf80v8B/za+z+fp+l/599f1f8A0L+1f92V85JJKf/ZADhCSU0EIQAAAAAAXQAAAAEBAAAADwBBAGQAbwBiAGUAIABQAGgAbwB0AG8AcwBoAG8AcAAAABcAQQBkAG8AYgBlACAAUABoAG8AdABvAHMAaABvAHAAIABDAEMAIAAyADAAMQA5AAAAAQA4QklNBAYAAAAAAAcACAAAAAEBAP/hDdtodHRwOi8vbnMuYWRvYmUuY29tL3hhcC8xLjAvADw/eHBhY2tldCBiZWdpbj0i77u/IiBpZD0iVzVNME1wQ2VoaUh6cmVTek5UY3prYzlkIj8+IDx4OnhtcG1ldGEgeG1sbnM6eD0iYWRvYmU6bnM6bWV0YS8iIHg6eG1wdGs9IkFkb2JlIFhNUCBDb3JlIDUuNi1jMTQ1IDc5LjE2MzQ5OSwgMjAxOC8wOC8xMy0xNjo0MDoyMiAgICAgICAgIj4gPHJkZjpSREYgeG1sbnM6cmRmPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5LzAyLzIyLXJkZi1zeW50YXgtbnMjIj4gPHJkZjpEZXNjcmlwdGlvbiByZGY6YWJvdXQ9IiIgeG1sbnM6eG1wPSJodHRwOi8vbnMuYWRvYmUuY29tL3hhcC8xLjAvIiB4bWxuczp4bXBNTT0iaHR0cDovL25zLmFkb2JlLmNvbS94YXAvMS4wL21tLyIgeG1sbnM6c3RFdnQ9Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC9zVHlwZS9SZXNvdXJjZUV2ZW50IyIgeG1sbnM6ZGM9Imh0dHA6Ly9wdXJsLm9yZy9kYy9lbGVtZW50cy8xLjEvIiB4bWxuczpwaG90b3Nob3A9Imh0dHA6Ly9ucy5hZG9iZS5jb20vcGhvdG9zaG9wLzEuMC8iIHhtcDpDcmVhdG9yVG9vbD0iQWRvYmUgUGhvdG9zaG9wIENDIDIwMTkgKFdpbmRvd3MpIiB4bXA6Q3JlYXRlRGF0ZT0iMjAyMi0wNC0wNlQxNzozMzoxMSswNTozMCIgeG1wOk1ldGFkYXRhRGF0ZT0iMjAyMi0wNC0wNlQxNzozMzoxMSswNTozMCIgeG1wOk1vZGlmeURhdGU9IjIwMjItMDQtMDZUMTc6MzM6MTErMDU6MzAiIHhtcE1NOkluc3RhbmNlSUQ9InhtcC5paWQ6MjU2N2MzNTAtODMyYS05ZTQ4LWE1YjYtMWYyZmZmN2U0MmU5IiB4bXBNTTpEb2N1bWVudElEPSJhZG9iZTpkb2NpZDpwaG90b3Nob3A6MzJhMmQwMGMtNjQ2NC02NzQ3LTk2N2UtM2FkNDQzMzEyOTY2IiB4bXBNTTpPcmlnaW5hbERvY3VtZW50SUQ9InhtcC5kaWQ6ZDU3Y2ViOWEtYmYyOS03YjRiLWE3MTItMDM2YmRiZGIwOTRhIiBkYzpmb3JtYXQ9ImltYWdlL2pwZWciIHBob3Rvc2hvcDpDb2xvck1vZGU9IjMiPiA8eG1wTU06SGlzdG9yeT4gPHJkZjpTZXE+IDxyZGY6bGkgc3RFdnQ6YWN0aW9uPSJjcmVhdGVkIiBzdEV2dDppbnN0YW5jZUlEPSJ4bXAuaWlkOmQ1N2NlYjlhLWJmMjktN2I0Yi1hNzEyLTAzNmJkYmRiMDk0YSIgc3RFdnQ6d2hlbj0iMjAyMi0wNC0wNlQxNzozMzoxMSswNTozMCIgc3RFdnQ6c29mdHdhcmVBZ2VudD0iQWRvYmUgUGhvdG9zaG9wIENDIDIwMTkgKFdpbmRvd3MpIi8+IDxyZGY6bGkgc3RFdnQ6YWN0aW9uPSJzYXZlZCIgc3RFdnQ6aW5zdGFuY2VJRD0ieG1wLmlpZDoyNTY3YzM1MC04MzJhLTllNDgtYTViNi0xZjJmZmY3ZTQyZTkiIHN0RXZ0OndoZW49IjIwMjItMDQtMDZUMTc6MzM6MTErMDU6MzAiIHN0RXZ0OnNvZnR3YXJlQWdlbnQ9IkFkb2JlIFBob3Rvc2hvcCBDQyAyMDE5IChXaW5kb3dzKSIgc3RFdnQ6Y2hhbmdlZD0iLyIvPiA8L3JkZjpTZXE+IDwveG1wTU06SGlzdG9yeT4gPC9yZGY6RGVzY3JpcHRpb24+IDwvcmRmOlJERj4gPC94OnhtcG1ldGE+ICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPD94cGFja2V0IGVuZD0idyI/Pv/uAA5BZG9iZQBkQAAAAAH/2wCEAAEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQECAgICAgICAgICAgMDAwMDAwMDAwMBAQEBAQEBAQEBAQICAQICAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDA//AABEIAaUCigMBEQACEQEDEQH/3QAEAFL/xAGiAAAABgIDAQAAAAAAAAAAAAAHCAYFBAkDCgIBAAsBAAAGAwEBAQAAAAAAAAAAAAYFBAMHAggBCQAKCxAAAgEDBAEDAwIDAwMCBgl1AQIDBBEFEgYhBxMiAAgxFEEyIxUJUUIWYSQzF1JxgRhikSVDobHwJjRyChnB0TUn4VM2gvGSokRUc0VGN0djKFVWVxqywtLi8mSDdJOEZaOzw9PjKThm83UqOTpISUpYWVpnaGlqdnd4eXqFhoeIiYqUlZaXmJmapKWmp6ipqrS1tre4ubrExcbHyMnK1NXW19jZ2uTl5ufo6er09fb3+Pn6EQACAQMCBAQDBQQEBAYGBW0BAgMRBCESBTEGACITQVEHMmEUcQhCgSORFVKhYhYzCbEkwdFDcvAX4YI0JZJTGGNE8aKyJjUZVDZFZCcKc4OTRnTC0uLyVWV1VjeEhaOzw9Pj8ykalKS0xNTk9JWltcXV5fUoR1dmOHaGlqa2xtbm9md3h5ent8fX5/dIWGh4iJiouMjY6Pg5SVlpeYmZqbnJ2en5KjpKWmp6ipqqusra6vr/2gAMAwEAAhEDEQA/AN/j37r3Xvfuvde9+691737r3Xvfuvde9+691737r3Xvfuvde9+691737r3Xvfuvde9+691737r3Xvfuvde9+691737r3Xvfuvde9+691737r3Xvfuvde9+691737r3Xvfuvde9+691737r3Xvfuvde9+691737r3Xvfuvde9+691737r3Xvfuvde9+691737r3Xvfuvde9+691737r3Xvfuvde9+691737r3Xvfuvde9+691737r3Xvfuvde9+691737r3Xvfuvde9+691737r3Xvfuvde9+691737r3Xvfuvde9+691737r3Xvfuvde9+691737r3Xvfuvde9+691737r3Xvfuvde9+691737r3Xvfuvde9+691737r3Xvfuvde9+691737r3Xvfuvde9+691737r3Xvfuvde9+691737r3Xvfuvde9+691737r3Xvfuvde9+691737r3X/9Df49+691737r3Xvfuvde9+691737r3Xvfuvde9+691737r3Xvfuvde9+691737r3Xvfuvde9+691737r3Xvfuvde9+691737r3Xvfuvde9+691737r3Xvfuvde9+691737r3Xvfuvde9+691737r3Xvfuvde9+691737r3Xvfuvde9+691737r3Xvfuvde9+691737r3Xvfuvde9+691737r3Xvfuvde9+691737r3Xvfuvde9+691737r3Xvfuvde9+691737r3Xvfuvde9+691737r3Xvfuvde9+691737r3Xvfuvde9+691737r3Xvfuvde9+691737r3Xvfuvde9+691737r3Xvfuvde9+691737r3Xvfuvde9+691737r3Xvfuvde9+691737r3Xvfuvde9+691737r3Xvfuvde9+691737r3Xvfuvde9+691737r3Xvfuvde9+691//R3+Pfuvde9+691737r3Xvfuvde9+691737r3Xvfuvde9+691737r3Xvfuvde9+691737r3Xvfuvde9+691737r3Xvfuvde9+691737r3Xvfuvde9+691737r3Xvfuvde9+691737r3Xvfuvde9+691737r3Xvfuvde9+691737r3Xvfuvde9+691737r3Xvfuvde9+691737r3Xvfuvde9+691737r3Xvfuvde9+691737r3Xvfuvde9+691737r3Xvfuvde9+691737r3Xvfuvde9+691737r3Xvfuvde9+691737r3Xvfuvde9+690mN3bpx2zcFXbgyrhKGgjMk7lioVFSSRmJWOQ2VYyfoffuvdKVfoeCLHm/PP8Ar/n37r3XL37r3Xvfuvde9+691737r3Xvfuvde9+691737r3Xvfuvde9+691737r3Xvfuvde9+691737r3Xvfuvde9+691737r3Xvfuvde9+691737r3Xvfuvde9+691737r3Xvfuvde9+691/9Lf49+691737r3Xvfuvde9+691737r3Xvfuvde9+691737r3Xvfuvde9+691737r3Xvfuvde9+691737r3Xvfuvde9+691737r3Xvfuvde9+691737r3Xvfuvde9+691737r3Xvfuvde9+691737r3Xvfuvde9+691737r3Xvfuvde9+691737r3Xvfuvde9+691737r3Xvfuvde9+691737r3Xvfuvde9+691737r3Xvfuvde9+691737r3Xvfuvde9+691737r3Xvfuvde9+691737r3Xvfuvde9+691737r3Xvfuvde9+690z5zPYnbWKyGczlbFjsTi6aesr62bV4aalpoZKieeQqpbRHDEzGwPA9+691rG/wA6n+dn8Yeqeg8l1J0zv+j7E763hPTVOL2ri4auCnx+CoZqihymQymTnxdZTUra6v8AaQoTIIXsRb37pxEJORjq074k/wA3b4K/MbbVLnupO5cXPX1FTDRZHbGUpMjQ5rC5OeKKWPHV0M1HGBM0UqsCLA3IsLe/dUKleI6s6BJH9R/X+o/rb37rXXL37r3Xvfuvde9+691737r3Xvfuvde9+691737r3Xvfuvde9+691737r3Xvfuvde9+691737r3Xvfuvde9+691737r3Xvfuvde9+691737r3Xvfuvde9+691737r3Xvfuvdf//T3+Pfuvde9+691737r3Xvfuvde9+691737r3Xvfuvde9+691737r3Xvfuvde9+691737r3Xvfuvde9+691737r3Xvfuvde9+691737r3Xvfuvde9+691737r3Xvfuvde9+691737r3Xvfuvde9+691737r3Xvfuvde9+691737r3Xvfuvde9+691737r3Xvfuvde9+691737r3Xvfuvde9+691737r3Xvfuvde9+691737r3Xvfuvde9+691737r3Xvfuvde9+691737r3Xvfuvde9+691737r3Xvfuvde9+691737r3XvfuvddEX45A/w/3r37r3VTn84n4VddfNH4lZ7aO8cBPkdwbXmly2y87jqMVubwFXJTvUVv2EbpLCYa6fFUZmBjJ/YWzD37qyNpNa9WU9cdc7L6o2jith9e7eodqbRwEf2uHweMR4qOipwFISNZJJG+v+Pv3Vel0RyCPwf94P19+69137917r3v3Xuve/de697917r3v3Xuve/de697917r3v3Xuve/de697917r3v3Xuve/de697917r3v3Xuve/de697917r3v3Xuve/de697917r3v3Xuve/de697917r3v3Xuv//U3+Pfuvde9+691737r3Xvfuvde9+691737r3Xvfuvde9+691737r3Xvfuvde9+691737r3Xvfuvde9+691737r3Xvfuvde9+691737r3Xvfuvde9+691737r3Xvfuvde9+691737r3Xvfuvde9+691737r3Xvfuvde9+691737r3Xvfuvde9+691737r3Xvfuvde9+691737r3Xvfuvde9+691737r3Xvfuvde9+691737r3Xvfuvde9+691737r3Xvfuvde9+691737r3Xvfuvde9+691737r3Xvfuvde9+691737r3Xvfuvde9+691ErKKmyED01VEs0L2Do30awYWP+sG/3n37r3UoC1+LX5PN+T/yL37r3Xfv3Xuve/de697917r3v3Xuve/de697917r3v3Xuve/de697917r3v3Xuve/de697917r3v3Xuve/de697917r3v3Xuve/de697917r3v3Xuve/de697917r3v3Xuve/de697917r//1d/j37r3Xvfuvde9+691737r3Xvfuvde9+691737r3Xvfuvde9+691737r3Xvfuvde9+691737r3Xvfuvde9+691737r3Xvfuvde9+691737r3Xvfuvde9+691737r3Xvfuvde9+691737r3Xvfuvde9+691737r3Xvfuvde9+691737r3Xvfuvde9+691737r3Xvfuvde9+691737r3Xvfuvde9+691737r3Xvfuvde9+691737r3Xvfuvde9+691737r3Xvfuvde9+691737r3Xvfuvde9+691737r3XvfuvdeP+P09+690E3avaGD6zh2KcxlsbjpN79mbG69xYr6yCl+9ye8Nw0GCpKCjE08Jnrqmor0SKNdTO7ABSSB7917oVtSn8/kC35DX/p+D7917rn7917rom3+3/Jt7917oHN69v4LZnZvU/W+Qq6SLK9qR7zbC08siioqhs99ppWmlTzIZBEd1w6yFe2pfpfn3W/y6GJWDi4IIP8AqSCP9uPfutdcvfuvde9+691737r3Xvfuvde9+691737r3Xvfuvde9+691737r3Xvfuvde9+691737r3Xvfuvde9+691737r3Xvfuvde9+691737r3Xvfuvde9+691737r3Xvfuvdf//W3+Pfuvde9+691737r3Xvfuvde9+691737r3Xvfuvde9+691737r3Xvfuvde9+691737r3Xvfuvde9+691737r3Xvfuvde9+691737r3Xvfuvde9+691737r3Xvfuvde9+691737r3Xvfuvde9+691737r3Xvfuvde9+691737r3Xvfuvde9+691737r3Xvfuvde9+691737r3Xvfuvde9+691737r3Xvfuvde9+691737r3Xvfuvde9+691737r3Xvfuvde9+691737r3Xvfuvde9+691737r3Xvfuvde9+691Cr6aWqp3ghq6iieRHRZ6ZkWVGZbBlZ45VDKTccHke/de61vv5w/wDLE+Q/yhzvxQz/AFx8qO4KSg2l8guuKLJ4HL5HA1uO2rLnOx9kyU/Z+248dsFZ6Pcuy48c9XDNVSz0okpoS0Lqrh/dORsF1VA4dXrdTdSbr66xVHQZ/ufs/s2op50knyO+8ngK6tqgojBjmbD7U2/EYn0EmyKeTz791QmprToeByP98ffutdM+cxlTlaGakpctkMNLLFNEK3GyQRVUJlQoJYnqKaqjWWE+pSUIB+oI49+691rO/wAwP+Vx8i+6P5hvwO7V2d8uO76Xb20J941FdmshX7fq891k+3d69abhqm2FkaHrpMViH3lR1KJKcjFXa5cRCU0Ksgf3TgcBSKZp1sVbA2HnNm0dPTZbsTeu95IkIeo3ZXYmrqZmICku2NweHjIFriyjkn37pvoTr2sAOP8Aef8AH/X9+6913f8A1rWJvf8Ap7917rjr/Is349PPJ+l7e/Go4g9e65/77/Y/09+691737r3Xvfuvde9+691737r3Xvfuvde9+691737r3Xvfuvde9+691737r3Xvfuvde9+691737r3Xvfuvde9+691737r3Xvfuvde9+691/9ff49+691737r3Xvfuvde9+691737r3Xvfuvde9+691737r3Xvfuvde9+691737r3Xvfuvde9+691737r3Xvfuvde9+691737r3Xvfuvde9+691737r3Xvfuvde9+691737r3Xvfuvde9+691737r3Xvfuvde9+691737r3Xvfuvde+vv3Xuve/de697917r3v3Xuve/de697917r3v3Xuve/de697917r3v3Xuve/de697917r3v3Xuve/de697917r3v3Xuve/de697917r3v3Xuve/de697917rq/wDvv9b+v9Pfuvdd+/de66Iv/vP+v/sPfuvdNuTxVFlko0rYllFDkaHK0wYKdFZjp1qaWUalaxjmQHix/offuvdOVuPyLAcD/D8e/de699P9gPeq5p17riZFH09f9QvqP+2HvdevdIfdu5Ov9rfaZ/fG4dp7YXFw1s9Jk905zEYKGkgkEDV0y1WXqqWOOILSoZGDBQEFzx7UW9neXjeHaWskj8KIrManhgA9ORwyytpiiZm9ACT/AC6I123/ADZ/gB05UxUW4Pkv1Rn8hLA88ON2Hv3ZG9MnKUkaMU8NDhdzTVMlXNIumOMKWckADn2ONq9r+eN3UyQcu3SRj8Ukckaj5kslKDzPl0d2vK++3Y1Jtsqr/SRl/wAI6CLZ382KDuWrWH4+fDX5e9n495kpP71ZPqfdGx9kx1U2kwK+6MtgqnEtEUcPIwmHjSxPBv7M7v2wO1KG37m3arZ6V0CdJJKDj2Kwb+XSqXlhrRQdw3W1ialdIcM37Aa9GGo8z/MK3tPMYtq/Hfp3CTxCLwboO799bqUS+VZaimrNq74xGNpZoIrBBJTP+4NRBXj2RNByFZIA11f3c484zHGnyw8bEg/I8OkJTYIf9FuJXH8OlV/mpP8APoftkdWbyx9PFUb47T3huTJLVxVD0tLV0FJgjHF42+3FLPhmyAikdTrBnuVNgQefZFe7laSMws9tijiKkVIJbPnUNSv5dIJrmJ9QitUVSPnX/DT+XQ7hQqhR9P8AH6kfnn+vsmOekfXL37r3Xvfuvde9+691737r3Xvfuvde9+691737r3Xvfuvde9+691737r3Xvfuvde9+691737r3Xvfuvde9+691737r3Xvfuvde9+691//Q3+Pfuvde9+691737r3Xvfuvde9+691737r3Xvfuvde9+691737r3Xvfuvde9+691737r3Xvfuvde9+691737r3Xvfuvde9+691737r3Xvfuvde9+691737r3Xvfuvde9+691737r3Xvfuvde9+691737r3Xvfuvde9+6910bfUmw5vc2H+x9+691jaVBcm+kAsZLWjAW5JaT9IAtz7917pjpN07Zr8m2FotxYGsywiaVsXS5nHVGTWOO+uU0MNQ9UES3LaLC3Pv3XulAPoL/APFPfuvdd+/de697917r3v3Xuve/de697917r3v3Xuve/de697917r3v3Xuve/de697917r3v3Xuve/de697917r3v3Xuve/de64syrwWAJvYXAJ/wCC3+p59+691w8oF7iQBQSWZSFsPqSxFgB/X37r3SYyW+Nm4QsuX3ZtfFME8pXJ7hxFAwjJZfIRVVcREd0Iv9Lg/wBPfuvdMX+mLqn/AJ+b11/6HO2P/rn7917ro9x9UAXPZvXSgfUtvnbAH+3OTHvRoMnrTEJliAPn0m91/JL4/bGxcma3j3h09tjGJT1NQK7P9m7KxNLJHSR+WfwTZDN00crIpFwrHkj+o9mVhtG67oQu27bcXBqB+nG75PD4QejnbOXt93r/AJJGzXdyKgViikkFT6lFanVXncX8+74DdWyTUOJ3duDtbKJFNJFH1dQ4/c+NlkQlY6c5vF5PIUMbzuvF/opDfT3Je0eynO260Z7WO2jPnKSh/wB5IB6HMPs/zo0fjXdiLZPSXUjf7yyg9Viduf8ACl/c2Wq/4R8cvjzRfxGsikpaCHsqPJbhytRXzuYaP7bCbQ3bt3IykuygIqs7vwvJt7kra/u720UZl37fjoFCfCKqtBxqzo46tb+3KR1O5XxAHHRQU/NlPQXbS3h/P2+drVVRgIN2fH7atZaiNTWbb3/05hpIK1GaSpozv4Zqor0ponILQz2Xhf1G/sxubT2Q5JAW4MV9dDNNcU7Y4A+HpAqfUdKJIuSNmA8TRPIPIFZDj/S0p+Y6Nz1N/IV7i3zWQZf5ofM/t/fJM8KyYHrfftZQ0jUN1kq6Suk3Nt3cdHMalyy+gIui9x+fYW3T3t2qyDxco8o2sOPimjBNfIjQyHH+HoquOdrSEaNo2iJOOXWufI4IPVv3TH8rv4O9GQU52n0BsjL5aCpgqf7yb1w+O3Pn5JqbSYGapqKOKkHjddXpgUajc+4q3j3I5y3okXW+TJCRTRGzItD8ga/z6C13zJvN9UTX7hacFJA6PVh9uYLb1L9jgcNisLRltYpMRj6TG0urSFuYKGGni1BVAvb6D2C5p5Z21zzM7erEk/tNeiVpHc1kkLH5mv8Ah6eQtiLWP0uT+r20MDqnXP8Arx/rH+vv3Xuve/de697917r3v3Xuve/de697917r3v3Xuve/de697917r3v3Xuve/de697917r3v3Xuve/de697917r3v3Xuve/de697917r3v3Xuve/de6//9Hf49+691737r3Xvfuvde9+691737r3Xvfuvde9+691737r3Xvfuvde9+691737r3Xvfuvde9+691737r3Xvfuvde9+691737r3Xvfuvde9+691737r3Xvfuvde9+691737r3Xvfuvde9+691737r3Xvfuvde9+690y7h3FgNpYTLbl3Rm8Rtzb2Bxtdmc5ns/kqLDYTC4jGUs1dkcrl8tkZqagxuNx9FTyTTzzSJFFEjOzBVJHuvdaj38yD/hUz1V1JVjYX8v2HY/yF3NLi8l/Eu2dw4fdtT1Zt/KPK9LQU2MpJq/rys3ZNDGn3bVFBUZCgKskZOsOh908sJYVJx1qi/IX+c5/Mt+TswftL5PZuko0hmpoMJ1ttHYfXWHgp53MksUc+3dq0m45bsbBpq6VwOAw96r08saAZGerYv8AhLv2b2Tvn+Y9nk3p2Hvvd0X+h7OymDcu79w5uk8v2W4z5RRZLIVFGsgKizCMG4H9Pex03MAAMdfRT9+6T9e9+691737r3Xvfuvde9+691737r3Xvfuvde9+691737r3Xvfuvde9+691737r3Xvfuvde9+6910bgcHm/F/wDeh9PfuvdIrffZGwOrtuV+8Oyd8bP6/wBpYqCaqye6N7blw21NvY6mp4nqKieuzWdraHG00UEETOxeUWVSTx7916leHWvf8vv+FOX8vjoWCbFdH7t/2azdrY2vcRdZUm5ItpY/KR+SKgpqveOV23QbUyaTS2dhSZQ2iHLLqU+/dOLE540HWtX3d/wqs/mYdhVIg6mo+huiMH9pUU2rE9XSbs3TI87Noq2rd9bw7AwC1FNGbIEpFjLcsjD3v7Bnp5YRUAZPVVfZ/wDNV/mJ931aNv75W9lZWp0ywx0m3MVsHZ6lallMiLT7D2XgZpC7AAFixX6La59+UMzBEGpz5AVP2UGenre3NzcJa2kTS3TEAIgZ3JJphFBck8AADU4Gegbhz3zE35Kk1RvfvivjZEpzXZjf+78NTiJndhY1ubxUckaF2JK3tfk/T2Ktu5G5u3Rl+j5euPBPFnXwgPzlKVpx8+sgOT/uo/eJ530Psfs9vS2RYAz3UP0MYr51vWttSgZYqSB5kE9CvtvqrumYCXdXeHZ1CvkXVSYnsneMlQUsC95zmspAD+Ba1vcmbL7JXUlG33dBEtR2xFSaefdSRa/6qdZcckf3bfMTRi69yubFs8isNmYmcr5/qFLuOpyBQj1p59DPitqy4CSBE3525kq2d4xCcz2/2LJLNJr0xJBSUe5KCObySNbSI2LHgf09ybtXtRyRtpEo26W4lU1LSyueHyQog/MdTtt33MPZHlJTNbcuXl9cqdRkurqV6ac/DCYIgPM6k/l1YL1J8Af5gfyUShi2H0Z3xn9tyzRU0e5d+V+4to7Rp0qinknjru0twbcx2Vp44iHlEEktlABsWAIkueb+QuV4/CfcrKKRR/ZxASP+fhByD5CvQF5y5z9lfb+GW0l5h2i2mVWIgtis0pp6i3WZ1J4DXT5cD1eD8av+E2eKqKWLKfK/tPOiV56aUbP6irsFgtNKi6quly2Wr9t7veV6hvQr0dZCVS9iGswi/f8A3+ljLRctbcnAjXMGbPkQqtHT/bKesIOevvA2t/N4PKuzqI6EeJcamrXgVUNHSn9JTnqz+j6I/k9/ywsX/FN55boTpOrNK+YGQ707YGe3flYsQsivV4bA793PlMjXy07KVEeMx5Z5iqhGkKj3D2++4vOXMRpue9P4dCAsapGoB4jsVSfzJPz6x73Pmffd5cyXl4ST5KqoP+Mgfzr1Vt8wf+FU/wATupKSfbfw22lN8hc0KKtNBuep27u7ZvXmMr11R0EdVi9z4vrXKZakke0shoqgjxKV1K5HsFOzOxZ2JJ8zX/L0SCN2JL/F9v8AxfUf+RB/NT+en8zb5Td0ZPvfN7Dx3UXU2xtpfZbE6v68odubbpc/vv8A0lSQVOVzeaqd0byq69f7mwCGP+LCnVEN4yzszV456rIoSgrnrbg9+6b697917r3v3Xuve/de697917r3v3Xuve/de697917r3v3Xuve/de697917r3v3Xuve/de697917r3v3Xuve/de697917r3v3Xuve/de697917r3v3Xuve/de697917r//S3+Pfuvde9+691737r3Xvfuvde9+691737r3Xvfuvde9+691737r3Xvfuvde9+691737r3Xvfuvde9+691737r3Xvfuvde9+691737r3Xvfuvde9+691737r3Xvfuvde9+691737r3Xvfuvde9+691737r3ULIZChxNDV5LJ1tJjsfQUtTXV+QrqiKkoaGio4XqKutraqoeOClo6SnjZ5ZHZURFLMQAT7917r5yn89v8Anmbn+YW+a745/E3sSPH/ABMwGNrMdurdG16Goosp3ZuSvrqlZqqDcuSp6fNUmxaHb0VPBFT0UVD9xNV1nnlqLQpTa6VRxgAE8etYADgDQqhf0hFVEH/BUUBVHv3TvXfv3Xutmz/hKd/28gz3/iGc7/7hbl9+HTM3AdfSK/33++t730m697917r3v3Xuve/de697917r3v3Xuve/de697917r3v3Xuve/de697917r3v3Xuve/de64M2n8j6mw5ubfgAf2vfv8HXs+nWvP/M8/wCFCfxY+EOPrtg9M7m2X8j/AJDTYnNGHaGzcrPujamw8rEhpMK3Yuf2/PS4CH7jIs0smLjzVJlDSU7MEQTQSNquMcenEiZ64PWhN8yP5nfze/mBbio6/wCQnbVVmKWipp8dhuv+tcLT7C2XQUdZVGplpFwm2Yoc5n2mqDbVlqvIyaAI1IUsrbRZJCscaFpScBck/YBUn8ul9nZzXdxHaWNtLPduQAkaNJISTQARoC5NeACmpwOiubb6E7W3EU+12dkMVQu6qa3MRU+EgGv6utNkajHVEwjXk6FY/j68exntft5zjutHj2OaK3JHfKBGPtpIUY/kOsm/b37m33jvcaWF9u9sb6w2tnVTc34js0ANKsI7ua3lcKMnQrenxY6Mntj4i4OGMSb1zuQyEpdT9lhJoqGAIouySyy0FRMSzcXSYcf48+5Q2r2XsI11b5uEskhzpjIVceROgn9jDHXSb2r/ALqnkRIEuPd/ni/vrrUD9Pt0sdtHpp3B3e2lkOo4qk4oPQ56H7bfU/X+0UP8C2xSRNrWT7islrMpUB0FgS+QqKmNbAfRVAv9fck7Vyjy1sv+4G1RqfVi0hqOBq7MPyp+XWfHt/8AdZ9g/aiPRyT7c2kExYN4lw895KWAoDW6lmUU9FUCuaV6Mz1T8e+8O86043pnqDsPs2eKqhoqkbH2jmMzQUM9SAYUyWUoKKTD4hWQ6i1TLEqxguSFBIN77fNq2hdV/fwwKBUBmVQwHHSKjV+Vej7n/wBwvbj23tfG545z2rZrcxsyC5uILcuq1qY4SyvJnFERiTRQCaDq7P4w/wDCfXvrfrRZj5K5odL4aOspFn2vh8ptfM7wrse9nq5qfKYiffOCoJEAaNRPCGD2YqV9xpvnvHtNovh7JF9TLQ0YhlQHyqGCMfXB4fPrmp7x/wB4t7Z7IJLD2ss/35elGpcSR3EVsr5CgxyLaysODEq1KYqD1ej1R/K4/lxfC/GTb7yO2sTGcXUQZKv7H7+7JeeioI8WhqFlqP4tkdvbBo6amCPLITQqDyXJUACI959yObd6UxT34igIPZEoQZ+eX/411zX9xvvbe93uYpt945jis9vKMvg2UEcCd3HvIecmlB/bYHDNSSvfJ/8A4UUfywvi5h3w3V3am2PkLumCnr5aXZ3QtJl8zgIJ4lWOkp6remC2tXde00lXMttDZKNljXW1kKt7AzyPI2uRyX9San9pz1jjLJdXcjS3MzySHzYlj+0kn+fWq/8AKH/hUZ/MX7ofIYLph+sPjfsatx1djJ02915i91b/AKyDILLDNNU5/fma7HwFNPDSPoiajo6ZkYljc6SrZya9bES8WqT/AKvTrX37H7b7R7gzj7k7U7C3h2Dm5FKNW7qzdZkUVGcytHBjC8eHpI2kYkrDTxgn6/QW18unAAOHQecWsxVUF2JNljQAXLMeFRVAub2Hv3DrfX0Vv+EpvxH3R0Z8NO1u89+7by+29yfJzs/A5zbMGaphTTVXVmxtm0Ue2K+kppIzPTwVW5d356Ji5WRzDYoAqlrDpNKQzY4Draf9+6Z697917r3v3Xuve/de697917r3v3Xuve/de697917r3v3Xuve/de697917r3v3Xuve/de697917r3v3Xuve/de697917r3v3Xuve/de697917r3v3Xuve/de697917r//T3+Pfuvde9+691737r3Xvfuvde9+691737r3Xvfuvde9+691737r3Xvfuvde9+691737r3Xvfuvde9+691737r3Xvfuvde9+691737r3Xvfuvde9+691737r3Xvfuvde9+691737r3Xvfuvde9+691737r3Wtr/wpT/mH574efEvGdMdaZ+gxHa3yuxPYOyqaU0qV+Vx3WsVBhdq9gZbHBg0eKrf4dv4Gkq39cc9OWhDNGwHunIl1E1GB181JVVEVI1EcaDSka/RFuTpuSxI5/r710r679+691737r3WzZ/wlO/7eQZ7/AMQznf8A3B3J78OmZuA6+kV730m697917r3v3Xuve/de697917r3v3Xuve/de697917r3v3Xuve/de66uP8Aff77n37r3Xrj+vv3Dr3UHI5OhxNDW5PJVdLQY/HUtTXV9dXVMNHRUVFRQvUVdZW1dQ8dPSUlLBGzySyMqRopZiACfe6Hj5de60uP5vn89XuzsnJz/HX+V9ksfn+vMvtnN4Ht75I0Gz56uCeszstRiF271Xube8WL2yIafbjS1FRl6SjySLJWQfbVkU0EqgTbTyXzZvoDbXsFw8RIGsr4aCvnqk0KR54PUmcpe0HuTzowbYOTb2W31AeK6eDCK+ZkmMaEAZOluHWrbtL4YPWvJku0dz5eur6idJaimxuU+5raoKoMhyOZyMWbq6yRz6Q33OoLfn6H3Mew+xkYjEvMu4OTX+zhIGPMFir8eFQw/wAvWbXtv9yPZ50F17m8zzE61pb2UkaVUZYPIY5xU8BokUgV86Hoy+1umeudix6dt7XpIHLCQ1VfPV5arZ0FlOvJz1SRlQP7CqL8+5V2jkrlrl9abbtSKag6nLSNj5uWH7AOs/fbD2e9n/bcaeUuTraGQupMk7yXUtRgd1w8qin9BVFcmvS1lpg0kUIUCWaRY6anUDy1ErsEWOmpoxqnkZyAEjUsxIAFyPYiKhQf4AM+g/yDrKXa98ohbWqxJk0oFUepp2qMGpNKAVNOj5dCfytPmx8h6qjn250VvTae0pKqmhqd6diY+DYmKhp6hlLVtDj96ZHbGZzsFNAGdzQxVAuAn6yFID33nvlbZgyy7vDJcBSQkZMhqPImMMqkn+Ij9nUW89ffP9hfa63kTdvcOxvN6VWYWtk7XkhZeCM9qlxHCWNAPFZOOr4QT1sPfHP/AIT9fGfY9NTZT5BZvd3cO6IaylnGLxe56vamxxDTaJGjNFgcVgt0zGee+v8A3JMvjChQp1EwnvXu1vV4xTaYkt4M5Khnz9pK/wDGa1r1zR92f70j3h5kkmsvbCwsNh2do2HiSWy3N4WaoqGmkmtgAtKf4uDqqTUUAPB2l8vf5Zn8uLArt7sHuf4/dDTQUdVV0+yaXN0WV7EysNGpgLQbS2+u4uxsq7yqYI2NNMWlui3a49xne7jfbg5lvLp5ZP6R/wAAFAPyHXObmvnvnLnvcH3PmzmG63C9bzlckDNe1BpjUVzRUA61nPmr/wAK0szFVPtz4HdU7aqoWxuQp6nszurbW566mFfOzxY+swO1P4715lqf7GImVhXUFTE7lQQQHQoTQ9BcQk/ET+XWrr8pf5j/AM3PmjlYsr8i+/t07tWCmmo6bCbbpMH1vteGkqJmqJ6c4brvE7WjyKSSMbtWtVSafQGCXX36tc06eCKCSB0SJQF/SqqT9SiIhY/1bQo1n/Xv791brwuf8P8AD6/1v7917r3+H1/3v/eLe/de6tR/lP8A8sLtT+ZZ3/hNs4nbG4/9l72Tu/acfyG7JxhgocZgNt1uSircrsyiztWTDDvHce06Kv8Atkpoqqpo28MskaiaDy+HVHfQMcT19XvZWytr9ebXwuy9l4Wk25tfbtEmOwmEoTIaXG0KO8iU0Bmkml0B5GN2Zib/AF976R/n0qvfuvde9+691737r3Xvfuvde9+691737r3Xvfuvde9+691737r3Xvfuvde9+691737r3Xvfuvde9+691737r3Xvfuvde9+691737r3Xvfuvde9+691737r3Xvfuvde9+691/9Tf49+691737r3Xvfuvde9+691737r3Xvfuvde9+691737r3Xvfuvde9+691737r3Xvfuvde9+691737r3Xvfuvde9+691737r3Xvfuvde9+691737r3Xvfuvde9+691737r3Xvfuvde9+691737r3XFzpRmP0VWJ/2AJ9+69183/8A4Vb9vzdifzD+t9hxEx4rpb47Y/GRUyyM0X8c3d2b2X/GqvSyJpmqaHbePQ8stoltbm/j0phFB9vWsX71091737r3XvfuvdbNn/CU7/t5Bnv/ABDOd/8AcHcnvw6Zm4Dr6RXvfSbr3v3Xuve/de697917r3v3Xuve/de697917r3v3Xuve/de64sbWv8An8/0P49+690z5jP4Xb9HLkc3lsbh8fTxvJPW5Stp8fSxRxLrdmmq5Io7IoJNj9PbsNvNcOqQRM8h4BQSf2Dp6GCa4cRwRM8pwAoJJr9nVSPyX/nN/GXpSobA7AkrO6t2/ZVkkmP29T5rC4rH1kZaKjhqs3msFR4urjqJbkmmqH0opvYlby7y17L8zb0puNwUWVpqGXKsxHEkKrFh+Y4/n1MPKnsnzNvw8fc3Wwswwy5V2I8yFRywp/SXj+fVCPyp/mvfKH5LY3IbTpspR9Wdd5nF5DEZvZ21aXD1k2WosnHLS1kdVuavwh3LReWgnaD/ACSsisp1Ah7H3OvLntByrsA8S5gN5eAgh5CwAp5BFbSc5yvy4dZP8ke0ft7yq63F1afX7gGBEkpkAHDhGr6CK57lPpw6qoixtPSQrT0dNT0lOikJT0kEVLCvH08dOsaX/wBhf3JK20cSaYolRB5KAo/YKdZK7XzDFAgjgRY4gaURVQfsUAfy69S4WvydVDQ42grcjXVEqQU9FjaOoyFbNNM4jiijpKOOadzJIwVSFsSfbUwRFMkjqsYGSSFAA41Jp/h6G9lzNDChmnnRIVyWYhVA8ySaDAFerXPi9/Jf+T3yBpV3DviCn6M2c1VBHBk9wyYPN5vJ07RrNVvQYTE5usraF4I2VV+9pow0ji1wrWiHmj3Y5Z2OXwLJje3IBqFDKoPkCxUA1/ok46i3nf74vIXIzi02nVvG6gVKR6441NaKGkeNVYE1roYmn5dbDHxf/lL/ABF+LNPNuLIbdp+x95U1VTZR98b8rMoaegixsfkUDAS52faMFPDKjTM70gJvZiVUAQNzH7mcx7+fBW4+ns6H9OML5/09If5fF1hJ7n/e793PchPoV3c7XsJjZTb24jFdXEmcRLPWlBiSg4jJPQS/OL+el8APgdSptrI9gY/srsY4+uqsR1l1fjc9lFqFpXNPHTzbrwW18tsPDvVVo8arUV0LBfWRo9XuPGbxG1MxLepr1jE73NzI8s0jPITlmJJP2kmp/b1qK/NT/hTx83fkdjqjanQ1DjfinsusxeSxGXixNLs3f27M7Bkg8L1FRlt17V3AuFlhopHijbHT07hmL3DqjDQrQsTnrawAZbOetcrdW793b8y82f3tufcO8s7PzPld0ZnJZ+sI1Fz4pMpUVS0iaiToh0Lq5tfn3rp0KBwHSd0v9bNf/EX4P1H0+nv3W6j169of6WYgfT0ngf0+nv3Xqj169pb/AFLf7Y/8U9+69UevXo0eaVYYVeeeRkWKmpY5KurmZm0qkNHTLNVTO7WCqqFmPABPv3Xq049Xn/y2P5Cny5+fOQ/vPuXEZH4+9G0GYxNFlOw940WLG4MxTVCisylNtjZNZl6bdEdXDjCvjqKvHCkM8yLr9EoXdOm2lC8OPX0cfhr8LOg/gp07QdLfH3ZlLtPbFPVDJZWo+7zGTy24s0KSmoP4tmstnspmstWVAoaOGJEepaKMISihnct7pMzFjUno2f1/PPAPH9Pr9fwb+/dV679+691737r3Xvfuvde9+691737r3Xvfuvde9+691737r3Xvfuvde9+691737r3Xvfuvde9+691737r3Xvfuvde9+691737r3Xvfuvde9+691737r3Xvfuvde9+691737r3X/9Xf49+691737r3Xvfuvde9+691737r3Xvfuvde9+691737r3Xvfuvde9+691737r3Xvfuvde9+691737r3Xvfuvde9+691737r3Xvfuvde9+691737r3Xvfuvde9+691737r3Xvfuvde9+691737r3WOb/NS/8ALN/+hT7917r5bH/Ci27fzVe7wzsw/u1thRqZm0L/AHq7E9C3PpUEk2HHPvx6VR8F/PqjT6e9dO9e9+691737r3WzZ/wlO/7eQZ7/AMQznf8A3B3J78OmZuA6+kV730m697917r3v3Xuve/de697917r3v3Xuve/de64E8i5t/sPr/h79p9evdBz2P271v1Hg6rcfY+8sHtLEUdJUVss+UqT9w9PSxyS1D0mMpknyleY0jPpghkZmsoBYgEy2zaNz3idbbbLJ5pmYCijFTwqxoo+0kdGG37VuO6TJb7daPLKxAwMZ4VJoo/Mjqij5Dfz2NqYqprMB8cdiNvi9JVwJv3cT5LBYukr5Q0VHNR4HM4fHZWoNNcTHzU7wsbKb2Ye585b9gb6dUn5l3D6cFgfCTS7EedXViorwwQePUvbN7ROqrPzFfeCa18JKMxA41ZWKivDBr/LqhPvH5R/IH5IZls1272Xm9xFI3p6XF0ceN21hqenlkaaSA4vadBgaCuDyE3aeKR9Ppvp495DbDydy7y1B4GzbYka1FWOqRieFdUhdhQeQI/b1LW1Jy7y1D4GzbfHEa1LHU7Gn9KTUR/tSP29F7+xsNKqqfXhEVVvb/UqAOPYk8BjSp4f6uPRn/WwAikhp/q+VenTCbRz+6cnS4XbOEym4MvXVEFLSY/D0FTkKiWonkWKCGT7eOSOmWaVwA8pRBySQASEtzLbWsTzXU6xQqCSWIAxxOTn8q9LYucIoUMstwEjHmTQU/Mf7PVz3xs/kd91dk0sOf73z8XTGH+9oyu2qRtv7q3Hl8cwWSrkWtweay2MxwFmhAkeKbUdQFrH3BnNHvhsW3M1tsEH1suk951xoreWGVWPrio8ugfvHv9ZbUGh2W2N3PQ0c1RFPlgqpPrwI6v8A+jfh18TvhvtWrqtqbT2ptiOIxZPNb03nl6vI1Hlx8D6K3+K72zWXjxCU0Ks5WmkhS41karH3jlzHzxzJzRN4m6bizRAEBFCooB44RU1f7avpw6gPmr3I5x5xnru+8SGEAgRppjUA8QRGqaq/0qmmOHVPXzw/4U2/DT4v177J6FpKv5S9iLj62WrpdvR7m2ptTbuSilenoafKbi3PtnFY7Mw1DjyMcbVz6YkINmZbhHh0CBEx603fnR/Ou+d3zsyIo939oVnXPW0dHXUVL1h1xBhsDj1gyUl60Vu68Tt7Cb1yBmgRItM1dIiovpsWe/q+nTwiUZPHog3Q3xj+Qfyl3fS7L+P3VO6u09zV1dQ4+Q4k0VHjKeoyMwgpRl94bkrsXtujdySx+6rkZYxqNl5966uWA49bOPx8/wCEk3yU3rh4Mz8he+dsdIVLVkEVRtDbmIxW+c6tEUjeplbL4zM7g26JbuyIFm+q3PBB97p0y0wHAdWc7O/4SNfC3E0Ui747g7U3/kJWXx1cn3W2kpkVCHRINu7nxUUokdtV3UkWsOPfuqeM3S0/6BM/5fH/ADuuzR/5M27P/s+9+694renXX/QJn/L3uB/Guzvz/wAxPu3/AHm2/vfuveK3p0+7f/4Si/y4cZWLU5mDsTc1MGhP2NXvbsDGxWR2LgzYrselqLShgD6uLce/de8VurX/AI1/yjv5e3xOCVHTnxr2di8qXp5pcxufK7z7FrmqaMMKaeD/AEkbo3hHRvFqv+yEBaxNyAffuqFmPE9WPwU0FNGkUEUcMcY0pHEixxoP9SkagIi/4Ae/dV6z+/de697917r3v3Xuve/de697917r3v3Xuve/de697917r3v3Xuve/de697917r3v3Xuve/de697917r3v3Xuve/de697917r3v3Xuve/de697917r3v3Xuve/de697917r3v3Xuve/de6//W3+Pfuvde9+691737r3Xvfuvde9+691737r3Xvfuvde9+691737r3Xvfuvde9+691737r3Xvfuvde9+691737r3Xvfuvde9+691737r3Xvfuvde9+691737r3Xvfuvde9+691737r3Xvfuvde9+691jm/zUv/ACzf/oU+/de6+Wx/wos/7eq94f8Aht7Y/wDeq7D9+6VR8F6o0966d697917r3v3Xutmz/hKd/wBvIM9/4hnO/wDuDuT34dMzcB19Ir3vpN1737r3Xvfuvde9+691xJsRx+f98ffsZ+XXuutf1+n+9f7370D14Z6BvuL5BdP9Cbeqtz9sb7wmz8XTUtRWBaxquuydTDTI7yGiwmIpcjmqz9BUeKne72UXJt7Odo2DeN+uFtdpsXmmY0xQKCeFWYhRx8yOj7YuWN95luUtdk22SeQsBUUCivCrsVUfmeHWvV8jP57mezSV23vjPssYOnlpaini7H3OIKusWrlWWOCoxO2sxhZYQKZXWS1bS2Z7Agrce8iuW/Ya3iaO45mvdbAgmFKgU8wzqwOeHa3+Tqf9n9irTbdM/NN/4kwyYI6gU9GkVvPh2tw6oq352Z2T27m5dx9m70ze9M3KLGqys6R00SF2lMdNiKFKXD0itIxY+KBLm39BbIXbNl23aLf6ba7COGEUwvH82NWOPUnoZ3F3tmywfS7VapBDQ4XjUerGrH8z0mIKQcWF7gD6/SxJta/+PHs5Efn5V6j3dOZqVPiZp/q8ulRtvZ24d2ZOlwe1sFlNxZirnhpaPG4iiqK2aSoqJFhgSVoVeKjWSVgNczJGv1JABI1cz2tjA9xe3CRW6gkliFGBWork/YKn0z0CLnmnT/ovl/q8urs/jb/JL7K3xS024PkLuL/RjjvvqVv7l4psNn83lseNEtV9xl8HmchQY+Ob1QDRNFOvLceg+4A5s9/Nl252teWbT6ybSf1W1oitwFFdVJ9cgjy9eiGfnGddQtwSacT/AMV1fp1b8f8A4zfEPZ1V/c7be0OucBSE5PL7j3Bl5qiZXpKaxqqjcm8stk66mjgpoCdEdSqCxbTqJJxk5h5v5i5pn+o3jc3kNKAAKqgH+iiqp+0ivQVvN0v9wbVdXLN8uA/YKD+XVAnz3/4VGfF74+ZSbYnxY28/yh3qmOrXyOdhXc+ydkbUyyyyQUFLXVO6ds4So3EsgX7hnxj1UQjXQTrIBDXDz6TLGSOtLH5p/wAz/wCaHzz3JFlu++4s1Xbex1JU0GE2LtaDC7K2xi6GsqGqaqmqY9k4TalRuRZpLAvlPunCDQCEZlOq+XT6oq8OPVfS6QCAERPU1gqRIFAGpmsERQP6m1h791brZV/k4f8ACfrfPz4xUffnyIrc51P8eMTuvB02F25JSrBujuvGwQ0ua3BT0po8zQ7k2fgDRTwUTVMiUFWz1cjU0heBjH6nTUkunC8evoZ9FfG/o/40bKo+vOi+ttudcbRotBjxmDiqpp53jjEKS5HNZSqyGdy0yRDSHqqmZwvF/r730mJJ4nobgAL2AF/rYWv/AMb9+611639eebj/AA9+69137917r3v3XuvH/iffuvdet7917r3v3Xuve/de697917r3v3Xuve/de697917r3v3Xuve/de697917r3v3Xuve/de697917r3v3Xuve/de697917r3v3Xuve/de697917r3v3Xuve/de697917r3v3Xuve/de697917r3v3Xuve/de6//X3+Pfuvde9+691737r3Xvfuvde9+691737r3Xvfuvde9+691737r3Xvfuvde9+691737r3Xvfuvde9+691737r3Xvfuvde9+691737r3Xvfuvde9+691737r3Xvfuvde9+691737r3Xvfuvde9+691jm/zUv/ACzf/oU+/de6+Wx/wos/7eq94f8Aht7Y/wDeq7D9+6VR8F6o0966d697917r3v3Xutmz/hKd/wBvIM9/4hnO/wDuDuT34dMzcB19Ir3vpN1737r3XVx/X3759boevXH9ffqda6CrtjuzqrpHbdXuztTe+F2Zg6KlqaySoyck0tXLT0cTzVDUOIoIazMZN0RbBKenlZmIUAswBMdr2fct5nFrtlm807GgC0pnAqSQo/MjoScs8o8yc4X8e28t7PNd3bMFogAUFjQanYrGlfV2ApUnAJ612flr/PiXK077a+HWKzFKk9HUw1vZm+tu4zGz0tbIzxwTbY2/lX3CaqOCncS3yFBSt5dK2K6iMguU/ZAI31XNsitQikMTMQR5h3Gggnh2s2OsxuSfulttRN77m3MTSggra28jOCo4iWRfDoSRT9ORxSprWnVCHZPcHbnd2Zg3D3H2VvLs/NUymGkrd5ZutysePheYztBisdNM2MxELz+srSwxKXsTyBbIXadk2jZIWt9o26G2hOSI1C1xTuIyxpjuJx1L0u38v8s2r2fL20W9jbGpKwoE1YpVmA1OaYqxJp0l6aAkg3Bsbgn6kn8n/Hj2eomPh7uog5i3rSSNVDQ9LHB4LJZmvpsZh8ZX5fJVk8NPS0GMo6iurJpqiQQwpHBSpJJeSRgoYgKD9SPr7VM0NtC008yJCoJJYgDAzUmnUDb/AL/pqfGPA9XOfGT+TR3r2PVY/cPd0uL6n2SayjkbER52gzW9sxQl1kq2jpMFFmcTj4jGDEDJkaWp1kkKAFf3CXN3vxy3tCyWnLwe93DQe7Qywq3AZfQ5PnhGWn5jqJ73d7m4eiyHT1sN9NfEv4x/F3E1Fb191vsvZ9RDH99lt65UHI7hZKKFy9TV7w3XXZTLUFLTwBmKrWJCp1PYEsxxb5k575r5seu9bzPLAMCMEJGK+kcYVPzK18uHRSWdviYk9U6fP3/hST8I/iTLmeveqMjuD5F94QYrIzU2I2Ptuo/0c4PLRiop8XBu3fe4srtCkqKaprUDucJ/F2SmR2K6zEkgQziuOrLGzZ4DrRt+ZH83j5+fOSuqqbuP5A75xnXk1JVY+Dp/r7NLsbrtaGseRqqHOYzZGN2cm9mqEk8RbMxVrrADGrCN3U+HAdKFRR5Z6rQREiRYoo44olvpiiRYokuB+iONVRfp+APfur9eIJP+9cX+vBFj7917rYO/kI/yipP5ifcNf2523hqSr+KnRe89r0O+cVka6spD2huttG6DsGloqA09ZkduthqKGHMSfc00Jp8vGqmciRYfDpqV9IoOPX0ztubcwG0sLjtu7XwWG21gMTTrSYrBbfxlHh8Li6RWZlpcbjMfBTUVDSqzkiOKNEBYm3J976S9PQv+bf7D37r3Xfv3Xuve/de697917r3v3Xuve/de697917r3v3Xuve/de697917r3v3Xuve/de697917r3v3Xuve/de697917r3v3Xuve/de697917r3v3Xuve/de697917r3v3Xuve/de697917r3v3Xuve/de697917r3v3Xuve/de697917r3v3Xuve/de6//0N/j37r3Xvfuvde9+691737r3Xvfuvde9+691737r3Xvfuvde9+691737r3Xvfuvde9+691737r3Xvfuvde9+691737r3Xvfuvde9+691737r3Xvfuvde9+691737r3Xvfuvde9+691737r3XvfuvdY5v81L/wAs3/6FPv3Xuvlsf8KLP+3qveH/AIbe2P8A3quw/fulUfBeqNPeuneve/de697917rZs/4Snf8AbyDPf+IZzv8A7g7k9+HTM3AdfSK976TddH/be/eppnr3Sd3Hunbmz8RW53c+cxeBw2Pp56utyWWrIKGjp6enieaZ2mndAxSKMnSupz+ATb27BBNcyrBbxM8rYAAqc/IdGW2bRum9XkNhtFhNc3sjBVSNS7EkgAUUHiSMmg+fWvt8wv59HVO06Sv2h8TKbI9kbsaiyVLUb8ze3avAbMwGQmjMGNlx0e5Ptc1mqulZjUsHw81GwEaF2JkRZr5W9ndyuXS65kZYLYEHw1YNIw86lKqoPDEgbjjgeuhftN9wLm+9aHefd6SPa9q1oy2kUyzXMqA1cSGDVHErYQUuFk+I6RRWOtN3F8je+PkfmYc93p2vvHs6uow6Y+DcOQAwmIjmlWd48NtegSi21illnQOzU9JEzFVuSEW2RWycvbNy/EYdo2yG2BpUoO5qVyz1LnHkWP8AM9Zv7fyJyN7dWUlhyLyxabZbuKuYk/UcgUBkmfVM9BUAPIwFT6mocUqluTbgj6kkkn6Nz+ePx7FEQ8vLqMOZL2inJ4HpU0UTSPFGiPJJLIiRxIjzTTO7aUihp4g8s7sxACopYkgDk+1iYy1MZzw/Ov8Alx69Y8cy3+kMS/AHq134p/ypfkl8iEodzZfF43qvrZshSw1md3dkYabcuQpP2Z65tvbWoaLPVplho5bKcjFQq0zqoawkZI45q94OVOVvEtYpXvd30khY1qingNchKDj/AAFzQH5A4rc3867fBI8EMxmudJFFFVB/02B+yvWzf8cfgJ8Z/jTSQT7M642/k93xy01VNv8A3PRSbj3WamjB+2lxuQ3FWZyXb6Rvd/HQPTxmT1lSwB94qc0e5fNvNjMl/ukqWJBHgxnw46HjqWMIH9KuGNMcOoQvNwub59U8pI8h5f5OjqqABwLcfm5P5+pJN/YD/wAPSHz+fWkl/wAKwPm1311T2L8YvjN033T2N1dtveXUfZe++4MJ17ubIbS/vpjM9uTGbV2tjtwZfBVNDmaigii2tmoTSLOsDx1cgkVw/Hun4VBqT1o7KkcSkRoiKzF2EUaRhnYAa3CBVZ2CgFjc2A/p710o65f77+v+9+/de697917ri+vxy+IhZjFIIWblUmKHxOws11WSxPB4/B9+691vCfAz/hQx/Kg+Bnxp2H8e+suiflPjafBU5ye78ri+tutdW8N61MFNja/dGSqqzvg1uRr6rD4igpzNOBIY6ZVsAoJ30wY3ZiWOOjk/9Bc38u3/AJ9J8vP/AEXHVn/29/fq9V8B/Ude/wCgub+Xb/z6P5ef+i46s/8At7+9V694L+o69/0Fzfy7f+fR/Lz/ANFx1Z/9vf36vXvBf1HXv+gub+Xb/wA+j+Xn/ouOrP8A7e/v1eveC/qOvf8AQXN/Lt/59H8vP/RcdWf/AG9/fq9e8F/Ude/6C5v5dpt/xiP5dnn/AJ9x1Z/vH/Gdvr79Xr3gv6jowPxX/wCFLPwh+W/yA6y+OnXnWfyZxO8+09wUG3cBkt3bF67x23KSuyGWxGHhky1bi+4c7kKemWpzMTM0VJOwQMQpIAO+qtEyipI62KEYOiuPowuPfum+uXv3Xuve/de697917r3v3Xuve/de697917r3v3Xuve/de697917r3v3Xuve/de697917r3v3Xuve/de697917r3v3Xuve/de697917r3v3Xuve/de697917r3v3Xuve/de697917r3v3Xuve/de6/9Hf49+691737r3Xvfuvde9+691737r3Xvfuvde9+691737r3Xvfuvde9+691737r3Xvfuvde9+691737r3Xvfuvde9+691737r3Xvfuvde9+691737r3Xvfuvde9+691737r3Xvfuvde9+691737r3WOb/NS/8ALN/+hT7917r5bH/Ciz/t6r3h/wCG3tj/AN6rsP37pVHwXqjT3rp3r3v3Xuve/de62bP+Ep3/AG8gz3/iGc7/AO4O5Pfh0zNwHX0h3ZVF2Nh/r2A/qT/h/vXv1ePSdVLGgHVWHzO/m1/GL4jJU7ZmzmQ7G7UqcPkK7DbO2PjRmsfT1UfmpMcdz7klyGJwWNo6nJRFZEgq56xIEd/DzF5Bzy3yDvnMGicRiGy1AFnOkkcTpUAkkA4qACaCuDTNH7v/ANxv3j98vD3pdvi2nklLhElurxzFIymjSfT24jlmkYRkaWeJIi5VfEw+nUM+U38xX5T/AC7yVRH2J2XuHGbFdJqaj6x2tkjgdlQ01U7NMmUocBR4Nd0ySKQmvKJVusXoDBGZTkryzyVsHLYH0tjG15x8RhqfHoWro+xKCuaV67Ve2P3W/Zv2MsYhyrynbTcwggvuFxH410zKKAxvM8xtx56bcxqW7itQD0TakRFVAoCoBZFUaVQH8BBYD/bexuo1MCTx6PeY5D3kscg1PGvSkpOF1N+D+omwUDkkm/0F/qfa9FHEDu/1f7PWPnMjsDQk5B/1f8X0d34u/B/5G/LTIyU/UGyY5sLQV1BSZreW68pT7Y2riBWXkExqK5my2XEFKryuMbR1xVQBbWyKxLzFzty5yjEG3i80ykErHGpd2p5UXtWpIHey/sBIxC90/cjlXktAN83Kl0yMVhjUySNT5L2pnA8Rkz8gSNqL4ifyjfjr8fcfTZnsfbW3+7OzI66jyNNuPd9DNlcRt2WiVWjhwGArTFt9rVaCf7iXHJUa9I1aUUDGLnD3f5k5hleHbbmSx2wqQUjbSz14lnFX4Y0h9P7T1z852919/wCabhkspns9tAI0IQC1fNmFW4YpqI/aerZo4YoUVI0RFQWVVUKqg/UIoFlB/oPcRnOTx6inrLx7917rhJcRvb66Gt/r2Nv959+6918xz/hTX2XUdhfzQ9y4ueomlh6w6g2hsCjildXSnNP2H23mp1gCSP40cZmO4OluPp9CdHpVCKL+fWvf9Ppx79071737r3Xvfuvde9+6913c/wBT/tz7917r1z/U/wC39+69165/qf8Abn37r3Xrn+p/259+69165/qf9uffuvdeuf6n/bn37r3XtRuBc3P+J/3v8e/de62Ef+E0fx6r+5f5k+0t8TUQqNtdEbebeuXrpfVDQ5vIVcmT2igQBi1TU1exanQR+gpckXHvw6amai06+nDGgjRUH0UW976S9c/fuvde9+691737r3Xvfuvde9+691737r3Xvfuvde9+691737r3Xvfuvde9+691737r3Xvfuvde9+691737r3Xvfuvde9+691737r3Xvfuvde9+691737r3Xvfuvde9+691737r3Xvfuvde9+691//S3+Pfuvde9+691737r3Xvfuvde9+691737r3Xvfuvde9+691737r3Xvfuvde9+691737r3Xvfuvde9+691737r3Xvfuvde9+691737r3Xvfuvde9+691737r3Xvfuvde9+691737r3Xvfuvde9+691jm/zUv/ACzf/oU+/de6+Wx/wos/7eq94f8Aht7Y/wDeq7D9+6VR8F6o0966d68SB9fz78Ovfn11fnj6f15+p+lvfh8+PXjj7etin/hND2v190t8697dhdn7qxWzNn4PpLck2QzmZqY6WlRocVump+2heQhZqyaOIiOIetmsAOR7X7dtl/us62232ryykj4QTSuMny6E/KfInN3uDutvsfJuwXO4blJIq6YUZ9JcgDWwBCD5sRgE+XV2Hzm/nwdidmTZXYXxUjzXW2wqrF1uOrt85WNcN2RW1VcamnNRga3bW58nHgoKWgdDFUQ1STioZm0DxozT/wAq+1FtZmK730pNchq6PijxQ5DINVTWoIpT7T12t+7T/du8qcqpZ8y+9DwbtzKk6SJZxky2KqmlqTJPbxmYs4IZGjKaABU62A16a3J5DL1tTksrX1uSyNZMZqvIZGqnra2slIAM1XWVLy1FTKQANTsT7l+GNIEVIkGkenXWSCztbCxSzsbWOK2jWiIihEUeiqoAUfIDrPT/ANix/C2twBcm9x/X2vU5HUeb52g5PA9CRsfZe79/5qk25sja+f3fn62enpqXDbbxdTla+Weql8NOjRQI0cAllawaRkT83sCRae6trKNp7u5jihGdTEAY48eP5dY9887ztGw2Mt/vW5wWlgiMWkmdUUBRU5JzQZooJ/aOtln4R/yKpa/H0u+fmBkZKKqTKY+tw3V+066rMUuOgEc9ZBvmfK4LHlZKmoURGlgNXTvBq1Ndipg/m33nETtZcrJUFSGmcDB8vDox4DIJ0mv2dclvez74tvJdTbN7XQeJAY3WS8mVa6jgG3CSNgA11NoYNSgxXrZZ2N19snrbb9LtXYO0tt7L23RktTYHauCxe3sRDIUSNpVxuIpKOiEzRxIpYICVUD6Ae8fL2+vdyuHu7+7lmuW4s7szEfMsSfXz6587pu26b3dyX+77jNdXzcZJXaRz501OS1MnFfM9LK3/ACL2kp0X16797611737r3XTfT8W5+v4P49+6918pT+fjIZP5rvytuX/b3FRRWf6emrzLgpyfR+5x9Ob+/HpXF8A6p1966c697917r3v3Xuve/de697917r3v3Xuve/de697917r3v3Xuur/73b/jf9Le/de67uLFiwVApLNzcLa5IFjqsPx9T7917r6Qn/CYb4Dbq+KPxO7B7o7Z2zLt/tL5N7v23nKPFZfHPBmtt9abM23LTbZxdUK/G0lfjq6fce7M808Mck0Jj8RDai4G+ksramxw62cvfumuve/de697917r3v3Xuve/de697917r3v3Xuve/de697917r3v3Xuve/de697917r3v3Xuve/de697917r3v3Xuve/de697917r3v3Xuve/de697917r3v3Xuve/de697917r3v3Xuve/de697917r/9Pf49+691737r3Xvfuvde9+691737r3Xvfuvde9+691737r3Xvfuvde9+691737r3Xvfuvde9+691737r3Xvfuvde9+691737r3Xvfuvde9+691737r3Xvfuvde9+691737r3Xvfuvde9+691737r3WOb/NS/8ALN/+hT7917r5bH/Ciz/t6r3h/wCG3tj/AN6rsP37pVHwXqjS4H1916e64mxsb/T88cH+vP5924ZbrRwCadDV1p0nubf1QtVOjYTAQzwrVV9YlRBPUxvZ5RjIjSypPMsI+rmNQzLz9bSFyh7db1zRJ48sZt9rDDVJIGDMOJ8MaTU09dIqRnjTKb2H+6tz17y7lFd3SHa+To5YxLcTLIkkik1YWq+E6uwSuXMahmQaqFitgux9gbX2Fj3odvY6KHyOklTXTwwSZGrZVMaGpqxGssgjUHSpJCliR9T7ye2DlbZeWrRrXa7ULqPc5CmRsU7mpU0HCpNKmnHrux7G+y3t37LbNLtHI2xxRNNIGluHSM3UxC6R4s4USMAK0UswGpiPiPS+Uki7G9+QTz/rXJ/of979m7Uqa+f+rPWUW3OulQD1JT/Aj9Q/1hf6c29pqqGFQa16ELGsTn5Hq0P4RfyuPkX8wM7BXRYSbrbrGiyeOizm+d6UObxAraOdlqK+PaEC4DKLlcnT45SVE4p6czSxKZbFygP5l9wNk5ZhKCUT3xU6UQqaHgNR1CgrxpU0rjrAj7yn3u/a72TsXtTuKbrzdJDI0VpavFLocdqG6bxo/DQvSujW+lXOn4Q2458P/wCX58ePhhgqqh6x2vFktz5KaGoyvYO6qHFZTe9W1PTmmjp4NwNQjKUGNCMzfbRzGISSO9rs18beZec985omV9wuSsC4EaFhGM1rprQn5kVoKdcHfef7xPuR74blFc81boYtriUrHaQM8dsoJ1EmHVoZzgayuqiqvBR0eAC1wAACb2tx/j/sfYTNfXPUEDOT1yF/8f8AY/X37r3Xfv3Xuve/de697917ri5sjG19Ksf9sCeP8ffuvdfK4/4UI7frMB/NZ+RklWCF3AuE3HR3R01Uddm9345CC8cYcCXFPyupf8fqBo9K4vg6pS9+6c697917r3v3Xuve/de69/h/vv8Afc+/de697917r3v3Xuve/de697917rwIIvcAagoLcXJ+gH9SfwPqffuvdbPH8jL+RTu35jbxg+Q/yl2vndi/HjrbeW0qzAbN3Niq3F5nuvLYyWPc1TCuFzGAqsbkOtxHBSUVeKieJ6layeJoHVBq2OmpJNIoOPX0dKSjpqGCKmo4IaamhXRFT08SQQRJ9QkMMSrHHGCSQoAAJPv3SXqV7917r3v3Xuve/de697917r3v3Xuve/de697917r3v3Xuve/de697917r3v3Xuve/de697917r3v3Xuve/de697917r3v3Xuve/de697917r3v3Xuve/de697917r3v3Xuve/de697917r3v3Xuve/de6/9Tf49+691737r3Xvfuvde9+691737r3Xvfuvde9+691737r3Xvfuvde9+691737r3Xvfuvde9+691737r3Xvfuvde9+691737r3Xvfuvde9+691737r3Xvfuvde9+691737r3Xvfuvde9+691737r3WOb/NS/8ALN/+hT7917r5bH/Ciz/t6r3h/wCG3tj/AN6rsP37pVHwXqjmCmqKyoipqWCepqJXVIqenjaWWV3YKERFBLFyQB/ifbsEE91LHb2sTyTMQAqipJJpQAefDpZb21xdzRW1rC8lw7AKqAlmJNAABxz0b7q/49QqseZ39GklQlRBJRYKP9ynCR2kP8WjqadFcvLxoHkQoDc8+8jeRfZkRrHunN6A3CuCkAyoAoT4wZQCScaRqBANTnrO72J+7FYXD2/MHuZEJJlmRorMHUmkUY/VK6ANVsaBrUrqBOaE5FLHHDGkUMcUMMfEUMMSxQwoOQscSBY4xf8AAFvc8GNY0SOONVjUUAAAAr5ADA/LrrHyqLeGGK3toligTCoihUUeiquFHyAA6dUYXP8AS1/9gLke03p1OWyyrpQV8+ltsjYe8+xs7QbW2JtXPbu3DkaqmoqLDbfxtVkqyeorJ0p6ZTFTxusSyTSBdUhRBe5IAJBZe3lvYRtPd3CRwqCSWIAxk8fP+fQvv+aOXuVNrud45j3i2sdriUu8s7rGiqg1Masc0ArQVPoOHW1j8A/5EW3tvUeM7K+YP2W4dyw5fGZXBdXYqaWv2xTUVD4ao02+6XPbcoUyVRV5BCk1FoqaSSkGkufI6jHzm33TmneSy2AssOghpTgknFU0seA4MaMD5Y649/ec/vLd33WW95P9hvEtNoaCSKbcZAEuGd6rqs2hncxqqHUk1Y5VkNQo0KTstYjD4rBY+kxWGx1BicXQReCgxmMoqbH4+hgBLCGjoqSOKmpYgxJ0oqi59wtJI8zmWVy0hySSST8yTk9chr2/vdxup73cLuWe9lbU8kjs7ufVnYlmPzJJ6dfdeknXvfuvde9+691737r3Xvfuvde9+691xYXBX8lSOf8AWt/xPv3XuvnOf8KzusRs3+YD0tvukpljx3bHxopy9THAI1nz2ye0uyKnLCR1hjSSaOg3hQknW72YX0jTq0elMJqpHWrd79091737r3XvfuvddElVLG1lBZz9AqjlmP8AQAD37r3RiaH4ifK/J0sFfjfjH3xXUVSgmpqyj63z1RTVETEgSwzQQPFKhKkXBP09+61qX16l/wCybfLw8/7Kx8gef+/Ybi/+pffuta1/i69/smvy8/7xZ+QP9f8AmWG4v95/ybke/de1r/F1IpPhR8x8lMKeh+KPyFqpuSsUXWecjYgWuSZ4oY/+TvfqHr2tf4ujodB/yN/5m3yEy1HQ4L43bj60xU2RoKCq3T3PT5fauDo0r5ERqySTAYndeSNNRxEySkUzFVHAY8e/U60ZFHn1tkfAj/hLT8eOj8nguw/l3uOj+QXYODzGMytHsehjp8j0rCcXLT10cOWw+5dqY193RVlfHaWOtoAjU6+O9pHA30w8pJGnh1tZYrEYvB0FNi8Pj6HF46ijEFHj8bSU9BQ0cIJYQUlHSxxU9NCCxIRFCgkm3Pv3TXTj7917r3v3Xuve/de697917r3v3Xuve/de697917r3v3Xuve/de697917r3v3Xuve/de697917r3v3Xuve/de697917r3v3Xuve/de697917r3v3Xuve/de697917r3v3Xuve/de697917r3v3Xuve/de697917r//1d/j37r3Xvfuvde9+691737r3Xvfuvde9+691737r3Xvfuvde9+691737r3Xvfuvde9+691737r3Xvfuvde9+691737r3Xvfuvde9+691737r3Xvfuvde9+691737r3Xvfuvde9+691737r3XvfuvdY5eYpP+Wb/APQp9+69wz18u3/hQhg8nuP+bP3PisRTNVVlTt/aiBQSI4Vbd3YaNNMwVmWFNV2KqxsDwfp7Ndl2PcuYdztdq2qHXeSMBTgACaampUhR5kAmgNAejXb7K4v5orW1jLSsQPsqaVPy9fs4dEc6w6nxWxYzV1rUmbz8kkbDJNRqqUIRbRpjvP5pobyksZFMZJA9PpB95m8ie1m2cjxPcXUkV5vjsD43hgCMAcItWpga1bUNBrTHaD1l/wC0XJm2cuuL2cR3O6mRSJGUfpgeUerUQa51AqSaY7Qeh3gbUeAeQSAf1f48/wBfcgzU7qcKeXn1nZypcoAvGpIPTtC3ABaxJ+nNj/Xn6n/bey2VQC38JFf9Xz6yQ5ZulVa1wCOjq/Db4Rd3/NrfE20eo8fQwYrD12Jo95b5zn3w2zsuLKyTGKbKfw2iyFfUVX2dLPOkCxKkghKmVL3AK5o5r2vlW1FzfuTKwYogpqalOFSBSpAJr58Olfuh7+8iexPLy7xzjcyNdSxyNb2sWkz3JjAqE8RkULqZFLFiV1AhW4dbvvwP/lt9I/Bna1XTbfpcfvrsjMVEFVnO085tigpdxO1NSGlSi2/JVVWeyu28O+p5HpYa94nlcsRfk4qc2867pzVch56xWYGIlYleNe7Ch2+ZWtOuIn3iPvVe4H3g92hbc55du5UgUrFt8Vw7Q5bUXmCrFHPKMASNCGCgAGmOrGFAAsAosfx9L/7YewaD8+sX8+fHrlYf097691737r3Xvfuvde9+691737r3Xvfuvde9+69176e/de61EP8AhWz8Ya/sD44dE/J3DxUay9Abj3btXcJNO/31Rt7s/IbCnjaKqjjKx02MXZ9bI6SGx83pA9V/eXT0JoSOvn8WYfUEc/kfT/A8+9dKeve/de697917rplV0eNwTHIrRyKDbVG4Kuv5HqUke/db6+j9/wAJpv5lcvyu+Nue+N/a2ar5+7/jjV4HG0WTzOVfKVnYnXG48NXVWK3FFVVLPkzkcXndt5hK1KkyBYnpzHNIWkSLfHpJKmk1HA9bO5Av+phf6C/v3TXXtP8Ai3+3/wCNe/de66KXBGpxcWuGII/1rfn37r3XQiUWvd7G48h12P8Ahqv7917rJb37r3Xvfuvde9+691737r3Xvfuvde9+691737r3Xvfuvde9+691737r3Xvfuvde9+691737r3Xvfuvde9+691737r3Xvfuvde9+691737r3Xvfuvde9+691737r3Xvfuvde9+691737r3Xvfuvde9+691737r3Xvfuvde9+691//9bf49+691737r3Xvfuvde9+691737r3Xvfuvde9+691737r3Xvfuvde9+691737r3Xvfuvde9+691737r3Xvfuvde9+691737r3Xvfuvde9+691737r3Xvfuvde9+691737r3XvfuvddXH0/wB9x9ffuvdeJA/2HN/fv8PXuqwf5iv80bpX+XvhMNj92YbOb/7U3nhMvlNk9bbeq8Vj6isgoZFx9Plc/lcpKxxOBly0oieenpMhMiRTMIGKIkki8g+228c+TSyW0qW+2QuqySuC3HJCqvxNpyAWUEkDUKkgRbBy3d767GN1jtlIDMamnnQAcTTyJA4Z9NAH5G987y+TndW9+9+wqfER7231LTDMPhKOGmpkpqKWvqKSjhdIadpoIJ8pUMt1QAytZRyTmpyryjsnJ1kbHZrRU1sC70GuQgUBZqVIA4Ak0qfU9TXte02e0W5t7OMAN8RxVvLJ4n5V4Z9egZi4IuSovYWHFz/sePYimFRUUrTqWOTr0K4UmlGH+odPcDeksxsiAuW+iokYLFnAvfTyT/h7LZV1EEcTj9vl1ljyhfVVQa1LKOrxv5aP8nbsb5mQ0XbXZWbj6x6HxmexCeGox2Rrd3dn0Focjk6faz0VTj8fhsU1Faleulq5Jo5qjUKVhEPJCnuB7nWHK7vtdlCZ94ZG4EBIjwGuoJY1zp0gECmrOCP3b+9Zs/tLDLy/sFidw5ykieh1IsNq2VQyhgzO4bvEYQKQtPEGo6d3TqXp3rbo7ZmO6/6r2ZtzY208axlp8NtnCYrB0b1Bjihatq6fE0dDT1NfJDBHG0zJ5DHGik2UWxO3Dcb7dbp7zcLp5rhvNmLGnpUkmnE0+Z65Uc083cx867xcb9zTvFxfbrIKGSaR5WAqTpUyMxC1JOkGlSTxJ6E3j6j8/n/H+vtDT0PQc68Bb3vr3Xfv3Xuve/de697917r3v3Xuve/de697917r3v3XuvfT37r3Rb/lz8edv/Kv42d2fH3ca45aTtjrDfmxKLIZSkFdTYXM7q2jm9uYjPGDxySl8NV5YTgxaZRo9DBrH37ramhB6+N5vHZu5+vN1bi2JvOkNDu7aOVlwW46S7MtNlYIYKmVE8iRvpENXGeVU/4e9dLgagHpOe/de697917r3v3Xuhh6E727P+NPb2wO7+oNz5Pam/evNy4XcWGyGPyFfj4K9cTl8fl5cBnP4fPBJkNuZuTGxw1tLJ5IJ6cskkbqSvv3WmUMKHr6lP8AKm/m2dK/zQ+rc9uTZ2By3WvZ2wcnQ4fsfqjcOTxmYyOGlyeMbKYzOYbK4lKeLK7ZySwVcEU89Lj6g1FBUA04VUd99I2QoaHq2pTe/wDT8f63++Hv3Veu7/77+nv3Xuu/fuvde9+691737r3Xvfuvde9+691737r3Xvfuvde9+691737r3Xvfuvde9+691737r3Xvfuvde9+691737r3Xvfuvde9+691737r3Xvfuvde9+691737r3Xvfuvde9+691737r3Xvfuvde9+691737r3Xvfuvde9+691737r3Xvfuvdf/19/j37r3Xvfuvde9+691737r3Xvfuvde9+691737r3Xvfuvde9+691737r3Xvfuvde9+691737r3Xvfuvde9+691737r3Xvfuvde9+691737r3Xvfuvde9+69176e/de6jVVZS0UYmq5kgjLpGHe4UySHTGvAPLH6e/de6kAg/T37r3Xm+n1t/vvz/h7917qmT+bN/NV258BttYvrrauDrd19/8AZ+ztxZXZdNHWY+jwuyaCN5cDjt57oepSrraqCLNySS01JT0z/dDHTo81P6GaWva/21n53unvbqcRbJbyoJDQlpD8RjShAHbQMxONakBsjoWcsctPvkpnkcLYxuA3GrHiVXy4cSTiowetBreW9t5diblyu8t/7q3DvXd2cmFRmdzbqzGSzubycq6tBq8jlauvr5I4y7FEeZwrO5HLH3m7a2dpY28VnY2scNqnwoihVH2BQAK/IeXU3xRRwIsUEapEOCqAAPsAoOkubA2/pc3tc25/Jsefar5dO/PrJEjNLEqKzSTSxU0EaAGSapnfxwQRAkAzTyMFUEgE/Uj3WQ9pOAAKn7BxPR5sN2La7Gr4SQfspx/2etuD+Vl/IzloajB9+/MzFYarnpclicvsjoqtxUuXp6Y450yDZHscZumiwlZLLko4olxqUddB4YJL1I87ImKvuR7wrIs2x8qSOFKkSXANK1xSLSdQoKnVVTUjtxUlnPXv1dQWM3L/ACbcSRs6sst0G0nuGnTCVOoUFTr1KakdvbU7X1PTw00SQ08MUEUQ0RxQxJDEickLHHGAiKCTwOOfeNJ1E1LVJ49YsMzuxZ2Jc8SfP7fn1mJA+pt9Pe+qnGT11rX+v+8H/inv3Xuva1/r/vB/4p7917r2tf6/7wf+Ke/de69rX+v+8H/inv3Xuva1/r/vB/4p7917r2tf6/7wf+Ke/de69rX+v+8H/inv3Xuva1/r/vB/4p7917r2tf6/7wf+Ke/de66LKfz/ALx9P8f9h7917rzFSDc/4X5uCfoR/j7917rQP/4VW/y/Kjrzt3YHzl62wuNg2N2Zg6nZHdNLjqAUtTj+yMXuqSt2/u6ungp4qOuG7cXvf7R2crUouE5aVTGsfulEL/hPWn7710/117917rv37r3XE35INv6WHIH5A/oPfuvdLnrnsvsPqDe23uyOq98br6737tXI0WUwG7Nm7gzG2s3QVePrIK+lUZPB12OyL0RqqZGlgEypKBY/Xj3XiARQ9bp3wO/4Vj7agxWM2B89urN2xZ98jR00Hd/VlXtSv2w9FVrBTTVG79n5+bZNTt5aCqDTtLTV2Xd6d29KtEqy76TtD/Dw626+gPlV0B8ottPu3oTs3bnZeAieKOau27NPKsEs0TVEUUyzQQqkjwjVYFhb8+/dNFSOI6MEHuLlWX/gwH/EE+/dV671r/X/AHg/8U9+6917Wv8AX/eD/wAU9+6917Wv9f8AeD/xT37r3Xta/wBf94P/ABT37r3Xta/1/wB4P/FPfuvde1L/AF/3v37r3XYYHgH37r3Xfv3Xuve/de697917r3v3Xuve/de697917r3v3Xuve/de697917r3v3Xuve/de697917r3v3Xuve/de697917r3v3Xuve/de697917r3v3Xuve/de697917r3v3Xuve/de6//0N/j37r3Xvfuvde9+691737r3Xvfuvde9+691737r3Xvfuvde9+691737r3Xvfuvde9+691737r3Xvfuvde9+691737r3Xvfuvde9+691737r3Xvfuvde9+691jlkSKN5JGCRopZ2P0CKCzE/wBOB7917qmb+Zr/ADX/AIsfD/rTB/x7tbbs+9s3vPbkeP2nRisq8xNgqLLU8O58zHTrjpozT4SCuheTkGzce/dbAr1Y1078pvj737hqXcXT3ae1t/4TIvCaHI4GoqZoKj7gAQaDPSwNqkKmwI9+610Gnzr+YWxvhT8f95dvbtqFmy1PiMxRbC2+Hkil3Pvg4yoO3MIJkoq9aaCszT0sEszRSLGs4YqwBBFfJfKt7zfvtrtVsKRFlMr/AMEde9qVFaLqIFRWnRtsu1Tbvfw2sfwVGs+i1yeIzSpHXzbe3O3OxO9uwtx9qdrbin3Vv3dlQlXnszNFFTrPMjStHFBTwARw08TSuyqL2aRueeOgu1bVYbJt9vte2QCOxiwqj8sn58P2dZA2lpb2UEdtbJphUYHQb/Qn68cj+v8AsD+Tx7MelPTpgsJldzZ3CbawOPqMtn9x5jG4HA4mjVXrMrmcvWwY/GY6kRmRWqa2tqY4kBIGpxz7amliggmuLhgsESl2Y8AqipJ+QAr1R5FjieSRqIoJPoABUk/YOt17+Uz/ACWsF8dqSi7z+UuCx+4+/IdwYvNbE2+NX8L6ngxUdNWIVqqPMVVNuHO1GcRZvuGgpfCtJENDA8Yf+6Hu5Nvzvs3LUzR7HoKyN5zaqjgQCqhcUq1dRyKdRDzRzhLuDNZ7bIUsdJVj/vyuD5CgpimeJz1sZqBY2v6vrf6+4Dz59ADhUddO2kAni5ANuSTfhR/Ukn37r3Wnt/PP/wCFBGQ6Hyn+ywfBXeFHN2xFT5aLtjtyjInh6srPuIaHH7XxGKyOCkhy+46yiNTVtUpWU/2IFO3jl8np90/HFXubrWI/4fe/m2f95o78/wDPVhf+vHvVenPCT069/wAPvfzbP+80t+f+erC/9ePfut+Enp17/h97+bZ/3mlvz/z1YX/rx7917wk9Ovf8PvfzbP8AvNLfn/nqwv8A149+694SenXv+H3v5tn/AHmlvz/z1YX/AK8e/de8JPTr3/D7382z/vNLfn/nqwv/AF49+694SenXv+H3v5tn/eaW/P8Az1YX/rx7917wk9Ovf8PvfzbP+80t+f8Anqwv/Xj37r3hJ6de/wCH3v5tn/eaW/P/AD1YX/rx7917wk9Ovf8AD7382z/vNLfn/nqwv/Xj37r3hJ6dc4f56383Kslgoaf5o78NXX1FPj6QDE4W7VddKKemUDwgXaZh+QPfq9a8NB5dfTx7V6A2n8lPjvurojvrFR7p2/2JsbObQ3fBMXjlI3BiMrhKjI0klPNE0VfR0mTdoWVhaSx976SgkUI49fJ//mE/AfuT+XR8gq3ojuCE1Zr8U+6uut4JDBSUvYOyBka7DjcdLQwZLL/w948tiqmKWB6iVkCo2o67DXSxGDCo6Iz/ALe1hb+lufp791br3v3Xuve/de697917rsMVIKmzDkW+pt+B+PfuvdPO0tx53YuaTcWz8tXbczsUkUq5XEyrT1glgfyQuJHjmUNE/I9P19+60QDxHVhWx/5wH8y7raigxmy/mD2nicfT1BqIaSefE10KSaIU5WTHRs66YFBF/futeGn8PQnf8PvfzbP+80d+f+enC/8AXj37rXhJ6de/4fe/m2f95pb8/wDPVhf+vHv3XvCT069/w+9/Ns/7zS35/wCerC/9ePfuveEnp17/AIfe/m2f95pb8/8APVhf+vHv3XvCT069/wAPvfzbP+80t+f+erC/9ePfuveEnp10f57/APNsuB/s6W++b/8ALqwv4/p+x9ffq9a8JPTrYa/4TlfzCfn58zflj2ZhvkL8iN09pdabF66w9dUbdy+PxtNSx5vc0W/BjavzUojl8scm2LgaWB0/j876blRVAI63arcg/wBL/wC8+/dMdd+/de697917r3v3Xuve/de697917r3v3Xuve/de697917r3v3Xuve/de697917r3v3Xuve/de697917r3v3Xuve/de697917r3v3Xuve/de697917r3v3Xuve/de6//0d/j37r3Xvfuvde9+691737r3Xvfuvde9+691737r3Xvfuvde9+691737r3Xvfuvde9+691737r3Xvfuvde9+691737r3Xvfuvde9+691737r3Xv8PfuvdRmqoFqVpTKonaLzCH+0YruA4FrWuh/23v3Xus5tYqebqRY/nixF/6+/de6Id8+PiH1V8suoMfsbsTa2NzlHj967UztF95BNL4Zcdk0q5Y08VbRlVqWhjDHUeFHB9+6srFTUHo6WH23gsBSpRYfG01DSwP/AJPTwKyxxcLZE1MxF9I9+PHqta5OcdaKv8/v5ax/IH5ZYXqnbOUeo2J8eNt5HbklOj+Smq+wtx5+prdzZFddJTSArgsJglQapVBQ2NyR7zX9j+Vm2LleXc7mKl7fyBq+kaKAg4n8TSenU18jbW1htjXMq0nncEf6QAaRx9S3VEN/z9f9Y3uBfgGwt9fc1euM9DUAn8unnbe38zuzcOB2pt3HVOY3JufM4zb238PRhWq8vnMzW0+NxeMpVZkQ1FbXVUcS3IGpx7ZnmS0gmurhwlvEpZ2PBVUVLH5AAn8uqSSJDG8srARqCST5Adb1H8o3+UXhPh9t8du95Y/Hbi+R+Zq4Z6CWJGFJ1jhYaKJG29ip6fL11Ll6yrybyzz1nipi6rCniHj1Nhf7pe6c3Nk37p2V2j5eQZ9Zmrh2BUFQBQBan8RrnqGOauaW3aX6WxYrt4Gf6ZrxOBQU8s+ec9XzqAAABYD6e4Tp5nj0CeumbTz9f99+P6n3vr3+HrTw/n6fz5KjpJ8l8PPhvuWmqO0cphNx4nujs6jlv/ouhyR/gdPtTD46uwM0OT3Nk8a9bUmqSsp/4fpp28c3kAXXTscerJ4daDVlAGgFU+oB5JP5Ytxdjxfj8e/dKuHXrW4tb/D37r3Xvfuvde9+691737r3Xvfuvde9+691737r3Xvfuvde9+6914m1v9e3+39+691YJ/K0+MGb+XHz2+MvU+Nx81fhabtzrvfe+2i02othbL33tnJ7tna9TSs18F9wQFcN6DYE+/dUkainr69sa2DNyfI2o34tcAf1PPHvfSPqtv8Ambfy0uk/5lXSNT192ZSfw3fO1sLu6Xp3saGGeryHXe7c9jaT7fJpjosrh48tjHzWFxs9XSSVEK1CUYTyIGLD3VlYrShx18s35Z/EDv74R9szdJ/I7Zj7K31/CRuDGQx1dNX4zcW3Wrq/GJuHBVtK7x1GOfI4upgJNiHhPFj710sVgwqOiyf61xbn+vHNh+Prb37rfXvfuvde9+691737r3Xfv3Xuuvfuvde9+691737r3Xvfuvde9+691737r3XY5IA+rEAD+pPAH+8+/db638P+Eifx0yW0Pjl8lvkfuDGCB+5u09l7U2PVTAeap2b1vs2pzByEGioa1NXZHtWojAaNGDU7HU17LvpLM1Tp8utwX37pnr3v3Xuve/de697917r3v3Xuve/de697917r3v3Xuve/de697917r3v3Xuve/de697917r3v3Xuve/de697917r3v3Xuve/de697917r3v3Xuve/de697917r3v3Xuv/0t/j37r3Xvfuvde9+691737r3Xvfuvde9+691737r3Xvfuvde9+691737r3Xvfuvde9+691737r3Xvfuvde9+691737r3Xvfuvde9+691737r3XRF/yR9P8Aff4e/de6IrnPjH2lkPmXt75BUXd+6aHrrF9YRbPrOs4qOE4qqzEW4N0ZQ1rVRyqyiGWlzMMJT7cgCMnVzYe6tUUpTo8k1TBT+PzSBPNKkMQN7vLIwREX/FmYD37qvXpYYalAsyK6hkex5AZCSh/HKn37r3SG7S3lT9c9adhdg1en7PYexN3b1qtfCrBtXAZDOS6iEkIBjoWv6W/1j9PazbbQ7huFhYIMzzJGPtdgo/w9PW0LT3MEA4u4X9pA/wAvXypstuPK7wyeQ3Zm53qczuSrfL5WoIAM1bMkUDu2kKtzHTL9APp76ZQwR2sS2sC/oxCi/Z1kxHEsMYiQURcD7Oo1NTT1lTR0VJFJPVV1ZS4+ipohqmq66umSno6SBSQGnqZ2CILi7EC497d1VGcsAigkn5DJP2Dz63XBY8FFT8h1vJ/ybP5SqfFTb0veXyF29TTfInO1tPJt3GSeodWbbgoIxLiIaimylVS5PK5PLTSyTzmGnISCAaCBf3hp7te545muf3NsM5GwIDqP+/nJw2QCFVQABU8TnqGubeaP3nJ9Ft8v+ILxP8Z9aUwAOGTxPWwZoFgAbAe4L48cnoC9dk6Rzz/jb+nv3WutVj+f7/PAk+IGKl+J/wAXM5BVfI3eG2s4d+7zppvT0rg8jNUbaoRHR1mDrKXJ7syhjyNRThaqmNE2Pib93zjx+6ejj1HUVx187aSV5pJJpXeSWaQyyyytqllkYKpkkaw1SFUAJ/w966VfPrH/AI/n37r3Xv8AX9+691737r3Xvfuvde9+691737r3Xvfuvde9+691737r3XvfuvddFgqPI90jRS7va+lQCS1vyQFPHv3Xvt6+jh/wmv8A5W+6fh90tvv5C9+7Z/gveveGYwqbcwdbEhyOwurtv4CqipMZUVFNlKqnlyWb3BujLCoBghdI6aE+oMNO+ksr6jQcB1tD+/dNde+vv3XuiE/Pf+XJ8av5ifWabA792kuSyOCx24Iuu960c1XBn+vM1naamX+N4c01XRipEGRxtFUyU7OgnakRfIlyffurKxUgg9fNY/mO/wAn35Wfy2s01d2Til3103WrUy7d7t2/RUeOwdaKaoWnqKTP4OLO52fbOTjDxSiE1VVqimB1C3OqdKkkDV8j1VORYAni4v8A7Dnkf7b37q/XX9f9b/b/AF49+6914G/+t/X37r3Xvfuvde9+691737r3Xvfuvde9+691737r3Xub2Av/AL76D+p9+690b74PfC3t755fIXZHQXUWHqKupzmb2/8A333DGkMlNsXYGQzENFuPeFZDNW48VP8AB8OlXVRwLNG0/wBo6h0Pq9+HVXbSK+fX12Oh+kOvPjj1PszpbqnCjbuwdhYqPDbcxPmlqDTUMcjyKHmmd5HkYyEEkk2HvfSMmpJ6GD37rXXvfuvde9+691737r3Xvfuvde9+691737r3Xvfuvde9+691737r3Xvfuvde9+691737r3Xvfuvde9+691737r3Xvfuvde9+691737r3Xvfuvde9+691737r3Xvfuvdf/9Pf49+691737r3Xvfuvde9+691737r3Xvfuvde9+691737r3Xvfuvde9+691737r3Xvfuvde9+691737r3Xvfuvde9+691737r3Xvfuvde9+691737r3XG45uQLH+o/3n+nv3Xuq5fnV8qtm/Hnfnw22jubK0eOr+9/kp1/1/gYqvLUuM+6kq+xOtcDV1EUdSC1ZBRPvCAyBbBQwBZdQPv3VgtQTXh1YtE4ZFBYBrXK3AYWuSCCbjj37qvRR/n7VT0vwg+Xc1NrMw+NHeqjxH1gN1Zu25Fla+m1/p7FXI4Dc5cq8P8Ako23/V5OjXYxq3nahw/xiP8A4+vXzEYgEhiUH0KgF2sFHLG5b6KB+SffRg11+v8AsdZFk1NaAdbUn8hT+WNR7zkpPmx3hh89QxbX3Xij0NtTJ4g4+gzL4umx+4peyaqXJU619VDS5mWiixxpRHAJ6Gp1SzE6IMaPez3He0VuT9lmRjJE31LqaldRK+EKGlSoYtXNGXA4mNed+YmiDbNZuKsp8Qg1pXGjHA0rWuaEdbgqiw/P1/P5/wAf9f3inwpUnqKfsHXMmwJ/pz7317qgf+eN/OQw/wDLV62xHX/XNHt3d3yb7g2ruio2RgMjmpkTYGGQpt6m7H3HisNIMz4KXNVskuNhkkoIsjLiquKOoBikaL3Tkaa65x18xXOZrMblzGU3DuDKVmczubrXyGZzORkjevytfLHHFLW1jRRRRtO8UKKSqqLKOPeulfy8umz37r3XXv3Xuve/de697917r3v3Xuve/de697917r3v3Xuve/de679+6907YHb+f3XnsVtXauCzu6d1ZytpMdhdrbXxNfuDc2Zr62pjo6KjxOAxUFVlMjU1NXMsUaQxO7yOFAJIB914kDiadbt38mD/AITgZHCZnafyp+etBm8HuraO7tvbl6v+PIxtLS0NNNgJ6DP0m6eyKzN0pz65Sm3BTRR02Oho8XJBFTSu00v3KCm30nklB7QOt29FCCw55uSfqT/U/wCw9+6Y65e/de697917rx5/4n/W9+690w7l2zg94YLMbZ3JjqfL4DcGMr8NmsVVqzUmSxWTpJqGvoapEZHaGqpKh43sykqx9+691qQ/Or/hKN0b2VXZfsX4Z9p57o3c0lBX1DdQ7g2zt3dnWWZy6iWfHR4WupMj1/mNoCYqtNNJU1GXGkpIFHjZJfdPLMwwRjrTm+VH8t35r/DHMy43vf49dl4TDpFU1EO/MFszeG4uuJ6ejfRWSnfMO3aTb0ApgVdwZ30xMHJAI966fDqeDDojcMkNQD9tPDVBeG+2mjqAh+oDGItp4/r791brIQQbEEH+hFj7917r3v3Xuuvfuvdd+/de669+691yAJF7EgfqNjZR9SWP9lQPqT+PfuvdHZ+EX8vr5PfP/sGg2Z8fOudwZ/ARZ3C4jePah29nKzrTYUOXqY1apz+6KOk/u+tfSY4S1goJ6+ilqIYuJEDa19TqrMF49fTV/lafyrelf5WnUO5+vus9x7j7C3f2JncbuXszszdlDiMXltz5PD4r+EYqiosThohDiMBjIZaiWCnmqK6dZ62oY1DKyJHvpK7lznh1aKL8f7z+P9459+6p137917r3v3Xuve/de697917r3v3Xuve/de697917r3v3Xuve/de697917r3v3Xuve/de697917r3v3Xuve/de697917r3v3Xuve/de697917r3v3Xuve/de697917r3v3Xuv/9Tf49+691737r3Xvfuvde9+691737r3Xvfuvde9+691737r3Xvfuvde9+691737r3Xvfuvde9+691737r3Xvfuvde9+691737r3Xvfuvde9+691xJI/2PBN7W/2/v3Xugz3jsDN7ooMpS47sve+0JchRVFLBWbekxS1GMkmhmjSsohW42rQVNO0oddYZSyLx9b+691pR/zxP5SHyU3p8kPhZn1+V/b3yDx/cXaO3+hKfJ9rbcwNXWdCVe8ey9i4Wgz+3KvbRwdLkcHUzb2NZNS1cUBWTEn/ACvTMfD7j0ojcUao62rvi/8AD3t3ofbdNgd9/N35Bd/1FIkcKZPflHs3GHxxxlT46fE4lgmp3LcyN6vfuI6Z1Bhw6HH5Ide1W+PjH8gutxW5DL1u9uk+2drUk1WkdTVGq3DsHOYalWOGCOATMJqwFVHLMbX59nXLd2LDmLYb8sAsN7A59KLKrGv7Ole3SiDcLCc8EmQn8mB6+fz/ACtfgjnPnh39t/aORhy+N6t2RV7b3J3Bm6DFtUiLDS5Opq02g1VVocdjq3d+KwORgRp/I6aQywy2KnOn3J52h5I2O4uYih3OYOkCE8WpTxMGpEZZSaYPqOp05k3uLZbCSZCDcSBhGCfOnxep0kg/5R19H7D4rH4THU2LxdJFQ4+ijENJRw38VNCCSIowSToBJPP5PvACWWWaRpJXLOTUk8T8+oAdmdizGrHpz9t9V6a82mXkxOUXAVFBSZs42uGGqsrRT5LF0+WNLKMbPksfS5LD1NfQQ1mhpoY6ulkljDKs0RIdfdbFK5604u8v+EqnaXyO7S3f3P3B/MkyO6ewt85EZDcGXb4zU0UMkq6vFTUkM3yFq56eiheR3VJJpnDyuS5BAX3TomK4UdBMP+Eck3/ewGt/2Hxqoj/834e/U634zfw9d/8AQHJN/wB7Aq3/ANJqov8A7f8A79Tr3jn+Hr3/AEByTf8AewKt/wDSaqL/AO3/AO/U6945/h69/wBAck3/AHsCrf8A0mqi/wDt/wDv1OveOf4evf8AQHJN/wB7Aq3/ANJqov8A7f8A79Tr3jn+Hr3/AEByTf8AewKt/wDSaqL/AO3/AO/U6945/h69/wBAck3/AHsCrf8A0mqi/wDt/wDv1OveOf4evf8AQHJN/wB7Aq3/ANJqov8A7f8A79Tr3jn+Hr3/AEByTf8AewKt/wDSaqL/AO3/AO/U6945/h69/wBAck3/AHsCrf8A0mmi/wDt/wDv1OveOf4R0OnW/wDwj2+N2Ir6Sp7X+ZPf2/KaGpppajF7N2T1/wBeUNZBFIHqaSV84nZz6KpRoLA+lSeD791ozMeA62IPhd/LX+I3wIwNdiPjj1jSbUrstNFNnN0VclNVbnzr09K1JTfxKtoKLFY6QQQySadFIhBka5Nxb3TZZm4no+aAAWAIt+D9f9f37qvXL37r3Xvfuvde9+691737r3Xvfuvde4+n9ffuvdR5qeKeKSGRbxTI8csRNllSRdDo4PJDLwfpwffuvdVXfKD+S1/Ly+Wcj5Ds7ofD0u5ZIqmE7w2o9NjdyKtS2vUtTmqLcNDrp5bvGft7BmNwffurBmGa9U9djf8ACQn4W5qrkqOuPk18oeu4pI5NGPq4uq930EcxLGN0jo9ibQm0ILBkMl2H0ZffunBMw4jotma/4Rw4QTx/3c/mA7qWlIcvHn/jvh8jVowK6NNTR9zYSJrC+r9n62+nv1Ot+Of4R0z/APQHJN/3sCrf/SaqL/7f/v1OveOf4evD/hHJNyD/ADAq21r/APZNNETx9Lf8Z/8AqPfqde8c/wAPTliP+EcOLNWn8f8A5gW4/wCHqQ0y4T46Ymhr5ADd0hqqvuzLwwuyiwYwOAebH6e/U6945/hHVgfxv/4Sr/y+umcxQ7k7L3Z3L8kctjsrj8lBjuyZdgYvaQWgljqDSPg8NsoZd6ermj/dtk1YpwpU+r37qplY162Qtj9f7Q6225j9o7HwNBtrbWJTRjcLjI5I6GjSwAEKSSyuOFH1Y/Qe/dNVJ4npYj+n4H0t9Lf8V9+69137917r3v3Xuve/de697917r3v3Xuve/de697917r3v3Xuve/de697917r3v3Xuve/de697917r3v3Xuve/de697917r3v3Xuve/de697917r3v3Xuve/de697917r3v3Xuve/de6//1d/j37r3Xvfuvde9+691737r3Xvfuvde9+691737r3Xvfuvde9+691737r3Xvfuvde9+691737r3Xvfuvde9+691737r3Xvfuvde9+691737r3Xvfuvde9+690E3Z/WFD2NV9f1dZK0bbG33tzelMgjR0lqcBm8VmIg2plKHyYxbEci54Pv3XuHQrRjSoA/F/wDYf4e/de68RqA5IsQf9e34P+v71Wpz17ok/wAK/g11l8IsJ23huusllcyO3e0Z+zc1XZmloKaropf7s7f25Qbdpf4ekUUuMxYw0tRE7r5TLXSg8BfYx5v5y3HnCbapb+JEFpbCFQtSD3u5c1PE6gCBiijo33ferjeZLV500+FHoA9ckk58zWn5dHaFvx7B4wOijrv37r3XVhwLfT6e/de67+nA9+691737r3Xvfuvde9+691737r3Xvfuvde9+691737r3Xvfuvde9+691737r3Xrf7zx/vv8Ab+/de697917r304Hv3Xuve/de697917r3v3Xuve/de697917r3v3Xuvf4+/de697917r304Hv3Xuve/de697917r3v3Xuve/de699ffuvde9+691737r3Xvfuvde9+691737r3Xvfuvde9+691737r3Xvfuvde9+691737r3Xvfuvde9+691737r3Xvfuvde9+691737r3Xvfuvde9+691737r3Xvfuvde9+691737r3Xvfuvde9+691737r3X//1t/j37r3Xvfuvde9+691737r3Xvfuvde9+691737r3Xvfuvde9+691737r3Xvfuvde9+691737r3Xvfuvde9+691737r3Xvfuvde9+691737r3Xvfuvde9+6910Re314/wB9z7917ru3+93/ANf37r3XEg24PI/3n/X96696jy67t/vv+K/4e9/4OvdeAt/T37r2fPj137917r3v3Xuve/de697917r3v3Xuve/de697917r3v3Xuve/de697917r3v3Xuve/de697917r3v3Xuve/de697917r3v3Xuve/de697917r3v3Xuve/de697917r3v3Xuve/de697917r3v3Xuve/de697917r3v3Xuve/de697917r3v3Xuve/de697917r3v3Xuve/de697917r3v3Xuve/de697917r3v3Xuve/de697917r3v3Xuve/de697917r3v3Xuve/de697917r3v3Xuve/de697917r3v3Xuve/de697917r3v3Xuv/X3+Pfuvde9+691737r3Xvfuvde9+691737r3Xvfuvde9+691737r3Xvfuvde9+691737r3Xvfuvde9+691737r3Xvfuvde9+691737r3Xvfuvde9+691737r3Xvfuvde9+691737r3Xvfuvde9+691737r3Xvfuvde9+691737r3Xvfuvde9+691737r3Xvfuvde9+691737r3Xvfuvde9+691737r3Xvfuvde9+691737r3Xvfuvde9+691737r3Xvfuvde9+691737r3Xvfuvde9+691737r3Xvfuvde9+691737r3Xvfuvde9+691737r3Xvfuvde9+691737r3Xvfuvde9+691737r3Xvfuvde9+691737r3Xvfuvde9+691737r3Xvfuvde9+691737r3Xvfuvde9+691737r3Xvfuvde9+691737r3Xvfuvde9+691737r3Xvfuvdf/0N/j37r3Xvfuvde9+691737r3Xvfuvde9+691737r3Xvfuvde9+691737r3Xvfuvde9+691737r3Xvfuvde9+691737r3Xvfuvde9+691737r3Xvfuvde9+691737r3Xvfuvde9+691737r3Xvfuvde9+691737r3Xvfuvde9+691737r3Xvfuvde9+691737r3Xvfuvde9+691737r3Xvfuvde9+691737r3Xvfuvde9+691737r3Xvfuvde9+691737r3Xvfuvde9+691737r3Xvfuvde9+691737r3Xvfuvde9+691737r3Xvfuvde9+691737r3Xvfuvde9+691737r3Xvfuvde9+691737r3Xvfuvde9+691737r3Xvfuvde9+691737r3Xvfuvde9+691737r3Xvfuvde9+691737r3Xvfuvde9+691737r3Xvfuvde9+691737r3X/9Hf49+691737r3Xvfuvde9+691737r3Xvfuvde9+691737r3Xvfuvde9+691737r3Xvfuvde9+691737r3Xvfuvde9+691737r3Xvfuvde9+691737r3Xvfuvde9+691737r3Xvfuvde9+691737r3Xvfuvde9+691737r3Xvfuvde9+691737r3Xvfuvde9+691737r3Xvfuvde9+691737r3Xvfuvde9+691737r3Xvfuvde9+691737r3Xvfuvde9+691737r3Xvfuvde9+691737r3Xvfuvde9+691737r3Xvfuvde9+691737r3Xvfuvde9+691737r3Xvfuvde9+691737r3Xvfuvde9+691737r3Xvfuvde9+691737r3Xvfuvde9+691737r3Xvfuvde9+691737r3Xvfuvde9+691737r3Xvfuvde9+691737r3Xvfuvde9+691//S3+Pfuvde9+691737r3Xvfuvde9+691737r3Xvfuvde9+691737r3Xvfuvde9+691737r3Xvfuvde9+691737r3Xvfuvde9+691737r3Xvfuvde9+691737r3Xvfuvde9+691737r3Xvfuvde9+691737r3Xvfuvde9+691737r3Xvfuvde9+691737r3Xvfuvde9+691737r3Xvfuvde9+691737r3Xvfuvde9+691737r3Xvfuvde9+691737r3Xvfuvde9+691737r3Xvfuvde9+691737r3Xvfuvde9+691737r3Xvfuvde9+691737r3Xvfuvde9+691737r3Xvfuvde9+691737r3Xvfuvde9+691737r3Xvfuvde9+691737r3Xvfuvde9+691737r3Xvfuvde9+691737r3Xvfuvde9+691737r3Xvfuvde9+691737r3Xvfuvdf/09/j37r3Xvfuvde9+691737r3Xvfuvde9+691737r3Xvfuvde9+691737r3Xvfuvde9+691737r3Xvfuvde9+691737r3Xvfuvde9+691737r3Xvfuvde9+691737r3Xvfuvde9+691737r3Xvfuvde9+691737r3Xvfuvde9+691737r3Xvfuvde9+691737r3Xvfuvde9+691737r3Xvfuvde9+691737r3Xvfuvde9+691737r3Xvfuvde9+691737r3Xvfuvde9+691737r3Xvfuvde9+691737r3Xvfuvde9+691737r3Xvfuvde9+691737r3Xvfuvde9+691737r3Xvfuvde9+691737r3Xvfuvde9+691737r3Xvfuvde9+691737r3Xvfuvde9+691737r3Xvfuvde9+691737r3Xvfuvde9+691737r3Xvfuvde9+691737r3X/9Tf49+691737r3Xvfuvde9+691737r3Xvfuvde9+691737r3Xvfuvde9+691737r3Xvfuvde9+691737r3Xvfuvde9+691737r3Xvfuvde9+691737r3Xvfuvde9+691737r3Xvfuvde9+691737r3Xvfuvde9+691737r3Xvfuvde9+691737r3Xvfuvde9+691737r3Xvfuvde9+691737r3Xvfuvde9+691737r3Xvfuvde9+691737r3Xvfuvde9+691737r3Xvfuvde9+691737r3Xvfuvde9+691737r3Xvfuvde9+691737r3Xvfuvde9+691737r3Xvfuvde9+691737r3Xvfuvde9+691737r3Xvfuvde9+691737r3Xvfuvde9+691737r3Xvfuvde9+691737r3Xvfuvde9+691737r3Xvfuvde9+691737r3Xvfuvde9+691//V3+Pfuvde9+691737r3Xvfuvde9+691737r3Xvfuvde9+691737r3Xvfuvde9+691737r3Xvfuvde9+691737r3Xvfuvde9+691737r3Xvfuvde9+691737r3Xvfuvde9+691737r3Xvfuvde9+691737r3Xvfuvde9+691737r3Xvfuvde9+691737r3Xvfuvde9+691737r3Xvfuvde9+691737r3Xvfuvde9+691737r3Xvfuvde9+691737r3Xvfuvde9+691737r3Xvfuvde9+691737r3Xvfuvde9+691737r3Xvfuvde9+691737r3Xvfuvde9+691737r3Xvfuvde9+691737r3Xvfuvde9+691737r3Xvfuvde9+691737r3Xvfuvde9+691737r3Xvfuvde9+691737r3Xvfuvde9+691737r3Xvfuvde9+691737r3Xvfuvdf/1t/j37r3Xvfuvde9+691737r3Xvfuvde9+691737r3Xvfuvde9+691737r3Xvfuvde9+691737r3Xvfuvde9+691737r3Xvfuvde9+691737r3Xvfuvde9+691737r3Xvfuvde9+691737r3Xvfuvde9+691737r3Xvfuvde9+691737r3Xvfuvde9+691737r3Xvfuvde9+691737r3Xvfuvde9+691737r3Xvfuvde9+691737r3Xvfuvde9+691737r3Xvfuvde9+691737r3Xvfuvde9+691737r3Xvfuvde9+691737r3Xvfuvde9+691737r3Xvfuvde9+691737r3Xvfuvde9+691737r3Xvfuvde9+691737r3Xvfuvde9+691737r3Xvfuvde9+691737r3Xvfuvde9+691737r3Xvfuvde9+691737r3Xvfuvde9+691737r3X/9ff49+691737r3Xvfuvde9+691737r3Xvfuvde9+691737r3Xvfuvde9+691737r3Xvfuvde9+691737r3Xvfuvde9+691737r3Xvfuvde9+691737r3Xvfuvde9+691737r3Xvfuvde9+691737r3Xvfuvde9+691737r3Xvfuvde9+691737r3Xvfuvde9+691737r3Xvfuvde9+691737r3Xvfuvde9+691737r3Xvfuvde9+691737r3Xvfuvde9+691737r3Xvfuvde9+691737r3Xvfuvde9+691737r3Xvfuvde9+691737r3Xvfuvde9+691737r3Xvfuvde9+691737r3Xvfuvde9+691737r3Xvfuvde9+691737r3Xvfuvde9+691737r3Xvfuvde9+691737r3Xvfuvde9+691737r3Xvfuvde9+691737r3Xvfuvde9+691//Q3+Pfuvde9+691737r3Xvfuvde9+691737r3Xvfuvde9+691737r3Xvfuvde9+691737r3Xvfuvde9+691737r3Xvfuvde9+691737r3Xvfuvde9+691737r3Xvfuvde9+691737r3Xvfuvde9+691737r3Xvfuvde9+691737r3Xvfuvde9+691737r3Xvfuvde9+691737r3Xvfuvde9+691737r3Xvfuvde9+691737r3Xvfuvde9+691737r3Xvfuvde9+691737r3Xvfuvde9+691737r3Xvfuvde9+691737r3Xvfuvde9+691737r3Xvfuvde9+691737r3Xvfuvde9+691737r3Xvfuvde9+691737r3Xvfuvde9+691737r3Xvfuvde9+691737r3Xvfuvde9+691737r3Xvfuvde9+691737r3Xvfuvde9+691737r3Xvfuvdf/0d/j37r3Xvfuvde9+691737r3Xvfuvde9+691737r3Xvfuvde9+691737r3Xvfuvde9+691737r3Xvfuvde9+691737r3Xvfuvde9+691737r3Xvfuvde9+691737r3Xvfuvde9+691737r3Xvfuvde9+691737r3Xvfuvde9+691737r3Xvfuvde9+691737r3Xvfuvde9+691737r3Xvfuvde9+691737r3Xvfuvde9+691737r3Xvfuvde9+691737r3Xvfuvde9+691737r3Xvfuvde9+691737r3Xvfuvde9+691737r3Xvfuvde9+691737r3Xvfuvde9+691737r3Xvfuvde9+691737r3Xvfuvde9+691737r3Xvfuvde9+691737r3Xvfuvde9+691737r3Xvfuvde9+691737r3Xvfuvde9+691737r3Xvfuvde9+691737r3X/9Lf49+691737r3Xvfuvde9+691737r3Xvfuvde9+691737r3Xvfuvde9+691737r3Xvfuvde9+691737r3Xvfuvde9+691737r3Xvfuvde9+691737r3Xvfuvde9+691737r3Xvfuvde9+691737r3Xvfuvde9+691737r3Xvfuvde9+691737r3Xvfuvde9+691737r3Xvfuvde9+691737r3Xvfuvde9+691737r3Xvfuvde9+691737r3Xvfuvde9+691737r3Xvfuvde9+691737r3Xvfuvde9+691737r3Xvfuvde9+691737r3Xvfuvde9+691737r3Xvfuvde9+691737r3Xvfuvde9+691737r3Xvfuvde9+691737r3Xvfuvde9+691737r3Xvfuvde9+691737r3Xvfuvde9+691737r3Xvfuvde9+691737r3Xvfuvde9+691//T3+Pfuvde9+691737r3RZvll8sOr/AIZ9Tt3L27T7qqdoruXDbVMWzsTR5rMfxPOpWyUTCirsrh4PtQtA+t/NdeLKb8HnL/L9/wAy7h+7duMYuNDP3kqKLSuQDnPp1Lfst7L84e/POg5D5Iks03s2ktxW6kaKLw4SgfuSOU6u8UGmhzkdV47C/ny/CHsXfOzOvtv4nvNM9vvdm3dm4R8jsLb1Nj0y+58xR4TGtXVEW+aiSCjWsrkMrrG7KlyFYixGN37T80WVrc3k0lp4UUbO1JGJooLGn6YzQY6yf5k/u4fvB8rcu7/zPud7y6dt22ynupQl5MzmK3iaWTQptFBbSh0gsATQEjj1dP7jLrAXr3v3Xuve/de697917oJ+9O6di/HXqPfndnZdbVUOyOu8HJnc5LQU8dXkqhPuIKGhxmLpZp6WGqy2YylZBSUsTyxJJUTopdQSwMNq2y73ncbTa7FQbqZ9K1wPUknNAACSaHAOOhp7dcg8xe6PO/Lft/ynAknMG6XAhhDkqimhd5JGAYrHFGrySMFYqiMQpIoahP8AoIX+BX/On7//APReba/+z/3Iv+s9zZ/v2z/5yN/1r6zg/wCTYf3kf+U7ln/stn/7Y+re+i+6di/InqPYfdnWlbVV2yOxMFFncHLXwR0uRp0889FXYzKUsNRVw0uWw+TpJ6SriSWVY6iB1DsBqMdbrtl3s243e13ygXUL6WpkeoIOKgggg0GCMdYP+4nIPMXtdzvzJ7f82QJHzBtdwYZghLIxoHSSNiFLRyxskkbFVLI6kqCadCx7L+gX1737r3QX919ubV6F6n3/ANy74jy0u0et9t1+6twx4Kjhr8w+MxqB6hcdRVFXQwVNUQfSjTRg/wCqHtftm3XG7bhabbalfqJ3CLqNBU8KkA0H5HoX8gck7z7kc6ctch8vNCu97rdpbwmZikQkc0XWyq5VfUhWPy6p8/6CF/gV/wA6fv8A/wDReba/+z/3I3+s9zZ/v2z/AOcjf9a+s5f+TYf3kf8AlO5Z/wCy2f8A7Y+rttrbioN37Z27uzFLULi9z4LEbixq1caw1S0Gax9PkqNamJJJkiqBT1K61DsFa4BP19xfPC9vPNbyU8SNyppwqpoafs65+bxtdzsm7bpst4VN5Z3MsEmk1XXE7RtpJAJXUpoSBUeQ6ffbXRd1AymRp8RjMjlqsSGlxdBWZGpEKh5TT0VPJUzCJGZA0hjiOkEgE/ke7xoZHSNfiYgD88dKLO1kvru1soaeNNIqLXAq7BRU5xU56o7/AOghf4Ff86fv/wD9F5tr/wCz/wByl/rPc2f79s/+cjf9a+uhn/JsP7yP/Kdyz/2Wz/8AbH1b30Z3JtH5CdR7C7q2HHmIdn9i4KLcO34s/RQY7MpQTTz06DI0VNWZCCmqNdO11WaQWtz7jrddtuNn3G72y7Km5hfS2k1WvyJAr+wdYP8AuJyJvftjztzJyDzI8Db7tVyYJjCxeIuACdDsqFlowyVX7Oq6vkJ/Oi+IHxn7k3z0b2LjO4596dfV9Bjs9LtvZeCyeEeoyOGxudpzQV1VvHG1FRGKLKxBi0EZD6hYgXIy2f2z5j3zbbXdbKS2FtMCV1OwbDFcgIfMHz6yj9sfuEe+Pu1yJy97icrXexLsG5xu8Inupo5QElkhbWi2siqdcbUo5xQ+dOgZ/wCghf4Ff86fv/8A9F5tr/7P/Zn/AKz3Nn+/bP8A5yN/1r6Hv/JsP7yP/Kdyz/2Wz/8AbH17/oIX+BX/ADp+/wD/ANF5tr/7P/fv9Z7mz/ftn/zkb/rX17/k2H95H/lO5Z/7LZ/+2PoZvj3/ADoviB8mO5NjdG9dYzuODenYNfX47Ay7k2XgsZhEqMdhslnag19dS7xyVRTxmixUoUrBIS+kWANwWbx7Z8x7Htt1ut7JbG2hALaXYtlguAUHmR59AL3O+4R74+0vInMPuJzTd7E2wbZGjzCC6mklIeWOFdCNaxqx1yLWrjFT5U6s67H33hOruvN+dm7lSuk2511szdG+9wJjII6rJPhNo4Ou3BlUx9LNPSxVNc1Bj5BFG0savJYFlBuANZWkt/eWljBTxppVjWuBqdgoqc0FTnHWI3KvLm4c4cz8t8pbS0Y3XdL+3s4TISsYluZUhjLsAxVNbjUQrECpAPDql/8A6CF/gV/zp+//AP0Xm2v/ALP/AHJn+s9zZ/v2z/5yN/1r6z2/5Nh/eR/5TuWf+y2f/tj6uR6l7L273P1f1525tBMlFtXszZm299bcjzFNFRZZMJunE0uZxiZKjgqayGlrlpKxBLGksio9wGYc+423Cxm2y/vNuuCv1EErRtQ1GpSVNDQVFRjA6wS515T3TkLm/mfknfGiO87Rfz2c5iYvGZbeRopDGxVSyalOklVJFCQOHQhe0fQY6q3+U/8AN4+KfxA7gy3SPa+O7aqd5YXE4LM1ku0toYXMYU0m4cfHk6AQ11buzETvMKeUeRTCArcAn6+x5sPt1v8AzHt0e6be9uLZmZRrdg1VNDgIfP59Zhezv3Ifeb3x5GsvcHku62VdhuJpolFzcyxS6oHMb1RLaQAah2nWajyHQk/DL+ZL8efnXnd87e6TouxqSv6+xOHzOeO+NtYvBU70ebrKyhoxj5MfuLONUTCaifWrLGFWxBN7BDzLyVvHKkVrNujQlJmIXQxbKgE1qq+vQU9+fupe5/3dNu5d3P3AuNqe23OeWKH6SeSZg0Sq7aw8EWkUcUILVNcDo/8A7CPWNHXvfuvde9+691UT3z/Ox+F/x57e330rvFe2M1uvrrMDb+467ZuzsLl9vLmY6Olqchj6PIVu7sTUz1GIqalqSqDQII6uCRAWChjIm0+2HM28bdabnbfTrbzLqUO7BtNSASAhGQKjPAg9Zu+3H93/AO/fufyPy5z/ALEdlt9l3SDxoEurqWKfwizKjsiW0igSqokjo5rG6MaEkAavhr/M3+M/zm3bu/Y/TL77oNz7M27Tbrr8Xvvb2NwE2QwU2SixFTXYf+H7gzn3ceLr6umjqdfi8Zq4batR0lnMvI2+cq29tdbn4RglcoDGxajUqAaqtKgGnHgegB77/dH92vu77JsfMPPq7dJtF/dNbpJZzyTBJhGZFSXXDDpMiLI0dNVfDetKCthnsHdYw9e9+691737r3Xvfuvde9+691737r3Xvfuvde9+691737r3Xvfuvde9+691737r3Xvfuvde9+691737r3Xvfuvde9+691737r3Xvfuvde9+691737r3Xvfuvde9+691737r3Xvfuvde9+691737r3Xvfuvde9+691737r3Xvfuvde9+691737r3Xvfuvde9+691737r3Xvfuvde9+691737r3Xvfuvde9+691737r3Xvfuvde9+691737r3Xvfuvdf/U3+Pfuvde9+691737r3VIP/Cgb/t3/L/4mrrb/wBxtze5R9of+VvH/PLJ/hXroR/dmf8AiS6f+K/ff8et+tO74n/9lTfGr/xP/Tf/AL8XbnvI/mD/AJIO9/8APHN/1bbruf70/wDTnPdn/wAVndP+0Gfr6Z/vB/r5KOve/de697917r3v3XutZ/8A4UZ/JX+73W3UvxVwOQ0ZPsPKt2nv+ngl0zJs7alRPi9nY+sivaSh3Bu1qqqXi6z4FTf8Gb/ZnZPGvdw3+VP04V8KP/TvlyPmqUH2SddaP7rL2n/enNfO3vJuVtW02uEbfZsRg3VwokunU+Tw23hxn1S8PWoj7yI67d9beH/Ccz5K/wB4utO2fitnsh5Mn11lV7R2BTzy6pW2buyohxu78dRRX/bocBu5KerfjmbPNz+Bjt7zbJ4N9t+/xJ2TL4Un+nTKE/NkqPsj64h/3pvtP+6+bOSveTbbalpusP7vvGAx9VbKZLZ2Pm81trjHotmOtl73CPXJjr3v3XuiK/zN/wDt3/8ALb/xCu7P/ceP2KuR/wDlbuXv+epP8PWRX3R//EmPZP8A6X9t/wAePXzm/eZvX1M9fT56J/5kh03/AOIq68/95HEe8F91/wCSpuX/AD0Sf8fPXyHe4v8A08Hnv/pc3v8A2ky9Cr7QdA3pKb7/AOPH3l/4am4v/dRWe1Fp/uVbf81F/wAI6OeXf+Vg2L/nsh/6uL18tz3nf19hXX0T/wCVh/27z+J//iKqD/3Z5T3htz7/AMrjzB/z0H/AOvly++N/4k970f8AS5f/AKtx9ar381H4j/K3sP5/fJDeWwPjH8hd87Qzu5dtT4Tdez+l+yNzbbzEEHX20aKebFZ3C7arcXkIoaymkidoZXCyxsp9SkCe+QuYeX7PlHZba73yziuURtSPNGrD9RzlWYEYIORw67H/AHN/e32Z5X+7R7VbDzL7ucsbdvlvaXAlt7rdbGCeIte3LgSQyzpIhKsrAMoJVgRgg9VHdi9VdodP52Da/bXW+/ertzVOMp81Tbd7F2fuHZOdqMPV1FXSUuWgxG5cdjMhLjKmqoJ4o51jMTyQyKGJRgJDsr+w3GIz7fewzwBtJaN1daihIqpIqAQaccj16za5W5y5Q5526TeOSuatt3jaElaJp7G5gu4VlVVZozLA8iCRVdGZC2oK6kijCubrfqPtfuTMVm3eoesewu1NwY7GSZrIYLrfZe5N8Zigw8VVSUMuWrMZtjG5StpcZFW18ELTuixLLNGhbU6g6vdx2/bY1m3G+ht4WbSGkdUBNCaAsQCaAmnGgPp1TmrnbkzkSxg3Tnfm7a9m2yWURJNfXUFpE8pVnEayXEkaNIUR2CAliqs1KKSLa/5V3xH+VvXnz++N+8t//GP5C7G2hgty7lnze694dL9kbZ23h4J+vt3UUE2Vzua21RYvHxTVlTHEjTSoGlkVR6mAMe8+8w8v3nKO9W1pvlnLcui6USaNmP6iHCqxJwCcDh1hR98j3t9meaPu0e6uw8te7nLG475cWluIre13WxnnlK3ts5EcMU7yOQqsxCqSFUk4BPW4X8z/APsjz5X/APitXev/AL67dPvHHln/AJWPl/8A57oP+rq9cMvYX/p+fsx/4tm0f93C36+aN7zc6+svr6R/8vr/ALIW+Hv/AIrX0x/77/A+8K+b/wDlauY/+e6b/q43Xym/ea/8SK98/wDxbN0/7TZujf8AsOdQd1oifz5f+3i/YP8A4YHVX/vIUfvK72n/AOVMs/8AmtL/AMfPX0a/3cH/AIi1yx/0s9x/7SW6Op/wmq/5mp8pv/Ef9df+9HuL2GPe3/cDYf8AmtJ/x1eoB/vYf+VO9nf+lnff9WIOtuf3jx1xI697917oDfkx3fg/jd0B233puHwyUHWmyMzuOCjnk8SZfNxQfa7ZwCyal0T7i3JVUlDGbj9yoXkezXY9rl3vd9u2qGuueVVr6LxZv9qoLH5DqQ/aX2+3H3W9y+Sfbva9Qud23CKAsBUxRE6p5qeYggWSZv6KHr5oO6dzZzeu59x7y3NkJstuTduey+5twZSoINRks5nshUZXLZCcgAGasr6qSRv9qY+83YIIrWCG2gQLBGgVQPJVFAPyAp19Z2z7Tt+wbRtWxbRbLDtVlbRQQxr8McUKLHGg+SoqqPkOjj/y3/kmfin8yel+1q6uNDtBtxJsvsZmkKUx2BvYDAbgrKxQV80W3fuostGlwDUY+P2GudNk/f8Ay1ue3qlbnRrj9fETuUD/AE1Cn2MeoK+9X7UD3m9iOfeTLe38TfBam6scVb6y0/WhVfQz6WtifJJm6+jQCGAZSGVgCrAgggi4II4II94ZdfLIQQSCKEdd+/da697917r3v3Xuve/de697917orny6+Znx3+CvVVP3X8nN8VXX/XFVuzEbHgztJtTd28pX3LnaLLZDF0BxGysHuDMKlRSYOpYzGAQp47M4LKD7raqWNBx6rJ/6CUf5Of8A3lFnP/RA/Ib/AO1d71Xq/hP6de/6CUf5Of8A3lFnP/RA/Ib/AO1d79Xr3hP6dHA+Gv8ANg+Cnz931ujrb4rdwZDsXeOzNptvjcWLrOtO0NkpQ7aXMYzAtXrX752ftzH1bjKZinj8MMrzWfVo0qxG+tMjLkjqxn37qnXvfuvde9+691737r3Xvfuvda7e8/8AhT7/ACxNibw3XsjOz/IIZvZu5M7tXMij6lpKikGV29lKrEZEUtQd3xmem+7o30PpXUtjYXt79Xp3wXOcdX/7M3Xit97P2pvjBfcnCby21gt14Y1kIp6s4rcOLpcvjvuqcPIIKn7SsTWmptLXFza/v3TXDpS+/de697917r3v3Xuve/de697917r3v3Xuve/de697917r3v3Xuve/de697917r3v3Xuve/de697917qtz+Zz/ADOOkP5XnR+M7Y7Yxmb3pubembn2v1b1bteeko8/vrPUlKtflJHydestHgNs7fopI5MjkZI5/B54Y44Zp54on91ZELmg6Ih/Kp/4UJdG/wAy3tut+Pma6gz3x07rq8Nl9w7D27kt8UXZG1+wcbt2llyW4Mfht1U+1tj11JurFYaCXIPQzYsRSUNNPJHUMYWQ+6u8RQVrUdbCXv3TXXvfuvde9+691737r3Xvfuvde9+691737r3Xvfuvde9+691737r3Xvfuvde9+691737r3X//1d/j37r3Xvfuvde9+691SD/woG/7d/y/+Jq62/8Acbc3uUfaH/lbx/zyyf4V66Ef3Zn/AIkun/iv33/HrfrTu+J//ZU3xq/8T/03/wC/F257yP5g/wCSDvf/ADxzf9W267n+9P8A05z3Z/8AFZ3T/tBn6+mf7wf6+Sjr3v3Xuve/de66ZlRWZmCqoLMzEBVUC5ZibAAAcn37j1sAsQAKk9fOV/mPfJM/K35kd0dr0Nca7aA3FJszrllk10w2BskHAberKMXPii3CtJJlpEuQs+Qksbe8zuS9k/cHLW2beyUuNGuT18R+5gf9LUJ9ijr6nPure1A9mfYnkLky4t/D3z6UXV9ijfWXf60yt6mDUtsDiqQr01w/C/f0vwWrPm+BUf3Tpe7abq4Yj7a19tviXiqN+mpPBxS75mgwagG/3bMLWFxc8zWg5qXlbH1BtTLWv4q/2dPXRV/s6Vv798tJ94qD7vfb++n5fbcPE1f6OJAVs9P+/PpA92f+FgZzTp4/lu/JM/FP5ldL9q11eaDZ8m4o9k9js8hjpTsDe+nAZ+trQLGWHbpqosuiXGqfHx+2+ddk/f8Ay1udgqVudGuP18RO5QP9NQp9jHpF96z2oHvN7Ec+8m29t4m+C1N3Y4q31lp+tCqehn0tbE+STN19GYEMAykMrAFWBBBBFwQRwQR7wy6+WUggkEUI679+610RX+Zv/wBu/wD5bf8AiFd2f+48fsVcj/8AK3cvf89Sf4esivuj/wDiTHsn/wBL+2/48evnN+8zevqZ6+nz0T/zJDpv/wARV15/7yOI94L7r/yVNy/56JP+Pnr5DvcX/p4PPf8A0ub3/tJl6FX2g6BvSU33/wAePvL/AMNTcX/uorPai0/3Ktv+ai/4R0c8u/8AKwbF/wA9kP8A1cXr5bnvO/r7Cuvon/ysP+3efxP/APEVUH/uzynvDbn3/lceYP8AnoP+AdfLl98b/wASe96P+ly//VuPo/8A7CPWNPWkz/won/7Lp2V/4rXsL/34Ha/vJ72b/wCVVuv+e6T/AKtxdfQF/dc/+I67/wD+LZef9oW3dKr/AITg/wDZYfcf/itWf/8Afo9V+2Pej/lXNt/57l/6tS9E396p/wBOM5E/8WyH/u37h1uee8aOuC3Ravmf/wBkefK//wAVq71/99dun2d8s/8AKx8v/wDPdB/1dXqWPYX/AKfn7Mf+LZtH/dwt+vmje83OvrL6+kf/AC+v+yFvh7/4rX0x/wC+/wAD7wr5v/5WrmP/AJ7pv+rjdfKb95r/AMSK98//ABbN0/7TZujf+w51B3WiJ/Pl/wC3i/YP/hgdVf8AvIUfvK72n/5Uyz/5rS/8fPX0a/3cH/iLXLH/AEs9x/7SW6Op/wAJqv8Amanym/8AEf8AXX/vR7i9hj3t/wBwNh/5rSf8dXqAf72H/lTvZ3/pZ33/AFYg625/ePHXEjr3v3XutZ//AIUZ/JU7d616l+K2ByHjyXYmVftLf9PBKVlXZ206ibGbPx1ZFe0tDn93PU1a8XWfAqb/ANZv9mdk8a93Df5U7IV8KP8A075cj5qlB9knXWf+6y9p/wB6c186+8m5W1bTa4Rt9mxGPqrlRJdOp8nhtvDjPql4fy1v/hP8WNxfMv5G7G6G2/XvhU3Gmbym4Nyfbmpg23tzb2HrMtkMpUoAQomlp4qOEkEGqqolsb2M0cz79Dy1st1u0yaimkKtaamYgAD+ZPyB66q/eA949r9h/azmH3I3O2Fw1qYo4YNWkzzzyrGkan5Bmlbh+nG5qKV6Llu3a2d2Nurc2yt04+bE7m2fuDM7X3FiqgaajG53AZGpxWWoJx+JaOvpJI2/xX2dW9xFdW8F1A4aCRFZT6qwBB/MHqVNk3jbuYtm2nmDZ7pZ9ovraK4gkXhJDMiyRuPkyMGH29fQG/lL/JT/AGZ34O9SblyeQ+/3r17Qt0/2A8knlqm3FsGmo6LHZGtkYl5qzcOz6jF5KZyBeercC9r+8RPcLZP3HzTuMCJS1mPjR+mmQkkD5K+pR8gOvmd++r7T/wCtF94bnbabS28Pl/dJP3nZACi+BeMzuijgFhuVuIEH8Eanz6sn9gnrFHr3v3Xuve/de697917r3v3XutZv/hV9/wBuvsB/4tP1P/7x/afvR8unYfj/AC60CPiX8PfkF84u2V6P+NGyqbf3Zb7azO7lwFVunaez4jgNvvRJla3+MbzzeAwoambIRWi+48r6vSpsbe6UlgoqTjqz7/oGv/nG/wDeLmE/9H98eP8A7aXv3VfFj/i62FP+E6H8p/52fAL5Qd4dk/Krp7H9c7O3n0JLsfbuUo+y+r97PXblbsLZeeWgag2LvHceQpEOLw9RJ5poo4bpp16mUH3TUrqwAU+fW4T730x1737r3Xvfuvde9+691737r3Xxefk1/wBlI/IP/wATf2v/AO95n/delw4Dr7C/xk/7Jt+Pn/iEOp//AHg8B7t0iPE/b0N/v3Wuve/de697917r3v3Xuve/de697917r3v3Xuve/de697917r3v3Xuve/de697917r3v3Xuve/de613f+FEf8rPuf8AmQdE9O7i+Ov8Nzncvxx3BvnI4frrLZjH7dh7B2p2TQbWg3RjsPnMzVUWBot142v2PjJqNa+elpJoDUoZ45DGsnunYnCk14Hqnf8AkGfyL/ml0b809l/L75X7CboraHR1DvSbZu0sxuLbOZ3l2JvHduzdwbAiQYzauZza4LauBxm5qusnqq2anmqamGmiggmhlnlh11eSRSukZ63qve+k/Xvfuvde9+691737r3Xvfuvde9+691737r3Xvfuvde9+691737r3Xvfuvde9+691737r3X//1t/j37r3Xvfuvde9+691SD/woG/7d/y/+Jq62/8Acbc3uUfaH/lbx/zyyf4V66Ef3Zn/AIkun/iv33/HrfrTu+J//ZU3xq/8T/03/wC/F257yP5g/wCSDvf/ADxzf9W267n+9P8A05z3Z/8AFZ3T/tBn6+mf7wf6+Sjr3v3Xuve/de6rV/m1/JX/AGWP4O9tbjxlf9jvbsWjXp7YDxy+GqTP79pa2iyeRo5FPkhrNv7Op8pkYHAOmopYwbar+xt7e7J+/OadvhdK2sJ8aT00xkEA/JnKqfkT1lh9yj2n/wBdz7w3JW13dt4nL+1yfvO8BFVMNmyvGjDgVmumt4HHmkjHy6+fxtjbeb3nuXb2z9tY+fLbj3XnMTtvb+KpgGqcnm87X0+LxWPp1JAaesr6qONB+WYe8vJ54raCa5ncLDGhZieAVRUn8gK9fTFu+7bfsO07pvm7XKw7VZW8k80jfDHFCjSSO3yVFLH5Dr6JlD8IdjU/wCj+DEn2f8AbpJ+uqjMrATF/feooWylTv9KfQP8ALf8ASPI2dVdNhUW9NuPeGzc0XR5uPNQr431Xiaf6FaCP7PD7Ps6+XW4+8JzDJ95Z/vEpr/eQ5gF8sVc/SK/hrZlq/B9CBaE1+Dz8+vncbv2pntibs3PsjdOPlxO59nbhzW1dx4ucWnxud2/kqnE5egmH/HWjr6SSNv8AFfeZFvcRXdvBdQPqgkRXU+qsAQfzB6+ofY9523mPZdo5h2a5WbaL+1iuIJBwkhmRZI3HyZGVh9vX0B/5THyV/wBme+D3Um5snkPv97df0LdQdgvJL5qptx7CpqOhoMjWyE65a3cW0J8ZkpnIF5qxwL2v7xD9wdk/cXNO4wIlLWY+NH6aZCSQPkr6lHyA6+Z376ntP/rRfeF522i0tfC5f3OT952QAovgXjM7og4BYLkXECj+CJT59WS+wV1il0RX+Zv/ANu//lt/4hXdn/uPH7FXI/8Ayt3L3/PUn+HrIr7o/wD4kx7J/wDS/tv+PHr5zfvM3r6mevp89E/8yQ6b/wDEVdef+8jiPeC+6/8AJU3L/nok/wCPnr5DvcX/AKeDz3/0ub3/ALSZehV9oOgb0lN9/wDHj7y/8NTcX/uorPai0/3Ktv8Amov+EdHPLv8AysGxf89kP/Vxevlue87+vsK6+if/ACsP+3efxP8A/EVUH/uzynvDbn3/AJXHmD/noP8AgHXy5ffG/wDEnvej/pcv/wBW4+j/APsI9Y09aTP/AAon/wCy6dlf+K17C/8Afgdr+8nvZv8A5VW6/wCe6T/q3F19AX91z/4jrv8A/wCLZef9oW3dKr/hOD/2WH3H/wCK1Z//AN+j1X7Y96P+Vc23/nuX/q1L0Tf3qn/TjORP/Fsh/wC7fuHW557xo64LdFq+Z/8A2R58r/8AxWrvX/3126fZ3yz/AMrHy/8A890H/V1epY9hf+n5+zH/AItm0f8Adwt+vmje83OvrL6G7D/Jj5H7dxONwO3/AJA924LBYahpcZh8Lh+1t94zE4rG0UKU9Fj8bjqLPQUdDQ0lPGscUUSJHGihVAAA9lcmx7LNI802z2rysSSxijJJOSSStST5k9R9fe0vtXul7d7luftny/cbjPI0kssu3Wckkkjks7yO8JZ3ZiSzMSWJJJJ6cv8AZsflN/3kt3//AOjk7F/+yP3T+r+w/wDRks/+cMf/AED0l/1l/Z3/AMJPyz/3K7H/AK0dBLu3em8d/wCam3Jvvdm5t67iqIaenqM/u3PZTcmanp6SMQ0sE2VzNVW10sNNCoSNWkKoosAB7MLe1trOIQWlukUINdKKFXPHCgDPQ12TYNi5asI9q5c2W02/a1ZmWG2hjgiDMasRHEqoCxyxAqTk9bIP/Car/manym/8R/11/wC9HuL3C3vb/uBsP/NaT/jq9cqf72H/AJU72d/6Wd9/1Yg625/ePHXEjrokKCzEKqglmJAAAFySTwAB791sAkgAVJ6+ct/Mf+SjfK35kdz9r0Vca7aC7ik2Z1yyyeSmXr/ZOrAberKMXIii3CKWXLSICQKjISWPvM7kvZP3By3tm3slLnRrk9fEfuYH/S1CfYo6+pv7q3tQPZn2J5C5MuLfw98+lF1fYo31t3+tMrepg1LbA+aQr1sBf8Jy/jR/Aevu2/lbn8f48jv3Ir1T17UTw6ZV2jtmop8tvXJUcpB8tDnt1fZ0hsRpmwUgI59xD7z75415t+wQv2Qr4sn+nYEID81Sp+xx1zP/AL033Z/eXM3JPsztt1W122L943qg4+pnVo7WNh5PDb+LJ80u1PVbX8+v40/6F/mQe1sLj/tdm/JDADesTxRiOkh7B299pguwaGIAXaerZsdmJ3JJafLvawFvY19pt7/efLf7vletzZPo+fhtVoz+Xcg+SDrK7+7c92f6++xI5M3C5177ypc/SkE1Y2U2qayc/Jf17ZB5JbL69C5/wnh+Sv8AcD5Fb6+OGdr/ABbf7220c5tWCaT9uPsfrylrcn9tTK5WOA5zZM+SaZh6pZMbTJY8WL/ePZPq9mtN6iT9a0fS/wDzTkIFT/pX009NTHoEf3oHtP8A1l9reXfdXbrau58uXfg3BAybG9ZI9TUyfBuxAFHBRPK1Rmu5j7xq64Ode9+691737r3Xvfuvde9+691rN/8ACr7/ALdfYD/xafqf/wB4/tP3o+XTsPx/l1qI/wAhf5k9BfBX55Q96fJHdGS2h1unTfYuz2y+K2xuDdtX/Htw1G3JMXS/wnbWPyWSMc646XVL4vGmn1EXHv3T8illoOPW67/0Ez/ygv8An/e+P/REdxf/AGH+/V6T+E/p0cv4U/zbfg7/ADB9+7r60+LnZO4d67v2XtBt9bhoMx1zvrZkFJttczi8A1ZFX7qwWKo6qUZPMU6eGN2lsxbTpUkb60yMoqR1rx/8Kovl38p/jT2/8RMV8ePkX3X0fjN1db9nZDcuP6p7L3dsOjz9djtz7ZpqCszFPtrLY6LI1NFT1EiRPKGZFdgCAT78enIVUg1HVCPSf/Cgf+Yx0d0J2z1XjO7d99idkdo7kwlbj+9O596Z7tndXVW1cVia6iyGC6xw2+JszhsRltxVtass9fULUxwRwjxUwqDHUwa6cMSkg0x1XB2B8kfmhns7Q9k9nd6fJfIbg3Kz5XEb03h2P2aa7LKrLIarCZrKZdWnpYDKuj7V/FEpUKFWw9+6vReAA62Sf5E/8/L5E7a+QPWHxC+Y/ZOd7o6f7h3HievNg9ldg5KbN9kdXb83DVpjdnwZLe2RlfMbt2RuLOVUOPqUy89TUY1p4Z4KiKnhlp5fdNSRihZRnq2z/hU38lfkN8augvivnPj13f2r0hmdzdwbyxW4sr1Vvzcuw8hnMZSbLhrKXH5Wr21kcdPX0dPVEyJHKzIr+oC/vx6pCASajr52+WyuTz2UyWczeQrMtmczkKzK5bK5GplrMhk8nkaiSsr8hXVc7PPVVlZVTPJLI7F3dixJJ9+6U9bdX/CY75l/Lbun+YBUdTdvfJfvTs7q/AfGDf8AV4PrzfnaO8t1bMxFVt3cPWmKwNRjdt5rL1mJo5sNjKqSnpWjiUwQyMiWUke/dMyqoWoGa9bD386n+dVsn+V9srD7D2FiMJ2X8s+ycNPltkbGy9ROdr7C2x5p6CPsbsiLHVNLlJ8ZU5GnlgxeLgmpp8rNTznzwRQO7b6ajj15Pw9aA/Z3z0/md/PrsWajzfefyX7j3Tm5pavHdW9UVm8abb9LGsqjTtzqLquGhwFJHTiVIzLDjjO40+WR29R10pCoo4Dpki7f/mifBncGD3DkN6/Nr4wZesqEmxC70qu5ut8fuBqUB/tp8DvJMdhN10KrH+5TVNNVU7qtnQge/deojeQPW4l/I9/4UQ5X5Z73218Qvm4238V3tuJf4d1J3XiKKi23t/tzMQw+Rdl7zwFFHT4XbXYuRSN2x1RQR02MzEg+0Smpqz7da7fTEkWkal4dW7/zz+0OyOmP5VXyz7M6i35u7rLsTa+I6sl23vnYm4MptbdeBlyfenWGFyMmIz2GqaPJ4967EZKopZTFIpkgndDdWIPuqRgFwCMdaAvxn/npfzDvj7P3Xmst8mO7+49y796PzXWXWrdwdmbq7K2t1bvfPb966zMnbFDtLeWVy2Ert1YDZW3MzQYt5IXSGqyivKk1OJqeXVelJjU0x59G+/kmbA+eHyt/ml/H3fHcPZfyvi2tjkzHyR3zv/eW8u2sdFv/AGPsYUVTjaGHN5fI0lNuzae8d9ZrB4mtpoppaOfGV8qFHi/bb3VZCoQ0A6P9/wAKh/mh8u/jf83OkNnfH75N979KbTzPxX25ubLbb6t7T3nsXB5PcVR232/i585XYvbeYx1HVZWbG4qlp2ndDK0NPGhOlFA91WJVKkkefVQ3WX/ChL+Yb1N8T919CYDuXfW6u2N79p5zduT+SnbG7cr2r2HtLr6r2jszBYvrvrdN8SZqi21bM4XJ1tRXSrVNH9/ekjpqjXUH3VzEpNaY6q+3n8i/mBvStPZe/u8fkduatydYKhd9bp7I7KyklXWs8mhqfcOSzEoeVWVggjl9IUhQALe/dXovCg6ts/lv/wDChr5q/DHe+3sH3Tv/AHp8pvjlPWUtFurYnZm4andG/tuYeSVI5sx1l2HuKar3FQ5TEU/qgxNfVz4WpjQwCOld1q4PdUaJW4Ch6+lJ0r3L1v8AIbqfr/u/qHc1FvHrTs7bGN3bs/cVDrSOvxOTi1rHU00ypU47KUFQr01bSTqlRR1cMsEyJLG6jfSUggkHj18xX5ifzPfn/wBc/P35TbfxPzW+V2J682J8we78NjNl4HvTsWiweM2Ztjunc9FRbXw2Ei3LTYukxVHg6FaSnpVEdPHCqxgKg410qVFKr2itOgy+YX8xL+Yl/Ms7X7A7ZgzffLdXYDONLtjqvqCTfb9ZdObarq2aHa2OyVNs6KLFTbjnpUVKnNZJfvslUhyrJEIoIvdbVVQAYr1tX/zgsl8ov5dn8jP4Z7T2X8ie7to997S7S6S2V2f2ngO1950+/c1ms91V3Hurf+3KnfFPm/7wZHaVLvKMQ0NJJUyQQ0OPo4kUJBEF90ylHkaox0QL/hMj82/mJ8i/5g+/Nid+fKPv3ufZVF8WOxdy0e0+z+2N7b327S7hoOyOnMfQ5ynxG4szkKCLK0lDlqqGOdUEqRVEiggOwPurSqoUUHn1cl/woU/nBb3/AJdPXGw+lvjtNjqP5Ld7YrNZqm3jkaKiy8XUnW+LqRiJd2UOFyMc+PyO7NyZtpaTDmrhqaKAUFZLNE7pCj76pEmo1PAdfP23F8jvmt3Xl9xdl7k7u+THZmWxB/i+5d41W/8Asvch29FNLJJFUV2TjydTT7dx8b6hAuqnp4lXTGFVQBrpTRRig6vS/knfzqP5im0fk11F8ZM1Wdn/ADa6v7Q3PjtpzdcbnydRvLtLZuPqDprd4bC7C3JWHJ43GbMxsbV1bRZqvO348ZSzDXj7mth903Ii6SeB6+jwzKis7sqIilndiFVVUXZmY2CqoFyT9Pe+kvWiF/OD/wCFMPZtf2Bu/wCOX8ubc9Fs3Ym1K+u23u75N0VHQZjdG+8vRSyUeVpuozkqetxO3Nl00yyRR50QzZHJOoqKGSkgWOaq1XpQkQpV+PWtxgpv5m/yskyPYu1m+dfyOcVcq5Te+3B3922sdb60ljrNw4oZ9IZ1AZSjSgqARYAW9+6d7FxgdD38a/5vv8z/AOBPYMeOoO8+2MpSbar4qXc/Q/yOq91b52i8MTRzVGDrtp77qzuLY8tQmkvNhqjEV4BH7ukkN7rRRGHDr6Lf8rX+Zz07/NB+Pw7V2BSnZ/Yuz6jH7e7q6ir8hHkMt15uuspZaiklpa1YqV85svc0dLPNh8n4YRUpBNDJHFU01RDHvpM6FDTy6Lx/PQ/mn5H+WF8ZNvZXrfHYnMfIfvTN5rZ3TsGehWswe2afb+Poq3e3ZGVxbNGM5DtCHMY+CmomYRS5HJ0zTB6eOaJ/dbjTWc8B184bffyx+d3ym3jnN37z71+SPcG6RHPmso9Ju7fWWpcHQI4aSah29t6oTB7TwVM9tMNHS0lFB9FRfeulQCjgB1Y//K7/AJ1f8yD43929b9bba3T2b8wthb13Rh9qf7LZvvN53sDObgbLVcdHBj+qdw5ZszujY+44zMzUkdLK2IaUl6ujmUal91R40IJ4dfUExtVPXY6granHVmIqayipaqoxOQehkr8XPUQRyy46ukxdbksZJWUUjmKVqaoqIC6kxyOlmO+knU337r3X/9ff49+691737r3XvfuvdUg/8KBv+3f8v/iautv/AHG3N7lH2h/5W8f88sn+FeuhH92Z/wCJLp/4r99/x63607vif/2VN8av/E/9N/8Avxdue8j+YP8Akg73/wA8c3/Vtuu5/vT/ANOc92f/ABWd0/7QZ+vpn+8H+vko697917r3v3XutMn/AIUOfJT/AEg/I7Y/xzwdf5dvdD7ZGY3PDDJ+3L2P2JTUOUlgqVjZo5zhNl0+M8LN6oZchVJYXa+Svs7sn0ey3W9Sp+tdvpX/AJpxkjH+mfVX1Cqeu8392B7T/wBWPavmH3T3G207pzJd+FbkjIsbJnjBWuR4t01xqAwywwtU4pVZ8C+7+ofjb8oeuu9O6dp7s3vtnrSXJ7iwu29oQ4Wavqt6pj5qPa1fVfx3KYmljo8BXVf8RR0lMoq6SCylS9h9zbte473sV7tW2XEcU89FZn1UCVqwGkE1YDTwpQnrMn7yHt7zx7re0HNPt3yDvVlt+7bsI4JZ7kyhFtS4a4RfBjkYtMi+AQV0+HJJkGnWzF/0EifFn/nxff8A/wBSuuv/ALNvcIf6y2/f9HWz/wCqn/QHXJb/AJNUe8X/AIUXln9t9/2yda0vz8716g+TPyh393t0vtLduyNu9kJh85ntubwgwlPXU+9o8dFjtx5CjGBymXpJKLPS0MdfI7yiVq2pqLqF0kzdyjtW47HsVptO53Ecs0FVVk1UKVqoOoA1WunhTSB11k+7V7dc8e0ntBy17c8/b3Zbhum1GWKGe2MpRrQuXgRvGjjYNCHaFQFKiKOKhJrS1L/hPB8lf7g/IjffxvzuQ8WA7120c9tSGeX9uLsbrymrckaWljciOFs7smoyLTsPVK+MpkseLAH3j2T6vZrTeokrNaPpf/mnIQKn/Svpp6amPWG/96D7T/1l9ruXPdXbrbVuXLt34NwQMmxvWRNTEZPg3YgCDgonlaozXcv9419cHeiK/wAzf/t3/wDLb/xCu7P/AHHj9irkf/lbuXv+epP8PWRX3R//ABJj2T/6X9t/x49fOb95m9fUz19Pnon/AJkh03/4irrz/wB5HEe8F91/5Km5f89En/Hz18h3uL/08Hnv/pc3v/aTL0KvtB0Dekpvv/jx95f+GpuL/wB1FZ7UWn+5Vt/zUX/COjnl3/lYNi/57If+ri9fLc9539fYV19E/wDlYf8AbvP4n/8AiKqD/wB2eU94bc+/8rjzB/z0H/AOvly++N/4k970f9Ll/wDq3H0f/wBhHrGnrSZ/4UT/APZdOyv/ABWvYX/vwO1/eT3s3/yqt1/z3Sf9W4uvoC/uuf8AxHXf/wDxbLz/ALQtu6VX/CcH/ssPuP8A8Vqz/wD79Hqv2x70f8q5tv8Az3L/ANWpeib+9U/6cZyJ/wCLZD/3b9w63PPeNHXBbotXzP8A+yPPlf8A+K1d6/8Avrt0+zvln/lY+X/+e6D/AKur1LHsL/0/P2Y/8WzaP+7hb9fNG95udfWX1uafEj+S98D+3fi38d+097bI3vWbw7F6X623ruiro+yN0Y+kqc9uXaWKy+VnpqCmqlp6OCWtq3ZYkARAbDge8aeYfczmzbt+3mwtbqIW0N1IigxoSFVyBUkVOBx64Ne9n39/vHcke8Puhydy/wAwbfHse17/AH1rbq1jbuywwXMkUYZ2XUxCKAWOScnow/8Aww1/Lo/59/2B/wCjU3d/9V+yf/XY5z/5TIf+cSf5uow/5OP/AHpf+mm2z/uXW3/QPWq//NV+N/VnxS+Ym7um+m8Zk8RsbEbS2Jl6Kiy+Zrs9WpW57b1Pkci75HIvJUyJJVSEqpNkHA49z1yDvV/v/LdvuW5SK100kgJChRRWIGBjh12N+5t7q84+83sXsnPfPd3DPzFPe3kbvHEkKFIZ2RAEQBQQoyfPierTP+E1X/M1PlN/4j/rr/3o9xewH72/7gbD/wA1pP8Ajq9Ydf3sP/Knezv/AEs77/qxB1tz+8eOuJHVa382v5K/7LF8He2tx4yv+x3t2JRr09sBkl8VUm4N+0tbRZLJUcgu8VXt7Z9Pk8jC4BtPSxg21X9jb292T9+c07fC6VtYT40nppjIIB+TOVU/InrLD7lPtP8A67v3huStqu7bxOX9rk/ed5UVUw2bK8aMOBWa5a3gYfwSMc0p189r3mD19OfW1p8Zv56fw7+NfQPUvRW3OjO+Zcd1psrEbdnroKTrqBMzm0iNZufcLQ/31Hjn3HuWrq6+QWFpKlvcA757Vcyb3u+4brNutprnlLUrJheCr8H4VAUfZ1xm92v7uv3092PcvnX3F3X3E5bW63bcJJwha+JiiJ028FfpciCBY4V/ooOin/zP/wCat8YPnv0Fievdt9TdvbU7H2bvbFbv2Tubc1Nsr+E08bwVOH3PhsjPit1ZDIxUOVw1cZgIoJNVZRU2rSoLKIOReQd95S3eS8n3C2kspYijqpevqrCqAVDCmSMM3U0fdC+5r7vfdt9yr3mfdeddjveVb/b5La7t4GuvEYgrLbyoJLdELxypp7nWkUstKkgGkbprtPcnSHbPXHcG0JfFuXrTem3d54hWkaOGqqcBk6fIHH1ZQEvQZOGFqaoQgiSCV1IIJHuUNysIN02+9265H6E8TIfkGBFR8xxHzHXQXnzk7avcLkrmrkbfErtO7WE9rJipVZo2TWteDxkh0PFXVSCCOvpl9Zdhbb7a652J2js+q+92r2HtHb29NvVJKa5MRuXFUuXoROqMwiqooKsJLHe8cqsp5B94QX1nPt97d2FytLiGRkYfNSQfyxj5dfJTzbyxuvJXNPMfJ++w+HvO1309rMuaCWCRonpXipKkqeDKQRg9Lj2l6D3Xvfuvde9+691737r3Ws3/AMKvv+3X2A/8Wn6n/wDeP7T96Pl07D8f5daDfw1+F/fHzz7kTof454PCbg7Ffa2c3iuP3BuTF7Vx/wDA9uvQR5Sf+K5eWGjE0TZGLTHfU9zb6H37pSzBRU8OrY/+gYP+bd/z6rqz/wBHfsH/AOuHv3VPGTq/3/hPR/KF+bX8u/5M919nfJ3Zuztt7S3t0VLsPAVW2+wttbvqp9xP2Bs3cAp56HC1M9RTU/8ADMNO3lcBNShfqR7901K6sAB69EY/4WIf8zu+E3/iK+2v/eu2n78erwcG6AX/AISxfAbqr5N/IPub5Id07Uwu+tufGDG7HpOv9obmoKfK7erO0uw5tyVFDuzIYmrjnoMrJsPB7QqHpYamN4oq/JU9Wg81LGyeHXpmIAA8+t5L50fGPrj5d/E/u/ojsna+I3Hit2debpXbsmQoaepqtq73o8DkJdm7y29PLFJJi9wbYzohqaaeKx9JjYNE8iNvphTpYEdfHx68zNdtzsDY24cXNJT5LA7x2zmcdURSPDLBXYvNUVdSTRSxlZIpIqiBWVlIZSLjn3rpb19sX3vpB18Xn5Nf9lI/IP8A8Tf2v/73mf8Adelw4Dr7C/xk/wCybfj5/wCIQ6n/APeDwHu3SI8T9vXyT/5jPyV3D8u/nB8me/dwZOfJw7z7Y3XBtFZpmmjxXXO28lNtnrjBUp1GNYMPsrEUMJKBVlkV5SNTtfXS1RpUDraY/lsfzxv5NH8uP42bL6b676Z+Uk2/HweKrO6O06Xp3qVNxdpdhPTLNncxkMtN3amVfblFkppocLj5H8WOx4RAplaaWT3TLxyMakinRue5P+FNv8nj5B9b7p6h7q+Pvyg7J623pjZsXuPaW6en+pshja6nlRlSeLX3mKjH5ShkIlpK2meGro6hVmgkjlRXG+qiJwagivWhrv7O7I2f3nujdPxoz2/8bsDbfZVduTo7cO7oaHA9m4Xb+L3A2W2DXZ4YHJ5TH0W8sJTw0xmmpKl4jVwmWMqCFGulI4Z49fRb/mk96y/J3/hNzvr5CVaU8OU7i+O3xV3/AJ+lpFCUtDujcPbnR9burHUyqAogxu43qoEsANMY4Hv3SVBSWn29aMf8onoPavyc/mU/D/pffWKo8/srcfbFJnd27eyMUc+M3FtvrvCZrsrNbeylPKrR1OLz2O2hJSVMRH7sEzrxe/v3ShzRGPX11YYYaeGKnp4o4III0hgghRYoYYYlCRxRRoFSOONFAVQAABYe99Iuvni/8K8f+3gHx9/8U72t/wC/q7v96PSqH4D9vQn/APCXj+Vv018jz2Z83PkRszC9lbc6s35T9XdM7C3VQU2X2g/YONweG3bu3fO48BXRS0Ofm23jNy4mDDxVCTUcdVUVM7xtUU9LJD7rUzkUUHrfRzG1tsbh27W7Qz+3MDnNpZLHHD5Da+YxGPye3a/ENEIDiq3CVtPPjarHGFQhgeJotItpt730m6+at/wo/wD5aXWnwH+Uewt/dC4Gn2f0d8oMFurcuF2FQLIuF2D2JsnIYaDsHb22IWMi4/aFbTbrxORoKMP46KSsqKanSKkgp4010ricsCDxHV6//CQ/5D5/e/xi+S/xuzeRnrqDoTs3aG9dmxVUzOcVtzvDG7nbIYLGxsAseMpt2dcV+QZVvpqstKzW8i38Om5hkH160vP5h3/Zf3zl/wDFw/kz/wC/p3t7908nwL9g6+m9/JL+Pm0Pjr/K/wDh/gNr4SjxOQ7H6c2X3rvmshplhyOf3r3Pt/G7+yOQzk5UT1mQx2PzNLjIzIS0NHj4IFskSqN9JZDV26rT/wCFan/bs3rj/wAXE6v/APfVd6e9Hq8Pxn7OqC/+Elf/AG8y7G/8U87Q/wDfqdGe/Dpyb4B9vRz/APhXf8UOyarsT47/ADUwmIyWa6wpOtU+Pm+6+hinqaPYu4MTvTde99mV2bVNSUNFvZd919JDVECL7nGpBI6yTUySePVYWFCvn0QX+SP/AD5tv/y2ti5j41d39KtvjoTdm/snvqXfnXa0FP2jtTMbhxmFwuXGWwWWno8H2Lt77XBU5hierxtfRqZQJqqMw08XurSR6jUHPW57/Lc2h/Kj7EzvbPzM/l24TqCq3X3XLjIO1c5siknw249qVBiSql2rW9dZVKKv6eGfyFL/ABHIUNPjsbDmqyNaxxUhIZV30w+sUDdIP/hQD8ldx/GL+Vn8iNx7LyVRh96dmQ7c6M25lqSUwVOOj7Ry8WI3fVUs8ZWopq5eu4cytNNEyywVTRyqQUv791uIVcdfNR+FWZ+Km2fkt1jun5q4fsLdHxy2tl5Nxb72X1jh8VmNz75bFUs1Vg9oSx5fdWzqaj25ms4kCZaZK1Kg44TRwaZZElj10qbVQ6ePW9htn/hVb/Ko2Vt7DbS2d0x8qNp7V25jqXD7f2ztrprpzBbfwWJoYlgosZh8PjO7KXHYzH0kKhIoYY0jRQAoA976TeC/qOqgP5zH82n+Uj/M0+P+Qp9q9PfIrbHyx2SuPqume3c91b1lhl8EWSp2zWw9+Z3C9uZfNZLY2YxMtSYYmp6o47JeKpgUA1Ec+unI0dDxGnoi3/CbP5LZ/oL+aR1Ds+DIzw7I+SOL3P0pvjGCZvtayevwlfubYVf9q14HyNBv3b1BBHNxJFS1tSqNaV1f3VpRVD8uthT/AIVufFDsrtToD46/J/Y2IyG4NtfGzO9jbf7VocbBJVz4DavbY2F/CN8VEMSFocHhs/sZKGumGrxvlKd2AjSR18em4WAJX1612v5Kv86Wf+VVm+xNp7t6You1ume5M3tvM7zrNt1VHgu2Nr5DbtLW46lyO3MjXocRurFpQ5CW+FyEtHH5z5IK2lLzif3Tkkeumc9bu/8AL3zP8oL5id1bv+fHwv271bVfJbPbTpsJ2RbEttLtPY6V804y2XznVlXNFR7d3LuZ6v7HI7ox1JKuajh8K5GpjM3k30w2tRpbh1c/79031737r3X/0N/j37r3Xvfuvde9+691SD/woG/7d/y/+Jq62/8Acbc3uUfaH/lbx/zyyf4V66Ef3Zn/AIkun/iv33/HrfrTo+K9RT0nye+ONXVzw0tLS989QVFTU1EqQ09PTw9hbekmnnmkZY4YYY1LMzEKqgkm3vJDf1LbFvSqCWNpMAB5/pt13T95IpZ/aH3VhhjZ5n5b3NVVQSzMbKcAADJJOABknA6+kr/pZ6r/AOfl9f8A/oZbc/8Arl7wq/d9/wD8oM3+8N/m6+Ur+pfOP/TJ7n/2Sz/9AdOeI39sTcFamNwO9dpZvIyJJJHQYjceHyVbJHCpeWRKWjrJp2SJBdiFso5PtuSzu4VLzWsip6lSB+0jpJfctcx7Zbtd7lsF7b2oIBeWCWNATgAsygVJ4Zz1B7R7F231D1tv3tTeNV9ntXrrZ+4t67gqAVEgxW28VVZasjp1cqJayeKlMcMY9UsrKigswHu9hZT7je2lhbLW4mkVF+1iAPyzk+Q6Ucn8rbtzxzXy3ybsUPibzul9Bawr5eJPIsalqcFBartwVQWNAD18zTuHtDcndna3Yvb28JvNubsree4t6ZnS7SQ09XuDJ1ORNDSFwClBjknWnp0AAjgiRQAAB7zf22wg2vb7LbrYfoQRKg+YUAVPzPE/M9fWryLyftPt/wAmcrcj7GmnaNpsILWLFCywxqmtqcXcgu54s7MSST0aPZH8sz54dj7P2zv7ZXxq35ndobywmO3JtjNwy7dpafMYLL0sdbi8pSw1+cpKsUlfRzJLEzxrrjdWHBB9kN1zxynZXM9pdb3ElzGxVl7jRgaEGikVBwc8eof5g+9t93HlXfN25a3/AN2Nttt8sLh4LiIidmimiYpJGxSFl1IwKsAxowIOQelT/wANL/zF/wDvFXsD/wA7do//AGSe2P8AXC5M/wCj/D+x/wDoHon/AODV+61/4WXbP94uf+tHSZ3n/LI+efXu0dzb73j8aN+YTaWzsFlNzbmzU023KmDEYHCUc2Ry2UqYaHO1VW1Lj6GneWUpG5WNCbWB9vW3PPKd5cQWltvcTXErhVXuFWY0AFVAqTgdG2w/e4+7fzPve08ubF7s7bcb3f3MdvbxATqZZpWCRxqXhVdTuwVasKkgefRXemO09ydH9tdb9w7Ql8e5OtN6bd3niVMjxQ1dRgMnT17Y2rZAS2PysEL01SliJIJXUggkez7c7CDdNvvdtuR+hPEyH5agRUfMcR8wOpg595O2r3C5K5r5G3xK7Vu1hPayYBKrNGya1r+OMkSIeKuqkEEV6+mX1n2DtvtnrrYnaGz6r73avYe0du7029UnTrkw+5cVS5eg8yqzCKpjp6tVlQm8cgZTYgj3hBfWc233t3YXK0uIZGRh81JB/LGPl18lXNvLO68lc08x8ob5D4e87XfT2s6+QlgkaJ6V4qWUlTwKkEYPRTP5m/8A27/+W3/iFd2f+48fsQcj/wDK3cvf89Sf4epp+6P/AOJMeyf/AEv7b/jx6+c37zN6+pnr6XvR/afWNP0r1BBP2NsOCeDq7r+GaGbd+345YZY9p4lJIpY3yKvHJG6kMpAIIsfeEW6WF8dz3EiymIM8n4G/jPy6+TH3C5O5ul5+54kj5V3Jo23i8IItpiCDcyEEEJQgjII6Fqj7M63yNXTUGP7B2RXV1bPFS0dFR7rwNVV1dTO4jhp6amgr3mnnmkYKqKpZmNgL+y9rG9RWd7OUIBUkowAHzNOgVPylzVawS3N1yzuEdvGpZna3mVVUCpZmKAAAZJJoBx6mb7/48feX/hqbi/8AdRWe62n+5Vt/zUX/AAjpjl3/AJWDYv8Ansh/6uL18tz3nf19hXX0T/5WH/bvP4n/APiKqD/3Z5T3htz7/wArjzB/z0H/AADr5cvvjf8AiT3vR/0uX/6tx9H/APYR6xp60mf+FE//AGXTsr/xWvYX/vwO1/eT3s3/AMqrdf8APdJ/1bi6+gL+65/8R13/AP8AFsvP+0LbulV/wnB/7LD7j/8AFas//wC/R6r9se9H/Kubb/z3L/1al6Jv71T/AKcZyJ/4tkP/AHb9w63PPeNHXBbotXzP/wCyPPlf/wCK1d6/++u3T7O+Wf8AlY+X/wDnug/6ur1LHsL/ANPz9mP/ABbNo/7uFv180b3m519ZfX0j/wCX1/2Qt8Pf/Fa+mP8A33+B94V83/8AK1cx/wDPdN/1cbr5TfvNf+JFe+f/AItm6f8AabN0b/2HOoO60RP58v8A28X7B/8ADA6q/wDeQo/eV3tP/wAqZZ/81pf+Pnr6Nf7uD/xFrlj/AKWe4/8AaS3R1P8AhNV/zNT5Tf8AiP8Arr/3o9xewx72/wC4Gw/81pP+Or1AP97D/wAqd7O/9LO+/wCrEHW3P7x464kdaZX/AAoc+Sv+kH5G7H+OWCr/AC7e6H20MzuiGGX9qbsfsOloMpJT1KIzRznBbLp8Z4Wb1wy5CqSwu18lfZ3ZPo9lut6lT9a7fSv/ADTjJGP9M+qvqFU9d5v7sD2n/qx7V8w+6m422nc+ZLvwrckZFjZM8YZScjxrpp9QGGWGFqnFKZ+hfit8hflBV7loeg+q9x9m1OzqbGVe51wP8OjjwsGalrYcS1bUZOuoKdHyEmNqPEgcu4gcgWViJK3bf9n2JYH3e/SBZCQuqvdppWgAJxUV+0dZ4+5HvJ7Y+0EG03HuVzla7RFfPItv42smUxBDJoWNHYhA6ajQAa1BNSOjJ/8ADS/8xf8A7xV7A/8AO3aP/wBknsl/1wuTP+j/AA/sf/oHqKP+DV+61/4WXbP94uf+tHXv+Gl/5i//AHir2B/527R/+yT37/XC5M/6P8P7H/6B69/wav3Wv/Cy7Z/vFz/1o6Jh2x1J2R0Zv3OdX9t7Ryuxd/bb/hxze2cysAr6Bcti6LNY2R2pZ6mllircVkYZ43jkdSkg5vcAS7fuNlutpFf7dcLLaPXSy8DQlTxocEEZ6nrkvnblX3E5b27m/kne4dx5au9fhXEVdD+HI8UgGoKwKSI6EMoIKnHW4j/wny+Sv+lD4sbl6Fzlf59z/Hnc7RYeKaTVUTdbb/nyOewLKZD5qj+FbnhzFK1rpT032qcAoPeOPu/sn0G/QbtElILyPP8AzUjorfZVdB+Z1H164X/3m3tP/VD3j2n3I2+207RzRZ1lIGBfWYSGbhhfEtzbSCtC8njNkhj1fx7iPrmr1737r3Xvfuvde9+691rN/wDCr7/t19gP/Fp+p/8A3j+0/ej5dOw/H+XWtr/wlY/7eo0//iuvcH/uVs734dPTfB+fX0rve+knXvfuvdaGH/CxD/md3wm/8RX21/7120/ej0pg4N0aj/hHf/zJL5tf+JT6k/8AeS3b78Oqz8V63Ha6kiyFFWUE5dYa2lqKSYxkLIIqmJ4ZDGzK6hwjmxIIv+Pe+mOviXba/wCPj2//ANrvFf8AufB710v6+23730g6+Lz8mv8AspH5B/8Aib+1/wD3vM/7r0uHAdfYX+Mn/ZNvx8/8Qh1P/wC8HgPdukR4n7evjt/IPrXOdM9+909SbspKil3B1j2z2DsLPUso8M4yG0925XB1hUvEQFmkoSyOFKsrBgCCPeuloNQD1un9Hf8ACWP4G/Ifp/rXvLq/5k/IDcGwO1NnYPeu18pS4vq6cPjs3RRVX2dX4MHItNlcVUM9JW07ES0tZBLDIFkjZRunTBmYEgrnoVP+gP74lf8AeV/yK/8APJ1p/wDWD36nXvHP8PWGn/4SDfEOrgiqaX5a/IWppqiNJYKinxPWM0E0UgDJLFLHgWjkjdTcMCQR79Tr3jn+Ho4/82H437d+IP8Awne7u+M+0tw5rde2+m+teltn4jcW4oqGDN5ekpvkl1dVrV5KLGQ09AlSz1ZBESKtgOPeuqodUoP+rh1p6/8ACcUA/wA5v4bAgEX+QpsRflfit3iynn8gi4/x9+6fl/s26+pz730j6+eD/wAK8f8At4B8ff8AxTva3/v6u7/ej0qh+A/b1Z5/wkV+TGxMx8a+/PiVWZihpO0tjdvV/dmJwVRPDT12e6533tbZW1avI4mmdhPkk2turaDpkZEBFMMtRB7eVL+HVJhkN5dbgXvfTHWgZ/wrs+TOwewe+/jH8Zdp5ahzO6vj9tXsXePZpoKiKqXb+a7jk2ENtbTyDRO32eco9u7CGSngYBhSZakc/rt70elMIIBPr0ab/hHV1nnKDYnzn7jq6eoTbe6t29IdaYGq0aaWpzewsP2LujdcAkIJlnoqHsjDNYEaFqOb6hp8Oqz/AIR1qM/zDv8Asv75y/8Ai4fyZ/8Af0729+6eT4F+wdfVv/l4f9u//gz/AOKd/GX/AN8rsn3vpI/xt9p6pC/4Vqf9uzeuP/FxOr//AH1XenvR6ch+M/Z1QX/wkr/7eZdjf+Kedof+/U6M9+HTk3wD7evoX9t4PqndHXe59p930Gxsv1Zu6kp9o7twvZC4WTZmepd0V9JgaHA5iDcBGKqmzmWyFPS00L3earmiSIGVkHvfSYVrjj1p2/zFf+EoWAycO5+0v5c+9pMBlFSsyw+M/aOVkrMDXOqPO2I6z7SrpXyWFmkEYjpKHcf3kMs0l5ctSxKANU6eWbyYdan/AMOPln8gf5anyy2/2xsap3Fs/eXWm8Ztq9tdbV8lVjKXeO3cTm/4fv8A6p39hJtMU0NSaKaArPGZsbkYoqqAxVVNFInunmUOtOt+n/hSxs+r7u/k/bk7C2XFU5LD7H3/ANId2uI4X+6l2jlKyXaBrmplDyBKKn7Hhqp/qIYIpHYhUYjx6TxYenWil/Ks+Kvx++a/zF2T8ZvkP2xunpvBdn4bPYrYO69qLt/7mt7SpkpMhtjalfLuWkq8bFT7ooKWvpKVQFmqMq9JBGS0oRvdKHYqtQOtuz/oD++JX/eV/wAiv/PJ1p/9YPe6dM+Of4esMv8AwkG+IcBiE/y1+QsJqJlp4BLiesYzNO6syQRB8CPJMyoxCi7EA8ce/U6945/h6MB8W/8AhL58avit8iumPkdtX5Ld8bh3H0t2Dt3sLD4HM4vYFNiMzW7drUrYsZlJsfhIq5MdXaDHN4nVzGxAIJv79TrRmJBFOtk/etRsyk2duup7Gn2xTdfwbczcu+KjesmKh2dDtGPG1Lbjl3XLnWXCR7cTECY1rVhFKKbX5fRq9+6ZFfLj1qYfzEP+ErnSvcb7g7a+AO88d0ZvPKR1GbHSm6pKvLdIbiq6lWqhDszcNIK7cfWcde8heOHx5jEqWSKnhoacArqnTyzEYbI60s9q7s+Uv8tj5ZyZHC1e4ukvkn8d9+VWHzOP+4VjS5XDVix5fbecioaqXFbr2ZuSkTx1EPkqMblsbOGVpIZUc+6UYYfI9fXM+L3d2P8Akr8buhfkLi6EYqi7s6f667RTD+UznCTb32pitw1mCeYgeaXCVlfJSu30Z4SRcc+99IiKEjodvfutdf/R3+Pfuvde9+691737r3VIP/Cgb/t3/L/4mrrb/wBxtze5R9of+VvH/PLJ/hXroR/dmf8AiS6f+K/ff8et+tG/3lN19DfXvfuvdXJ/yGv+3i/X3/hgdq/+8hWe4292P+VMvP8AmtF/x8dYI/3j/wD4i1zP/wBLPbv+0lervP8AhQh8lf8ARl8XtrdAYOv8G5vkJucHORQy2nh626/qMfm8yHMTCWn/AIxuqfEU6arJUU0dXH6gGAi72f2T67fZ93lSsFnH2/8ANSSqr+xdZ+R0nrnx/dj+0/8AW33f3j3L3G21bRyxZ/pEjBvr0PFFSuG8K3Fy5plHaFsVB61QPiH0BlvlH8lenOiMUtQE3/vPHUOerKUEzYrZ2OEma3tmksCPJh9pY2sqEBsGkjVbjV7yA5i3ePYdk3LdpKfoxEqD5ue1F/Nyo/PrtB74e5dl7P8AtPz37jXhXVtlg7wq3CS6ekVpEflLcyRIacAxNDTr6VeEwuK23hsRt7BUFNisHgcZQYXDYujjEVJjcVi6WKhx1BSxDiOmo6SBI0X8KoHvCWWWSaWSaVy0rsWJPEkmpJ+ZPXyebhf3u6397um43LzbhczPLLIxqzySMXd2PmzMSxPmT05+6dJOm7L4nG5/E5TBZqip8lh81jq3E5bHVcYlpchjcjTS0ddRVMR4kp6qlmeN1PBViPd45HikSWJisikEEcQQagj7D0qsb27229s9xsLhor63lSSN1NGSRGDI6nyZWAIPkR181L5e9BZP4vfJfubonJLUGPr7e2Tx+BqqoHz5TZ2Q8ea2RmZeAPLl9o5KiqXAuFeUrc2v7za5d3dN92TbN2SlZogWA8nHa4/Jww/Lr6xPY/3KtPd/2m5D9xrUrr3Pb43mVeEd0lYruIfKK5jlQeoUHz62yP8AhPn8lf8ASh8V9x9DZyv8+6PjxudoMRFNKWqJut9/T5HPYBg0h8s/8K3LBmKUhdS09MKVPSCg94++7+yfQb/Du0SUgvI8/wDNSOit+1dB+Z1H164tf3m3tP8A1P8AeTavcjb7bTtHNFnWQgdovrMJDNwwPEgNtJmheTxmyQx6sM/mb/8Abv8A+W3/AIhXdn/uPH7B3I//ACt3L3/PUn+HrGD7o/8A4kx7J/8AS/tv+PHr5zfvM3r6meve/de6N/8Ay+/+y6fh7/4sr0t/78HAew5zf/yqvMf/ADwzf9W26g77zX/iOvvn/wCKnun/AGhTdfRk33/x4+8v/DU3F/7qKz3hnaf7lW3/ADUX/COvlm5d/wCVg2L/AJ7If+ri9fLc9539fYV19E/+Vh/27z+J/wD4iqg/92eU94bc+/8AK48wf89B/wAA6+XL743/AIk970f9Ll/+rcfR/wD2EesaetJn/hRP/wBl07K/8Vr2F/78Dtf3k97N/wDKq3X/AD3Sf9W4uvoC/uuf/Edd/wD/ABbLz/tC27pVf8Jwf+yw+4//ABWrP/8Av0eq/bHvR/yrm2/89y/9Wpeib+9U/wCnGcif+LZD/wB2/cOtzz3jR1wW6LV8z/8Asjz5X/8AitXev/vrt0+zvln/AJWPl/8A57oP+rq9Sx7C/wDT8/Zj/wAWzaP+7hb9fNG95udfWX19I/8Al9f9kLfD3/xWvpj/AN9/gfeFfN//ACtXMf8Az3Tf9XG6+U37zX/iRXvn/wCLZun/AGmzdG/9hzqDutET+fL/ANvF+wf/AAwOqv8A3kKP3ld7T/8AKmWf/NaX/j56+jX+7g/8Ra5Y/wClnuP/AGkt0dT/AITVf8zU+U3/AIj/AK6/96PcXsMe9v8AuBsP/NaT/jq9QD/ew/8AKnezv/Szvv8AqxB1tWdodi7a6i63352nvGq+y2r11tDcO9NwVAKCRcVtvFVWWrI6dZGRZayeKlMcMYOqWVlRblgPcB2FnPuN7aWFstbiaRUUfNiAPyzn0HXGvlDlbdud+a+W+Ttih8Ted0voLWFc08SeRY1LU4KC1XbgqgscA9fM07i7Q3J3Z2t2N29u+Xy7l7K3nuLemYAkaSGnq9wZSpyJoKQuFK0GNjnWnp0ACxwRIoAAAGb+22EG17fZbdbD9CCJUHzCgCp+Z4n5nr61eReT9q9v+TOVuR9jTTtO02EFrFihZYY1TW1PxuQXc5JdmJJJr1u7fyOPjN/oD+Eu2t55nHmk3v8AIfIf6V8y80Wiqh2lU04x/W2N16VMlFJtiIZaIEXSTMSi5FveL3unvn735ontonra2a+EPTWDWQ/bq7D/AKQdfPp/eG+7X+uT94HdthsLnXy9yvF+7ogDVTcq2u+kp5OLg/TN6raoerkPcbdYJde9+691qef8KO/jZ9lnel/lfgcfpp83TTdM9h1EEWmMZXGrkNzdfZCo8YPlqshjGy9JJM9iI6CmjufSBkB7L73qi3Pl+V8qfGjHyNFkA+QOggerMeu0X91b7r/Ubdz77Mbjc/q27jdLJSc+HJogvUWvBUkFtIqji00zUGSawP5OXyV/2W75z9ZTZWv+y2T3A0nS29PLKI6WOHelXRLtTJTmQingTFb6o8Y8s728NG1R6lVm9jv3I2T99cq3wjSt1bfrJ69gOseuYy1B5tTrLz79ftP/AK633d+bY7K28TmDYwN1taCrE2qv9RGKZJktGnCoPilEWCQOvoBe8ROvmh697917r3v3Xuve/de61m/+FX3/AG6+wH/i0/U//vH9p+9Hy6dh+P8ALr523VXcvb/RO6hvrpDtbsnpve4x1ZhxvHqrfO6OvN1DEZEwtkMWNw7RymIy4x1c1PGZoPN4pTGupTpFvdKiAcEdGW/4c1/mR/8Aewb5v/8ApV/fP/2e+/dV0J/CP2dbPP8Awll+W/yt+Qny/wDkLtnvz5N/ITvDbeF+Ns2dw23+3+6OyOy8Jic2Oz9g48ZnGYree5M1QUGVFBWTQfcRRpN4ZXTVpZgfefTUygKKAcekd/wsQ/5nd8Jv/EV9tf8AvXbT9+PW4ODdGo/4R3/8yS+bX/iU+pP/AHkt2+/Dqs/Fetyf3vpjr4km2v8Aj49v/wDa7xX/ALnwe9dL+vtt+99IOvi8/Jr/ALKR+Qf/AIm/tf8A97zP+69LhwHX2F/jJ/2Tb8fP/EIdT/8AvB4D3bpEeJ+3rUc/4Ub/AMkXsXtTe+c/mA/EHZlfvfcOWxdF/syHT21aCWt3Vk6vA0EGNoe2ti4KjSSp3FVz4Wjhps7jaSNqxnpo66KKdpa1o9Hp6KQDtY9ayXwf/m5fPT+XTR5TZvQHapo+vazK1GRy3T/Yu36Peewoc60ipX1+Pw2WWLLbRylU8OmsbE1eParZR9wJGjjKe6eZFbJGejK/Jj/hRV/NG+TWycp11ku3tt9PbSz9HNjtxUnQ20I9g5rN4+ePxT0Uu9KzI7i3ziqWpjLLMmNyVEJ43aOXXGxT37qoiQZp1YT/AMJpevf5q27u3Nubg6m7V7F6v+Ae2c29d2qd+0Tbl6p34lPUiXI7C6g2pu2KqoE3zn6qMRV2bwP2n8HhDSVdRJL9vQ1fuqylKZHd1s//APChv/tzl80P+1J0/wD/AAQ/UfvfTUX9ovWjT/wnE/7fN/Df/wAuG/8AgVe8veulEv8AZt19Tj3vpH188H/hXj/28A+Pv/ine1v/AH9Xd/vR6VQ/Aft617enMN8tOjdp7f8AnX0XH2XsLa2xe0Mj1vj+++v56umptn9jUW39vZup2xn8ni3kOGjz23t40yRw5KNKHNQy1FMgqBFUxJ7pw6T2nqzrcX/Ck7+btuPYk+yJPkLt3D1FXQvj6zfO3eoOsMNvueCWJIZpIMpS7YGLxFdJGrWqaCipKmFnLxPG4Rk91TwkrWnVcfxg+Kfyp/mO/IVeveoMBuntntHfOdl3Bv8A39uWuyuQxm3o81kZKjPdj9s77rxXyY7HmpllnqKuqeasr6g+KnjqauWOGT3VywUVPDr6sP8AL6+FHXn8vr4pdYfGLryo/i8Oz6CfJby3jNRpQ12/+xc861+8t511MstQ1MuTyR8VFTPLO1DjKempfLIIA530jZixJPXynf5h3/Zf3zl/8XD+TP8A7+ne3vXStPgX7B19W/8Al4f9u/8A4M/+Kd/GX/3yuyfe+kj/ABt9p6pC/wCFan/bs3rj/wAXE6v/APfVd6e9HpyH4z9nVBf/AAkr/wC3mXY3/innaH/v1OjPfh05N8A+3o4H/Csje3zxO++uNk5jbmRwPwCp6fD5fZu5dlz5KuwW9O3zSOcqnclZDT08OC3Tt1zLHt3Fz6qSWhMtbTTT1L1UVD49Vh05/i6p+6K/4UR/zUegurIepMD3pid9YPF4sYbau4O2tk4PsDfO0aGOmNNSR43duURMlnP4eNJg/jpy6xhFjA8KiP36vThiQmtOif8Awo+H3yI/mk/L/G9e7Yi3FuncHYu+Z98d8duVtLLW0Oydvbgz82Y3/wBm7zypSOhirql6mqlpIJXjkyuUkjpYQZZQB7rbMEWvX1pt6dNdcdhdN7j6B3jtulzvVO7evMh1Zn9rVjytBXbJyeAk2zVYtqgOKmKT+EyaEnRlmikAkRg6hhvpHU1r59fLS/ml/wApb5Gfyue56+epx25t1fH3I7jau6V+Q+DpKpcbPS/dmrweC3fk8XGkWy+z8MqKktPKadayWFqmgMkN/HrpWjhx8+jDdLf8KYv5q3TuyaDY9Z2V153FTYmhix2K3H3N15BuTelNSQRiKAVu6NvZbaeR3JWRqLtV5c5CsmbmWWQ+/daMSHy6Ih8tP5mnz1/mDb12hU939zbv3bU4HclDX9Zdc7AoI9obW23uuWcU+HrdpbO2VTUJrd3pNUeGkyNR97mR5PGlQQ2n37qwRV4Dr6EX8iXrv+Zhs/4tvnP5jXa26dz1+6f4PP011f2VRUmS7g632bTwVJnrO0t81EC7ryed3Q00MkGJy89ZkMRTwgVMyTzPR0e+k0hSvYOqvv8AhV9vX5443qDrzZ3W22chR/BXN08NX3hvfY8uQr81kew6fMv/AHf2n21FS0qNtjrOnijpKnGPregymYcpVus1PQxPo9Xh01Nfi61n/iV/Po/mWfDfrej6g657mxu8ut8JQJjdn7Z7g2rj+w/7i0UKCKnotp52uek3PRYiigVI6XGzV1Ri6ONAsFNGuoH3TrRoxqRnooOy9kfLb+af8xKihxFPnO6fkh8gt5y53dWfehhpKCj+7npafK7u3NJiKGDDbN2FtLHGISyJDBRY+ihjghjFooj7qxIRfkOvrcfGzpXEfG/49dHfH7AVsmUw/SfUvX3VlDlpYft58xFsXauL22czUQeSbw1OXkxzVMiB2CvKQCR730iJqSehr9+611//0t/j37r3Xvfuvde9+690h+wesutu2tvnafavXux+zNqmtpskds9g7TwO89vnI0QlWjyBw246DJY41tIJ3EUvj8kYdtJFz7VWd9e7fN9RYXksFxQjVG7I1DxGpSDQ+Yr0IeWebua+Stz/AH1ybzPuG0bz4bR+PZXM1rNoampPFgeN9DUGpdVDQVGB0Bv+yKfCH/vDf4q/+k9dR/8A2IezX+tfNH/TSX//AGUTf9B9SH/wRf3g/wDwu3OX/c63L/tp69/sinwh/wC8N/ir/wCk9dR//Yh79/Wvmj/ppL//ALKJv+g+vf8ABF/eD/8AC7c5f9zrcv8Atp6Wewviz8Y+q9x0+8OsPjn0R1xu6kp6ukpN07C6h6+2fuOlpa+FqeupqfN7e29jsnDT1lOxSVFlCyISrAjj2mu9+3y/hNtfb1dzW5IJWSaR1JHA6WYjHljHRFzJ7xe7nOW1S7Fzf7p8x7rsjsrNb3m5XtzAzIdSM0U0zxllbKkrVTkUPTl2R8c/j33LlaHO9v8ARPTXaubxePGJxmY7I6w2RvnK47FCpnrBjKHIbnweUq6THirqZJfDG6x+SRmtqYkt2W87xtsbxbdu1zbxMakRyugJpSpCsATQAV406Scqe6fudyJZ3O3cj+42/bNt80viSRWO4XdpG8mkL4jpbyxqz6VVdTAtpUCtAOm/YHxd+M/VG4Y93dW/HborrXdkNJVUEO59gdR7A2duGKhrlVK2jjzW3dv47JJSViKBLGJQkgADA+73e+75uEJt7/ebue3JB0yTSOtRwOlmIqPI06U8y+8Hu1zntb7Jzh7o8xbtsrOrm3vNyvLqAumUYxTzPGWU5VtNV8iOh19lXUdde9+691737r3QE7++Lnxl7W3FNu/tH469E9k7sqKWloqjdG/uo+v947inoqGPxUVJNm9xbfyOSlpaOI6Yo2lKRrwoA9mtpv2+bfCLaw3m7gtwSdMc0iLU8TpVgKnzx1I3LXvD7t8mbWmx8n+6XMe07Krs4t7Pcr21gDOauwigmSMMxyxC1Y5JPTn1t8dfj701k8hmuoOiunOqczlqAYvKZbrbrHZOxsnksYKiKrGOyFftjCYuqrKAVUCS+GR2j8iK1rgH3S93neNyRItx3W5uIlNQJJXcA8KgMxANMVHSTmv3S9zefLS22/nj3F33ebCGTxI476/u7uOOTSV1olxLIqvpJXUoDaSRWhPQjbn2ttne238vtPee3cFu7au4KKbG57bO58Rj8/t/N46oFp8fl8Nlaerx2SopwLPFNG8bD6g+0UE89rNHcW0zx3CGqspKsp9QwoQfmD0Fdo3jd+X9zst62HdLmx3m2kEkM9vK8M0TrweOWNleNx5MrAjyPRcv9kU+EP8A3hv8Vf8A0nrqP/7EPZ1/Wvmj/ppL/wD7KJv+g+pT/wCCL+8H/wCF25y/7nW5f9tPXv8AZFPhD/3hv8Vf/Seuo/8A7EPfv6180f8ATSX/AP2UTf8AQfXv+CL+8H/4XbnL/udbl/209PG3/hr8QNpZ3D7o2r8U/jbtnc23cnRZrb+4tv8ARnWGFzuDzGNqI6vHZbD5fG7XpshjMnQVcSywTwSJLFIoZWBAPtubmXmO4ikguN/vXgdSrK08rKwOCCCxBBGCDg9Idz9+PfHe9uvtn3n3l5ru9ouonimgm3fcJYZopFKvHLE9wySRupKujqVZSQQQejGzwQVUE1LVQxVNNUxSQVFPPGk0E8EyGOWGaKQNHLFLGxVlYEMDY8eyUEqQymjDqLI5JIZI5oZGSVGBVgSCCDUEEZBByCMg9Fa/2RT4Q/8AeG/xV/8ASeuo/wD7EPZ9/Wvmj/ppL/8A7KJv+g+ph/4Iv7wf/hducv8Audbl/wBtPRitq7T2rsXb2J2jsjbW39nbTwFItBgtsbVw2O29t7C0KM7pR4nC4imo8bjqRXdiI4YkQEk259k1xcXF3NJcXU7yXDmrM7FmY+pYkkn5k9RdvO9bzzFul7vfMG7XN/vVy+ua4uJXmnlc4LSSyM0jtQAamYnHHpQe2eizoEuxfjR8ce4M7Bujtr4/9Jdo7mpsZT4Wm3F2L1VsTe2dp8PSVFXV0uJgy+5cDk8hFjKaqr55Y4FkESSTSMFBdiTSy3vetuiMG37vdQQFtRWOWRFqaAmisBUgAV44Hp1IHK3uz7qcjbdJs/JXuZzBs+0PK0rQWO43lpC0rKqtIYoJo0MjKiKzldRVFBNFFM3W/wAb/jx03mKzcXUPQvTHVe4MjjJMLkM71v1dsfY+Yr8PLVUldLiazJ7YwWLrarGS1tBBM0Du0TSwxuV1IpGr3et53KNYdx3a5uIVbUFkldwDQioDMQDQkV40J9eqc1e6vuhz3YwbXzv7kb/vO2RSiVIb7cLu7iSUKyCRY7iaRFkCO6hwAwVmWtGIIz+yzoBdNmawuH3Jh8tt3cWJxme2/nsZX4XO4LNUFLlMPmsPlKWWhyeJy2Mrop6LI4zI0U7wzwTI8U0TsjqVJHu8UskEkc0MjJMjBlZSQVINQQRkEHIIyD0rsL++2q+st02u9lttztpUlhmido5YpY2DxyRyIQ6SI4DI6kMrAMpBAPRaP9kU+EP/AHhv8Vf/AEnrqP8A+xD2ef1r5o/6aS//AOyib/oPqWf+CL+8H/4XbnL/ALnW5f8AbT0ZLb23sBtLBYfa+1MHh9s7Z27jKLC7f27t7GUWFwWDw+Np46THYnD4jGwU2PxmMoKWJYoYII0iijUKqgAD2STTTXEsk9xKzzuxZmYlmYnJJJqSScknJ6inc903Le9xvt43ncZ7vd7qV5Zp5pHlmmlkYs8ksrlnkkdiWd3YszEkkk9PHtvpD0BG/fiz8Y+1Nx1G8Oz/AI59Edj7uq6ekpKvdO/eoevt4bjqqWghWnoaaoze4dvZHJzU9HTqEiRpSsaAKoA49m1pv2+WEItrHeruG3BJCxzSIoJ4nSrAZ88Z6kflv3i93OTdqi2LlD3T5j2rZEZmW3s9yvbaBWc6nZYoZkjDM2WIWrHJqenrrT4/dDdL1WVr+nekuoup67O09NSZus60622bsSqzFLRySTUlNlaja+FxU2Qp6WaZ3jSYuqMxKgEn21fbvu25rGm5bpcXCoSVEsjyAE8SNTGlfOnRfzZ7me5HPsNnbc9e4O971b27M0S399dXixMwAZo1uJZAjMAAxUAkAA8Ol1u7Z20ewNuZXZ2/Nrbc3ttHOwJS5za27sHjNybczNNHPFUx02VweZpa3GZGBKmBJAk0TqHRWtcA+0lvc3FnNHc2lw8VwhqroxVlPCoZSCMeh6Duyb7vfLO62e+8ubxdbfvdsxaK4tpZIJ4mIKlo5YmWRCVJUlWBoSOBPReP9kU+EP8A3hv8Vf8A0nrqP/7EPZz/AFr5o/6aS/8A+yib/oPqT/8Agi/vB/8Ahducv+51uX/bT0Z7HY7H4fH0OJxNDR4vFYujpcdjMZjqWCix+Ox9FAlNRUNDRUyRU1JR0lNEscUUaqkaKFUAAD2RO7yO8kjlpGJJJNSSckknJJPE9RFdXVzfXNze3tzJNeTSM8kjsXd3clmd2YlmZmJZmYkkkkkk9TPdemOve/de6Re/ut+u+1tuzbQ7R2FsvsnadRVUtbPtff21sHvHbs9bQyGWiq5sJuKhyONkqqOU6opGiLxtypB9qrS9vNvmFzYXcsFwARqjZkah4jUpBofPPR/y1zXzRyZuib5yfzJf7TvSoyC4s7ia1nCuKOolgdJArDDKGowwQegOh+DXwnppoqin+Hvxap6inljmgnh+PvU0U0M0TB4pYpU2irxyxuoKsCCCLj2aHmrmdgVbmO/IP/LxL/0H1Icn3h/vASxvFL7584tEwIIO9bkQQcEEG5oQRgg8ejS+yHqHeve/de697917r3v3Xugy7Y6V6b762vHsfvPqXrLufZcWVpM7FtDtjYe1uxdrxZughqqehzMe394YrMYlMrRU9dPHFUCETRpM6qwDsD7rYJHA9Ft/4bK/luf96+fhB/6Sh0N/9gXv3W9b/wAR/b17/hsr+W5/3r5+EH/pKHQ3/wBgXv3Xtb/xH9vQt9QfEj4pfHvN5Pc3Qfxk+PfR+5M1ijgszuDqDpfrfrTN5bCGrpsgcNk8rszbeFr6/FGvo4Z/t5ZHh80SPp1KpHutFieJPUruL4rfGD5EV2EyfyA+OHQveeS2zSVdBtzIdxdP9e9m1236HITRVFfR4Sr3rt3Nz4qkrZ6eN5o4GjSR0UsCQCPdeBI4E9OvTvxz+Pfx3oc3jPj/ANEdNdGY3c1VSV25Mf071hsjrKh3BXY+GanoKzN0mysHhIMrVUUFRIkMk6yPGjsFIBIPuvEk8T0Mvv3WuiSRfyz/AOXBDJHND/L9+EkM0LpLFLF8U+iI5IpI2DpJG6bDDI6MAQQQQR791bW/8R/b0dv37qvRLsn/AC3f5d2byWQzOZ+BPwuy+Yy9dV5PK5XJ/Fvo6vyWTyVfUSVVdkMhXVWxZaqtrq2qleSWWR2kkkYsxJJPv3VtbfxHo4eMxmNwmNx+Gw2PocRh8RQ0mMxWKxlJT0GNxmNoKeOlocfj6GljipaKhoqWJI4oo0WOONQqgAAe/dV6ne/de6It8gv5ZPwA+U+bqt0d8/Evpffm7sg+vJb1basO2t85VtOlTl967Pl2/urK+MH0fcVkoT+zb37qwdhwboHuvf5Jn8qXrDNU+4Nr/B3pWpyVLKk9Od7Y/OdnUEU0ZvHKuH7Lzm7cP5I29SnwXVgCOQD791syOfxdWeYvF4zCY6hw+Fx1DiMRi6SnoMZisXSU9BjsdQ0sSw0tFQ0NJHFTUlJTQoEjjjVURQAAAPfuqdJnsPrbrvt3Zua667Y2Fsvs/r7ciUcW4ti9h7Wwe9dm5+PHZGjy+PjzW2NyUOTwmUShy2Pp6qETwSCKogjkWzorD3XgSMg56Azrj4L/AAl6c3nhux+ovh38WOq+w9ufxH+72/OuPj51LsfeeC/i+KrsDlv4NujbG0cXnMX/ABTB5SpoqjwTp56SolhfVHI6n3WyzHBY06NP791rr54P/CvH/t4B8ff/ABTva3/v6u7/AHo9KofgP29W3f8ACS7bu392/wAuX5MbY3XgsPufbed+Wu78ZnNvbhxlFmsHmcbV9JdJxVWPyuJyUFTQZGhqYmKyRTRvG6mxBHvw6bm+MfZ1bhuT+Rl/KW3VuObdWU+DvUVNlKip+6kptuS7x2ft0S+Ty6Ytn7R3ThNpU9Pq/wB0x0KxafTp08e99U8R/wCLqwHpX4/9HfG/Z8WwOguo+vOndmRzfcvt3rnaWF2njqut0CN8jko8PR0r5XKyoAJKqpaWok/tOffuqkkmpOehf9+610TncX8uz+X5u/cGd3Zuz4LfDndG6t0ZjJ7i3LuXcXxj6Uze4NxbgzdbPksznc7mclsipyOWzGWyNTJUVVVUSST1E8jO7MzEn3VtTfxHo123du7f2ht/BbT2ngsPtfau18PjNu7Z2zt3GUWE2/t3b+EooMbhsFgsNjYKbHYjD4jHU0dPS0tPHHBTwRqiKqqAPdV6Q/bfSHS3f22abZXe3UPV/dWzaPMUu4qPaXbewNqdj7ZpdwUNJX0FFnabA7xxOZxUGYo6HKVUMVUsQnjiqZUVgsjg+62CRwPSA6l+HPxF6C3NU716J+K/xw6V3lWYeq27V7t6l6P6y643NVbfrqqhr63BVOe2dtjDZWfD1ldi6aaWlaUwSS08TspaNCPdeLMeJPQ4bq2ntbfW3cxtDe22sBvHae4aKXG5/a+6sNjtw7dzmOnt5qDMYTL01XjcnRTaRqimieNrcj37rXVXOe/kWfyk9yZ+XcuQ+D3U9PkZqj7l6bA1m99q4ASeQy6Ytp7X3Zh9rQU+o28SUaxafTp08e/dX8R/4urA+kvj10V8bNnpsDoDqHrvpvZqz/dy7f652lhdqUFdXadDZLKriaSmly+UkTh6qqaaof8AtOffuqkk5J6GL37rXTNuLbm3t34PKbY3ZgcNujbWco5sdmtvbixdDm8HmMfULpnocpiclBU0GQo514eKWN0YfUH37r3VXu9P5HH8pnfuZnz2d+DvT9FXVMjSyxbO/vZ11iA7SCU+Lb/Xu5tr4CnQsP0x0yra4tYke/dX8R/4ujGfHj+XX8GfihkYs58evix0z1nueCNoYN54nZ1Bkd+QQPH4ZaeDfu4Bl95Q00yf5yNa4JIeWBPPv3WizNxPRz/fuq9QsljcdmcfXYjL0FFlcVlKSox+SxmSpYK7H5GgrInp6uirqKqjlpqukqoJGSSORWR0YhgQbe/de6q43z/JB/lP9ibiqd07i+D3TlNlquZ6ioTaEW5+usM80jF5HG2evdxbX23HrdiSFpACT71Tq4kcfi6Od8e/iX8Zfift+r2x8buiusel8TkTC2YGwtp4vC5LPyUy6KefcmdhgOd3JUwJwktfU1EijgED3vqpYtxPRhvfutde9+691//T3+Pfuvde9+691737r3Xvfuvde9+691737r3Xvfuvde9+691737r3Xvfuvde9+691737r3Xvfuvde9+691737r3Xvfuvde9+691737r3Xvfuvde9+691737r3Xvfuvde9+691737r3Xvfuvde9+691737r3Xvfuvde9+691737r3Xvfuvde9+691737r3Xvfuvde9+691737r3Xvfuvde9+691737r3Xvfuvde9+691737r3Xvfuvde9+691737r3Xvfuvde9+691737r3Xvfuvde9+691737r3Xvfuvde9+691737r3Xvfuvde9+691737r3Xvfuvde9+691737r3Xz8/wDhWdQ9e1Xz06Dfd26N54PIL8RNsJT023Nh4PdNHLRjuXukpPNXZPsbZ00FS0xdTEtPIoVVbyEsVXR6Uw10n7erdP8AhJjTbVpfgb34m0c1uHOY9vl1uZ6ip3HtnG7WrIqw9N9LhoIaHGbt3jDPTCEIwlaojYszL4wFDN4dNzfEPs62mPe+muve/de697917r3v3Xuve/de697917r3v3Xuve/de697917r3v3Xuve/de697917r3v3Xuve/de697917r3v3Xuve/de697917r3v3Xuve/de6//2Q==">
            </div>
          </div>

        </div>
        <div class="cs-invoice_head cs-mb10">
          <div class="cs-invoice_left">
            <b class="cs-primary_color">Invoice To:</b>
            <p>
              <?php echo $invoice_data['full_name']; ?>, <br>
              <?php echo $invoice_data['address']; ?>,<br>
              <?php echo $invoice_data['landmark']; ?>,<br>
              <?php echo $invoice_data['city']; ?>,<br>
              <?php echo $invoice_data['state']; ?>,<br>
              <?php echo $invoice_data['pincode']; ?>.<br>
              <?php if($invoice_data['model_name'] != null)
              {?>
                <label><b>Model Name:</b><?php echo $invoice_data['model_name']; ?><br>
                <label><b>Making Year:</b><?php echo $invoice_data['make_year']; ?><br>
              <?php } ?>
             
            </p>
          </div>
          <!-- <div class="cs-invoice_right cs-text_right">
            <b class="cs-primary_color">Pay To:</b>
            <p>
              Biman Airlines <br>
              237 Roanoke Road, North York, <br>
              Ontario, Canada <br>
              demo@email.com
            </p>
          </div> -->
        </div>
        <div class="cs-table cs-style1">
          <div class="cs-round_border">
            <div class="cs-table_responsive">
              <table>
                <thead>
                  <tr>
                    <th class="cs-width_1 cs-semi_bold cs-primary_color cs-focus_bg">S.No</th>
                    <th class="cs-width_4 cs-semi_bold cs-primary_color cs-focus_bg">Spare</th>
                    <th class="cs-width_2 cs-semi_bold cs-primary_color cs-focus_bg">Qty</th>
                    <th class="cs-width_1 cs-semi_bold cs-primary_color cs-focus_bg">Price</th>
                    <th class="cs-width_2 cs-semi_bold cs-primary_color cs-focus_bg cs-text_right">Total</th>
                  </tr>
                </thead>
                <tbody>
                  <?php $i = 1;

                  foreach ($html_data as $orderData) {
                  ?>

                    <tr>
                      <td class="cs-width_1"><?php echo $i++; ?></td>
                      <td class="cs-width_4"><?php echo $orderData['spare_name']; ?></td>
                      <td class="cs-width_2">1</td>
                      <td class="cs-width_1"><?php echo $orderData['product_price']; ?></td>
                      <td class="cs-width_2 cs-text_right"><?php echo $orderData['product_price']; ?></td>
                    </tr>
                  <?php } ?>
                </tbody>
              </table>
            </div>
            <div class="cs-invoice_footer cs-border_top">
              <div class="cs-left_footer cs-mobile_hide">

              </div>
              <div class="cs-right_footer">
                <table>
                  <tbody>
                    <tr class="cs-border_left">
                      <td class="cs-width_3 cs-semi_bold cs-primary_color cs-focus_bg">Subtoal</td>
                      <td class="cs-width_3 cs-semi_bold cs-focus_bg cs-primary_color cs-text_right"><?php echo $orderData['total_amount']; ?></td>
                    </tr>
                    <!-- <tr class="cs-border_left">
                      <td class="cs-width_3 cs-semi_bold cs-primary_color cs-focus_bg">Tax</td>
                      <td class="cs-width_3 cs-semi_bold cs-focus_bg cs-primary_color cs-text_right">-₹ 20</td>
                    </tr> -->
                  </tbody>
                </table>
              </div>
            </div>
          </div>
          <div class="cs-invoice_footer">
            <div class="cs-left_footer cs-mobile_hide"></div>
            <div class="cs-right_footer">
              <table>
                <tbody>
                  <tr class="cs-border_none">
                    <td class="cs-width_3 cs-border_top_0 cs-bold cs-f16 cs-primary_color">Total Amount</td>
                    <td class="cs-width_3 cs-border_top_0 cs-bold cs-f16 cs-primary_color cs-text_right">₹ <?php echo $orderData['total_amount']; ?>/-</td>
                  </tr>
                </tbody>
              </table>
            </div>
          </div>
        </div>
        <!-- <div class="cs-note">
          <div class="cs-note_left">
            <svg xmlns="http://www.w3.org/2000/svg" class="ionicon" viewBox="0 0 512 512">
              <path d="M416 221.25V416a48 48 0 01-48 48H144a48 48 0 01-48-48V96a48 48 0 0148-48h98.75a32 32 0 0122.62 9.37l141.26 141.26a32 32 0 019.37 22.62z" fill="none" stroke="currentColor" stroke-linejoin="round" stroke-width="32" />
              <path d="M256 56v120a32 32 0 0032 32h120M176 288h160M176 368h160" fill="none" stroke="currentColor" stroke-linecap="round" stroke-linejoin="round" stroke-width="32" />
            </svg>
          </div>
          <div class="cs-note_right">
            <p class="cs-mb0"><b class="cs-primary_color cs-bold">Note:</b></p>
            <p class="cs-m0">Here we can write a additional notes for the client to get a better understanding of this invoice.</p>
          </div>
        </div> -->
        <!-- .cs-note -->
      </div>
      <!-- <div class="cs-invoice_btns cs-hide_print">
        <a href="javascript:window.print()" class="cs-invoice_btn cs-color1">
          <svg xmlns="http://www.w3.org/2000/svg" class="ionicon" viewBox="0 0 512 512">
            <path d="M384 368h24a40.12 40.12 0 0040-40V168a40.12 40.12 0 00-40-40H104a40.12 40.12 0 00-40 40v160a40.12 40.12 0 0040 40h24" fill="none" stroke="currentColor" stroke-linejoin="round" stroke-width="32" />
            <rect x="128" y="240" width="256" height="208" rx="24.32" ry="24.32" fill="none" stroke="currentColor" stroke-linejoin="round" stroke-width="32" />
            <path d="M384 128v-24a40.12 40.12 0 00-40-40H168a40.12 40.12 0 00-40 40v24" fill="none" stroke="currentColor" stroke-linejoin="round" stroke-width="32" />
            <circle cx="392" cy="184" r="24" />
          </svg>
          <span>Print</span>
        </a>
        <button id="download_btn" class="cs-invoice_btn cs-color2">
          <svg xmlns="http://www.w3.org/2000/svg" class="ionicon" viewBox="0 0 512 512">
            <title>Download</title>
            <path d="M336 176h40a40 40 0 0140 40v208a40 40 0 01-40 40H136a40 40 0 01-40-40V216a40 40 0 0140-40h40" fill="none" stroke="currentColor" stroke-linecap="round" stroke-linejoin="round" stroke-width="32" />
            <path fill="none" stroke="currentColor" stroke-linecap="round" stroke-linejoin="round" stroke-width="32" d="M176 272l80 80 80-80M256 48v288" />
          </svg>
          <span>Download</span>
        </button>
      </div> -->
    </div>
  </div>
  <script src="assets/js/jquery.min.js"></script>
  <script src="assets/js/jspdf.min.js"></script>
  <script src="assets/js/html2canvas.min.js"></script>
  <script src="assets/js/main.js"></script>
</body>

</html>