    <footer class="footer footer-static footer-light navbar-border navbar-shadow">
        <div class="">
            <p class="clearfix blue-grey lighten-2 text-sm-center mb-0 px-2">
        <span class="float-md-left d-block d-md-inline-block">Copyright © 2021 
          <a class="text-bold-800 grey darken-2" href="#" target="_blank">Easy Trade</a>
        </span>
        <!-- <span class="float-md-right d-none d-lg-block">Designed by <a href="www.adnectics.com"><strong>Adnectics</strong></a></i> -->
        <span id="scroll-top" style="display: inline;"></span></span></p>
        </div>
    </footer>
 <!-- <script src="<?php echo base_url(); ?>admin_assets/app-assets/vendors/js/charts/chart.min.js"></script> -->
 <script src="<?php echo base_url(); ?>admin_assets/app-assets/js/jquery-3.2.1.min.js"></script>
 <script src="<?php echo base_url(); ?>admin_assets/app-assets/vendors/js/vendors.min.js"></script>
    <!-- BEGIN Vendor JS-->

    <!-- BEGIN: Page Vendor JS-->
   
    <!-- END: Page Vendor JS-->

    <!-- BEGIN: Theme JS-->
    <script src="<?php echo base_url(); ?>admin_assets/app-assets/js/core/app-menu.min.js"></script>
    <script src="<?php echo base_url(); ?>admin_assets/app-assets/js/core/app.min.js"></script>
    <script src="<?php echo base_url(); ?>admin_assets/app-assets/js/scripts/customizer.min.js"></script>
    <script src="<?php echo base_url(); ?>admin_assets/app-assets/js/scripts/footer.min.js"></script>
    <script src="<?php echo base_url(); ?>admin_assets/app-assets/vendors/js/tables/datatable/datatables.min.js"></script>
    <script src="<?php echo base_url(); ?>admin_assets/app-assets/js/scripts/pages/crypto-wallet.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-sweetalert/1.0.1/sweetalert.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.4/js/bootstrap-datepicker.js"></script>

     <script type="text/javascript">
        

         function getIFSC()
         {
          var ifsc = $('#ifsc_code').val();
           $.ajax({  
               url:"<?php echo base_url() ?>get_IFSC_Code",  
               method:'POST',  
               data:'ifsc_code='+ifsc,
               dataType: 'JSON',
               success:function(data)  
               {  
                    console.log(data);
                    if(data.message == "success") {
                        $('#bank_name').val(data.bank);
                        $('#branch').val(data.branch); 
                    } else {
                        alert("Invalid IFSC Code");
                        $('#bank_name').val('');
                        $('#branch').val('');
                    }  
               }       
          });
         }
        function previewFile(input){
        $('.new_image').show();     
        $('.panlabel').show();
        $("#previewImg").show();

        var file = $("input[type=file]").get(0).files[0];
        if(file){
            var reader = new FileReader();
 
            reader.onload = function(){
                $("#previewImg").attr("src", reader.result);
            }
            reader.readAsDataURL(file);
        }
    }
    
    </script>
    

</body>
</html>
