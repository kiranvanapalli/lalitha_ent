<?php

defined('BASEPATH') OR exit('No direct script access allowed');



/*

| -------------------------------------------------------------------------

| URI ROUTING

| -------------------------------------------------------------------------

| This file lets you re-map URI requests to specific controller functions.

|

| Typically there is a one-to-one relationship between a URL string

| and its corresponding controller class/method. The segments in a

| URL normally follow this pattern:

|

|	example.com/class/method/id/

|

| In some instances, however, you may want to remap this relationship

| so that a different class/function is called than the one

| corresponding to the URL.

|

| Please see the user guide for complete details:

|

|	https://codeigniter.com/user_guide/general/routing.html

|

| -------------------------------------------------------------------------

| RESERVED ROUTES

| -------------------------------------------------------------------------

|

| There are three reserved routes:

|

|	$route['default_controller'] = 'welcome';

|

| This route indicates which controller class should be loaded if the

| URI contains no data. In the above example, the "welcome" class

| would be loaded.

|

|	$route['404_override'] = 'errors/page_missing';

|

| This route will tell the Router which controller/method to use if those

| provided in the URL cannot be matched to a valid route.

|

|	$route['translate_uri_dashes'] = FALSE;

|

| This is not exactly a route, but allows you to automatically route

| controller and method names that contain dashes. '-' isn't a valid

| class or method name character, so it requires translation.

| When you set this option to TRUE, it will replace ALL dashes in the

| controller and method URI segments.

|

| Examples:	my-controller/index	-> my_controller/index

|		my-controller/my-method	-> my_controller/my_method

*/ 

$route['default_controller'] = 'frontend_side/frontend/index';
// frontend views
$route['index'] = 'frontend_side/Frontend/index';
// $route['contactus'] = 'frontend_side/Frontend/contactus';
// $route['frontend_login'] = 'frontend_side/Frontend/login';
// $route['about'] = 'frontend_side/Frontend/about';
// $route['error_page'] = 'frontend_side/Frontend/error_page';
// $route['products'] = 'frontend_side/Frontend/products';
// $route['testimonial'] = 'frontend_side/Frontend/testimonial';
$route['privacy_policy']='frontend_side/Frontend/privacy_policy';
// $route['terms_conditions']='frontend_side/Frontend/terms_conditions';
// $route['fare_policy']='frontend_side/Frontend/fare_policy';
// $route['download']='frontend_side/Frontend/download';
// $route['loadview'] ='frontend_side/Frontend/loadHTML';

//Load HTML

$route['load_html']='api/Orders/loadHTML';

/** admin side **/
$route['dashboard'] = 'admin_dashboard/admin_dashboards';
$route['load'] = 'admin_dashboard/admin_dashboards/load';

$route['login'] = 'admin/admin/index';

$route['profileview'] = 'admin/admin/profile_update';

$route['admin_update_pass'] = 'admin/admin/password_update';

$route['update-phone'] = 'admin/admin/phone_update';

$route['forgetpassword_page'] = 'admin/admin/forgetpassword';

$route['forgotpassword'] = 'admin/admin/checkemail';



$route['logout'] = 'admin/admin/logout';



//Admin Options

$route['trading'] ='admin_views/Admin_Views/trading';
$route['user_info'] = 'admin_views/Admin_Views/user_information';
$route['coins'] = 'admin_views/Admin_Views/coins';


//Admin User Details

$route['user_details'] ='users/Users';
$route['get_users'] = 'users/Users/getusers';
$route['adduser'] = 'users/Users/adduserdetails';
$route['edituserdetails'] = 'users/Users/user_edit';
$route['update_user_admin'] = 'users/Users/update_user_details';
$route['delete_user'] = 'users/Users/delete_user';
$route['get_deposit_details/(:any)'] = 'users/Users/depositdetails/$1';


//Add Money to user
$route['add_money_user_list'] ='add_money/Add_money';

$route['add_money_get_users'] = 'add_money/Add_money/getusers';
$route['add_money_edituserdetails'] = 'add_money/Add_money/user_edit';
$route['add_money_update_user_admin'] = 'add_money/Add_money/update_user_details';
$route['moneyadd_list'] = 'add_money/Add_money/moneyadd_list';
$route['getmoneyaddusers'] = 'add_money/Add_money/getmoneyaddusers';


//Asset Details

$route['asset'] = 'assets/assets';
$route['getassets'] = 'assets/assets/getassets';
$route['addassets'] = 'assets/assets/addassetdetails';
$route['editassets'] = 'assets/assets/edit_asset';
$route['update_asset'] = 'assets/assets/update_asset';
$route['delete_asset'] = 'assets/assets/delete_asset';


//Contact Us

$route['contact_us'] = 'contact_us/contact_us/index';
$route['get_message_data'] = 'contact_us/contact_us/get_message_data';
$route['delete_message'] = 'contact_us/contact_us/delete_message';




//Bank Details

$route['get_IFSC_Code'] ='users/Users/getifsccode'; 

//api

$route['api_login'] = 'api/authentication/login';
$route['user_registration'] = 'api/authentication/user_registration';
$route['update_user'] = 'api/authentication/user_update';
$route['otp_send'] = 'api/authentication/otp_save';
$route['otp_verify'] = 'api/authentication/otp_verify';
$route['pin_verify'] = 'api/authentication/pin_verify';

//Reports

$route['money_deposit_details_daily'] = 'reports/Reports';
$route['money_deposit_details_getdata'] = 'reports/Reports/getusers';
$route['betting_report'] = 'reports/Reports/bet_data';
$route['betting_report_get_users'] = 'reports/Reports/betting_report_get_users';
$route['filter'] = 'reports/Reports/filter';
$route['slots_report'] = 'reports/Reports/slot_report';
$route['getslotreport']='reports/Reports/getslots_report';



//support ticket

$route['support_tickets'] = 'tickets/Tickets';
$route['getsupport_ticket_details'] = 'tickets/Tickets/getticketlist';
$route['editticket'] = 'tickets/Tickets/editticket';
$route['update_ticket_details'] = 'tickets/Tickets/update_ticket_details';

//Withdraw's

$route['withdraw'] = 'withdraw/Withdraw';
$route['getwithdrawrequest'] = 'withdraw/Withdraw/getwithdrawrequest';
$route['editwithdraw'] = 'withdraw/Withdraw/editwithdraw';
$route['update_withdraw_details'] = 'withdraw/Withdraw/update_withdraw_details';




/* END HERE */

$route['404_override'] = '';

$route['translate_uri_dashes'] = FALSE;

